#ifndef HARDWARE_SETTINGS_H
#define HARDWARE_SETTINGS_H

// Type of board in the testbench
// define only one of this targets


// #define BENCH_VFAT3_RAD     // VAFT3 chips on Rad board
// #define BENCH_VFAT3_GEM     // Single VFAT3 on GEM Chamber
#define BENCH_VFAT3_X5      // 5 VAFT3 chips connected using adapter cards
// #define BENCH_VFAT3_PKG     // VFAT3 pakaged tester


//
//  NOTE:
//
// HAS_FMC_ADC: if defined Vmon is read using the I2C ADC on the test board. Else the VFAT3 internal ADC is used
//
// USE_GTP: Define it if the data receiver is implemented usign ARTIX GPT
//
// HAS_TRG_RECORDER: Define if the firmware has been compiled with trigger recorder

// Define software options dependent on hardware configuration
#if defined( BENCH_VFAT3_RAD )
    #define HAS_FMC_ADC             
    #define NUM_VFAT3_RECEIVERS 1

#elif defined( BENCH_VFAT3_GEM )
    #define USE_GTP			    // Set if receivers need System PLL 
    #define HAS_TRG_RECORDER	// Set if the firmware is compiled with trigger recorder
    #define NUM_VFAT3_RECEIVERS 1

#elif defined( BENCH_VFAT3_X5 )
    #define USE_GTP			    // Set if receivers need System PLL 
    #define HAS_TRG_RECORDER	// Set if the firmware is compiled with trigger recorder
    #define NUM_VFAT3_RECEIVERS 5
    #define GTP_USE_MAP    0x2aa // Map of used GTP. Needed for reset check

#elif defined( BENCH_VFAT3_PKG )
    #define USE_GTP			    // Set if receivers need System PLL 
    #define NUM_VFAT3_RECEIVERS 1
    #define HAS_FMC_ADC

#else
    // Deliberate error    
    #error One BENCH_VFAT3_* macro MUST be defines 
#endif

#endif // HARDWARE_SETTINGS_H



