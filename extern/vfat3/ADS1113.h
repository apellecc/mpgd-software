/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */

#ifndef ADS1113_H
#define ADS1113_H

#include <stdint.h>
#include "i2cslave.h"


class ADS1113 : public I2Cslave
{
public:
	ADS1113(I2Cbus *bus, uint8_t address);
	void setConfiguration();
	void convert(uint8_t channel, uint16_t *result);
	void convert(uint8_t channel, float *result);

private:
	void write(uint8_t address, uint16_t data);
	void addWriteAddressPointer(uint8_t address);
	void read(uint8_t address, uint16_t *data);
	void read(uint16_t *data);

private:
	enum {
		REG_Convertion		= 0x00,	
		REG_Config			= 0x01,	
		REG_Lo_thresh		= 0x02,	
		REG_Hi_thresh		= 0x02
	};

	enum {
		CFG_DR_8			= 0 << 5,	
		CFG_DR_16			= 1 << 5,	
		CFG_DR_32			= 2 << 5,	
		CFG_DR_64			= 3 << 5,	
		CFG_DR_128			= 4 << 5,	
		CFG_DR_250			= 5 << 5,	
		CFG_DR_475			= 6 << 5,	
		CFG_DR_860			= 7 << 5,	

		CFG_Mode			= 1 << 8,	
		CFG_OS				= 1 << 15
	};

private:
    uint16_t CFG_shadow;

};



#endif // ADS1113_H
