/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#ifndef Tcalibrate_h
#define Tcalibrate_h

#include "TestBench.h"
#include "VFAT3chip.h"
#include "GenericTest.h"

class Tcalibrate : public GenericTest
{
public:
 	Tcalibrate(TestBench &tb);
	bool run(string param);

private:
	bool runTH();
	bool runCFD();

private:
	VFAT3chip::VFAT3config_t	chipCfg;
	VFAT3board					*board;
	VFAT3chip					*chip;
	static const int 			numPulses = 100;
	static const int 			CalDacHigh;
	static const int 			CalDacLow;
	static const int 			CalDacStartScan;
	static const int 			CalDacEndScan;

	unsigned long				evCounter;
	std::array<long, 128> 		histogram;

	void resetHistogram();
	static void 	histogramCB(void *ctx, VFAT3dataParser::VFAT3data_t *data);
	int injectChannel(int ch, unsigned chargeDAC);
	int findThreshold(int channel);
	int tuneLocalZcc(VFAT3chip::VFAT3config_t &chipCfg, int channel);
	int tuneLocalArm(VFAT3chip::ChannelConfig_t &chCfg, int channel, int target);
	bool calibrateLocalArm();
	bool calibrateLocalZcc();
	float measSumChDelay(VFAT3chip::CalibrationConfig_t &calCfg);

	void saveChannelsFile();
};

#endif	// Tcalibrate_h


