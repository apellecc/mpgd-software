#include <iostream>
#include <fstream>
#include "VFAT3_data.h"
#include "Tutils.h"
#include <string>
#include <stdint.h>

using std::string;
using std::cin;
using std::cout;
using std::endl;
using std::ofstream;


#define DUMP_DAC_SCAN 1

bool VFAT3_data::Data_dump()
{
	ofstream outputFile;
	outputFile.open (string("Data/")+string("Chip_test_data_")+Tutils::filenameSuffix()+".txt");

	outputFile <<"Date_and_time" << endl;
	outputFile << Tutils::filenameSuffix() << endl << endl;

	//writing chip ID to file
	outputFile << "Chip_ID: "<< VFAT3.VFAT3_id << endl << endl;

	//writing power consumption to file
	outputFile << "Chip_power_consumption " << endl << endl;
	outputFile << "Analog_power_supply: "<< VFAT3.pwr_d.AVDD << " V" << endl;
	outputFile << "Analog_current_consumption: "<< VFAT3.pwr_d.IAVDD << " A" << endl;
	outputFile << "Analog_power_consumption: "<< VFAT3.pwr_d.IAVDD * VFAT3.pwr_d.AVDD << " W" << endl << endl;
	outputFile << "Digital_power_supply: " << VFAT3.pwr_d.DVDD << " V" << endl;
	outputFile << "Digital_current_consumption: "<< VFAT3.pwr_d.IDVDD << " A" << endl;
	outputFile << "Digital_power_consumption: "<< VFAT3.pwr_d.IDVDD * VFAT3.pwr_d.DVDD << " W" << endl << endl;
	outputFile << "EFUSE_power_supply: " << VFAT3.pwr_d.VDD_EFUSE << " V" << endl << endl;

	//writing voltage references to file
	outputFile << "Chip_voltage_references" << endl << endl;
	outputFile << "Bandgap_voltage_reference: "<< VFAT3.vref_d.Vbgr << " V" << endl;
	outputFile << "ADCs_negative_input_reference: " << VFAT3.vref_d.ADC_VinM << " V" << endl;
	//outputFile << "ADC0_reference_gain: " << VFAT3.vref_d.ADC0_vrf_gain << " V/LSB" << endl;
	//outputFile << "ADC0_reference_offset: " << VFAT3.vref_d.ADC0_vrf_offset << " V" << endl;
	outputFile << "ADC0_reference_voltage: "<< VFAT3.adc_d.ADC0_vref << " V" << endl;
	outputFile << "ADC0_reference_optimal_configuration_value: " << VFAT3.adc_d.ADC0_vref_optVal_ndac << endl << endl;
	
	#if DUMP_DAC_SCAN
		outputFile << "ADC0_reference_scan" << endl <<endl;
		outputFile << "Reg._config._value " << "ADC0_voltage_reference_[V]"<< endl;  
		for (int i = 0; i<4; i++) outputFile << i << " " << VFAT3.vref_d.ADC0_vrf_Scan[i] << endl;
		outputFile << endl <<endl;
	#endif
	
	outputFile << "ADC0_gain: " << VFAT3.adc_d.ADC0_gain*1000 << " mV/LSB"	<< endl;
	outputFile << "ADC0_offset: " << VFAT3.adc_d.ADC0_offset*1000 << " mV"	<< endl;
	outputFile << "ADC1_gain: " << VFAT3.adc_d.ADC1_gain*1000 << " mV/LSB"	<< endl;
	outputFile << "ADC1_offset: " << VFAT3.adc_d.ADC1_offset*1000 << " mV"	<< endl << endl;
	
	//writing Iref data to file
	outputFile << "Chip_current_reference_Iref" << endl << endl;
	outputFile << "Iref_gain: " << VFAT3.iref_d.gain << " A/LSB" << endl;
	outputFile << "Iref_offset: " << VFAT3.iref_d.offset << " A" << endl;
	outputFile << "Iref_optimal_value: " << VFAT3.iref_d.optVal << " A" << endl;
	outputFile << "Iref_optimal_configuration_value: " << VFAT3.iref_d.optVal_ndac << endl << endl;
	
	#if DUMP_DAC_SCAN
		outputFile << "Iref_reference_scan" << endl <<endl;
		outputFile << "Reg._config._value " << "Iref_current_reference_[A]"<< endl;  
		for (int i = VFAT3.iref_d.ScanStart; i<VFAT3.iref_d.ScanStop; i++) outputFile << i << " " << VFAT3.iref_d.ScanData[i-VFAT3.iref_d.ScanStart] << endl;
		outputFile << endl <<endl;
	#endif

	//Writing External temperature sensor bias current to file
	outputFile << "External_temperature_sensor_bias_current" << endl << endl;
	outputFile << "Ibias: " << VFAT3.ext_tsens_d.ibias << " A" << endl << endl;

	//writing bias DAC data to file
	int vDAC_size = 18;
	string dac_unit;
	outputFile << "Chip_bias_DACs" << endl << endl;

	for (int i_dac = 0; i_dac < vDAC_size; i_dac++)
	{
		dac_unit = (VFAT3.dac_d[i_dac].monSel < 16) ? " A" : " V";
		outputFile << VFAT3.dac_d[i_dac].name << "_bias_DAC" << endl;
		outputFile << "gain: "<< VFAT3.dac_d[i_dac].gain << dac_unit << "/LSB" << endl;
		outputFile << "offset: "<< VFAT3.dac_d[i_dac].offset << dac_unit << endl;
		outputFile << "DAC_optimal_value: " << VFAT3.dac_d[i_dac].optVal << " " << dac_unit << endl;
		outputFile << "DAC_optimal_configuration_value: " << VFAT3.dac_d[i_dac].optVal_ndac << endl << endl;
	}

	#if DUMP_DAC_SCAN
		outputFile << "DAC_value ";
		for (int i_dac = 0; i_dac < vDAC_size; i_dac++)
		{
			dac_unit = (VFAT3.dac_d[i_dac].monSel < 16) ? "_[A] " : "_[V] ";
			outputFile << VFAT3.dac_d[i_dac].name << dac_unit;
		}
		outputFile << endl;
		for (int v = 0; v < 256 ; v++)
		{
			outputFile << v << " ";
			for (int i_dac = 0; i_dac < vDAC_size; i_dac++)
			{
				if (v <= VFAT3.dac_d[i_dac].maxValue)
				{
					outputFile << VFAT3.dac_d[i_dac].ScanData[v]<< " ";
				}
				else
				{
					outputFile << 0 << " ";
				}
			}
			outputFile << endl;
		}
		outputFile << endl << endl;
	#endif

	// writing calibration circuit data
	outputFile << "Calibration" << endl;
	outputFile << "Voltage_pulse_gain: " << VFAT3.calib_d.vpul_gain*1000 << " mV/LSB" << endl;
	outputFile << "Voltage_pulse_offset: " << VFAT3.calib_d.vpul_offset*1000 << " mV" << endl;
	outputFile << "Voltage_step_baseline: " << VFAT3.calib_d.cal_bsl*1000 << " mV" << endl;
	outputFile << "Current_pulse_gain: " << VFAT3.calib_d.ipul_gain*1e9 << " nA/LSB" << endl;
	outputFile << "Current_pulse_offset: " << VFAT3.calib_d.ipul_offset*1e9 << " nA" << endl;
	outputFile << "Injection_capacitance: " << VFAT3.calib_d.Inj_Cap << " fF" << endl << endl;

	//writing channel data to file
	
	outputFile << "Ch " << "HG_[mv/fC] " << "MG_[mV/fC] " << "LG_[mV/fC] " << "ENC_HG_[e-] " << "ENC_MG_[e-] " << "ENC_LG_[e-] " << endl;
	for (int ich = 0; ich<128; ich++)
	{
		outputFile << ich << " ";
		outputFile << VFAT3.chn_d[ich].gain[0] << " ";
		outputFile << VFAT3.chn_d[ich].gain[1] << " ";
		outputFile << VFAT3.chn_d[ich].gain[2] << " ";
		outputFile << VFAT3.chn_d[ich].noise[0]*6180 << " ";
		outputFile << VFAT3.chn_d[ich].noise[1]*6180 << " ";
		outputFile << VFAT3.chn_d[ich].noise[2]*6180 << " ";
		outputFile << endl;
	}
	outputFile << endl;
/*
	//writing scurves
	outputFile << "Scurves" << endl;
	outputFile << "Charge ";
	for (int ich = 0; ich<128; ich++) outputFile << "Ch_" << ich << " ";
	outputFile << endl;
	for (int i = 0; i<256; i++)
	{
		for (int ich = 0 ; ich<129; ich++)
		{ 
			if (ich) outputFile << VFAT3.chn_d[ich-1].scurve[1][i] << " ";
			else outputFile << VFAT3.chn_d[ich].scurve[0][i] << " ";
		 }
		outputFile << endl;
	}
	outputFile << endl;
*/	

	outputFile << "Local_Arm_dac " << endl << endl;
	outputFile << "Ch " << "Gain_[mV/LSB] " << " Offset_[mV]" << endl;
	for (int ich = 0 ; ich < 128; ich++)
	{
		outputFile << ich << " ";
		outputFile << VFAT3.chn_d[ich].armdac_gain*1000 << " ";
		outputFile << VFAT3.chn_d[ich].armdac_offset*1000 << " ";
		outputFile << endl;
	}
	outputFile << endl << endl;
	
	#if DUMP_DAC_SCAN
		outputFile << "DAC_value ";
		for (int ich = 0; ich<128; ich++) outputFile << "Ch_" << ich << "_[mV] ";
		outputFile << endl;
		for (int v = 0; v < 19; v++)
		{
			outputFile << VFAT3.chn_d[1].vx_armdac[v] << " ";
			for (int ich = 0; ich<128; ich++) outputFile << VFAT3.chn_d[ich].vy_armdac[v]*1000 << " ";
			outputFile << endl;
		}
		outputFile << endl << endl;
	#endif

	outputFile << "Peaking_time" << endl << endl;

	outputFile << "Ch " << "PT_25_[ns] " << "PT_50_[ns] " << "PT_75_[ns] " << "PT_100_[ns] " << endl;
	for (int ich = 0; ich < 128 ; ich++ )
	{
		outputFile << ich << " ";
		for (int ipk = 0; ipk < 4; ipk++) outputFile << VFAT3.chn_d[ich].peakTime[ipk] << " ";
		outputFile << endl;
	}
	outputFile << endl;


	outputFile << "Ch " << "Bonding status" << endl;
	for (int ich = 0; ich<128; ich++)
	{
		outputFile << ich << " ";
		switch (VFAT3.chn_d[ich].bonding)
		{
			case 0:
					outputFile << "ok" << endl; break;
			case 1:
					outputFile << "open" << endl; break;
			case 2: 
					outputFile << "short" << endl; break;
			default:
					outputFile << "not_connected_to_ball_but_shorted_to_neighbour_channel" << endl; break;
		}
	}
	outputFile << endl;
	
	
	outputFile.close();
	return true;
}


bool VFAT3_data::Data_dump_elab()
{
	ofstream outputFile;
	outputFile.open (string("Data/")+string("Elab_Chip_test_data_")+Tutils::filenameSuffix()+".dat");

	outputFile <<"Date_and_time" << endl;
	outputFile << Tutils::filenameSuffix() << endl;


	//writing chip ID to file
	outputFile << "Chip_ID"<< endl;
	outputFile << VFAT3.VFAT3_id << endl;

	//writing power consumption to file
	outputFile << "Analog_power_supply[V]"<< endl;
	outputFile << VFAT3.pwr_d.AVDD << endl;
	outputFile << "Analog_current_consumption[A]"<< endl;
	outputFile << VFAT3.pwr_d.IAVDD << endl;
	outputFile << "Digital_power_supply[V]" << endl;
	outputFile << VFAT3.pwr_d.DVDD << endl;
	outputFile << "Digital_current_consumption[A]"<< endl;
	outputFile << VFAT3.pwr_d.IDVDD << endl;
	outputFile << "EFUSE_power_supply[V]" << endl;
	outputFile << VFAT3.pwr_d.VDD_EFUSE << endl;

	//writing voltage references to file
	outputFile << "Bandgap_voltage_reference[V]"<< endl;
	outputFile << VFAT3.vref_d.Vbgr << endl;
	outputFile << "ADCs_negative_input_reference[V]" << endl;
	outputFile << VFAT3.vref_d.ADC_VinM << endl;
	outputFile << "ADC0_reference_voltage[V]"<< endl;
	outputFile << VFAT3.adc_d.ADC0_vref << endl;
	outputFile << "ADC0_reference_optimal_configuration_value" << endl;
	outputFile << VFAT3.adc_d.ADC0_vref_optVal_ndac << endl;
	
/*	#if DUMP_DAC_SCAN
		outputFile << "ADC0_reference_scan" << endl <<endl;
		outputFile << "Reg._config._value " << "ADC0_voltage_reference_[V]"<< endl;  
		for (int i = 0; i<4; i++) outputFile << i << " " << VFAT3.vref_d.ADC0_vrf_Scan[i] << endl;
		outputFile << endl <<endl;
	#endif 
*/
	
	outputFile << "ADC0_gain[mV/LSB]" << endl;
	outputFile << VFAT3.adc_d.ADC0_gain*1000 << endl; // mV/LSB
	outputFile << "ADC0_offset[mV]" << endl;
	outputFile <<  VFAT3.adc_d.ADC0_offset*1000 << endl;
	outputFile << "ADC1_gain[mV/LSB]" << endl;
	outputFile << VFAT3.adc_d.ADC1_gain*1000 << endl;
	outputFile << "ADC1_offset[V]" << endl;
	outputFile << VFAT3.adc_d.ADC1_offset*1000 << endl;
	
	//writing Iref data to file
	outputFile << "Iref_gain[A/LSB]" << endl;
	outputFile << VFAT3.iref_d.gain << endl;
	outputFile << "Iref_offset[A]" << endl;
	outputFile << VFAT3.iref_d.offset << endl;
	outputFile << "Iref_optimal_value[A]" << endl;
	outputFile << VFAT3.iref_d.optVal << endl;
	outputFile << "Iref_optimal_configuration_value" << endl;
	outputFile << VFAT3.iref_d.optVal_ndac << endl;
	
/*	#if DUMP_DAC_SCAN
		outputFile << "Iref_reference_scan" << endl <<endl;
		outputFile << "Reg._config._value " << "Iref_current_reference_[A]"<< endl;  
		for (int i = VFAT3.iref_d.ScanStart; i<VFAT3.iref_d.ScanStop; i++) outputFile << i << " " << VFAT3.iref_d.ScanData[i-VFAT3.iref_d.ScanStart] << endl;
		outputFile << endl <<endl;
	#endif
*/
	//Writing External temperature sensor bias current to file
	outputFile << "External_temperature_sensor_bias_current[A]" << endl;
	outputFile << VFAT3.ext_tsens_d.ibias << endl;

	//writing bias DAC data to file
	int vDAC_size = 18;
	string dac_unit;

	for (int i_dac = 0; i_dac < vDAC_size; i_dac++)
	{
		outputFile << VFAT3.dac_d[i_dac].name << "_bias_DAC" << endl;
		outputFile << VFAT3.dac_d[i_dac].gain << endl;
		outputFile << VFAT3.dac_d[i_dac].offset << endl;
		outputFile << VFAT3.dac_d[i_dac].optVal << endl;
		outputFile << VFAT3.dac_d[i_dac].optVal_ndac << endl;
	}

/*
	#if DUMP_DAC_SCAN
		outputFile << "DAC_value ";
		for (int i_dac = 0; i_dac < vDAC_size; i_dac++)
		{
			dac_unit = (VFAT3.dac_d[i_dac].monSel < 16) ? "_[A] " : "_[V] ";
			outputFile << VFAT3.dac_d[i_dac].name << dac_unit;
		}
		outputFile << endl;
		for (int v = 0; v < 256 ; v++)
		{
			outputFile << v << " ";
			for (int i_dac = 0; i_dac < vDAC_size; i_dac++)
			{
				if (v <= VFAT3.dac_d[i_dac].maxValue)
				{
					outputFile << VFAT3.dac_d[i_dac].ScanData[v]<< " ";
				}
				else
				{
					outputFile << 0 << " ";
				}
			}
			outputFile << endl;
		}
		outputFile << endl << endl;
	#endif
*/
	// writing calibration circuit data
	outputFile << "Calibration_Voltage_pulse_gain[mV/LSB]" << endl;
	outputFile << VFAT3.calib_d.vpul_gain*1000 <<  endl;
	outputFile << "Calibration_Voltage_pulse_offset[mV]" << endl;
	outputFile << VFAT3.calib_d.vpul_offset*1000 << endl;
	outputFile << "Calibration_Voltage_step_baseline[mV]" << endl;
	outputFile << VFAT3.calib_d.cal_bsl*1000 << endl;
	outputFile << "Calibration_Current_pulse_gain[nA/LSB]" << endl;
	outputFile << VFAT3.calib_d.ipul_gain*1e9 << endl;
	outputFile << "Calibration_Current_pulse_offset[nA]" << endl;
	outputFile << VFAT3.calib_d.ipul_offset*1e9 << endl;

	//writing channel data to file
	
	outputFile << "Gain_and_Noise" << endl;
	for (int ich = 0; ich<128; ich++)
	{
		outputFile << VFAT3.chn_d[ich].gain[0] << endl;
		outputFile << VFAT3.chn_d[ich].gain[1] << endl;
		outputFile << VFAT3.chn_d[ich].gain[2] << endl;
		outputFile << VFAT3.chn_d[ich].noise[0]*6180 << endl;
		outputFile << VFAT3.chn_d[ich].noise[1]*6180 << endl;
		outputFile << VFAT3.chn_d[ich].noise[2]*6180 << endl;
	}

/*
	//writing scurves
	outputFile << "Scurves" << endl;
	outputFile << "Charge ";
	for (int ich = 0; ich<128; ich++) outputFile << "Ch_" << ich << " ";
	outputFile << endl;
	for (int i = 0; i<256; i++)
	{
		for (int ich = 0 ; ich<129; ich++)
		{ 
			if (ich) outputFile << VFAT3.chn_d[ich-1].scurve[1][i] << " ";
			else outputFile << VFAT3.chn_d[ich].scurve[0][i] << " ";
		 }
		outputFile << endl;
	}
	outputFile << endl;
*/	

	outputFile << "Local_Arm_dac_Gain_[mV/LSB]_and_Offset_[mV]" << endl;
	for (int ich = 0 ; ich < 128; ich++)
	{
		outputFile << VFAT3.chn_d[ich].armdac_gain*1000 << endl;
		outputFile << VFAT3.chn_d[ich].armdac_offset*1000 << endl;
	}
/*	
	#if DUMP_DAC_SCAN
		outputFile << "DAC_value ";
		for (int ich = 0; ich<128; ich++) outputFile << "Ch_" << ich << "_[mV] ";
		outputFile << endl;
		for (int v = 0; v < 19; v++)
		{
			outputFile << VFAT3.chn_d[1].vx_armdac[v] << " ";
			for (int ich = 0; ich<128; ich++) outputFile << VFAT3.chn_d[ich].vy_armdac[v]*1000 << " ";
			outputFile << endl;
		}
		outputFile << endl << endl;
	#endif
*/
	outputFile << "Peaking_time_PT_25_[ns]_PT_50_[ns]_PT_75_[ns]_PT_100_[ns]" << endl;

	for (int ich = 0; ich < 128 ; ich++ )
	{
		for (int ipk = 0; ipk < 4; ipk++) outputFile << VFAT3.chn_d[ich].peakTime[ipk] << endl;
	}

	outputFile << "Bonding_status" << endl;
	for (int ich = 0; ich<128; ich++)
	{
		outputFile << VFAT3.chn_d[ich].bonding << endl;

	}
	outputFile << endl;
	
	outputFile.close();
	return true;
}


