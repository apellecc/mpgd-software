/*
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include "mexception.h"
#include "TesterCtrl.h"


TesterCtrl::TesterCtrl(WishboneBus *wbbPtr, uint32_t baseAdd) : 
			MWbbSlave(wbbPtr, baseAdd)
{
    cfg = 0;
    led = 0;
}


//*****************************************************************************
//   Configuration
//*****************************************************************************
void TesterCtrl::setTestType(testType_t type)
{
    uint32_t t;

    switch (type) {
        case TTYPE_Functional:  t=CFG_TTYPE_FUNC;   break;
        case TTYPE_ATE:         t=CFG_TTYPE_ATE;    break;
        case TTYPE_BIST:        t=CFG_TTYPE_BIST;   break;
        case TTYPE_EYE:         t=CFG_TTYPE_EYE;    break;
        default: throw MException("Illegal testType selection in TesterCtrl::setTestType");
    }

    cfg &= ~CFG_TTYPE_MASK;
    cfg |= t;

    wbb->addWrite(baseAddress+regCfg, cfg);
    wbb->execute();
}

void TesterCtrl::setExtInj(bool od, bool ev)
{
    cfg &= ~(CFG_EXT_INJ_OD | CFG_EXT_INJ_EV);
    if (od)
        cfg |= CFG_EXT_INJ_OD;
    if (ev)
        cfg |= CFG_EXT_INJ_EV;
    wbb->addWrite(baseAddress+regCfg, cfg);
    wbb->execute();
}

//*****************************************************************************
//    BIST
//*****************************************************************************
void TesterCtrl::BISTstart()
{
    wbb->addWrite(baseAddress+regBISTstart, 0x01);
    wbb->execute();
}

void TesterCtrl::BISTgetStatus(BISTstatus_t *status)
{
    uint32_t d;

    wbb->addRead(baseAddress+regBISTstatus, &d);
    wbb->execute();
       
    status->running = (d & BIST_RUNNING_MASK) >> BIST_RUNNING_SHIFT;
    status->counter = (d & BIST_COUNTER_MASK) >> BIST_COUNTER_SHIFT;
}

//*****************************************************************************
//    LED / push button
//*****************************************************************************
void TesterCtrl::setLED(LEDid_t ledId, bool on, bool flash)
{
    uint32_t l;

    switch (ledId) {
        case LED_Start:  l=LED_START;   break;
        case LED_Pass:   l=LED_PASS;    break;
        case LED_Fail:   l=LED_FAIL;    break;
        default: throw MException("Illegal LED selection in TesterCtrl::setLED");
    }

    if (on)
        led |= (l<<LED_ON_SHIFT);
    else
        led &= ~(l<<LED_ON_SHIFT);

    if (flash)
        led |= (l<<LED_FLASH_SHIFT);
    else
        led &= ~(l<<LED_FLASH_SHIFT);

    wbb->addWrite(baseAddress+regLED, led);
    wbb->execute();
}

bool TesterCtrl::getPushButton()
{
    uint32_t d;

    wbb->addRead(baseAddress+regPB, &d);
    wbb->execute();
    
    return (d != 0);
}



