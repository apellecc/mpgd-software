/*
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>.
 *
 */

#ifndef TESTER_CTRL_H
#define TESTER_CTRL_H

#include <stdint.h>
#include "mwbbslave.h"

class TesterCtrl: public MWbbSlave
{
public:
    typedef enum {
        TTYPE_Functional,
        TTYPE_ATE,
        TTYPE_BIST,
        TTYPE_EYE
    } testType_t; 

    typedef struct {
        bool        running;
        uint32_t    counter;
    } BISTstatus_t;    

    typedef enum {
        LED_Start,
        LED_Pass,
        LED_Fail,
    } LEDid_t; 


public:
    TesterCtrl(WishboneBus *wbbPtr, uint32_t baseAddress);
    void setTestType(testType_t type);
    void setExtInj(bool od, bool ev);
    void BISTstart();
    void BISTgetStatus(BISTstatus_t *status);
    void setLED(LEDid_t led, bool on, bool flash);
    bool getPushButton();

private:			
    uint32_t    cfg;
    uint32_t    led;
		
    // WBB Slave registers map 
	enum regAddress_e {
		regCfg				= 0,
		regBISTstart    	= 1,
		regBISTstatus		= 2,
        regLED              = 3,
        regPB               = 4
	};

	enum cfg_fields_e {
        CFG_TTYPE_FUNC      = (0<<0),
        CFG_TTYPE_ATE       = (1<<0),
        CFG_TTYPE_BIST      = (2<<0),
        CFG_TTYPE_EYE       = (3<<0),
        CFG_TTYPE_MASK      = (3<<0),

        CFG_EXT_INJ_OD      = (1<<2),
        CFG_EXT_INJ_EV      = (1<<3)
	};

 	enum BIST_fields_e {
        BIST_RUNNING_MASK    = (1<<26),
        BIST_RUNNING_SHIFT   = 26,
        BIST_COUNTER_MASK    = 0xffffff,
        BIST_COUNTER_SHIFT   = 0
    };

 	enum LED_fields_e {
        LED_START            = (1<<0),
        LED_PASS             = (1<<1),
        LED_FAIL             = (1<<2),
        LED_ON_SHIFT         = 0,
        LED_FLASH_SHIFT      = 3
    };
};



#endif // TESTER_CTRL_H
