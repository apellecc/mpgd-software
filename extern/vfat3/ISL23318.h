/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */

#ifndef ISL23318_H
#define ISL23318_H

#include <stdint.h>
#include "i2cslave.h"


class ISL23318 : public I2Cslave
{
public:
	ISL23318(I2Cbus *bus, uint8_t address);
	void write(uint8_t reg, uint8_t data);
	uint8_t read(uint8_t add);

private:
	enum {
		REG_WR				= 0x00,
		REG_ACR				= 0x10
	};

	enum {
		ACR_SHDNnn			= 0x40
	};

public:
	void setWiper(uint8_t data)
		{
			write(REG_ACR, 0x00);
			write(REG_WR, data);
		}

	uint8_t getWiper()
		{
			return read(REG_WR);
		}

};



#endif // ISL23318_H
