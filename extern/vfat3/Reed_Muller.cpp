#include <iostream>
#include <string>
#include "rmencode.h"
#include "rmdecode.h"
#include "Reed_Muller.h"

using std::string;
using std::cin;
using std::cout;
using std::endl;

Reed_Muller::Reed_Muller()
{
}

Reed_Muller::~Reed_Muller()
{
}

bool Reed_Muller::run(string param, int *chip_id, int *chip_id_efuse)
{
	rmencode RMenc;
	rmdecode RMdec;

	int chip_ID = 0;
	
	bool status;

	if (param=="ENCODE")
	{
		chip_ID = *chip_id;
		status = RMenc.run(2, 5, chip_ID, chip_id_efuse);			
	}
	else if (param=="DECODE")
	{
		status = RMdec.run(2, 5, chip_id_efuse, &chip_ID);
		if (status) *chip_id = chip_ID;
	}
	else
	{
		cout << "Reed-Muller procedure error, choose ENCODE or DECODE" << endl;
		status = false;
	}

return status;
}
