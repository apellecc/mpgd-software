/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#ifndef PTscurves_h
#define PTscurves_h


#include "TestBench.h"
#include "GenericTest.h"

class PTscurves : public GenericTest
{
public:
 	PTscurves(TestBench &tb);
    bool run(string param);
	bool run(string param, double *vENC, double *vTH);
	bool rungainfast(double *vTH);

private:
	bool runSerial(double *vENC, double *vTH);
	bool runParallel(double *vENC, double *vTH);
    void generateReport(string filename);
	void resetHistogram();
	static void 	histogramCB(void *ctx, VFAT3dataParser::VFAT3data_t *data);
    void pulseAtChargeDAC(int chargeDAC, unsigned long nPulses);

private:
	static const long	maxChargeDAC = 256;

	VFAT3board		*board;
	VFAT3chip		*chip;
    VFAT3chip::CalibrationConfig_t 	calCfg;

	static const int 	numPulses = 1000;
    static const int    numPulsesSearch = 50;
	unsigned long		evCounter;
	std::array<long, 128> histogram;
   	long 	        hitMatrix[maxChargeDAC][128];
   	double	        chargeArray[maxChargeDAC];
   	double	        thresholds[128];
   	double	        ENC[128];

};

#endif // TScurves_h


