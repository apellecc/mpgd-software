#ifndef VFAT3_DATA_H
#define VFAT3_DATA_H

#include <string>
#include <stdint.h>

class VFAT3_data
{
public:
	struct VFAT3_pwr_s{
		float AVDD;
		float DVDD;
		float IAVDD;				//current absorbed by the analogue section
		float IDVDD;				//current absorbed by the digital section
		float VDD_EFUSE;
	};

	struct Iref_s{
		int ScanStart;
		int ScanStop;
		float ScanData[32];
		double gain;
		double offset;
		int optVal_ndac;
		float optVal;
	};

	struct Vref_s{
		float Vbgr;				//band-gap reference voltage
		float ADC_VinM;			//ADC negative input reference
		float ADC0_vrf_Scan[4];	//ADC0 vref scan data
		double ADC0_vrf_gain;
		double ADC0_vrf_offset;
	};

	struct DAC_s{
		std::string name;		//DAC name
		int monSel;				//used as DAC id
		int maxValue;			//DAC dimension
		float ScanData[256]; 	//data measured by external ADC
		double gain;			//gain from linear best-fit
		double offset;			//offset from linear best-fit
		int optVal_ndac;		//DAC optimal value to get the desired bias
		float optVal;
	};

	struct Ext_tsens_s{
		double ibias;
	};

	struct ADC_s{
		double ADC0_gain;
		double ADC0_offset;
		int ADC0_vref_optVal_ndac;
		double ADC0_vref;
		double ADC1_gain;
		double ADC1_offset;
	};

	struct Calib_s{
		double vpul_gain;
		double vpul_offset;
		double ipul_gain;
		double ipul_offset;
		double cal_bsl;
		const int Inj_Cap = 100; //injection capacitance in fF
	};

	struct Channel_s{
		double gain[3];
		double peakTime[4];
		double noise[3];
		int bonding;			//0 ok, 1 open, 2 short, 3 open but shorted with the neighbour
		double armdac_gain;
		double armdac_offset;
		float vx_armdac[19];
		float vy_armdac[19];
		double scurve[2][256];
	};

	struct VFAT3_s{
		int VFAT3_id;			//EFUSE id
		VFAT3_pwr_s pwr_d;
		Iref_s iref_d;
		Vref_s vref_d;
		DAC_s dac_d[20];
		ADC_s adc_d;
		Ext_tsens_s ext_tsens_d;
		Calib_s calib_d;
		Channel_s chn_d[128];
	};

public:

	VFAT3_s VFAT3;
	bool Data_dump();
	bool Data_dump_elab();

};


#endif
