/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#include "TestBench.h"
#include "TBconfig.h"
#include "VFAT3board.h"
#include "Tcfd.h"
#include "Tutils.h"
#include <iostream>
#include <fstream>

using std::string;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;




Tcfd::Tcfd(TestBench &t) : GenericTest(t)
{
	board = tb.board;
	chip = tb.chip;
	chipCfg = tb.chipConfig;
}


bool Tcfd::run(string param)
{
	VFAT3chip::VFAT3config_t       	chipCfg = tb.chipConfig;
	VFAT3chip::CalibrationConfig_t 	calCfg = tb.chipConfig.CAL;
	VFAT3chip::ChannelConfig_t		chCfg;

	int const minDac = 6;
	int const maxDac = 100;

	// Open output file
	ofstream outputFile;
	outputFile.open (string("Data/")+string("Timewalk_")+Tutils::filenameSuffix()+".txt");

	// disable cal bit and anable mask on all channels
	for (int i=0; i<129; i++){
		chipCfg.CH[i].cal = false;
		chipCfg.CH[i].mask = true;
	}

	chipCfg.DISC.arm_dac = 10;			// Arming global threshold
	chipCfg.CAL.POL = 0;				// Positive injection
	chipCfg.CAL.MODE = 2;				// Cal Pulse mode 2=Ipulse
	chipCfg.CAL.FS = 1;					// Cal current pulse scale factor (0-3)
	chipCfg.CAL.DUR = 1;				// 1=25ns
	chipCfg.TRG.PS = 1;					// Pulse Stretcher control (1-8)
	chipCfg.TRG.syncLevelEnable = 0;	// Disable level mode
	chipCfg.TRG.ST = 1;					// Self Trigger

	chip->setConfig(chipCfg);

	// Init the charge injection
	tb.initInjCharge(calCfg);

	// Send BC0
	board->comPortTx->sendBC0();

	// start run
	board->mRunControl->startRun();
	board->comPortTx->sendRunMode();

	// read reference current
	outputFile << "VIref=" << tb.getVmon(VFAT3chip::Imon_Iref) << endl;

	// read threshold voltage
	outputFile << "Vthreshold=" << tb.getVmon(VFAT3chip::Vmon_Vth_Arm) << endl;

	// column heads
	outputFile << "Channel ";
	for (int chargeDAC=minDac; chargeDAC<=maxDac; chargeDAC++){
		// set calibration DAC
		if (calCfg.MODE == 1){
			calCfg.DAC = 255-chargeDAC;		// Vpulse mode
		} else if (calCfg.MODE == 2) {
			calCfg.DAC = chargeDAC;
		} else {
			cerr << "Please set Calibration MODE != 0" << endl;
			exit(0);
		}
		chip->addSetCalibrationConfig(calCfg);
		chip->execute();
		usleep(1000);

		outputFile << tb.getInjCharge(calCfg) << " ";
	}
	outputFile << endl;

	// Loop on channel
	for (int ch=0; ch<128; ch++){
//	for (int ch=81; ch<=81; ch++){
		outputFile << ch << " ";
		cout << "Channel " << ch << endl;

		chCfg = tb.chipConfig.CH[ch];
		chCfg.cal = true;
		chCfg.mask = false;
		chip->addSetChannelConfig(ch, chCfg);
		chip->execute();

		// loop on charge
		for (int chargeDAC=minDac; chargeDAC<=maxDac; chargeDAC++){
			// set calibration DAC
			if (calCfg.MODE == 1){
				calCfg.DAC = 255-chargeDAC;		// Vpulse mode
			} else if (calCfg.MODE == 2) {
				calCfg.DAC = chargeDAC;
			} else {
				cerr << "Please set Calibration MODE != 0" << endl;
				exit(0);
			}
			chip->addSetCalibrationConfig(calCfg);
			chip->execute();
		//	usleep(1000);

			outputFile << tb.measChDelay(calCfg) << " ";
		}
		outputFile << endl;

		chCfg = tb.chipConfig.CH[ch];
		chCfg.cal = false;
		chCfg.mask = true;
		chip->addSetChannelConfig(ch, chCfg);
		chip->execute();
	}

	// stop the run
	board->comPortTx->sendSConly();
	board->mRunControl->stopRun();

	// restore chip configuration
	chip->setConfig(tb.chipConfig);

	outputFile.close();
	return true;
}


