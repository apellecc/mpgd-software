/*
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>.
 *
 */

#include <iostream>
#include <unistd.h>
#include <strings.h>
#include "PTlcldac.h"
#include "PTscurves.h"
#include "Tutils.h"

const double lcldac_gain_min = 0;
const double lcldac_gain_max = 1.2e-3;

const double lcldac_offset_min = -5e-3;
const double lcldac_offset_max = 5e-3;

PTlcldac::PTlcldac(TestBench &t) : GenericTest(t)
{
}

PTlcldac::~PTlcldac()
{
}

bool PTlcldac::run(string param)
{
	PTscurves fit_scurve(tb);
	bool fit_status;

	double vpul[19][128];
	double vbsl[128];
	double gain;
	double offset;
	float vx[19];
	float vy[19];
	double vnoise[128];
	int GBL_dac = 100;

	// disable cal bit on all channels
	for (int i=0; i<128; i++)
	{
		tb.chipConfig.CH[i].zcc_dac = 0;
		tb.chipConfig.CH[i].arm_dac = 0;
		tb.chipConfig.CH[i].mask = false;
		tb.chipConfig.CH[i].cal = false;
	}
	//calibration settings
	tb.chipConfig.CAL.MODE = 1;	// voltage injection
	tb.chipConfig.CAL.POL = 0;		// negative injected charge, the CSA output is a positive pulse
	tb.chipConfig.CAL.DUR = 100;
	tb.chipConfig.CAL.PHI = 0;

	//CFD settings
	tb.chipConfig.DISC.PT = 100;
	tb.chipConfig.DISC.SEL_POL = 0;
	tb.chipConfig.DISC.EN_HYST = 1;
	tb.chipConfig.DISC.FORCE_EN_ZCC = 0;
	tb.chipConfig.DISC.FORCE_TH = 0;
	tb.chipConfig.DISC.SEL_COMP_MODE = 1;

	//CSA settings - medium gain
	tb.chipConfig.PRE.TP = 100;
	tb.chipConfig.PRE.RES = 1;
	tb.chipConfig.PRE.CAP = 1;

	//Set Global ARM DAC
	tb.chipConfig.DISC.arm_dac = GBL_dac;
	tb.setChipConfig();

	//	fit_status = fit_scurve.rungainfast(vbsl);

	fit_status = fit_scurve.run("PARALLEL",vnoise,vbsl);




	for ( int v = -63; v < 64; v = v + 7)
	{
		vx[ v / 7 + 9 ] = v;
		for ( int ch = 0; ch < 128 ; ch++ ) tb.chipConfig.CH[ch].arm_dac = v;
		tb.setChipConfig();
		
		//fit_status = fit_scurve.rungainfast(vpul[v / 7 + 9 ]);
		fit_status = fit_scurve.run("PARALLEL",vnoise,vpul[v / 7 + 9 ]);
/*cout << "v: " << v << endl;
for (int ii = 0; ii <128; ii++) cout << "ch: " << ii << " " << vpul[v/7+9][ii] << endl;
cout << endl;*/
	}

	for (int ch = 0; ch<128; ch++)
	{
		tb.chipConfig.CH[ch].arm_dac = 0;

		for(int v = 0; v<19; v++)
		{
			vy[v]= ( GBL_dac * tb.ptdata->VFAT3.dac_d[16].gain + tb.ptdata->VFAT3.dac_d[16].offset ) * ( vpul[v][ch] / vbsl[ch] - 1 );
			tb.ptdata->VFAT3.chn_d[ch].vx_armdac[v] = vx[v];
			tb.ptdata->VFAT3.chn_d[ch].vy_armdac[v] = vy[v];
			if ((vy[v] <= vy[v-1]) && v)
			{
				cout << "Channel " << ch << " local DAC monotonicy error" << endl;
				//throw TestError("VFAT3 test error");
			}
			
		}

		fit_status = Tutils::bestFitLin(19,vx,vy,&gain,&offset);

		if ( (gain < lcldac_gain_min) || (gain > lcldac_gain_max) )
		{
			cout << "Channel " << ch << " local arm dac gain error, gain: " << gain*1000 << " mV/[LSB]" << endl;
			cout << "Gain should be included between " << lcldac_gain_min*1000 << " mV/[LSB] and " << lcldac_gain_max*1000 << " mV/[LSB]" << endl;
			throw TestError("VFAT3 test error");
		}

		if ( (offset < lcldac_offset_min) || (offset > lcldac_offset_max) )
		{
			cout << "Channel " << ch << " local arm dac offset error, offset: " << offset*1000 << " mV" << endl;
			cout << "Offset should be included between " << lcldac_offset_min*1000 << " mV and " << lcldac_offset_max*1000 << " mV" << endl;
			throw TestError("VFAT3 test error");
		}

		tb.ptdata->VFAT3.chn_d[ch].armdac_gain = gain;
		tb.ptdata->VFAT3.chn_d[ch].armdac_offset = offset;

		//cout << "Channel " << ch << " lcl arm dac gain: " << gain << " offset: " << offset <<endl;
	}

	tb.setChipConfig();

    return true;
}

