#include <string>

//defines structs for socket handling
#include <netinet/in.h>

using namespace std;


class TcpClient
{
public:
	TcpClient();
	~TcpClient();

	void connectToServer( string &hostname, int port=5025 );
	void disconnect( );
	void transmit( string &txString );
	void receive( string &rxString, int timeout=100 );

	string getCurrentHostName( ) const;
	int    getCurrentPort( ) const;

private:
	typedef struct sockaddr_in SockAddrStruct;
	typedef struct hostent     HostInfoStruct;

	string           currentHostName;
	int              currentPort;
	int              currentSocketDescr;

	SockAddrStruct   serverAddress;
	HostInfoStruct * currentHostInfo;
	bool             clientIsConnected;

	int              receiveBufferSize;
};
