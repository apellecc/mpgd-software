#include "PTtsens.h"
#include "VFAT3chip.h"
#include "Tutils.h"
#include <unistd.h>
#include <iostream>

// Internal temperature sensor acceptance range
const float tsens_int_min = 0.1;	// min
const float tsens_int_max = 1;		// max

// External temperature sensor acceptance range
const float tsens_ext_min = 0.5*0.9; // min, nominal 500 uA in a 1 kOhm resistor
const float tsens_ext_max = 0.5*1.1; // max

PTtsens::PTtsens(TestBench &t) : GenericTest(t)
{
}

bool PTtsens::run(string param){
	VFAT3chip::MonitorConfig_t monCfg = tb.chipConfig.MON;
	float vmon;

	// check internal temperature sensor
	monCfg.SEL = tb.chip->V_Tsens_Int;
	tb.chip->addSetMonitorConfig(monCfg);
	tb.chip->execute();
	usleep(10000);

	tb.board->readmon(VFAT3board::V_VMON, &vmon);

	if ( (vmon <= tsens_int_min) || (vmon >= tsens_int_max) )
	{
		cout<<"Internal temperature sensor test failed!"<<endl;
		cout<<"Internal temperature sensor monitored voltage: " << vmon << " V" << endl;
		cout<<"Monitored voltage shold be included between " << tsens_int_min << " V and " << tsens_int_max << " V" << endl;
		throw TestError("Internal temperature sensor test failed!");
	}

		
	// check external temperature sensor
	monCfg.SEL = tb.chip->V_Tsens_Ext;
	tb.chip->addSetMonitorConfig(monCfg);
	tb.chip->execute();
	usleep(10000);

	tb.board->readmon(VFAT3board::V_VMON, &vmon);

	if ( (vmon <= tsens_ext_min) || (vmon >= tsens_ext_max) )
	{
		cout<<"External temperature sensor test failed!"<<endl;
		cout<<"External temperature sensor monitored voltage: " << vmon << " V" << endl;
		cout<<"Monitored voltage shold be included between " << tsens_ext_min << " V and " << tsens_ext_max << " V" << endl;
		throw TestError("External temperature sensor test failed!");
	}
	
	tb.ptdata->VFAT3.ext_tsens_d.ibias = vmon / 1000;
	cout<<"Temperature sensors test passed!"<<endl;
	return true;
}
