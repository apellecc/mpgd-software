/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <mexception.h>
#include "TRGrcv.h"


TRGrcv::TRGrcv() 
{
}


TRGrcv::TRGrcv(WishboneBus *wbbPtr, uint32_t baseAdd) : 
			MWbbSlave(wbbPtr, baseAdd)
{
}

TRGrcv::~TRGrcv()
{
}

//
// set register
//
void TRGrcv::addSetReg(uint16_t address, uint32_t val)
{
	if (!wbb)
		throw MIPBusError("No IPBus configured");

	wbb->addWrite(baseAddress+address, val);
}

//
// Read register
//
void TRGrcv::addGetReg(uint16_t address, uint32_t *val)
{
	if (!wbb)
		throw MIPBusError("No IPBus configured");

	wbb->addRead(baseAddress+address, val);
}

//
// Disable (or enable) the receiver 
//
void TRGrcv::addEnable(bool d)
{
	if (!wbb)
		throw MIPBusError("No IPBus configured");

	wbb->addRMWbits(baseAddress+regCtrl, ~CTRL_RCVENABLE, d ? CTRL_RCVENABLE : 0);
}

