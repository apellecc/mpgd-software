#include <iostream>
#include <stdio.h>
#include <sys/file.h>
#include "PTeFuse.h"
#include "Reed_Muller.h"
#include <errno.h>
#include <string.h>
#include <unistd.h>


PTeFuse::PTeFuse(TestBench &t):GenericTest(t)
{
	slowControl = tb.board->slowControl;
}

PTeFuse::~PTeFuse()
{
}

bool PTeFuse::read(int *chip_id)
{	
	uint32_t chipID;
	int chip_id_efuse[32];
	int chip_id_r;
	bool status;

	slowControl->addRead(VFAT3chip::RegChipID, &chipID);
	slowControl->execute();

	cout << "chip id raw " << chipID << endl;

	for (int i = 0; i <32; i++) chip_id_efuse[i] = (chipID & 1 << i)? 1 : 0;

	status = ReedMuller.run("DECODE", &chip_id_r, chip_id_efuse);

	if (status)
	{
		cout << "Programmed Chip ID: " << chip_id_r << endl;
	}
	else
	{
		cout << "Reed Muller decoding error" << endl;
		throw TestError("Chip ID reading error!");
	}

	*chip_id = chip_id_r;

	return true;
}


bool PTeFuse::write()
{
	FILE *chip_ID_FILE = fopen("Chip_ID.txt","r+"); //chip id file
	int prgTime = 20e-6/25e-9; // programming time
	char ans;
	int chip_id_p = 0; //chip_id to be programmed
	int chip_id_r = 0; //chip_id read
	int chip_id_efuse[32]; //chip id bit array

	bool status;

	for (int i = 0; i < 32; i++) chip_id_efuse[i] = 0;

	if (chip_ID_FILE) // if file exists
	{
		int res_lock =flock(fileno(chip_ID_FILE), LOCK_EX); // opening with lock, if already locked waits until unlock

		if (res_lock) //check for locking errors
		{
			cout << "Locking error: " << strerror(errno) << endl;
			fclose(chip_ID_FILE);
			throw TestError("Chip ID programming error!");
		}

		fscanf(chip_ID_FILE,"%d",&chip_id_p); // reading the last programmed chip id
		chip_id_p++; //increasing the chip id
		fseek(chip_ID_FILE,0,SEEK_SET); // going to the file beginning
	}
	else
	{
		while ((ans != 'y') && (ans !='n'))
		{
			cout << "Chip_ID.txt file does not exist. Do you want to create a new one? [y/n]" << endl;
			if (cin >> ans)
			{
				scanf("%*c");
				switch (ans)
				{
					case 'y':
						while ((chip_id_p < 20000) || (chip_id_p > 65535))
						{
							cout << "Insert a new chip ID" << endl;
							if (cin >> chip_id_p) 
							{
								scanf("%*c");
								if ((chip_id_p < 20000) || (chip_id_p > 65535)) 
								{
									cout << "Error Chip ID should be included between 20000 and 65535" << endl;
									chip_id_p = 0;
								}
							}
							else
							{
								chip_id_p = 0;
								cout << "Error: wrong data type" << endl;
								cin.clear();
								while (ans!='\n')
								{
									scanf("%c", &ans);
								}
							}
						}
						chip_ID_FILE = fopen("Chip_ID.txt","w+");
						flock(fileno(chip_ID_FILE), LOCK_EX);
//						fprintf(chip_ID_FILE,"%d",chip_id_p);
//						fclose(chip_ID_FILE);
						ans = 'y';
						break;
					case 'n':
							cout << "Chip ID not fused, file not created!" << endl;
							return true;
							break;
					default:
							cout << "Wrong selection" << endl;
							while (ans!='\n')
							{
								scanf("%c", &ans);
							}
							break;
				}
			}
			else
			{
				cout << "Wrong input" << endl;
				cin.ignore();
				cin.clear();
			}
		}
	}	

	if (chip_id_p > 65535) //checking that the chip id is not bigger than 65535 (16 bits)
	{
		cout << "Chip ID to be programmed equal to: " << chip_id_p << endl;
		cout << "Chip ID cannot be higher than 65535 (16 bits)" << endl;
		fclose(chip_ID_FILE);
		throw TestError("Chip ID programming error!");
	}

	if (chip_id_p < 20000) //checking that the chip id is not lower than 20000
	{
		cout << "Chip ID to be programmed equal to: " << chip_id_p << endl;
		cout << "Chip ID cannot be lower than 20000" << endl;
		fclose(chip_ID_FILE);
		throw TestError("Chip ID programming error!");
	}

	read(&chip_id_r); // reading the chip id 

	if (chip_id_r != 0) // checking the the chip id is not already programmed
	{
		cout << "Error!" << endl;
		cout << "Chip ID already programmed!"<< endl;
		fclose(chip_ID_FILE);
		throw TestError("Chip ID programming error!");
	}
	cout << "Chip id to be programmed: " << chip_id_p << endl;
	cout << "Programmare? y/n" << endl;
	cin >> ans;
	scanf("%*c");
	if (ans !='y')
	{
		cout <<"Programmazione annullata." << endl;
		fclose(chip_ID_FILE);
		throw TestError("Chip ID programming error!");
	}

	status = ReedMuller.run("ENCODE", &chip_id_p, chip_id_efuse); //ReedMuller chip_id encoding

	if (status)
	{
		for (int i = 0; i < 32; i++)
		{		
			if (chip_id_efuse[i]) 
			{
				tb.chip->eFuseBit(i, prgTime); // programming bits equal to 1
				//usleep(10000);
			}	
		}

		read(&chip_id_r); // read the programmed chip_id

		if (chip_id_r == chip_id_p) {
			cout << "Chip ID correctly programmed" << endl;
			tb.ptdata->VFAT3.VFAT3_id = chip_id_r;
			fprintf(chip_ID_FILE,"%d",chip_id_p); //saving the chip id
			fclose(chip_ID_FILE);
		}
		else
		{
			cout << "Error!" << endl;
			cout << "Chip ID to be programmed: " << chip_id_p << endl;
			fclose(chip_ID_FILE);
			//throw TestError("Chip ID programming error!");				
		}
	}
	else
	{
		cout << "Reed Muller encoding error" << endl;
		fclose(chip_ID_FILE);
		throw TestError("Chip ID programming error!");
	}
	return true;
}



bool PTeFuse::run(string param) {
	
	int chip_id;
	
	tb.board->setTestType(TesterCtrl::TTYPE_Functional);
	tb.board->setDiffMux(VFAT3board::MUX_SEL_TXD);
	tb.board->onOffVDD(VFAT3board::VDD_EFUSE, true);

	usleep(1000);

	tb.chipInit();

	cout << "Sync check " << endl << endl;
    if (!tb.syncVerify()){
        cout << "Sync check FAILED" << endl;
        throw TestError("Sync check FAILED");
    }

    // Update testbench and chip configuration
    tb.setChipConfig();

	if (param == "READ")
	{
		read(&chip_id);
		tb.ptdata->VFAT3.VFAT3_id = chip_id;
	}
	else if (param == "WRITE")
	{
		write();
	}
	else
	{
		cout << "Parameter error, choose READ or WRITE" << endl;
		throw TestError("Chip ID programming error!");
	}

	
    return true;
}

