/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */

#include <iostream>
#include "IPbusTester.h"
#include "mexception.h"
#include "ipbus.h"

using std::string;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;

#define RO_ADDRESS 0x010000
#define RO_VALUE_0 0x56464154
#define RO_VALUE_1 0x00030001

#define RW_ADDRESS 0x010002


#define MAX_NUM_TRANSACTION	1000


IPbusTester::IPbusTester(TestBench &t) : GenericTest(t)
{
	ipBus = t.board->slowControl;
}

/*
IPbusTester::IPbusTester(IPbus *bus)
{
	ipBus = bus;
}
*/

void IPbusTester::readAndCheck(uint32_t size, uint32_t address, uint32_t *chk, bool fifoMode)
{
	uint32_t rData[MAX_NUM_TRANSACTION];

	if (fifoMode)
		ipBus->addNIRead(size, address, rData);		
	else
		ipBus->addRead(size, address, rData);		
	ipBus->execute();
	
	for (unsigned i=0; i<size; i++)
		if (rData[i] != chk[i]) {
			printf("******* Wrong data from read. Aspected:0x%08x Read:0x%08x\n\n", chk[i], rData[i]);
			exit(0);
		}

}

void IPbusTester::testTransactions()
{
	uint32_t rData;
	uint32_t chk[MAX_NUM_TRANSACTION];

	cout << "Void packet test" << endl;
	ipBus->execute();
	ipBus->execute();

	cout << "Idle transaction test" << endl;
	ipBus->addIdle();
	ipBus->execute();

	cout << "Single Read transaction test" << endl;
	chk[0] = RO_VALUE_0;
	readAndCheck(1, RO_ADDRESS, chk);

	cout << "Multiple Read transaction test" << endl;
	chk[0] = RO_VALUE_0;
	chk[1] = RO_VALUE_1;
	readAndCheck(2, RO_ADDRESS, chk);

	cout << "Non-incrementing Read transaction test" << endl;
	chk[0] = RO_VALUE_0;
	chk[1] = RO_VALUE_0;
	readAndCheck(2, RO_ADDRESS, chk, true);

	cout << "Single Write transaction test" << endl;
	chk[0] = 0x12345678;
	ipBus->addWrite(RW_ADDRESS, chk[0]);
	ipBus->execute();
	readAndCheck(1, RW_ADDRESS, chk);

	cout << "Multiple Write transaction test" << endl;
	chk[0] = 0x00005678;
	chk[1] = 0x00004321;
	chk[2] = 0x000055aa;
	ipBus->addWrite(3, 0x0000, chk);
	ipBus->execute();
	readAndCheck(3, 0x0000, chk);

	cout << "Non-incrementing Write transaction test" << endl;
	chk[0] = 0x11111111;
	chk[1] = 0x22222222;
	ipBus->addNIWrite(2, RW_ADDRESS, chk);
	ipBus->execute();
	readAndCheck(1, RW_ADDRESS, chk+1);

	cout << "Read-Modify-Write-Bits transaction test" << endl;
	ipBus->addWrite(RW_ADDRESS, 0x12345678);
	ipBus->execute();
	ipBus->addRMWbits(RW_ADDRESS, ~0x0000ff00, 0x00006500, &rData);
	ipBus->execute();
	chk[0] = 0x12346578;
	if (rData != chk[0]) {
		printf("******* Wrong data from RMWB.  Aspected:0x%08x Read:0x%08x\n\n", chk[0], rData);
		exit(0);
	}
	readAndCheck(1, RW_ADDRESS, chk);

	cout << "Read-Modify-Write-Sum transaction test" << endl;
	ipBus->addWrite(RW_ADDRESS, 0x12345678);
	ipBus->execute();
	ipBus->addRMWsum(RW_ADDRESS, 0x00000008, &rData);
	ipBus->execute();
	chk[0] = 0x12345680;
	if (rData != chk[0]) {
		printf("******* Wrong data from RMWS.  Aspected:0x%08x Read:0x%08x\n\n", chk[0], rData);
		exit(0);
	}
	readAndCheck(1, RW_ADDRESS, chk);
}


void IPbusTester::testMultiTransactions()
{
	uint32_t rData;
	uint32_t chk[MAX_NUM_TRANSACTION];

	cout << "Idle + write + read test" << endl;
	chk[0] = 0x12345678;
	ipBus->addIdle();
	ipBus->addWrite(RW_ADDRESS, chk[0]);
	readAndCheck(1, RW_ADDRESS, chk);


	cout << "Idle + multi write + multi read test" << endl;
	ipBus->addIdle();
	chk[0] = 0x00000099;
	chk[1] = 0x00000088;
	chk[2] = 0x00000077;
	chk[3] = 0x00000066;
	chk[4] = 0x00000055;
	chk[5] = 0x00000044;
	chk[6] = 0x00000033;
	chk[7] = 0x00000022;
	chk[8] = 0x00000011;
	ipBus->addWrite(9, 0, chk);	// 9 register starting from 0
	readAndCheck(9, 0, chk);


	cout << "Idle + Write + RMWB + Read test" << endl;
	ipBus->addIdle();
	chk[0] = 0x98765432;
	ipBus->addWrite(RW_ADDRESS, chk[0]);
	chk[0] = (chk[0] & ~0x00ff0000) | 0x00330000;
	ipBus->addRMWbits(RW_ADDRESS, ~0x00ff0000, 0x00330000, &rData);
	readAndCheck(1, RW_ADDRESS, chk);
	if (rData != chk[0]) {
		printf("******* Wrong data from RMWB.  Aspected:0x%08x Read:0x%08x\n\n", chk[0], rData);
		exit(0);
	}
}


void IPbusTester::testErrors()
{
	bool detected;
	uint32_t rData[MAX_NUM_TRANSACTION];
	int saveBufferSize;
	
	cout << "Bad header version test" << endl;
	detected = false;
	ipBus->addBadIdle(true, false);
	try{
		ipBus->execute();
	} catch (MIPBusError) {
		detected = true;
	}
	if (!detected){
		printf("******* Bad header version not detected\n\n");
		exit(0);
	}
	
	cout << "Bad info code test" << endl;
	detected = false;
	ipBus->addBadIdle(false, true);
	try{
		ipBus->execute();
	} catch (MIPBusError) {
		detected = true;
	}
	if (!detected){
		printf("******* Bad info code not detected\n\n");
		exit(0);
	}

	cout << "Read bus error test" << endl;
	detected = false;
	ipBus->addRead(6, RW_ADDRESS, rData);
	try{
		ipBus->execute();
	} catch (MIPBusError) {
		detected = true;
	}
	if (!detected){
		printf("******* Remote bus Read error not detected\n\n");
		exit(0);
	}

	cout << "Non Incrementing Read bus error test" << endl;
	detected = false;
	ipBus->addNIRead(5, RW_ADDRESS+100, rData);
	try{
		ipBus->execute();
	} catch (MIPBusError) {
		detected = true;
	}
	if (!detected){
		printf("******* Remote bus Read error not detected\n\n");
		exit(0);
	}

	cout << "RMWB Read bus error test" << endl;
	detected = false;
	ipBus->addRMWbits(RW_ADDRESS+100, 0x00000000, 0x00000000, rData);
	try{
		ipBus->execute();
	} catch (MIPBusError) {
		detected = true;
	}
	if (!detected){
		printf("******* Remote bus Read error not detected\n\n");
		exit(0);
	}

	cout << "RMWB Write bus error test" << endl;
	detected = false;
	ipBus->addRMWbits(RO_ADDRESS, 0x00000000, 0x00000000, rData);
	try{
		ipBus->execute();
	} catch (MIPBusErrorWrite) {
		detected = true;
	}
	if (!detected){
		printf("******* Remote bus Write error not detected\n\n");
		exit(0);
	}

	cout << "Read timeout error test" << endl;
	detected = false;
	ipBus->addRead(0xff0000, rData);
	try{
		ipBus->execute();
	} catch (MIPBusErrorReadTimeout) {
		detected = true;
	}
	if (!detected){
		printf("******* Remote bus timeout in read not detected\n\n");
		exit(0);
	}

	cout << "Write timeout error test" << endl;
	detected = false;
	ipBus->addWrite(0xff0000, 0x44332211);
	try{
		ipBus->execute();
	} catch (MIPBusError) {
		detected = true;
	}
	if (!detected){
		printf("******* Remote bus timeout in write not detected\n\n");
		exit(0);
	}

	cout << "Overflow Read error test" << endl;
	detected = false;
	saveBufferSize = ipBus->getBufferSize();
	ipBus->setBufferSize(saveBufferSize+1000);
	ipBus->addNIRead((saveBufferSize/4)+50, RW_ADDRESS, rData);
	try{
		ipBus->execute();
	} catch (MIPBusError) {
		detected = true;
	}
	if (!detected){
		printf("******* Remote bus overflow TX buffer not detected\n\n");
		exit(0);
	}
	ipBus->setBufferSize(saveBufferSize);

	cout << "Overflow info error test" << endl;
	detected = false;
	saveBufferSize = ipBus->getBufferSize();
	ipBus->setBufferSize(saveBufferSize+1000);
	ipBus->addNIRead(saveBufferSize/4, RW_ADDRESS, rData);
	for (int i=0; i<50; i++)
		ipBus->addIdle();
	try{
		ipBus->execute();
	} catch (MIPBusError) {
		detected = true;
	}
	if (!detected){
		printf("******* Remote bus overflow TX buffer not detected\n\n");
		exit(0);
	}
	ipBus->setBufferSize(saveBufferSize);

	cout << "Underflow on Write error test" << endl;
	for (int i=1; i<=8; i++){
		detected = false;
		ipBus->addWrite(RW_ADDRESS, 0x00006500);
		ipBus->cutTX(i);
		try{
			ipBus->execute();
		} catch (MIPBusError) {
			detected = true;
		}
		if (!detected){
			printf("******* Remote bus underflow RX buffer not detected\n\n");
			exit(0);
		}
	}

	cout << "Underflow on Read error test" << endl;
	for (int i=1; i<=4; i++){
		detected = false;
		ipBus->addRead(RW_ADDRESS, rData);
		ipBus->cutTX(i);
		try{
			ipBus->execute();
		} catch (MIPBusError) {
			detected = true;
		}
		if (!detected){
			printf("******* Remote bus underflow RX buffer not detected\n\n");
			exit(0);
		}
	}

	cout << "Underflow on RMWB error test" << endl;
	for (int i=1; i<=12; i++){
		detected = false;
		ipBus->addRMWbits(RW_ADDRESS, ~0x0000ff00, 0x00006500, rData);
		ipBus->cutTX(i);
		try{
			ipBus->execute();
		} catch (MIPBusError) {
			detected = true;
		}
		if (!detected){
			printf("******* Remote bus underflow RX buffer not detected\n\n");
			exit(0);
		}
	}

	cout << "Underflow on RMWS error test" << endl;
	for (int i=1; i<=8; i++){
		detected = false;
		ipBus->addRMWsum(RW_ADDRESS, 0x00000008, rData);
		ipBus->cutTX(i);
		try{
			ipBus->execute();
		} catch (MIPBusError) {
			detected = true;
		}
		if (!detected){
			printf("******* Remote bus underflow RX buffer not detected\n\n");
			exit(0);
		}
	}

	cout << "Underflow on Instruction test" << endl;
	for (int i=1; i<=4; i++){
		detected = false;
		ipBus->addIdle();
		ipBus->addIdle();
		ipBus->cutTX(i);
		try{
			ipBus->execute();
		} catch (MIPBusError) {
			detected = true;
		}
		if (!detected){
			printf("******* Remote bus underflow RX buffer not detected\n\n");
			exit(0);
		}
	}
}

void IPbusTester::runAll()
{
	cout << "*********************************************************" << endl;
	cout << "  Running all tests on IPBus transactor" << endl; 
	cout << "*********************************************************" << endl;

	testTransactions();
	testMultiTransactions();
	testErrors();

	cout << "*********************************************************" << endl;
	cout << "   All tests run successfully" << endl;
	cout << "*********************************************************" << endl;
}

