/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */

#ifndef VFAT3RCV_H
#define VFAT3RCV_H
#include <mwbbslave.h>
#include <stdint.h>
#include "ComPortRx.h"

class VFAT3rcv: public ComPortRx
{
public:
	typedef struct dataFormat_s {
		bool			enablePZS;
		uint8_t			PZSmaxPartitions;
//		bool			suppressZeroPacket;
//		bool			suppressZeroData;
		uint8_t			eventCountSize;
		uint8_t			bunchCrossSize;
	} dataformat_t;

public:
    VFAT3rcv();
    VFAT3rcv(WishboneBus *wbbPtr, uint32_t baseAddress);
	~VFAT3rcv();
	void setBusAddress(WishboneBus *wbbPtr, uint32_t baseAddress);
	void addSetReg(uint16_t address, uint32_t val);
	void addGetReg(uint16_t address, uint32_t *val);
	void addEnable(bool d);
	void addInvertInput(bool d);
	void addGetNReg(uint16_t address, uint32_t *data, int size);
	void addGetRxStatus(uint32_t *d);
	void addGetSCpkt(int size, uint32_t *rdData);
	void addSetDataFormat(dataformat_t &f);

private:					// WBB Slave registers map 
	enum regAddress_e {
		regOpMode		= 0,
		regHDLCstat		= 1,
		regDataFormat	= 2,
		HDLCrxBuffer	= 0x100,
		HDLCrxBufferEnd = 0x200,
		rdpBase			= 0x00800000
		};

private:
	enum opModeBits_e {
		OPMODE_RCVENABLE			= (1<<0),
		OPMODE_INVERT_POLARITY		= (1<<1)
	};

	enum dataFormat_e {
		enablePZS				= 0x0800,
		PZSmaxPartitions_mask 	= 0x07c0,
		PZSmaxPartitions_shift 	= 6,
//		suppressZeroPacket		= 0x0020,
//		suppressZeroData		= 0x0010,
		eventCountSize_mask		= 0x000c,
		eventCountSize_shift	= 2,
		bunchCrossSize_mask		= 0x0003,
		bunchCrossSize_shift	= 0
	};

public:
	enum HDLCstatBits_e {
		HDLCaddressMask	= 0xff,
		HDLCready		= 0x100
	};

};



#endif // VFAT3RCV_H
