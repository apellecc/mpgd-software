/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */


#ifndef VFAT3chip_H
#define VFAT3chip_H

#include "IPbusSC.h"

class VFAT3rcv;
class VFAT3dataParser;

#define CHIP_NUM_CHANNELS	128
#define CHIP_CFG_CHANNELS	129

class VFAT3chip
{
public:
	VFAT3chip(int chip_id, IPbusSC *sc, VFAT3rcv *rcv, VFAT3dataParser *parser);
	~VFAT3chip();
	void setup();
	void dumpRegisters();
	void dumpConfig();
	void addWriteReg(uint32_t reg, uint32_t val);
	void writeReg(uint32_t reg, uint32_t val);
	void addRMWreg(uint32_t add, uint32_t mask, uint32_t data);
	void addReadReg(uint32_t, uint32_t *data);
	void addReadADC(int adc, uint32_t *data);


public:
	typedef enum regs_e {
		//
		// Global configuration registers [ Block 0 ]
		//
		// Channel configuration registers
		RegCfgChannel_0		= 0x0000,		// Channel 0   config
		RegCfgChannel_127	= 0x007f,		// Channel 127 config
		RegCfgChannel_128	= 0x0080,		// Test channel

		// General bit mapped registers
		RegCfgCtr_0			= 0x0081,		// Control register 0
		RegCfgCtr_1			= 0x0082,		// Control register 1 (DF Related)
		RegCfgCtr_2			= 0x0083,		// Control register 2 (Front End Related)
		RegCfgCtr_3			= 0x0084,		// Control register 3 (CFD Related)
		RegCfgCtr_4			= 0x0085,		// Control register 4 (Signal selection monitoring)
		RegCfgCtr_5			= 0x0086,		// Control register 5

		// dedicated configuration registers
		RegCfgThr			= 0x0087,		// Global Threshold
		RegCfgHyst			= 0x0088,		// Global Hysteresis
		RegCfgLat			= 0x0089,		// Latency
		RegCfgCal_0			= 0x008a,		// Calibration
		RegCfgCal_1			= 0x008b,		// Calibration

		// Bias
		RegCfgBias_0		= 0x008c,		// Bias DAC 1 & Bias DAC 2
		RegCfgBias_1		= 0x008d,		// Preamp
		RegCfgBias_2		= 0x008e,		// Preamp
		RegCfgBias_3		= 0x008f,		// Shaper
		RegCfgBias_4		= 0x0090,		// Shaper + SD
		RegCfgBias_5		= 0x0091,		// SD
		RegCfgBias_6		= 0x0092,		// SLVS

		// RUN/SLEEP
		RegCfgRun			= 0xffff,		// Run

		// Hardware ID registers	[ Block 1 ]
		// Hardware ID
		RegID				= 0x10000,		// Hardware ID
		RegVER				= 0x10001,		// Hardware revision
		RegRWreg			= 0x10002,		// general purpose read/write register
		RegChipID			= 0x10003,		// Chip ID
		RegProg				= 0x10004,		// Program register
	
		// ADC Control logic		[ Block 2 ]
		RegADCread_0		= 0x20000,		// ADC Read - The read trigger three conversions @1MHz
											// Return the result of the last conversion
		RegADCread_1		= 0x20001		// As ADC_READ_0 but on the secondary ADC
	} regs_t;

	//	Channel register bitfields definition
	enum CFG_CHANNEL_BITS {
		CfgCh_arm_dac_mask	= 0x007f,		// bits 6:0  // ARM comparator local DAC (6 bits+sign)
		CfgCh_arm_dac_shift	= 0,
		CfgCh_zcc_dac_mask	= 0x3f80,		// bits 13:7 // Zero crossing local DAC (6 bits+sign)
		CfgCh_zcc_dac_shift	= 7,
		CfgCh_mask			= 0x4000,		// bit 14
		CfgCh_cal			= 0x8000		// bit 15
	};	

	// Global Control register 0
	enum CFG_CTR_0_BITS {
		CfgCtr_0_DDR				= 0x0001,		// bit 0
		CfgCtr_0_ST					= 0x0002,		// bit 1		// Self Trigger
		CfgCtr_0_syncLevelEnable	= 0x0004,		// bit 2		// Enable Level Mode
		CfgCtr_0_PS_mask			= 0xe000,		// bits 15:13	// Pulse Stretcher control. From 1 to 8 clock cycles
		CfgCtr_0_PS_shift			= 13
	};	
	
	// Global Control register 1
	enum CFG_CTR_1_BITS {
		CfgCtr_1_BCb				= 0x0001,		// bit 0		// Bx Counter number of bytes to send. 0:2B 1:3B
		CfgCtr_1_ECb_mask			= 0x0006,		// bit 2:1		// Event Counter number of bytes to send. 00:1B 01:2B 10:3B 11:1B
		CfgCtr_1_ECb_shift			= 1,
		CfgCtr_1_TT_mask			= 0x0030,		// bit 5:4		// Time Tag format 00:EC+BC 01:EC 10:BC 11:TriggerCode
		CfgCtr_1_TT_shift			= 4,
		CfgCtr_1_SZD				= 0x0040,		// bit 6		// Suppress Zero Data
		CfgCtr_1_SZP				= 0x0080,		// bit 7		// Suppress Zero Packet
		CfgCtr_1_DT					= 0x0100,		// bit 8		// Enable Partial Zero Suppression (SPZS)
		CfgCtr_1_PAR_mask			= 0x3c00,		// bit 13:10	// In SPZS mode set the maximum number of partition to send
		CfgCtr_1_PAR_shift			= 10,		
		CfgCtr_1_P16				= 0x8000		// bit 15		// In SPZS mode send only the partition summary
	};

	// Global Control register 2
	enum CFG_CTR_2_BITS {
		CfgCtr_2_CAP_PRE_mask		= 0x0003,		// bit 1:0		// Preamplifier Capacity set
		CfgCtr_2_CAP_PRE_shift		= 0,
		CfgCtr_2_RES_PRE_mask		= 0x001c,		// bit 4:2		// Preamplifier Resistence set
		CfgCtr_2_RES_PRE_shift		= 2,
		CfgCtr_2_TP_FE_mask			= 0x00e0,		// bit 7:5		// Peaking time Frontend
		CfgCtr_2_TP_FE_shift		= 5
	};

	// Global Control register 3
	enum CFG_CTR_3_BITS {
		CfgCtr_3_SEL_COMP_MODE_mask	= 0x0003,		// bit 1:0
		CfgCtr_3_SEL_COMP_MODE_shift = 0,
		CfgCtr_3_FORCE_TH			= 0x0004,		// bit 2
		CfgCtr_3_FORCE_EN_ZCC		= 0x0008,		// bit 3
		CfgCtr_3_SEL_POL			= 0x0010,		// bit 4
		CfgCtr_3_EN_HYST			= 0x0020,		// bit 5
		CfgCtr_3_PT_mask			= 0x03c0,		// bit 9:6
		CfgCtr_3_PT_shift			= 6
	};

	// Global Control register 4
	enum CFG_CTR_4_BITS {
		CfgCtr_4_MON_SEL_mask		= 0x003f,		// bit 5:0		ADC input MUX selection
		CfgCtr_4_MON_SEL_shift		= 0,
		CfgCtr_4_MON_GAIN			= 0x0080,		// bit 7		Gain of ADC monitoring buffer
		CfgCtr_4_VREF_ADC_mask		= 0x0300,		// bit 9:8		Tuning of internal ADC reference
		CfgCtr_4_VREF_ADC_shift		= 8
	};


	// Global Control register 5
	enum CFG_CTR_5_BITS {
		CfgCtr_5_Iref_mask			= 0x003f,		// bit 5:0		// Tuning of global reference current
		CfgCtr_5_Iref_shift			= 0
	};

	// Global Threshold
	enum CFG_THR_BITS {
		CfgThr_ARM_DAC_mask			= 0x00ff,		// bit 7:0		// Arming global threshold
		CfgThr_ARM_DAC_shift		= 0,
		CfgThr_ZCC_DAC_mask			= 0xff00,		// bit 15:8		// ZCC global threshold
		CfgThr_ZCC_DAC_shift		= 8
	};

	// Hysteresis Register
	enum CFG_HYST_BITS {
		CfgHyst_HYST_DAC_mask		= 0x3f,			// bit 5:0	
		CfgHyst_HYST_DAC_shift		= 0
	};

	// Latency
	enum CFG_LAT_BITS {
		CfgLatency_LAT_mask			= 0xffff,		// bit 15:0	
		CfgLatency_LAT_shift		= 0
	};

	// Calibration
	enum CFG_CAL_0_BITS {
		CfgCal_0_MODE_mask			= 0x0003,		// bit 1:0		00:Normal 01:Voltage Pulse 1x:Current Pulse
		CfgCal_0_MODE_shift			= 0,
		CfgCal_0_DAC_mask			= 0x03fc,		// bit 9:2		Amplitude of voltage step or current pulse
		CfgCal_0_DAC_shift			= 2,
		CfgCal_0_EXT				= 0x0400,		// bit 10		Enable external analog voltage step
		CfgCal_0_PHI_mask			= 0x3800,		// bit 13:11	Pulse phase in 1/320 MHz delay steps
		CfgCal_0_PHI_shift			= 11,
		CfgCal_0_SEL_POL			= 0x4000		// bit 14		Pulse polarity
	};

	enum CFG_CAL_1_BITS {
		CfgCal_1_CAL_DUR_mask		= 0x01ff,		// bit 8:0		Pulse duration in 320 MHz clock cycle
		CfgCal_1_CAL_DUR_shift		= 0,
		CfgCal_1_CAL_FS_mask		= 0x0600,		// bit 10:9		Selection of Full Scale current pulse calibration
		CfgCal_1_CAL_FS_shift		= 9
	};

	// Bias Registers
	enum CFG_BIAS_0_BITS {
		CfgBias_0_CFD_DAC_1_mask	= 0x003f,		// bit 5:0
		CfgBias_0_CFD_DAC_1_shift	= 0,
		CfgBias_0_CFD_DAC_2_mask	= 0x0fc0,		// bit 11:6
		CfgBias_0_CFD_DAC_2_shift	= 6
	};

	enum CFG_BIAS_1_BITS {
		CfgBias_1_PRE_I_BIT_mask	= 0x00ff,		// bit 7:0
		CfgBias_1_PRE_I_BIT_shift	= 0,
		CfgBias_1_PRE_I_BSF_mask	= 0x3f00,		// bit 13:8
		CfgBias_1_PRE_I_BSF_shift	= 8
	};
		
	enum CFG_BIAS_2_BITS {
		CfgBias_2_PRE_VREF_mask		= 0x00ff,		// bit 7:0
		CfgBias_2_PRE_VREF_shift	= 0,
		CfgBias_2_PRE_I_BLCC_mask	= 0x3f00,		// bit 13:8
		CfgBias_2_PRE_I_BLCC_shift	= 8
	};


	enum CFG_BIAS_3_BITS {
		CfgBias_3_SH_I_BDIFF_mask	= 0x00ff,		// bit 7:0
		CfgBias_3_SH_I_BDIFF_shift	= 0,
		CfgBias_3_SH_I_BFCAS_mask	= 0xff00,		// bit 15:8
		CfgBias_3_SH_I_BFCAS_shift	= 8
	};
	
	enum CFG_BIAS_4_BITS {
		CfgBias_4_SD_I_BDIFF_mask	= 0x00ff,		// bit 7:0
		CfgBias_4_SD_I_BDIFF_shift	= 0
	};

	enum CFG_BIAS_5_BITS {
		CfgBias_5_SD_I_BFCAS_mask	= 0x00ff,		// bit 7:0
		CfgBias_5_SD_I_BFCAS_shift	= 0,
		CfgBias_5_SD_I_BSF_mask		= 0x3f00,		// bit 13:8
		CfgBias_5_SD_I_BSF_shift	= 8
	};

	enum CFG_BIAS_6_BITS {
		CfgBias_6_SLVS_VREF_mask	= 0x003f,		// bit 5:0
		CfgBias_6_SLVS_VREF_shift	= 0,
		CfgBias_6_SLVS_IBIAS_mask	= 0x0fc0,		// bit 5:0
		CfgBias_6_SLVS_IBIAS_shift	= 6
	};


	enum monitorChannel_e {
		Imon_Iref	            = 0,
		Imon_Calib_IDC          = 1,
		Imon_Preamp_InpTran     = 2,
		Imon_Preamp_LC          = 3,
		Imon_Preamp_SF          = 4,
		Imon_Shap_FC            = 5,
		Imon_Shap_Inpair        = 6,
		Imon_SD_Inpair          = 7,
		Imon_SD_FC              = 8,
		Imon_SD_SF              = 9,
		Imon_CFD_Bias1          = 10,
		Imon_CFD_Bias2          = 11,
		Imon_CFD_Hyst           = 12,
		Imon_CFD_Ireflocal      = 13,
		Imon_CFD_ThArm          = 14,
		Imon_CFD_ThZcc          = 15,
		Imon_SLVS_Ibias         = 16,
		Vmon_BGR                = 32,
		Vmon_Calib_Vstep        = 33,
		Vmon_Preamp_Vref        = 34,
		Vmon_Vth_Arm            = 35,
		Vmon_Vth_ZCC            = 36,
		V_Tsens_Int             = 37,
		V_Tsens_Ext             = 38,
		ADC_ref                 = 39,
		ADC_VinM                = 40,
		SLVS_Vref               = 41
	};

	enum PRG_BITS {
		Prg_BIT_mask				= 0x001f,		// bit 4:0
		Prg_BIT_shift				= 0,
		Prg_TIME_mask				= 0x3ff00,		// bit 18:8
		Prg_TIME_shift				= 8
	};


public:
	typedef struct ChannelConfig_s {
		int8_t		zcc_dac;
		int8_t		arm_dac;
		bool		mask;
		bool		cal;
	} ChannelConfig_t;

	typedef struct PreConfig_s {
		uint8_t 	TP;					// Peaking time Frontend
		uint8_t 	RES;				// Preamplifier Resistence set
		uint8_t 	CAP;				// Preamplifier Capacity set
	} PreConfig_t;

	typedef struct DiscriminatorConfig_s {
		uint8_t 	PT;				
		bool 		EN_HYST;		
		bool 		SEL_POL;		
		bool 		FORCE_EN_ZCC;	
		bool 		FORCE_TH;		
		uint8_t 	SEL_COMP_MODE;	
		int8_t		zcc_dac;
		int8_t		arm_dac;
		int8_t		hyst_dac;
	} DiscriminatorConfig_t;

	typedef struct MonitorConfig_s {
		uint8_t 	VREF_ADC;			// Tuning of internal ADC reference
		bool 		GAIN;				// Gain of ADC monitoring buffer
		uint8_t 	SEL;				// ADC input MUX selection
	} MonitorConfig_t;

	typedef struct ReadoutConfig_s {
		uint8_t 	MAX_PAR;			// In SPZS mode set the maximum number of partition to send (0-16)
		bool 		EnablePZS;			// Enable Partial Zero Suppression (SPZS)
		bool 		SZP;				// Suppress Zero Packet
		bool 		SZD;				// Suppress Zero Data
		uint8_t 	ECb;				// Event Counter number of bytes to send ( 0-3)
		uint8_t		BCb;				// Bx Counter number of bytes to send (0, 2, 3)
	} ReadoutConfig_t;

	typedef struct CalibrationConfig_s {
		bool 		POL;				// Pulse polarity
		uint8_t 	PHI;				// Pulse phase in 1/320 MHz delay steps
		bool 		EN_EXT;				// Enable external analog voltage step
		uint8_t 	DAC;				// Amplitude of voltage step or current pulse
		uint8_t 	MODE;				// 00:Normal 01:Voltage Pulse 1x:Current Pulse
		uint8_t 	FS;					// Selection of Full Scale current pulse calibration
		uint16_t 	DUR;				// Pulse duration in 320 MHz clock cycle
	} CalibrationConfig_t;

	typedef struct BiasConfig_s {
		uint8_t 	Iref;				// Tuning of global reference current	
		uint8_t 	CFD_DAC_1;			
		uint8_t 	CFD_DAC_2;			
		uint8_t 	PRE_I_BSF;		
		uint8_t 	PRE_I_BIT;			
		uint8_t 	PRE_I_BLCC;			
		uint8_t 	PRE_VREF;			
		uint8_t 	SH_I_BFCAS;			
		uint8_t 	SH_I_BDIFF;			
		uint8_t 	SD_I_BDIFF;			
		uint8_t 	SD_I_BSF;			
		uint8_t 	SD_I_BFCAS;			
		uint8_t 	SLVS_IBIAS;		
		uint8_t 	SLVS_VREF;		
	} BiasConfig_t;

	typedef struct TriggerConfig_s {
		uint16_t	latency;
		uint8_t		PS;					// Pulse Stretcher control. From 1 to 8 clock cycles
		bool 		syncLevelEnable;	// Enable Level Mode 
		bool 		ST;					// Self Trigger
		bool 		DDR;				// DDR mode
	} TriggerConfig_t;

	typedef struct ADCConfig_s {
		float		OFFSET;				// input offset
		float		LSB;				// LSB value
	} ADCConfig_t;

	// Chip configuration
	typedef struct config_s	{
		ChannelConfig_t 		CH[CHIP_CFG_CHANNELS];
		PreConfig_t				PRE;
		DiscriminatorConfig_t	DISC;
		BiasConfig_t			BIAS;
		CalibrationConfig_t		CAL;
		MonitorConfig_t			MON;
		TriggerConfig_t			TRG;
		ReadoutConfig_t			RO;
		bool					run;
		ADCConfig_t				ADC;
	} VFAT3config_t;

	// Acquisition statistics
	typedef struct statistics_s {
		unsigned long ch[CHIP_NUM_CHANNELS];
		unsigned long numHit;
	} statistics_t;


private:
	int 				chipID;
	VFAT3rcv 			*receiver;
	VFAT3dataParser 	*parser;	
	IPbusSC				*slowControl;

private:
	void chkRange(std::string valName, int val, int min, int max);

public:
	int getChipID() 						{return chipID;}
//	VFAT3dataParser* getDataParser() 		{return parser;}

	void addSetChannelConfig(int ch, ChannelConfig_t &c);
	void addSetTriggerConfig(TriggerConfig_t &c);
	void addSetReadoutConfig(ReadoutConfig_t &c);
	void addSetPreConfig(PreConfig_t &c);
	void addSetDiscriminatorConfig(DiscriminatorConfig_t &c);
	void addSetMonitorConfig(MonitorConfig_t &c);
	void addSetCalibrationConfig(CalibrationConfig_t &c);
	void addSetBiasConfig(BiasConfig_t &c);
	void addSetRun(bool r);
	void setConfig(VFAT3config_t &cfg);
	void getConfig(VFAT3config_t &cfg);
	void eFuseBit(int bit, int prgTime);
	void execute() {slowControl->execute();}
};

#endif // VFAT3chip_H
