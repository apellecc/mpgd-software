/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#include "TestBench.h"
//#include "TBconfig.h"
//#include "VFAT3board.h"
//#include "VFAT3chip.h"
#include "PTdac.h"
#include "VFAT3_data.h"
#include <iostream>
#include <unistd.h>
#include "Tutils.h"
#include <cmath>

using std::string;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;

// Analog power supply settings
static const float AVDD_1V2_imin = 7e-3;		// minimum current for final check after power on and configuration
static const float AVDD_1V2_imax = 200e-3;		// maximum current for final check after power on and configuration

// Digital power supply settings
static const float DVDD_1V2_imin = 90e-3;		// minimum current for final check after power on and configuration
static const float DVDD_1V2_imax = 120e-3;		// maximum current for final check after power on and configuration

PTdac::DAC_t DACv[] = {
	// dac name, reg, mask, shift, dim dac, monsel, optimal value, gain min, gain max, offset min, offset max, fit min, fit max
	{ string("Imon_Calib_IDC"), VFAT3chip::RegCfgCal_0, VFAT3chip::CfgCal_0_DAC_mask, VFAT3chip::CfgCal_0_DAC_shift, 1, 255, 0, 25e-9*0.8, 40e-9, -100e-9, 200e-9, 0 , 150 },// da verificare gli estremi

	{ string("Imon_Preamp_InpTran"), VFAT3chip::RegCfgBias_1, VFAT3chip::CfgBias_1_PRE_I_BIT_mask, VFAT3chip::CfgBias_1_PRE_I_BIT_shift, 2, 255, 30e-6, 130e-9, 160e-9, 80e-9, 350e-9, 0, 150  },
	{ string("Imon_Preamp_LC"), VFAT3chip::RegCfgBias_2, VFAT3chip::CfgBias_2_PRE_I_BLCC_mask, VFAT3chip::CfgBias_2_PRE_I_BLCC_shift, 3, 63, 2.5e-6, 80e-9, 200e-9, -80e-9, 150e-9, 0, 40  },
	{ string("Imon_Preamp_SF"), VFAT3chip::RegCfgBias_1, VFAT3chip::CfgBias_1_PRE_I_BSF_mask, VFAT3chip::CfgBias_1_PRE_I_BSF_shift, 4, 63, 6.5e-6, 450e-9, 650e-9, -300e-9, 350e-9, 0, 40 },

	{ string("Imon_Shap_FC"), VFAT3chip::RegCfgBias_3, VFAT3chip::CfgBias_3_SH_I_BFCAS_mask, VFAT3chip::CfgBias_3_SH_I_BFCAS_shift, 5, 255, 26e-6, 150e-9, 280e-9, -500e-9, 700e-9, 0, 150 },
	{ string("Imon_Shap_Inpair"), VFAT3chip::RegCfgBias_3, VFAT3chip::CfgBias_3_SH_I_BDIFF_mask, VFAT3chip::CfgBias_3_SH_I_BDIFF_shift, 6, 255, 16e-6, 150e-9, 300e-9, -400e-9, 800e-9, 0, 150 },

	{ string("Imon_SD_Inpair"), VFAT3chip::RegCfgBias_4, VFAT3chip::CfgBias_4_SD_I_BDIFF_mask, VFAT3chip::CfgBias_4_SD_I_BDIFF_shift, 7, 255, 28e-6, 160e-9, 280e-9, -300e-9, 600e-9, 0, 150 },
	{ string("Imon_SD_FC"), VFAT3chip::RegCfgBias_5, VFAT3chip::CfgBias_5_SD_I_BFCAS_mask, VFAT3chip::CfgBias_5_SD_I_BFCAS_shift, 8, 255, 27e-6, 160e-9, 260e-9, -500e-9, 650e-9, 0, 150 },
	{ string("Imon_SD_SF"), VFAT3chip::RegCfgBias_5, VFAT3chip::CfgBias_5_SD_I_BSF_mask, VFAT3chip::CfgBias_5_SD_I_BSF_shift, 9, 63, 7.5e-6, 450e-9, 600e-9, -100e-9, 500e-9, 0, 40},

	{ string("Imon_CFD_Bias1"), VFAT3chip::RegCfgBias_0, VFAT3chip::CfgBias_0_CFD_DAC_1_mask, VFAT3chip::CfgBias_0_CFD_DAC_1_shift, 10, 63, 20e-6, 400e-9, 700e-9, -250e-9, 400e-9, 0, 40 },
	{ string("Imon_CFD_Bias2"), VFAT3chip::RegCfgBias_0, VFAT3chip::CfgBias_0_CFD_DAC_2_mask, VFAT3chip::CfgBias_0_CFD_DAC_2_shift, 11, 63, 20e-6, 400e-9, 700e-9, -250e-9, 400e-9, 0, 40 },//
	{ string("Imon_CFD_Hyst"), VFAT3chip::RegCfgHyst, VFAT3chip::CfgHyst_HYST_DAC_mask, VFAT3chip::CfgHyst_HYST_DAC_shift, 12, 63, 0.5e-6, 80e-9, 180e-9, -130e-9, 130e-9, 0, 40 },

	{ string("Imon_CFD_ThArm"), VFAT3chip::RegCfgThr, VFAT3chip::CfgThr_ARM_DAC_mask, VFAT3chip::CfgThr_ARM_DAC_shift, 14, 255, 3.2e-6, 90e-9, 130e-9, -250e-9, 350e-9, 0, 150 },
	{ string("Imon_CFD_ThZcc"), VFAT3chip::RegCfgThr, VFAT3chip::CfgThr_ZCC_DAC_mask, VFAT3chip::CfgThr_ZCC_DAC_shift, 15, 255, 1.1e-6, 80e-9, 130e-9, -200e-9, 500e-9, 0, 150 },

	{ string("Vmon_Calib_Vstep"), VFAT3chip::RegCfgCal_0, VFAT3chip::CfgCal_0_DAC_mask, VFAT3chip::CfgCal_0_DAC_shift, 33, 255, 0, -3.3e-3, -1.8e-3, 0.75, 1.1, 0, 150 },
	{ string("Vmon_Preamp_Vref"), VFAT3chip::RegCfgBias_2, VFAT3chip::CfgBias_2_PRE_VREF_mask, VFAT3chip::CfgBias_2_PRE_VREF_shift, 34, 255, 430e-3, 4e-3, 6e-3, -7e-3, 5e-3, 0, 150 },
	{ string("Vmon_Vth_Arm"), VFAT3chip::RegCfgThr, VFAT3chip::CfgThr_ARM_DAC_mask, VFAT3chip::CfgThr_ARM_DAC_shift, 35, 255, 100e-3, 1.8e-3, 3e-3, -7e-3, 7e-3, 0, 150 },
	{ string("Vmon_Vth_ZCC"), VFAT3chip::RegCfgThr, VFAT3chip::CfgThr_ZCC_DAC_mask, VFAT3chip::CfgThr_ZCC_DAC_shift, 36, 255, 30e-3, 1.8e-3, 3e-3, -7e-3, 8e-3, 0, 150 }
};


PTdac::PTdac(TestBench &t) : GenericTest(t)
{
}

bool PTdac::dac_check(VFAT3_data::DAC_s *DAC_s, DAC_t dac) 
{
	bool fit_status;

	fit_status = Tutils::bestFitLinDAC(dac.fit_min, dac.fit_max, DAC_s->ScanData, &DAC_s->gain, &DAC_s->offset);

	if (fit_status)
	{
		if ((DAC_s->gain>=dac.gain_min) && (DAC_s->gain<=dac.gain_max) && (DAC_s->offset>=dac.offset_min) && (DAC_s->offset<=dac.offset_max))
		{
			cout<<dac.name<<" test passed!"<<endl;
			//cout<<dac.name<<" gain = "<<DAC_s.gain<<endl;
			//cout<<dac.name<<" offset = "<<DAC_s.offset<<endl;
			cout<<dac.name<<" DAC optimal value = "<<DAC_s->optVal_ndac<<endl;
			return true;
		} 
		else
		{
			cout<<dac.name<<" test not passed: parameters out of acceptance range!"<<endl;
			cout<<dac.name<<" gain = "<<DAC_s->gain<<endl;
			cout<<dac.name<<" gain should be included between: "<< dac.gain_min << " and " << dac.gain_max <<endl;
			cout<<dac.name<<" offset = "<<DAC_s->offset<<endl;
			cout<<dac.name<<" offset should be included between: "<< dac.offset_min << " and " << dac.offset_max <<endl;
			return false;
		}
	}
	else 
	{
		cout<<dac.name<<"DAC linear fitting error"<<endl;
		return false;
	}


}

bool PTdac::run(string param)
{
	VFAT3chip::MonitorConfig_t monCfg = tb.chipConfig.MON;
	VFAT3_data::DAC_s DAC_s;

	VFAT3board::powerADC_t res;

	float d_conv = 0;
	float delta = 0;
	float delta_min = 10;
	int i_dac = 0;
	int mon_cnt = 0; //counter for monotonicity consecutive errors on the same conf. byte, the usleep func is too slow

	tb.chipConfig.CAL.MODE = 1;
	tb.chipConfig.CAL.POL = 0;
	tb.setChipConfig();

	for (const auto& dac: DACv)
	{

		delta_min = 10; // reset delta value for optimal value search
		mon_cnt = 0;
		DAC_s.name = dac.name;
		DAC_s.maxValue = dac.maxValue;
		DAC_s.monSel = dac.monSel;
		DAC_s.optVal_ndac = 0;

		// set monitor mux
		monCfg.SEL = dac.monSel;
		tb.chip->addSetMonitorConfig(monCfg);
		tb.chip->addRMWreg(dac.reg, ~dac.mask, 0);
		tb.chip->execute();
		usleep(10000);

//uint32_t reg_val;
//int valore;
		for (uint32_t v=0; v<=dac.maxValue; v++)
		{
			// set DAC
			if (!mon_cnt)
			{
				tb.chip->addRMWreg(dac.reg, ~dac.mask, v << dac.shift);
				tb.chip->execute();			
			}
			usleep(100);

			tb.board->readmon(VFAT3board::V_VMON, &d_conv);
			// Read External ADC


/*if (mon_cnt)
{
			for (int aaa = 0; aaa <200; aaa++){

//				tb.chip->addReadReg(dac.reg,&reg_val);
//				tb.chip->execute();	
//				valore = (reg_val & dac.mask) >> dac.shift;
				tb.board->readmon(VFAT3board::V_VMON, &d_conv);
//				cout << valore << " " << d_conv << endl;
cout << d_conv << endl;
				usleep(100);
			}
cout << endl;
getchar();
}*/

			//d_conv = tb.getVmon();
			// conversion
			if (dac.monSel < 17) 
			{
				d_conv = d_conv/20000 - tb.ptdata->VFAT3.iref_d.optVal; // current in A
				DAC_s.ScanData[v] = d_conv; // for current monitoring there is the iref shift
			}
			else
			{
				DAC_s.ScanData[v] = d_conv; // voltage in V
			}

			//saving data
			tb.ptdata->VFAT3.dac_d[i_dac].ScanData[v] = DAC_s.ScanData[v];
			//cout << "DAC: " << v << " lettura: " << DAC_s.ScanData[v] << endl;
			// Monotonicity test
			if (v)
			{
				//cout << DAC_s.ScanData[v] << endl;
				if (dac.monSel == 33)
				{
					if (DAC_s.ScanData[v]>=DAC_s.ScanData[v-1])
					{	
						if 	(mon_cnt ==3)
						{
							cout<<dac.name<<" monotonicity test failed!"<<endl;
							mon_cnt = 0;
							//throw TestError("DAC monotonicity test failed!");
						}
						else
						{
							mon_cnt++;
							v--;
						}
					}
					else
					{
						mon_cnt = 0;
					}
				}
				else 
				{
					if (DAC_s.ScanData[v]<=DAC_s.ScanData[v-1])
					{
						if (mon_cnt == 3)
						{		
							cout<<dac.name<<" monotonicity test failed!"<<endl;
							mon_cnt = 0;
							//throw TestError("DAC monotonicity test failed!");
						}
						else
						{
							mon_cnt++;
							v--;
						}
					}
					else
					{
						mon_cnt = 0 ;
					}
				}
				
			}

		//optimal value 
			if (dac.optVal)
			{
				delta = abs( d_conv - (dac.optVal) );
				if (delta_min >= delta)
				{
					delta_min = delta;
					DAC_s.optVal_ndac = v;
					DAC_s.optVal = d_conv;
				}
			}
//			cout << to_string(v) << " " << to_string(adc0) << " " << to_string(adc1) << " " << to_string(adcFMC) << endl;
		}

		if (dac_check(&DAC_s,dac))
		{
			//saving data
			tb.ptdata->VFAT3.dac_d[i_dac].name = DAC_s.name;
			tb.ptdata->VFAT3.dac_d[i_dac].monSel = DAC_s.monSel;
			tb.ptdata->VFAT3.dac_d[i_dac].maxValue = DAC_s.maxValue;
			tb.ptdata->VFAT3.dac_d[i_dac].gain = DAC_s.gain;
			tb.ptdata->VFAT3.dac_d[i_dac].offset = DAC_s.offset;
			tb.ptdata->VFAT3.dac_d[i_dac].optVal_ndac = DAC_s.optVal_ndac;
			tb.ptdata->VFAT3.dac_d[i_dac].optVal = DAC_s.optVal;
		
			switch (i_dac)
			{
				case 0: tb.chipConfig.CAL.DAC = 0; break;
				case 1: tb.chipConfig.BIAS.PRE_I_BIT = DAC_s.optVal_ndac; break;
				case 2: tb.chipConfig.BIAS.PRE_I_BLCC = DAC_s.optVal_ndac; break;
				case 3: tb.chipConfig.BIAS.PRE_I_BSF = DAC_s.optVal_ndac; break;

				case 4: tb.chipConfig.BIAS.SH_I_BFCAS = DAC_s.optVal_ndac; break;
				case 5: tb.chipConfig.BIAS.SH_I_BDIFF = DAC_s.optVal_ndac; break;

				case 6: tb.chipConfig.BIAS.SD_I_BDIFF = DAC_s.optVal_ndac; break;
				case 7: tb.chipConfig.BIAS.SD_I_BFCAS = DAC_s.optVal_ndac; break;
				case 8: tb.chipConfig.BIAS.SD_I_BSF = DAC_s.optVal_ndac; break;

				case 9: tb.chipConfig.BIAS.CFD_DAC_1 = DAC_s.optVal_ndac; break;
				case 10: tb.chipConfig.BIAS.CFD_DAC_2 = DAC_s.optVal_ndac; break;
				case 11: tb.chipConfig.DISC.hyst_dac = 5; break;
				case 12: tb.chipConfig.DISC.arm_dac = DAC_s.optVal_ndac; break;
				case 13: tb.chipConfig.DISC.zcc_dac = DAC_s.optVal_ndac; break;

				case 14: tb.chipConfig.CAL.DAC = 0; break;
				case 15: tb.chipConfig.BIAS.PRE_VREF = DAC_s.optVal_ndac; break;
				case 16: tb.chipConfig.DISC.arm_dac = DAC_s.optVal_ndac; break;
				case 17: tb.chipConfig.DISC.zcc_dac = DAC_s.optVal_ndac; break;

				default: throw TestError("Error in DAC optimal value setting!"); break;
			}
			
			i_dac++;

			//DAC optimal value reg set
			//tb.chip->addRMWreg(dac.reg, ~dac.mask, DAC_s.optVal_ndac << dac.shift);
			tb.setChipConfig();

			//saving data for calibration
			if (DAC_s.monSel == 1) //current injection
			{
				tb.ptdata->VFAT3.calib_d.ipul_gain = DAC_s.gain;
				tb.ptdata->VFAT3.calib_d.ipul_offset = DAC_s.offset;
				tb.ptdata->VFAT3.dac_d[i_dac].optVal_ndac = 0;
				tb.ptdata->VFAT3.dac_d[i_dac].optVal = 0;
			}
			
			if (DAC_s.monSel == 33) //voltage step
			{
				tb.ptdata->VFAT3.calib_d.vpul_gain = DAC_s.gain;
				tb.ptdata->VFAT3.calib_d.vpul_offset = DAC_s.offset;
				tb.ptdata->VFAT3.dac_d[i_dac].optVal_ndac = 0;
				tb.ptdata->VFAT3.dac_d[i_dac].optVal = 0;
			}
			

		}
		else
		{
			throw TestError("DAC test error");
		}

		// measuring the base line voltage for the voltage step injection
		d_conv = 0;
		if (dac.monSel == 33)
		{
			tb.chipConfig.MON.SEL = dac.monSel;
			tb.chipConfig.CAL.POL= 1; //change polarity
			tb.setChipConfig();

			tb.ptdata->VFAT3.calib_d.cal_bsl = 0;

			//mean value of 10 samples
			for (int im = 0; im<10; im++) 
			{
				tb.board->readmon(VFAT3board::V_VMON, &d_conv);
				tb.ptdata->VFAT3.calib_d.cal_bsl = d_conv + tb.ptdata->VFAT3.calib_d.cal_bsl;
			}
				tb.ptdata->VFAT3.calib_d.cal_bsl = tb.ptdata->VFAT3.calib_d.cal_bsl/10;
		}	

	}

	tb.board->getPowerADC(&res);

	cout << endl <<  "Analogue power supply: " << res.V_V12A << " V" << endl;
	cout << "Analogue power supply current absorption: " << res.I_V12A*1000 << " mA" << endl << endl;
	cout << "Digital power supply: " << res.V_V12D << " V" << endl;
	cout << "Digital power supply current absorption: " << res.I_V12D*1000 << " mA" << endl	<< endl;
	cout << "EFUSE power supply: " << res.V_EFUSE << " V" << endl << endl;

	if (res.I_V12A < AVDD_1V2_imin || res.I_V12A > AVDD_1V2_imax)
	{
		cout << "Analog current absorption out of limits: " << res.I_V12A << " A"<< endl;
		cout << "Analog current lower limit: " << AVDD_1V2_imin << " A" << endl;
		cout << "Analog current upper limit: " << AVDD_1V2_imax << " A" << endl;
		throw TestError("Analog current absorption out of limits");
	}

	if (res.I_V12D < DVDD_1V2_imin || res.I_V12D > DVDD_1V2_imax)
	{
		cout << "Digital current absorption out of limits: " << res.I_V12D << " A"<< endl;
		cout << "Digital current lower limit: " << DVDD_1V2_imin << " A" << endl;
		cout << "Digital current upper limit: " << DVDD_1V2_imax << " A" << endl;
		throw TestError("Digital current absorption out of limits");
	}


	tb.ptdata->VFAT3.pwr_d.AVDD = res.V_V12A;
	tb.ptdata->VFAT3.pwr_d.IAVDD = res.I_V12A;
	tb.ptdata->VFAT3.pwr_d.DVDD = res.V_V12D;
	tb.ptdata->VFAT3.pwr_d.IDVDD = res.I_V12D;
	tb.ptdata->VFAT3.pwr_d.VDD_EFUSE = res.V_EFUSE;	


	cout << "Power supply accomplished" << endl;
	return true;
}


