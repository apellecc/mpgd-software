/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <mexception.h>
#include "VFAT3rcv.h"


VFAT3rcv::VFAT3rcv() 
{
}


VFAT3rcv::VFAT3rcv(WishboneBus *wbbPtr, uint32_t baseAdd) : 
			ComPortRx(wbbPtr, baseAdd)
{
}

VFAT3rcv::~VFAT3rcv()
{
}

//
// set register
//
void VFAT3rcv::addSetReg(uint16_t address, uint32_t val)
{
	if (!wbb)
		throw MIPBusError("No IPBus configured");

	wbb->addWrite(baseAddress+address, val);
}

//
// Read register
//
void VFAT3rcv::addGetReg(uint16_t address, uint32_t *val)
{
	if (!wbb)
		throw MIPBusError("No IPBus configured");

	wbb->addRead(baseAddress+address, val);
}

void VFAT3rcv::addGetNReg(uint16_t address, uint32_t *data, int size)
{
	if (!wbb)
		throw MIPBusError("No IPBus configured");

	wbb->addRead(size, baseAddress+address, data);
}

//
// Disable (or enable) the receiver 
//
void VFAT3rcv::addEnable(bool d)
{
	if (!wbb)
		throw MIPBusError("No IPBus configured");

	wbb->addRMWbits(baseAddress+regOpMode, ~OPMODE_RCVENABLE, d ? OPMODE_RCVENABLE : 0);
}

void VFAT3rcv::addInvertInput(bool d)
{
	if (!wbb)
		throw MIPBusError("No IPBus configured");

	wbb->addRMWbits(baseAddress+regOpMode, ~OPMODE_INVERT_POLARITY, d ? OPMODE_INVERT_POLARITY : 0);
}

void VFAT3rcv::addSetDataFormat(dataformat_t &f)
{
	uint32_t tmp;
		
	tmp  = f.enablePZS ? enablePZS : 0;
	tmp |= (f.PZSmaxPartitions << PZSmaxPartitions_shift) & PZSmaxPartitions_mask;
	tmp |= (f.eventCountSize << eventCountSize_shift) & eventCountSize_mask;
	tmp |= (f.bunchCrossSize << bunchCrossSize_shift) & bunchCrossSize_mask;

	addSetReg(regDataFormat, tmp);
}


//
//	HDLC receiver
//
void VFAT3rcv::addGetRxStatus(uint32_t *d)
{
	addGetReg(regHDLCstat, d);
}

void VFAT3rcv::addGetSCpkt(int size, uint32_t *rdData)
{
	addGetNReg(HDLCrxBuffer, rdData, size);
	addSetReg(regHDLCstat, HDLCready);
}


