/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#include <string>
#include <string.h>
#include <vector>
#include <iostream>
#include <fstream>
#include "Parsing.h"
#include "TBconfig.h"
#include <unistd.h>

using std::string;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;

TBconfig::TBconfig()
{
	init();
}

void TBconfig::init()
{
	// Erase all varibles before load
	setDefaults();
	channelNum = 0;
}

void TBconfig::setChannelDefaults(VFAT3chip::ChannelConfig_t &ch)
{
	ch.zcc_dac = 0;
	ch.arm_dac = 0;
	ch.mask = false;
	ch.cal = false;
}

void TBconfig::setDefaults()
{
	// TMP variable
	setChannelDefaults(channel);

	mosaicAddress = "192.168.168.250";
	scopeAddress = "192.168.168.254";

	for (int i=0; i<33; i++)
		VFAT3config.CH[i] = channel;

	VFAT3config.PRE.TP = 25;
	VFAT3config.PRE.RES = 0;
	VFAT3config.PRE.CAP = 0;

	VFAT3config.DISC.PT = 25;
	VFAT3config.DISC.EN_HYST = 1;
	VFAT3config.DISC.SEL_POL = 0;
	VFAT3config.DISC.FORCE_EN_ZCC = 0;
	VFAT3config.DISC.FORCE_TH = 0;
	VFAT3config.DISC.SEL_COMP_MODE = 0;
	VFAT3config.DISC.zcc_dac = 0x0b;
	VFAT3config.DISC.arm_dac = 0x20;
	VFAT3config.DISC.hyst_dac = 0x05;

	VFAT3config.BIAS.Iref = 0x20;
	VFAT3config.BIAS.CFD_DAC_1 = 0x28;
	VFAT3config.BIAS.CFD_DAC_2 = 0x28;
	VFAT3config.BIAS.PRE_I_BSF = 1;
	VFAT3config.BIAS.PRE_I_BIT = 1;
	VFAT3config.BIAS.PRE_I_BLCC = 15;
	VFAT3config.BIAS.PRE_VREF = 86;
	VFAT3config.BIAS.SH_I_BFCAS = 1;
	VFAT3config.BIAS.SH_I_BDIFF = 1;
	VFAT3config.BIAS.SD_I_BDIFF = 1;
	VFAT3config.BIAS.SD_I_BSF = 1;
	VFAT3config.BIAS.SD_I_BFCAS = 1;
	VFAT3config.BIAS.SLVS_IBIAS = 40;
	VFAT3config.BIAS.SLVS_VREF = 40;

	VFAT3config.CAL.POL = 0;
	VFAT3config.CAL.PHI = 0;
	VFAT3config.CAL.EN_EXT = 0;
	VFAT3config.CAL.DAC = 0;
	VFAT3config.CAL.MODE = 0;
	VFAT3config.CAL.FS = 0;
	VFAT3config.CAL.DUR = 0x1ff;

	VFAT3config.MON.VREF_ADC = 1;
	VFAT3config.MON.GAIN = 0;
	VFAT3config.MON.SEL = 0;

	VFAT3config.TRG.latency = 0;
	VFAT3config.TRG.PS = 1;
	VFAT3config.TRG.syncLevelEnable = 0;
	VFAT3config.TRG.ST = 0;
	VFAT3config.TRG.DDR = 0;

	VFAT3config.RO.MAX_PAR = 1;
	VFAT3config.RO.EnablePZS = 0;
	VFAT3config.RO.SZP = 0;
	VFAT3config.RO.SZD = 0;
	VFAT3config.RO.ECb = 1;
	VFAT3config.RO.BCb = 2;

	VFAT3config.run = false;
}

void TBconfig::SetChannel(void *userContext)
{
	TBconfig *p = (TBconfig *) userContext;
	int i = p->channelNum;

	p->VFAT3config.CH[i] = p->channel;
	p->setChannelDefaults(p->channel);
}



bool TBconfig::readFile(std::string filename)
{
	ParserOstream os(cout);

// GRAMMAR
	Token::SetDefaultLeader(" \t\n");

	// Common tokens and rules
	Token	L_BRACE		 	("{");
	Token	R_BRACE		 	("}");
	Token	COLON			(":");
	Token	SEMICOLON		(";");
	Token	QUOTE			("\"");
	Token	STAR			("\\*");
	Token	EQUAL			("=");
	Token	PLUS			("\\+");
	Token	APOSTROPHE		("'");
	Token	EXCLAMATION		("!"); 
	Token	STRING_TO_QUOTE ("[^\"]+");
	Token	STRING			("\"[^\"]+\"|[^ \t\n';=]+");
	Token	INTEGER			("[-]?[0-9]+");
//	Token	FLOAT			("[-]?[0-9]+[.]?[0-9]+[e|E]?[-]?[0-9]+");
	Token	FLOAT			("[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?");
	Token	BOOL			("TRUE|FALSE|true|false|0|1");

	Token	DOUBLE_SLASH	("//");
	Token	HASH			("#");
	Token	COMMENT_STRING	("*", "[^\n]+");
	Rule	Comment			("Comment");
	Comment = HASH + Opt(COMMENT_STRING);
	Rule	Comments		("Comments");
	Comments = Comment + Opt(Comments);

	Token	SCOPE			("SCOPE");
	Token	MOSAIC			("MOSAIC");
	Token	ADDRESS			("ADDRESS");
	

	// Scope
	Rule	ScopeAddressCfg ("ScopeAddressCfg");
	ScopeAddressCfg = ADDRESS + EQUAL + STRING + SEMICOLON + (scopeAddress << S3);
	Rule	ScopeCfgs 		("ScopeCfgs");
	ScopeCfgs = Opt(Comments) + Opt(ScopeAddressCfg) + Opt(Comments);
	Rule	ScopeBlock 		("ScopeBlock");
	ScopeBlock = Opt(Comments) + SCOPE + L_BRACE + ScopeCfgs + R_BRACE;
	
	// Mosaic
	Rule	MosaicAddressCfg("MosaicAddressCfg");
	MosaicAddressCfg = ADDRESS + EQUAL + STRING + SEMICOLON + (mosaicAddress << S3);
	Rule	MosaicCfgs 		("MosaicCfgs");
	MosaicCfgs = Opt(Comments) + Opt(MosaicAddressCfg) + Opt(Comments);
	Rule	MosaicBlock 	("MosaicBlock");
	MosaicBlock = Opt(Comments) +MOSAIC + L_BRACE + MosaicCfgs + R_BRACE;


	// VFAT3 Channel
	Token	ZCC_DAC			("zcc_dac");
	Token	ARM_DAC			("arm_dac");
	Token	MASK			("mask");
	Token	CAL				("cal");

	Rule	ChOpt_zcc_dac		("ChOpt_zcc_dac");
	ChOpt_zcc_dac = ZCC_DAC + EQUAL + INTEGER + SEMICOLON + (channel.zcc_dac << S3);
	Rule	ChOpt_arm_dac		("ChOpt_arm_dac");
	ChOpt_arm_dac = ARM_DAC + EQUAL + INTEGER + SEMICOLON + (channel.arm_dac << S3);
	Rule	ChOpt_mask		("ChOpt_mask");
	ChOpt_mask = MASK + EQUAL + BOOL + SEMICOLON + (channel.mask << S3);
	Rule	ChOpt_cal		("ChOpt_cal");
	ChOpt_cal = CAL + EQUAL + BOOL + SEMICOLON + (channel.cal << S3);

	Rule	VFAT3CfgChannelOpt ("VFAT3CfgChannelOpt");
	VFAT3CfgChannelOpt = Comment | 
						 ChOpt_zcc_dac |
						 ChOpt_arm_dac |
						 ChOpt_mask |
						 ChOpt_cal;

	Rule	VFAT3CfgChannelOpts ("VFAT3CfgChannelOpts");
	VFAT3CfgChannelOpts = VFAT3CfgChannelOpt + Opt(VFAT3CfgChannelOpts);

	Token	CHANNEL			("CHANNEL");
	Rule	VFAT3CfgChannel ("VFAT3CfgChannel");
	ParserExec	 			execSetChannel(SetChannel, this);
	VFAT3CfgChannel = CHANNEL + INTEGER + L_BRACE + VFAT3CfgChannelOpts + R_BRACE + (channelNum << S2) + execSetChannel;

	// VFAT3 Preamplifier
	Token	TP				("TP");
	Token	RES				("RES");
	Token	CAP				("CAP");

	Rule	PREOpt_TP	("PREOpt_TP");
	PREOpt_TP = TP + EQUAL + INTEGER + SEMICOLON + (VFAT3config.PRE.TP << S3);
	Rule	PREOpt_RES	("PREOpt_RES");
	PREOpt_RES = RES + EQUAL + INTEGER + SEMICOLON + (VFAT3config.PRE.RES << S3);
	Rule	PREOpt_CAP	("PREOpt_CAP");
	PREOpt_CAP = CAP + EQUAL + INTEGER + SEMICOLON + (VFAT3config.PRE.CAP << S3);
	Rule	VFAT3CfgPREOpt ("VFAT3CfgPREOpt");
	VFAT3CfgPREOpt = Comment | 
					PREOpt_TP |
					PREOpt_RES |
					PREOpt_CAP;

	Rule	VFAT3CfgPREOpts ("VFAT3CfgPREOpts");
	VFAT3CfgPREOpts = VFAT3CfgPREOpt + Opt(VFAT3CfgPREOpts);

	Token	PRE			("PRE");
	Rule	VFAT3CfgPRE ("VFAT3CfgPRE");
	VFAT3CfgPRE = PRE + L_BRACE + VFAT3CfgPREOpts + R_BRACE;

	// VFAT3 Discriminator
	Token	PT				("PT");
	Token	EN_HYST			("EN_HYST");
	Token	SEL_POL			("SEL_POL");
	Token	FORCE_EN_ZCC	("FORCE_EN_ZCC");
	Token	FORCE_TH		("FORCE_TH");
	Token	SEL_COMP_MODE	("SEL_COMP_MODE");
	Token	HYST_DAC		("hyst_dac");
	
	Rule	DISCOpt_PT	("DISCOpt_PT");
	DISCOpt_PT = PT + EQUAL + INTEGER + SEMICOLON + (VFAT3config.DISC.PT << S3);
	Rule	DISCOpt_EN_HYST	("DISCOpt_EN_HYST");
	DISCOpt_EN_HYST = EN_HYST + EQUAL + BOOL + SEMICOLON + (VFAT3config.DISC.EN_HYST << S3);
	Rule	DISCOpt_SEL_POL	("DISCOpt_SEL_POL");
	DISCOpt_SEL_POL = SEL_POL + EQUAL + BOOL + SEMICOLON + (VFAT3config.DISC.SEL_POL << S3);
	Rule	DISCOpt_FORCE_EN_ZCC	("DISCOpt_FORCE_EN_ZCC");
	DISCOpt_FORCE_EN_ZCC = FORCE_EN_ZCC + EQUAL + BOOL + SEMICOLON + (VFAT3config.DISC.FORCE_EN_ZCC << S3);
	Rule	DISCOpt_FORCE_TH	("DISCOpt_FORCE_TH");
	DISCOpt_FORCE_TH = FORCE_TH + EQUAL + BOOL + SEMICOLON + (VFAT3config.DISC.FORCE_TH << S3);
	Rule	DISCOpt_SEL_COMP_MODE	("DISCOpt_SEL_COMP_MODE");
	DISCOpt_SEL_COMP_MODE = SEL_COMP_MODE + EQUAL + INTEGER + SEMICOLON + (VFAT3config.DISC.SEL_COMP_MODE << S3);
	Rule	DISCOpt_ZCC_DAC	("DISCOpt_ZCC_DAC");
	DISCOpt_ZCC_DAC = ZCC_DAC + EQUAL + INTEGER + SEMICOLON + (VFAT3config.DISC.zcc_dac << S3);
	Rule	DISCOpt_ARM_DAC	("DISCOpt_ARM_DAC");
	DISCOpt_ARM_DAC = ARM_DAC + EQUAL + INTEGER + SEMICOLON + (VFAT3config.DISC.arm_dac << S3);
	Rule	DISCOpt_HYST_DAC	("DISCOpt_HYST_DAC");
	DISCOpt_HYST_DAC = HYST_DAC + EQUAL + INTEGER + SEMICOLON + (VFAT3config.DISC.hyst_dac << S3);

	Rule	VFAT3CfgDISCOpt ("VFAT3CfgDISCOpt");
	VFAT3CfgDISCOpt = Comment | 
					DISCOpt_PT |
					DISCOpt_EN_HYST |
					DISCOpt_SEL_POL |
					DISCOpt_FORCE_EN_ZCC |
					DISCOpt_FORCE_TH |
					DISCOpt_SEL_COMP_MODE |
					DISCOpt_ZCC_DAC |
					DISCOpt_ARM_DAC |
					DISCOpt_HYST_DAC;

	Rule	VFAT3CfgDISCOpts ("VFAT3CfgDISCOpts");
	VFAT3CfgDISCOpts = VFAT3CfgDISCOpt + Opt(VFAT3CfgDISCOpts);

	Token	DISC			("DISC");
	Rule	VFAT3CfgDISC ("VFAT3CfgDISC");
	VFAT3CfgDISC = DISC + L_BRACE + VFAT3CfgDISCOpts + R_BRACE;

	// VFAT3 Bias
	Token	IREF			("Iref");
	Token	CFD_DAC_1		("CFD_DAC_1");
	Token	CFD_DAC_2		("CFD_DAC_2");
	Token	PRE_I_BSF		("PRE_I_BSF");
	Token	PRE_I_BIT		("PRE_I_BIT");
	Token	PRE_I_BLCC		("PRE_I_BLCC");
	Token	PRE_VREF		("PRE_VREF");
	Token	SH_I_BFCAS		("SH_I_BFCAS");
	Token	SH_I_BDIFF		("SH_I_BDIFF");
	Token	SD_I_BDIFF		("SD_I_BDIFF");
	Token	SD_I_BSF		("SD_I_BSF");
	Token	SD_I_BFCAS		("SD_I_BFCAS");
	Token	SLVS_VREF		("SLVS_VREF");
	Token	SLVS_IBIAS		("SLVS_IBIAS");
	Rule	BIASOpt_IREF	("BIASOpt_IREF");
	BIASOpt_IREF = IREF + EQUAL + INTEGER + SEMICOLON + (VFAT3config.BIAS.Iref << S3);
	Rule	BIASOpt_CFD_DAC_1	("BIASOpt_CFD_DAC_1");
	BIASOpt_CFD_DAC_1 = CFD_DAC_1 + EQUAL + INTEGER + SEMICOLON + (VFAT3config.BIAS.CFD_DAC_1 << S3);
	Rule	BIASOpt_CFD_DAC_2	("BIASOpt_CFD_DAC_2");
	BIASOpt_CFD_DAC_2 = CFD_DAC_2 + EQUAL + INTEGER + SEMICOLON + (VFAT3config.BIAS.CFD_DAC_2 << S3);
	Rule	BIASOpt_PRE_I_BSF	("BIASOpt_PRE_I_BSF");
	BIASOpt_PRE_I_BSF = PRE_I_BSF + EQUAL + INTEGER + SEMICOLON + (VFAT3config.BIAS.PRE_I_BSF << S3);
	Rule	BIASOpt_PRE_I_BIT	("BIASOpt_PRE_I_BIT");
	BIASOpt_PRE_I_BIT = PRE_I_BIT + EQUAL + INTEGER + SEMICOLON + (VFAT3config.BIAS.PRE_I_BIT << S3);
	Rule	BIASOpt_PRE_I_BLCC	("BIASOpt_PRE_I_BLCC");
	BIASOpt_PRE_I_BLCC = PRE_I_BLCC + EQUAL + INTEGER + SEMICOLON + (VFAT3config.BIAS.PRE_I_BLCC << S3);
	Rule	BIASOpt_PRE_VREF	("BIASOpt_PRE_VREF");
	BIASOpt_PRE_VREF = PRE_VREF + EQUAL + INTEGER + SEMICOLON + (VFAT3config.BIAS.PRE_VREF << S3);
	Rule	BIASOpt_SH_I_BFCAS	("BIASOpt_SH_I_BFCAS");
	BIASOpt_SH_I_BFCAS = SH_I_BFCAS + EQUAL + INTEGER + SEMICOLON + (VFAT3config.BIAS.SH_I_BFCAS << S3);
	Rule	BIASOpt_SH_I_BDIFF	("BIASOpt_SH_I_BDIFF");
	BIASOpt_SH_I_BDIFF = SH_I_BDIFF + EQUAL + INTEGER + SEMICOLON + (VFAT3config.BIAS.SH_I_BDIFF << S3);
	Rule	BIASOpt_SD_I_BDIFF	("BIASOpt_SD_I_BDIFF");
	BIASOpt_SD_I_BDIFF = SD_I_BDIFF + EQUAL + INTEGER + SEMICOLON + (VFAT3config.BIAS.SD_I_BDIFF << S3);
	Rule	BIASOpt_SD_I_BSF	("BIASOpt_SD_I_BSF");
	BIASOpt_SD_I_BSF = SD_I_BSF + EQUAL + INTEGER + SEMICOLON + (VFAT3config.BIAS.SD_I_BSF << S3);
	Rule	BIASOpt_SD_I_BFCAS	("BIASOpt_SD_I_BFCAS");
	BIASOpt_SD_I_BFCAS = SD_I_BFCAS + EQUAL + INTEGER + SEMICOLON + (VFAT3config.BIAS.SD_I_BFCAS << S3);
	Rule	BIASOpt_SLVS_IBIAS	("BIASOpt_SLVS_IBIAS");
	BIASOpt_SLVS_IBIAS = SLVS_IBIAS + EQUAL + INTEGER + SEMICOLON + (VFAT3config.BIAS.SLVS_IBIAS << S3);
	Rule	BIASOpt_SLVS_VREF	("BIASOpt_SLVS_VREF");
	BIASOpt_SLVS_VREF = SLVS_VREF + EQUAL + INTEGER + SEMICOLON + (VFAT3config.BIAS.SLVS_VREF << S3);

	Rule	VFAT3CfgBIASOpt ("VFAT3CfgBIASOpt");
	VFAT3CfgBIASOpt = Comment | 
					BIASOpt_IREF | 
					BIASOpt_CFD_DAC_1 | 
					BIASOpt_CFD_DAC_2 | 
					BIASOpt_PRE_I_BSF | 
					BIASOpt_PRE_I_BIT | 
					BIASOpt_PRE_I_BLCC | 
					BIASOpt_PRE_VREF | 
					BIASOpt_SH_I_BFCAS | 
					BIASOpt_SH_I_BDIFF | 
					BIASOpt_SD_I_BDIFF | 
					BIASOpt_SD_I_BSF | 
					BIASOpt_SD_I_BFCAS | 
					BIASOpt_SLVS_IBIAS | 
					BIASOpt_SLVS_VREF;

	Rule	VFAT3CfgBIASOpts ("VFAT3CfgBIASOpts");
	VFAT3CfgBIASOpts = VFAT3CfgBIASOpt + Opt(VFAT3CfgBIASOpts);
	Token	BIAS			("BIAS");
	Rule	VFAT3CfgBIAS ("VFAT3CfgBIAS");
	VFAT3CfgBIAS = BIAS + L_BRACE + VFAT3CfgBIASOpts + R_BRACE;


	// VFAT3 Calibration
	Token	POL			("POL");
	Token	PHI			("PHI");
	Token	EN_EXT		("EN_EXT");
	Token	DAC			("DAC");
	Token	MODE		("MODE");
	Token	FS			("FS");
	Token	DUR			("DUR");
	Rule	CALOpt_POL	("CALOpt_POL");
	CALOpt_POL = POL + EQUAL + BOOL + SEMICOLON + (VFAT3config.CAL.POL << S3);
	Rule	CALOpt_PHI	("CALOpt_PHI");
	CALOpt_PHI = PHI + EQUAL + INTEGER + SEMICOLON + (VFAT3config.CAL.PHI << S3);
	Rule	CALOpt_EN_EXT	("CALOpt_EN_EXT");
	CALOpt_EN_EXT = EN_EXT + EQUAL + BOOL + SEMICOLON + (VFAT3config.CAL.EN_EXT << S3);
	Rule	CALOpt_DAC	("CALOpt_DAC");
	CALOpt_DAC = DAC + EQUAL + INTEGER + SEMICOLON + (VFAT3config.CAL.DAC << S3);
	Rule	CALOpt_MODE	("CALOpt_MODE");
	CALOpt_MODE = MODE + EQUAL + INTEGER + SEMICOLON + (VFAT3config.CAL.MODE << S3);
	Rule	CALOpt_FS	("CALOpt_FS");
	CALOpt_FS = FS + EQUAL + INTEGER + SEMICOLON + (VFAT3config.CAL.FS << S3);
	Rule	CALOpt_DUR	("CALOpt_DUR");
	CALOpt_DUR = DUR + EQUAL + INTEGER + SEMICOLON + (VFAT3config.CAL.DUR << S3);

	Rule	VFAT3CfgCALOpt ("VFAT3CfgCALOpt");
	VFAT3CfgCALOpt = Comment | 
					CALOpt_POL |
					CALOpt_PHI |
					CALOpt_EN_EXT |
					CALOpt_DAC |
					CALOpt_MODE |
					CALOpt_FS |
					CALOpt_DUR;

	Rule	VFAT3CfgCALOpts ("VFAT3CfgCALOpts");
	VFAT3CfgCALOpts = VFAT3CfgCALOpt + Opt(VFAT3CfgCALOpts);
	Token	CALIB			("CAL");
	Rule	VFAT3CfgCAL ("VFAT3CfgCAL");
	VFAT3CfgCAL = CALIB + L_BRACE + VFAT3CfgCALOpts + R_BRACE;


	// VFAT3 Monitor
	Token	VREF_ADC	("VREF_ADC");
	Token	GAIN		("GAIN");
	Token	SEL			("SEL");
	Rule	MONOpt_VREF_ADC ("MONOpt_VREF_ADC");
	MONOpt_VREF_ADC = VREF_ADC + EQUAL + INTEGER + SEMICOLON + (VFAT3config.MON.VREF_ADC << S3);
	Rule	MONOpt_GAIN	("MONOpt_GAIN");
	MONOpt_GAIN = GAIN + EQUAL + BOOL + SEMICOLON + (VFAT3config.MON.GAIN << S3);
	Rule	MONOpt_SEL	("MONOpt_SEL");
	MONOpt_SEL = SEL + EQUAL + INTEGER + SEMICOLON + (VFAT3config.MON.SEL << S3);
	Rule	VFAT3CfgMONOpt ("VFAT3CfgMONOpt");
	VFAT3CfgMONOpt = Comment | 
					MONOpt_VREF_ADC |
					MONOpt_GAIN |
					MONOpt_SEL;

	Rule	VFAT3CfgMONOpts ("VFAT3CfgMONOpts");
	VFAT3CfgMONOpts = VFAT3CfgMONOpt + Opt(VFAT3CfgMONOpts);
	Token	MON			("MON");
	Rule	VFAT3CfgMON ("VFAT3CfgMON");
	VFAT3CfgMON = MON + L_BRACE + VFAT3CfgMONOpts + R_BRACE;

	// VFAT3 Trigger
	Token	LATENCY		("latency");
	Token	PS			("PS");
	Token	SYNC_LEVEL_ENABLE	("syncLevelEnable");
	Token	ST			("ST");
	Token	DDR			("DDR");
	Rule	TRGOpt_latency ("TRGOpt_latency");
	TRGOpt_latency = LATENCY + EQUAL + INTEGER + SEMICOLON + (VFAT3config.TRG.latency << S3);
	Rule	TRGOpt_PS ("TRGOpt_PS");
	TRGOpt_PS = PS + EQUAL + INTEGER + SEMICOLON + (VFAT3config.TRG.PS << S3);
	Rule	TRGOpt_syncLevelEnable ("TRGOpt_syncLevelEnable");
	TRGOpt_syncLevelEnable = SYNC_LEVEL_ENABLE + EQUAL + BOOL + SEMICOLON + (VFAT3config.TRG.syncLevelEnable << S3);
	Rule	TRGOpt_ST ("TRGOpt_ST");
	TRGOpt_ST = ST + EQUAL + BOOL + SEMICOLON + (VFAT3config.TRG.ST << S3);
	Rule	TRGOpt_DDR ("TRGOpt_DDR");
	TRGOpt_DDR = DDR + EQUAL + BOOL + SEMICOLON + (VFAT3config.TRG.DDR << S3);
	Rule	VFAT3CfgTRGOpt ("VFAT3CfgTRGOpt");
	VFAT3CfgTRGOpt = Comment | 
					TRGOpt_latency | 
					TRGOpt_PS | 
					TRGOpt_syncLevelEnable | 
					TRGOpt_ST | 
					TRGOpt_DDR;

	Rule	VFAT3CfgTRGOpts ("VFAT3CfgTRGOpts");
	VFAT3CfgTRGOpts = VFAT3CfgTRGOpt + Opt(VFAT3CfgTRGOpts);
	Token	TRG			("TRG");
	Rule	VFAT3CfgTRG ("VFAT3CfgTRG");
	VFAT3CfgTRG = TRG + L_BRACE + VFAT3CfgTRGOpts + R_BRACE;

	// VFAT3 Readout
	Token	MAX_PAR		("MAX_PAR");
	Token	EPZS		("EnablePZS");
	Token	SZP			("SZP");
	Token	SZD			("SZD");
	Token	ECB			("ECb");
	Token	BCB			("BCb");
	Rule	ROOpt_MAX_PAR ("ROOpt_MAX_PAR");
	ROOpt_MAX_PAR = MAX_PAR + EQUAL + INTEGER + SEMICOLON + (VFAT3config.RO.MAX_PAR << S3);
	Rule	ROOpt_EnablePZS ("ROOpt_EnablePZS");
	ROOpt_EnablePZS = EPZS + EQUAL + BOOL + SEMICOLON + (VFAT3config.RO.EnablePZS << S3);
	Rule	ROOpt_SZP ("ROOpt_SZP");
	ROOpt_SZP = SZP + EQUAL + BOOL + SEMICOLON + (VFAT3config.RO.SZP << S3);
	Rule	ROOpt_SZD ("ROOpt_SZD");
	ROOpt_SZD = SZD + EQUAL + BOOL + SEMICOLON + (VFAT3config.RO.SZD << S3);
	Rule	ROOpt_ECb ("ROOpt_ECb");
	ROOpt_ECb = ECB + EQUAL + INTEGER + SEMICOLON + (VFAT3config.RO.ECb << S3);
	Rule	ROOpt_BCb ("ROOpt_BCb");
	ROOpt_BCb = BCB + EQUAL + INTEGER + SEMICOLON + (VFAT3config.RO.BCb << S3);

	Rule	VFAT3CfgROOpt ("VFAT3CfgROOpt");
	VFAT3CfgROOpt = Comment | 
					ROOpt_MAX_PAR |
					ROOpt_EnablePZS |
					ROOpt_SZP |
					ROOpt_SZD |
					ROOpt_ECb |
					ROOpt_BCb;

	Rule	VFAT3CfgROOpts ("VFAT3CfgROOpts");
	VFAT3CfgROOpts = VFAT3CfgROOpt + Opt(VFAT3CfgROOpts);
	Token	RO			("RO");
	Rule	VFAT3CfgRO ("VFAT3CfgRO");
	VFAT3CfgRO = RO + L_BRACE + VFAT3CfgROOpts + R_BRACE;

	// RUN/Sleep
	Token	RUN		("RUN");
	Rule	VFAT3RunOpt ("VFAT3RunOpt");
	VFAT3RunOpt = RUN + EQUAL + BOOL + SEMICOLON + (VFAT3config.run << S3);

	// ADC calibration parameters
	Token	OFFSET	("OFFSET");
	Token	LSB		("LSB");
	Rule	ADCoffset ("ADCoffset");
	ADCoffset = OFFSET + EQUAL + FLOAT + SEMICOLON + (VFAT3config.ADC.OFFSET << S3);
	Rule	ADClsb ("ADClsb");
	ADClsb = LSB + EQUAL + FLOAT + SEMICOLON + (VFAT3config.ADC.LSB << S3);

	Rule	VFAT3ADCOpt ("VFAT3ADCOpt");
	VFAT3ADCOpt = Comment | 
					ADCoffset |
					ADClsb;

	Rule	VFAT3ADCOpts ("VFAT3ADCOpts");
	VFAT3ADCOpts = VFAT3ADCOpt + Opt(VFAT3ADCOpts);
	Token	ADC			("ADC");
	Rule	VFAT3CfgADC ("VFAT3CfgADC");
	VFAT3CfgADC = ADC + L_BRACE + VFAT3ADCOpts + R_BRACE;

	// VFAT3 chip
	Rule	VFAT3Cfg 		("VFAT3Cfg");
	VFAT3Cfg = 	Comment | 
				VFAT3CfgChannel |
				VFAT3CfgPRE  |
				VFAT3CfgDISC |
				VFAT3CfgBIAS |
				VFAT3CfgCAL  |
				VFAT3CfgMON |
				VFAT3CfgTRG |
				VFAT3CfgRO |
				VFAT3RunOpt |
				VFAT3CfgADC;

	Rule	VFAT3Cfgs 		("VFAT3Cfgs");
	VFAT3Cfgs = VFAT3Cfg + Opt(VFAT3Cfgs);

	Token	VFAT3			("VFAT3");
	Rule	VFAT3Block 	("VFAT3Block");
	VFAT3Block = Opt(Comments) + VFAT3 + L_BRACE + VFAT3Cfgs + R_BRACE;

	Rule	CfgFile 	("CfgFile");
	CfgFile = ScopeBlock + MosaicBlock + VFAT3Block;


// THIS SAYS SWITCH ON PARSER DEBUGGING IFF ENV VAR PARSER_DEBUG DEFINED
//	Parser::Debug("PARSER_DEBUG");
//	Parser::DebugTo("debug.log");
//	Parser::Debug(true);


	// Erase all varibles before load
	init();

	// RUN GRAMMAR ON INPUT FILE
	pstream ps(filename);
	cout << "Reading CFG file " << filename << endl;
	
	if (!CfgFile(ps)){	
		cerr << "ERROR reading file " << filename; 
        if (errno!=0){
            cerr << ": " << strerror(errno) << endl;
        } else {
    		ps.PrintErrorLine();
        }
		return false;
	}	

	ps.Close();
	cout << "Configuration file read" << endl;
	

	return true;
}

