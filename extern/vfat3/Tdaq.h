/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#ifndef Tdaq_h
#define Tdaq_h

#include <iostream>
#include <fstream>
#include "TestBench.h"
#include "VFAT3chip.h"
#include "TRGdataParser.h"
#include "GenericTest.h"

class Tdaq : public GenericTest
{
public:
 	Tdaq(TestBench &tb);
	bool run(string param);

private:
	VFAT3chip::VFAT3config_t	chipCfg;
	VFAT3board					*board;
	VFAT3chip					*chip;
	ofstream 					outputFile;
	long						evCounter;
	long						trgCounter;

private:
	static void VFAT3dataSave(void *ctx, VFAT3dataParser::VFAT3data_t *data);
	static void TRGdataSave(void *ctx, TRGdataParser::TRGdata_t *data);


};

#endif	// Tdaq_h

