/*
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>.
 *
 */

#include <iostream>
#include <unistd.h>
#include <strings.h>
#include "PTgain.h"
#include "PTscurves.h"

// Arm thresholds for noise scan
static const int hgain_arm_noise = 100; //only for hight gain, for the other gain settings the noise measurement is done using the gain thresholds

// Arm thresholds for gain scan

// High gain
static const int hgain_arm_l = 50;
static const int hgain_arm_h = 150;

// Medium gain
static const int mgain_arm_l = 50;
static const int mgain_arm_h = 100;

// Low gain
static const int lgain_arm_l = 70;
static const int lgain_arm_h = 100;

//noise check limits in fC

// High gain
static const double enc_hgain_min = 0.1;
static const double enc_hgain_max = 0.6;

// Medium gain
static const double enc_mgain_min = 0.1;
const double enc_mgain_max = 0.6;

// Low gain
static const double enc_lgain_min = 0.1;
static const double enc_lgain_max = 0.6;

//gain check limits in mV/fC

// High gain
static const double hgain_min = 45-(4*4);
static const double hgain_max = 45+(4*4);

// Medium gain
static const double mgain_min = 16.6-(4*0.8);
static const double mgain_max = 16.6+(4*0.8);

// Low gain
static const double lgain_min = 8-(4*0.6);
static const double lgain_max = 8+(4*0.6);

PTgain::PTgain(TestBench &t) : GenericTest(t)
{
	armDAC = {string("Vmon_Vth_Arm"), VFAT3chip::RegCfgThr, VFAT3chip::CfgThr_ARM_DAC_mask, VFAT3chip::CfgThr_ARM_DAC_shift };
}

PTgain::~PTgain()
{
}

bool PTgain::run(string param)
{
	PTscurves fit_scurve(tb);
	bool fit_status;

	double vENC[128];
	double vTH_l[128];
	double vTH_h[128];
	double gain = 0 ;

	int HG_g_cnt = 0; //counter for channel that do not pass the test
	int MG_g_cnt = 0;
	int LG_g_cnt = 0;

	int HG_n_cnt = 0;
	int MG_n_cnt = 0;
	int LG_n_cnt = 0;

	// Monitor on Iref
	tb.chipConfig.MON.SEL = 0;

	// disable cal bit on all channels
	for (int i=0; i<128; i++)
	{
		tb.chipConfig.CH[i].arm_dac = 0;
		tb.chipConfig.CH[i].zcc_dac = 0;
		tb.chipConfig.CH[i].mask = false;
		tb.chipConfig.CH[i].cal = false;
	}

	//calibration settings
	tb.chipConfig.CAL.MODE = 2;		// current injection
	tb.chipConfig.CAL.POL = 0;		// negative injected charge, the CSA output is a positive pulse
	tb.chipConfig.CAL.DUR = 1;
	tb.chipConfig.CAL.PHI = 0;
	tb.chipConfig.CAL.FS = 2;

	//CFD settings
	tb.chipConfig.DISC.PT = 100;
	tb.chipConfig.DISC.SEL_POL = 0;
	tb.chipConfig.DISC.EN_HYST = 1;
	tb.chipConfig.DISC.FORCE_EN_ZCC = 0;
	tb.chipConfig.DISC.FORCE_TH = 0;
	tb.chipConfig.DISC.SEL_COMP_MODE = 1;

	//CSA settings - high gain
	tb.chipConfig.PRE.TP = 100;
	tb.chipConfig.PRE.RES = 0;
	tb.chipConfig.PRE.CAP = 0;

	//Set Global ARM DAC
	tb.chipConfig.DISC.arm_dac = hgain_arm_noise;
	tb.setChipConfig();
	usleep(10000);

	fit_status = fit_scurve.run(param, vENC, vTH_l);

	//Set Global ARM DAC
	tb.chipConfig.DISC.arm_dac = hgain_arm_l;
	tb.setChipConfig();
	usleep(10000);

	fit_status = fit_scurve.rungainfast(vTH_l);

	tb.chipConfig.DISC.arm_dac = hgain_arm_h;
	tb.setChipConfig();
	usleep(10000);

	fit_status = fit_scurve.rungainfast(vTH_h);

	if (fit_status)
	{

		for (int v = 0; v<128; v++) {
			//cout << "canale " << v << " rumore " << vENC[v] << endl;
			gain = ((hgain_arm_h - hgain_arm_l)*tb.ptdata->VFAT3.dac_d[16].gain + tb.ptdata->VFAT3.dac_d[16].offset)/(vTH_h[v]-vTH_l[v])*1000; // gain in mV/fC

			if ((gain <= hgain_min) || (gain >= hgain_max))
			{
				cout << "Channel "<< v <<" in HG conf: gain test not passed!"<<endl;
				cout << "Gain: " << gain << " mV/fC" << endl;
				cout << "Gain should be included between "<< hgain_min << " mV/fC and " << hgain_max << " mV/fC" << endl;
				HG_g_cnt++;
			}
			if ((vENC[v] <= enc_hgain_min) || (vENC[v] >= enc_hgain_max))
			{
				cout<<"Channel "<<v<<" in HG conf: noise test not passed!"<<endl;
				cout << "Noise: " << vENC[v] << " fC" << endl;
				cout << "Noise should be included between "<< enc_hgain_min << " fC and " << enc_hgain_max << " fC" << endl;
				HG_n_cnt++;
			}
			tb.ptdata->VFAT3.chn_d[v].gain[0] = gain;
			tb.ptdata->VFAT3.chn_d[v].noise[0] = vENC[v];
		}
	}
	else
	{
		cout<<"High gain scurve measurement error!"<<endl;
		throw TestError("scurve measurement error!");
	}


	//CSA settings - medium gain
	tb.chipConfig.PRE.TP = 100;
	tb.chipConfig.PRE.RES = 1;
	tb.chipConfig.PRE.CAP = 1;

	//Set Global ARM DAC
	tb.chipConfig.DISC.arm_dac = mgain_arm_l;
	tb.setChipConfig();
	usleep(10000);

	fit_status = fit_scurve.run(param, vENC, vTH_l); // noise measurement

	fit_status = fit_scurve.rungainfast(vTH_l);	// first measurement for gain calculation

	tb.chipConfig.DISC.arm_dac = mgain_arm_h;
	tb.setChipConfig();
	usleep(10000);

	fit_status = fit_scurve.rungainfast(vTH_h); // second measurement for gain calculation

	if (fit_status)
	{

		for (int v = 0; v<128; v++) {

			gain = (( mgain_arm_h - mgain_arm_l ) * tb.ptdata->VFAT3.dac_d[16].gain + tb.ptdata->VFAT3.dac_d[16].offset)/(vTH_h[v]-vTH_l[v])*1000; // gain in mV/fC

			if ((gain <= mgain_min) || (gain >= mgain_max))
			{
				cout<<"Channel "<<v<<" in MG conf: gain test not passed!"<<endl;
				cout << "Gain: " << gain << " mV/fC" << endl;
				cout << "Gain should be included between "<< mgain_min << " mV/fC and " << mgain_max << " mV/fC" << endl;
				MG_g_cnt++;
			}
			if ((vENC[v] <= enc_mgain_min) || (vENC[v] >= enc_mgain_max))
			{
				cout<<"Channel "<<v<<" in MG conf: noise test not passed!"<<endl;
				cout << "Noise: " << vENC[v] << " fC" << endl;
				cout << "Noise should be included between "<< enc_mgain_min << " fC and " << enc_mgain_max << " fC" << endl;
				MG_n_cnt++;
			}
			tb.ptdata->VFAT3.chn_d[v].gain[1] = gain;
			tb.ptdata->VFAT3.chn_d[v].noise[1] = vENC[v];
		}
	}
	else
	{
		cout<<"Medium gain scurve measurement error!"<<endl;
		throw TestError("scurve measurement error!");
	}

	//CSA settings - low gain
	tb.chipConfig.PRE.TP = 100;
	tb.chipConfig.PRE.RES = 2;
	tb.chipConfig.PRE.CAP = 2;

	//Set Global ARM DAC
	tb.chipConfig.DISC.arm_dac = lgain_arm_l;
	tb.setChipConfig();
	usleep(10000);

	fit_status = fit_scurve.run(param, vENC, vTH_l);

	fit_status = fit_scurve.rungainfast(vTH_l);

	tb.chipConfig.DISC.arm_dac = lgain_arm_h;
	tb.setChipConfig();

	usleep(10000);
	fit_status = fit_scurve.rungainfast(vTH_h);

	if (fit_status)
	{

		for (int v = 0; v<128; v++) {

			gain = ((lgain_arm_h - lgain_arm_l)*tb.ptdata->VFAT3.dac_d[16].gain + tb.ptdata->VFAT3.dac_d[16].offset)/(vTH_h[v]-vTH_l[v])*1000; // gain in mV/fC
			if ((gain <= lgain_min) || (gain >= lgain_max))
			{
				cout<<"Channel "<<v<<" in LG conf: gain test not passed!"<<endl;
				cout << "Gain: " << gain << " mV/fC" << endl;
				cout << "Gain should be included between "<< lgain_min << " mV/fC and " << lgain_max << " mV/fC" << endl;
				LG_g_cnt++;				
			}
			if ((vENC[v] <= enc_lgain_min) || (vENC[v] >= enc_lgain_max))
			{
				cout<<"Channel "<<v<<" in LG conf: noise test not passed!"<<endl;
				cout << "Noise: " << vENC[v] << " e-" << endl;
				cout << "Noise should be included between "<< enc_lgain_min << " fC and " << enc_lgain_max << " fC" << endl;
				LG_n_cnt++;					
			}
			tb.ptdata->VFAT3.chn_d[v].gain[2] = gain;
			tb.ptdata->VFAT3.chn_d[v].noise[2] = vENC[v];
		}
	}
	else
	{
		cout<<"Low gain scurve measurement error!"<<endl;
		throw TestError("scurve measurement error!");
	}




    return true;
}

