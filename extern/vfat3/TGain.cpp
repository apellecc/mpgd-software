/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#include <fstream>
#include <iostream>
#include "TestBench.h"
#include "TBconfig.h"
#include "VFAT3board.h"
#include "VFAT3chip.h"
#include "Tutils.h"
#include "TGain.h"

using std::string;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;

const long	TGain::maxChargeDAC = 255;

const long	TGain::minThresholdDAC = 10;
const long	TGain::maxThresholdDAC = 255;
const long	TGain::stepThresholdDAC = 10;

const int	TGain::startChannel = 9;
const int	TGain::stopChannel  = 9;

TGain::TGain(TestBench &t) : GenericTest(t)
{
	board = tb.board;
}

void TGain::resetHistogram()
{
	evCounter = 0;
	for (int i=0; i<128; i++)
		histogram[i] = 0;
}

void TGain::histogramCB(void *ctx, VFAT3dataParser::VFAT3data_t *data)
{
	TGain *p = (TGain *) ctx;
	
	p->evCounter++;

	for (int i=0; i<128; i++)
		if (data->hit[i])
			p->histogram[i]++;
}


bool TGain::run(string param)
{
	uint32_t 						count;
	VFAT3chip::VFAT3config_t       	chipCfg = tb.chipConfig;
	VFAT3chip::CalibrationConfig_t 	calCfg = tb.chipConfig.CAL;
	VFAT3chip::DiscriminatorConfig_t discCfg = tb.chipConfig.DISC;
	VFAT3chip::ChannelConfig_t		chCfg;
	std::array<long, 128> 			hist;

	long 	hitMatrix[maxChargeDAC+1][128];
	double	chargeArray[maxChargeDAC+1];
	double	hitArray[maxChargeDAC+1];

	chip = tb.chip;

	// Open output file
	ofstream outputFile;
	outputFile.open (string("Data/")+string("Gain_")+Tutils::filenameSuffix()+".txt");

	// disable cal bit on all channels
	for (int i=0; i<129; i++)
		chipCfg.CH[i].cal = false;
	chip->setConfig(chipCfg);

	// Init the charge injection
	tb.initInjCharge(calCfg);

	// Set consumer function
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, histogramCB);

	// Setup pulses
	board->pulser->setConfig(10, 500);	// 12.5 us (plus 250 ns) period

	// Send BC0
	board->comPortTx->sendBC0();

	// start run
	board->mRunControl->startRun();
	board->comPortTx->sendRunMode();

	// read reference current
	float VIref = tb.getVmon(VFAT3chip::Imon_Iref);
	outputFile << "VIref= " << VIref << endl;

	// write column headers
	outputFile << "Vthreshold ScurveTh" << endl;

	// Loop on threshold
	for (int thDAC=minThresholdDAC; thDAC<maxThresholdDAC; thDAC+=stepThresholdDAC){
		cout << "thDAC: " << thDAC << endl;

		//	set arming threshold DAC
		discCfg.arm_dac = thDAC;
		chip->addSetDiscriminatorConfig(discCfg);
		chip->execute();

		// read threshold voltage
		float Vth = tb.getVmon(VFAT3chip::Vmon_Vth_Arm);
		outputFile << Vth << " ";

		// Loop on charge
		for (int chargeDAC=0; chargeDAC<maxChargeDAC; chargeDAC++){
			// cout << chargeDAC << endl;
			// outputFile << chargeDAC << " ";
	
			// set calibration DAC
			if (calCfg.MODE == 1){
				calCfg.DAC = 255-chargeDAC;		// Vpulse mode
			} else if (calCfg.MODE == 2) {
				calCfg.DAC = chargeDAC;
			} else {
				cerr << "Please set Calibration MODE != 0" << endl;
				exit(0);
			}
			chip->addSetCalibrationConfig(calCfg);
			chip->execute();
			usleep(1000);

			float injCharge= tb.getInjCharge(calCfg);
			// outputFile << injCharge << " ";
			chargeArray[chargeDAC] = injCharge;

			// Loop on channels
			for (int ch=startChannel; ch<=stopChannel; ch++){
				chCfg = tb.chipConfig.CH[ch];
				chCfg.cal = true;
				chip->addSetChannelConfig(ch, chCfg);
				chip->execute();

				// start pulses injection				
				board->pulser->run(numPulses);
	
				resetHistogram();
				long res;
				do {
					res = board->pollData(50);		// timeout 50 ms
					if (res == 0){
						cerr << "Timeout reading data" << endl;
						exit(0);
					}
				} while (evCounter<numPulses);

				// store data for the pulsed channel	
				hist[ch] = histogram[ch];

				// reset cal bit
				chCfg.cal = false;
				chip->addSetChannelConfig(ch, chCfg);
				chip->execute();
			}

			// copy into the result matrix
			for (int i=startChannel; i<=stopChannel; i++)
				hitMatrix[chargeDAC][i] = hist[i];
		}

		// get Scurve threshold
		// Fit matrix data
		for (int ch=startChannel; ch<=stopChannel; ch++){
			for (int i=0; i<=maxChargeDAC; i++)
				hitArray[i] = hitMatrix[i][ch];

			double th, ENC;
			double thErr, ENCerr;

			int ret = Tutils::fitErf(numPulses, maxChargeDAC, chargeArray, hitArray, 
						&th, &thErr, &ENC, &ENCerr);
			if (ret)
				cout << "ch:" << ch << " th:" << th << endl;

			if (ret)
				outputFile 	<< th << " " << thErr << endl;


/*
			if (ret){
				outputFile 	<< ch << " " << th << " " << thErr << " " << ENC << " " << ENCerr << endl;
//				plotFile << ch << " " << ENC*6250.0 << endl;
			} else {
				outputFile 	<< "ch:" << ch << "-1 -1" << endl;
//				plotFile << ch << " " << -1 << endl;
			}
*/

		}
	}

	board->mTriggerControl->getTriggerCounter(&count);
	cout << "Trigger count:" << count << endl;
	board->mRunControl->stopRun();
	board->comPortTx->sendSConly();
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, NULL);

	// restore chip configuration
	chip->setConfig(tb.chipConfig);

	outputFile.close();
//	plotFile.close();
	return true;
}



