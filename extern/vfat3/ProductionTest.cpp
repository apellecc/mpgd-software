/*
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>.
 *
 */

#include <iostream>
#include <fstream>
#include "ProductionTest.h"
#include "PTpower.h"
#include "PTextInjection.h"
#include "PTiref.h"
#include "PTvref.h"
#include "PTdac.h"
#include "PTadc.h"
#include "PTtsens.h"
#include "PTscurves.h"
#include "PTgain.h"
#include "PTlcldac.h"
#include "PTmaxrate.h"
#include "PTpeaktime.h"
#include "Tutils.h"
#include "miscTest.h"
#include "PTeFuse.h"
#include "Reed_Muller.h"

ProductionTest::ProductionTest(TestBench &t) : GenericTest(t)
{
}

ProductionTest::~ProductionTest()
{
}

bool ProductionTest::run(string param)
{

    PTpower         Power(tb);
    PTextInjection  ExtInjection(tb);
	PTiref			Iref(tb);
	PTvref			Vref(tb);
	PTdac			DACt(tb);
	PTadc			ADCt(tb);
	PTtsens			Tsens(tb);
	PTgain			Gain(tb);
	PTlcldac		Lcldac(tb);
	PTmaxrate		Maxrate(tb);
	PTpeaktime		Peaktime(tb);
	test_EYE		TestEYE(tb);
	test_ATE		TestATE(tb);
	test_BIST		TestBIST(tb);
	PTeFuse			EFuse(tb);
	Reed_Muller		ReedMuller;

    testList_t TestList [] = {
	    { &Power, 	        "",				"Switch ON the power supply and check Voltages/Currents" },
/*		{ &Iref,			"",				"Current reference test" },
		{ &Vref,			"",				"Voltage references test" },
		{ &DACt,			"",				"Bias and threshold DAC test" },
		{ &ADCt,			"",				"ADC test" },
		{ &Tsens,			"",				"Temperature sensors test" },
		{ &Lcldac,			"",				"Local Arm dac test"},
//		{ &Gain,			"SERIAL", 		"Gain and noise test"},
		{ &Maxrate,			"",				"Max rate test"},
//		{ &Peaktime,		"",				"Peaking time test"},
		//{ &TestEYE,			"",				"SLVS TX EYE diagram"},
	    { &ExtInjection, 	"",				"Verify input bondings" },
		{ &TestATE,			"",				"Scan path test"},
		{ &TestBIST,		"",				"BIST"},*/
//		{ &EFuse,			"WRITE",			"EFUSE"},
	
    };
/*
int num_int;
int num_fuse[32];
num_int = 34365;
ReedMuller.run("ENCODE",&num_int , num_fuse);

for (int iii=31; iii>=0; iii--) cout << num_fuse[iii];
cout<< endl;
num_fuse[0] = 1;
num_fuse[1] = 0;
num_fuse[2] = 1;
num_fuse[3] = 1;
num_fuse[4] = 1;
num_fuse[5] = 0;
num_fuse[6] = 0;
num_fuse[7] = 0;
num_fuse[8] = 1;
num_fuse[9] = 1;
num_fuse[10] = 0;
num_fuse[11] = 1;
num_fuse[12] = 0;
num_fuse[13] = 0;
num_fuse[14] = 0;
num_fuse[15] = 1;
num_fuse[16] = 1;
num_fuse[17] = 0;
num_fuse[18] = 1;
num_fuse[19] = 1;
num_fuse[20] = 1;
num_fuse[21] = 0;
num_fuse[22] = 0;
num_fuse[23] = 0;
num_fuse[24] = 0;
num_fuse[25] = 0;
num_fuse[26] = 1;
num_fuse[27] = 0;
num_fuse[28] = 1;
num_fuse[29] = 1;
num_fuse[30] = 1;
num_fuse[31] = 0;
for (int iii=31; iii>=0; iii--) cout << num_fuse[iii];
cout << endl;
num_int=0;
ReedMuller.run("DECODE",&num_int , num_fuse);
cout << num_int << endl;
*/
    // Loop over all production tests
    for (;;){ 
        try {
            // switch OFF all power supply
            tb.board->onOffVDD(VFAT3board::VDD_ALL, false);

            // set pushbutton LED flashing and wait for user to press it
            tb.board->setLEDflash(TesterCtrl::LED_Start);
            cout << endl << ">>>>> Insert device in the ZIP socket and press pushbutton to start tests..." << endl;

 // FIXME: skip pushbutton
#ifdef BENCH_VFAT3_PKG
//            while (tb.board->getPushButton());
//            while (!tb.board->getPushButton());
            cout << "Press ENTER" << endl;
            getchar();
#else
            cout << "Press ENTER" << endl;
            getchar();
#endif
		
            // set start LED ON, Flash Pass/Fail
            tb.board->setLEDoff(TesterCtrl::LED_Start);
            //tb.board->setLEDflash(TesterCtrl::LED_Pass);
            //tb.board->setLEDflash(TesterCtrl::LED_Fail);

            // Set tester in functional mode (Clock ON)
            tb.board->setTestType(TesterCtrl::TTYPE_Functional);
            tb.board->setDiffMux(VFAT3board::MUX_SEL_TXD);

            tb.board->setExtInj(false, false);

			// Set INA gain
			tb.board->setINAgain(VFAT3board::INA_GAIN_50);

            // Run all tests in the list
            // Failing test will exit with "TestError" exception
            for (const auto& test: TestList){
                cout << "Running \"" << test.description << "\"" << endl;
system("date +%H:%M:%S");
				test.classPtr->run(test.parameter);
            }

            // All tests PASS: set Pass/Fail LEDS  
            tb.board->setLEDon (TesterCtrl::LED_Pass);
            tb.board->setLEDoff(TesterCtrl::LED_Fail);

			// Dump test data to file in a foleder ./Data
			tb.ptdata->Data_dump();
			tb.ptdata->Data_dump_elab();

        } catch (TestError &e) {
            cerr << endl << "TEST FAILED. " << e.what() << endl << endl;

            // FAIL: set Pass/Fail LEDS  
            tb.board->setLEDoff(TesterCtrl::LED_Pass);
            tb.board->setLEDon (TesterCtrl::LED_Fail);         
        }
    }



    return true;
}

