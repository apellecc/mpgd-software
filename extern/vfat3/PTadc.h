#include "VFAT3_data.h"
#include "TestBench.h"
#include "GenericTest.h"


class PTadc : public GenericTest
{
public:
	PTadc(TestBench &tb);
	bool run(string param);

private:

	typedef struct DAC_s {
		string name;
		uint32_t reg;
		uint32_t mask;
		uint32_t shift;
		uint32_t monSel;
		uint32_t maxValue;
	} DAC_t;
	
	DAC_t calib_dac;

};
