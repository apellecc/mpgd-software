/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#include "TestBench.h"
#include "TBconfig.h"
#include "VFAT3board.h"
#include "TmonState.h"
#include <iostream>
#include <fstream>

using std::string;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;

TmonState::TmonState(TestBench &t) : GenericTest(t)
{
}

char *ImonName[] = { 
	(char *) "Imon_Iref",
	(char *) "Imon_Calib_IDC",
	(char *) "Imon_Preamp_InpTran",
	(char *) "Imon_Preamp_LC",
	(char *) "Imon_Preamp_SF",
	(char *) "Imon_Shap_FC",
	(char *) "Imon_Shap_Inpair",
	(char *) "Imon_SD_Inpair",
	(char *) "Imon_SD_FC",
	(char *) "Imon_SD_SF",
	(char *) "Imon_CFD_Bias1",
	(char *) "Imon_CFD_Bias2",
	(char *) "Imon_CFD_Hyst",
	(char *) "Imon_CFD_Ireflocal",
	(char *) "Imon_CFD_ThArm",
	(char *) "Imon_CFD_ThZcc",
	(char *) "Imon_SLVS_Ibias",
};

char *VmonName[] = { 
	(char *) "Vmon_BGR",
	(char *) "Vmon_Calib_Vstep",
	(char *) "Vmon_Preamp_Vref",
	(char *) "Vmon_Vth_Arm",
	(char *) "Vmon_Vth_ZCC",
	(char *) "V_Tsens_Int",
	(char *) "V_Tsens_Ext",
	(char *) "ADC_ref",
	(char *) "ADC_VinM",
	(char *) "SLVS_Vref"
};


bool TmonState::run(string param)
{
	uint32_t adc0, adc1;
	float adcFMC=0.0;
	VFAT3chip::MonitorConfig_t monCfg = tb.chipConfig.MON;


	for (int sel=0; sel<17; sel++){
		// set Monitor selection mux
		monCfg.SEL = sel;

		// Read chip ADCs
		tb.chip->addSetMonitorConfig(monCfg);
		tb.chip->execute();
		usleep(10000);
		tb.chip->addReadADC(0, &adc0);
		tb.chip->addReadADC(1, &adc1);
		tb.chip->execute();
		float Vadc0 = (float) adc0 * tb.chipConfig.ADC.LSB - tb.chipConfig.ADC.OFFSET;

		// Read External ADC
		adcFMC = tb.getVmon();
		cout << to_string(adc0) << "(" << Vadc0 << " V)\t" << to_string(adc1) << " " << to_string(adcFMC) << " " << ImonName[sel] << endl;
	}


	for (int sel=32; sel<42; sel++){
		// set Monitor selection mux
		monCfg.SEL = sel;

		// Read chip ADCs
		tb.chip->addSetMonitorConfig(monCfg);
		tb.chip->execute();
		usleep(10000);

		tb.chip->addReadADC(0, &adc0);
		tb.chip->addReadADC(1, &adc1);
		tb.chip->execute();
		float Vadc0 = (float) adc0 * tb.chipConfig.ADC.LSB - tb.chipConfig.ADC.OFFSET;

		// Read External ADC
		adcFMC = tb.getVmon();
		cout << to_string(adc0) << "(" << Vadc0 << " V)\t" << to_string(adc1) << " " << to_string(adcFMC) << " " << VmonName[sel-32] << endl;
	}

	return true;
}


