/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#include <fstream>
#include <iostream>
#include <math.h>
#include "TestBench.h"
#include "TBconfig.h"
#include "VFAT3board.h"
#include "VFAT3chip.h"
#include "Tutils.h"
#include "TScurves.h"

using std::string;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;

TScurves::TScurves(TestBench &t) : GenericTest(t), hitMatrix()
{
	board = tb.board;
}

void TScurves::resetHistogram()
{
	evCounter = 0;
	for (int i=0; i<128; i++)
		histogram[i] = 0;
}

void TScurves::histogramCB(void *ctx, VFAT3dataParser::VFAT3data_t *data)
{
	TScurves *p = (TScurves *) ctx;
	
	p->evCounter++;

	for (int i=0; i<128; i++)
		if (data->hit[i])
			p->histogram[i]++;
}

void TScurves::pulseAtChargeDAC(int chargeDAC, unsigned long nPulses)
{
    // set calibration DAC
    if (calCfg.MODE == 1){
	    calCfg.DAC = 255-chargeDAC;		// Vpulse mode
    } else if (calCfg.MODE == 2) {
	    calCfg.DAC = chargeDAC;
    } else {
	    cerr << "Please set Calibration MODE != 0" << endl;
	    exit(0);
    }
    chip->addSetCalibrationConfig(calCfg);
    chip->execute();

	// start pulses injection				
	board->pulser->run(nPulses);

	resetHistogram();
	long res;
	do {
		res = board->pollData(50);		// timeout 50 ms
		if (res == 0){
			cerr << "Timeout reading data" << endl;
			throw TestError("Timeout reading data");
		}
	} while (evCounter<nPulses);
}


bool TScurves::run(string param)
{
	chip = tb.chip;

    if (param == "PARALLEL")
        return runParallel();
    else
        return runSerial();
}

// Scurves channel by channel
bool TScurves::runSerial()
{
	uint32_t 						count;
	VFAT3chip::VFAT3config_t       	chipCfg = tb.chipConfig;
	VFAT3chip::ChannelConfig_t		chCfg;
	std::array<long, 256> 			hist;
	double	                        hitArray[maxChargeDAC];

	calCfg = tb.chipConfig.CAL;

	// Open output files
    string filename = string("Data/")+string("Scurves_")+Tutils::filenameSuffix();
	ofstream outputFile;
	outputFile.open (filename+".txt");
	ofstream plotFile;
	plotFile.open (filename+".dat");

	// disable cal bit on all channels
	for (int i=0; i<129; i++)
		chipCfg.CH[i].cal = false;
	chip->setConfig(chipCfg);

	// Init the charge injection
	tb.initInjCharge(calCfg);

	// Set consumer function
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, histogramCB);

	// Setup pulses
	board->pulser->setConfig(10, 380);	// 10 us (plus 250 ns) period

	// Send BC0
	board->comPortTx->sendBC0();

	// start run
	board->mRunControl->startRun();
	board->comPortTx->sendRunMode();

	// read reference current
	float VIref = tb.getVmon(VFAT3chip::Imon_Iref);
	outputFile << "VIref= " << VIref << endl;

	// read threshold voltage
	float Vth = tb.getVmon(VFAT3chip::Vmon_Vth_Arm);
	outputFile << "Vthreshold= " << Vth << endl;

	// write column headers
	outputFile << "DAC Charge[fC]";
	for (int i=0; i<128; i++)
		 outputFile << " ch_" << i;
	outputFile << endl;

    // start plot
    plotInit();

#ifdef OLD_SCAN
	// Loop on charge
	for (int chargeDAC=0; chargeDAC<maxChargeDAC; chargeDAC++){
 		cout << chargeDAC << endl;
	
		// set calibration DAC
		if (calCfg.MODE == 1){
			calCfg.DAC = 255-chargeDAC;		// Vpulse mode
		} else if (calCfg.MODE == 2) {
			calCfg.DAC = chargeDAC;
		} else {
			cerr << "Please set Calibration MODE != 0" << endl;
			exit(0);
		}
		chip->addSetCalibrationConfig(calCfg);
		chip->execute();
		usleep(1000);

		float injCharge= tb.getInjCharge(calCfg);
		chargeArray[chargeDAC] = injCharge;

		// Loop on channels
		for (int ch=0; ch<128; ch++){
			chCfg = tb.chipConfig.CH[ch];
			chCfg.cal = true;
			chip->addSetChannelConfig(ch, chCfg);
			chip->execute();

			// start pulses injection				
			board->pulser->run(numPulses);
	
			resetHistogram();
			long res;
			do {
				res = board->pollData(50);		// timeout 50 ms
				if (res == 0){
					cerr << "Timeout reading data" << endl;
					exit(0);
				}
			} while (evCounter<numPulses);

			// store data for the pulsed channel	
			hist[ch] = histogram[ch];

			// reset cal bit
			chCfg.cal = false;
			chip->addSetChannelConfig(ch, chCfg);
			chip->execute();
		}


		// copy into the result matrix
		for (int i=0; i<128; i++)
			hitMatrix[chargeDAC][i] = hist[i];

        // update plot
        plotUpdate();
	}
#else
	// Fill charge array (~ 1.5 s)
    cout << "Fill charge array" << endl;
	for (int chargeDAC=0; chargeDAC<maxChargeDAC; chargeDAC++){
		// set calibration DAC
		if (calCfg.MODE == 1){
			calCfg.DAC = 255-chargeDAC;		// Vpulse mode
		} else if (calCfg.MODE == 2) {
			calCfg.DAC = chargeDAC;
		} else {
			cerr << "Please set Calibration MODE != 0" << endl;
			exit(0);
		}
		float injCharge= tb.getInjCharge(calCfg);
		chargeArray[chargeDAC] = injCharge;
    }

	// Loop on channels
	for (int ch=0; ch<128; ch++){
        cout << "Scurve on channel " << ch << endl;
		chCfg = tb.chipConfig.CH[ch];
        
        // set cal bit
		chCfg.cal = true;
		chip->addSetChannelConfig(ch, chCfg);

        // Search threshold
        int chargeDAC=0;
	    for (int i=0x80; i!=0; i>>=1){
		    chargeDAC = chargeDAC|i;
            pulseAtChargeDAC(chargeDAC, numPulsesSearch);
		    if (histogram[ch] > numPulsesSearch/2)
			    chargeDAC &= ~i;
        }
        int threshold = chargeDAC;
        
        // Scan around the threshold
	    for (int chargeDAC=threshold; chargeDAC<256; chargeDAC++){
            pulseAtChargeDAC(chargeDAC, numPulses);

			// store data for the pulsed channel	
			hist[chargeDAC] = histogram[ch];

            if (hist[chargeDAC] == numPulses){
                // fill remaining points
                while (chargeDAC<256)
                    hist[chargeDAC++] = numPulses;
                break;
            }
        }    

	    for (int chargeDAC=threshold-1; chargeDAC>=0; chargeDAC--){
            pulseAtChargeDAC(chargeDAC, numPulses);

			// store data for the pulsed channel	
			hist[chargeDAC] = histogram[ch];

            if (hist[chargeDAC] == 0){
                while (chargeDAC>=0)
                    hist[chargeDAC--] = 0;
                break;
            }
        }    

		// reset cal bit
		chCfg.cal = false;
		chip->addSetChannelConfig(ch, chCfg);

		// copy into the result matrix
		for (int dac=0; dac<maxChargeDAC; dac++)
			hitMatrix[dac][ch] = hist[dac];

        // update plot
        plotUpdate();
	}

#endif

	board->mTriggerControl->getTriggerCounter(&count);
	cout << "Trigger count:" << count << endl;
	board->mRunControl->stopRun();
	board->comPortTx->sendSConly();
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, NULL);

	// restore chip configuration
	chip->setConfig(tb.chipConfig);

//    cout<<"Press ENTER" << endl;
//    getchar();
    plotClose();

    // Write output data file
	for (int chargeDAC=0; chargeDAC<maxChargeDAC; chargeDAC++){
		outputFile << chargeDAC << " ";
		outputFile << chargeArray[chargeDAC] << " ";

		for (int ch=0; ch<128; ch++)
			outputFile << hitMatrix[chargeDAC][ch] << " ";
		outputFile << endl;
    }
    
    // Fitting data header
    cout << "Fitting data" << endl;
	outputFile << endl << "Ch Th ThErr ENC ENCerr" << endl;

	// Fit matrix data
	for (int ch=0; ch<128; ch++){
		chCfg = tb.chipConfig.CH[ch];
		for (int i=0; i<maxChargeDAC; i++)
			hitArray[i] = hitMatrix[i][ch];

		double th, ENCval;
		double thErr, ENCerr;

		int ret = Tutils::fitErf(numPulses, maxChargeDAC, chargeArray, hitArray, 
					&th, &thErr, &ENCval, &ENCerr);
	//	if (ret)
	//		cout << "ch:" << ch << " th:" << th << " fC - ENC:" << noise << "fC" << endl;
        
		if (ret && !chCfg.mask){
            thresholds[ch] = th;
            ENC[ch] = fabs(ENCval);
			outputFile 	<< ch << " " << th << " " << thErr << " " << ENCval << " " << ENCerr << endl;
			plotFile << ch << " " << ENCval*6250.0 << endl;
		} else {
            thresholds[ch] = 0;
            ENC[ch] = 0;
			outputFile 	<< "ch:" << ch << "-1 -1" << endl;
			plotFile << ch << " " << -1 << endl;
		}
	}

	outputFile.close();
	plotFile.close();

    cout << "Creating report" << endl;
    generateReport(filename);       // generate pdf report
	return true;
}


// parallel version of Scurves generation
bool TScurves::runParallel()
{
	uint32_t 						count;
	VFAT3chip::VFAT3config_t        chipCfg = tb.chipConfig;
	VFAT3chip::CalibrationConfig_t  calCfg = tb.chipConfig.CAL;
	VFAT3chip::ChannelConfig_t		chCfg;
	double	                        hitArray[maxChargeDAC];

	// Open output files
    string filename = string("Data/")+string("Scurves_")+Tutils::filenameSuffix();
	ofstream outputFile;
	outputFile.open (filename+".txt");
	ofstream plotFile;
	plotFile.open (filename+".dat");

	// enable cal bit on all channels
	for (int i=0; i<128; i++)
		chipCfg.CH[i].cal = true;
	chip->setConfig(chipCfg);

	// Set consumer function
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, histogramCB);

	// Setup pulses
	board->pulser->setConfig(10, 1000);	// 25 us (plus 250 ns) period

	// Send BC0
	board->comPortTx->sendBC0();

	// start run
	board->mRunControl->startRun();
	board->comPortTx->sendRunMode();

	// Fill charge array (~ 1.5 s)
    cout << "Fill charge array" << endl;
	for (int chargeDAC=0; chargeDAC<maxChargeDAC; chargeDAC++){
		// set calibration DAC
		if (calCfg.MODE == 1){
			calCfg.DAC = 255-chargeDAC;		// Vpulse mode
		} else if (calCfg.MODE == 2) {
			calCfg.DAC = chargeDAC;
		} else {
			cerr << "Please set Calibration MODE != 0" << endl;
			exit(0);
		}
		float injCharge= tb.getInjCharge(calCfg);
		chargeArray[chargeDAC] = injCharge;
    }

	// Loop on charge
	for (int chargeDAC=0; chargeDAC<maxChargeDAC; chargeDAC++){
		cout << chargeDAC << endl;
		outputFile << chargeDAC << " ";
		
		// set calibration DAC
		if (calCfg.MODE == 1)
			calCfg.DAC = 255-chargeDAC;		// Vpulse mode
		else if (calCfg.MODE == 2)
			calCfg.DAC = chargeDAC;
		else {
			cerr << "Please set Calibration MODE != 0" << endl;
			exit(0);
		}
		chip->addSetCalibrationConfig(calCfg);
		chip->execute();
		usleep(1000);

		// start pulses injection				
		board->pulser->run(numPulses);
	
		resetHistogram();
		long res;
		do {
			res = board->pollData(50);		// timeout 50 ms
			if (res == 0){
				cerr << "Timeout reading data" << endl;
				exit(0);
			}
		} while (evCounter<numPulses);

		for (int i=0; i<128; i++)
			outputFile << histogram[i] << " ";
		outputFile << endl;

   		// copy into the result matrix
		for (int ch=0; ch<128; ch++)
			hitMatrix[chargeDAC][ch] = histogram[ch];
	}

	board->mTriggerControl->getTriggerCounter(&count);
	cout << "Trigger count:" << count << endl;
	board->mRunControl->stopRun();
	board->comPortTx->sendSConly();
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, NULL);


    // Fitting data header
    cout << "Fitting data" << endl;
	outputFile << endl << "Ch Th ThErr ENC ENCerr" << endl;

	// Fit matrix data
	for (int ch=0; ch<128; ch++){
		chCfg = tb.chipConfig.CH[ch];
		for (int i=0; i<maxChargeDAC; i++)
			hitArray[i] = hitMatrix[i][ch];

		double th, ENCval;
		double thErr, ENCerr;

		int ret = Tutils::fitErf(numPulses, maxChargeDAC, chargeArray, hitArray, 
					&th, &thErr, &ENCval, &ENCerr);
	//	if (ret)
	//		cout << "ch:" << ch << " th:" << th << " fC - ENC:" << noise << "fC" << endl;
        
		if (ret && !chCfg.mask){
            thresholds[ch] = th;
            ENC[ch] = fabs(ENCval);
			outputFile 	<< ch << " " << th << " " << thErr << " " << ENCval << " " << ENCerr << endl;
			plotFile << ch << " " << ENCval*6250.0 << endl;
		} else {
            thresholds[ch] = 0;
            ENC[ch] = 0;
			outputFile 	<< "ch:" << ch << "-1 -1" << endl;
			plotFile << ch << " " << -1 << endl;
		}
	}

	outputFile.close();
    generateReport(filename);       // generate pdf report

	return true;
}

//
//  Plot replated functions
//
void TScurves::plotMapInit(Gnuplot *gp)
{
	gp->set_ylabel("INJ DAC");
	gp->set_xlabel("Channel");
	gp->set_xrange(0, 127);
	gp->set_yrange(0, maxChargeDAC-1);
	gp->set_zrange(0, 100);
    gp->set_cbrange(0, 100);
    gp->cmd("set style data pm3d");
    gp->cmd("set pm3d implicit at b");
    gp->cmd("set pm3d corners2color c1");
    gp->cmd("set view map");
    gp->cmd("unset surface");
    gp->cmd("set palette model RGB");
    gp->cmd("set palette defined ( 0 \"white\", 0.001 \"medium-blue\", 0.5 \"skyblue\", 0.99 \"orange\", 1 \"yellow\" )");
}

void TScurves::plotInit()
{
	gPlot = new Gnuplot("pm3d");
    plotMapInit(gPlot);
}

void TScurves::plotClose()
{
	delete gPlot;
}

void TScurves::plotUpdate()
{
	const int vectSize = 128*maxChargeDAC;
	std::vector<double> x (vectSize);
	std::vector<double> y (vectSize);
	std::vector<double> z (vectSize);

    int vIndex = 0;
	for (int ch=0; ch<128; ch++){
        for (int dac=0; dac<maxChargeDAC; dac++){
    		x[vIndex] = ch;
            y[vIndex] = dac;
            z[vIndex] = (hitMatrix[dac][ch]*100.0)/numPulses;
            vIndex++;
        }
    }

	gPlot->reset_plot();
	gPlot->plot_xyz(x, y, z, "S-Curves");
}

void TScurves::generateReport(string filename)
{
	Gnuplot *gp = new Gnuplot("pm3d");
#if HAS_GNUPLOT_PDF
    gp->cmd("set terminal pdf color size 7, 10");     // Set terminal PDF on A4 page
    *gp << "set output " << filename+"pdf";
#else
    // -sPAPERSIZE=a4
    gp->cmd("set terminal postscript eps enhanced color size 10, 7");     // Set terminal to postscript
    // -gNNNNxMMMM where NNNN is the width in pixels at 720 dpi (720 pixels == 1 inch), 
    // and MMMM is the height in pixels at 720 dpi
    string setOutput = "set output '|ps2pdf -r600 -dPDFFitPage -g7200x5040 - " + filename + ".pdf'";
//    string setOutput = "set output '" + filename + ".ps'";
    gp->cmd(setOutput);
#endif

    // Setup multiplot page
    gp->cmd("set multiplot layout 2,2 title \"S-Curves scan report\"");

    {
        // Plot S-Curves map
        plotMapInit(gp);
	    gp->set_yrange(chargeArray[0], chargeArray[maxChargeDAC-1]);
	    gp->set_ylabel("Charge [fC]");

	    const int vectSize = 128*maxChargeDAC;
	    std::vector<double> x (vectSize);
	    std::vector<double> y (vectSize);
	    std::vector<double> z (vectSize);
        int vIndex;

        // Plot Scurves map
        vIndex = 0;
	    for (int ch=0; ch<128; ch++){
            for (int dac=0; dac<maxChargeDAC; dac++){
        		x[vIndex] = ch;
                y[vIndex] = chargeArray[dac];
                z[vIndex] = (hitMatrix[dac][ch]*100.0)/numPulses;
                vIndex++;
            }
        }
	    gp->plot_xyz(x, y, z, "S-Curves");
    }

    // Plot overlaped curves
    {
        gp->cmd("set title \"S-Curves\" font \",18\"");
	    gp->set_xlabel("Charge [fC]");
	    gp->set_ylabel("Hits [%]");
	    gp->set_xrange(chargeArray[0], chargeArray[maxChargeDAC-1]);
	    gp->set_yrange(0, 100);
        gp->cmd("set size 0.5,0.45");
//        gp->cmd("set origin 0.5,0.54");
        gp->cmd("unset colorbox");
        gp->cmd("set style line 1 linetype 1 linecolor rgb \"red\" linewidth 2");

        std::ofstream tmp;
        std::string name;
        string cmdString = "plot ";

	    for (int ch=0; ch<128; ch++){
            name = gp->create_tmpfile(tmp);
            for (int dac=0, vIndex = 0; dac<maxChargeDAC; dac++, vIndex++){
                tmp << static_cast<float>(chargeArray[dac]) << " " << 
                        static_cast<float>((hitMatrix[dac][ch]*100.0)/numPulses) << std::endl;
            }
            tmp.flush();
            tmp.close();

            if (ch!=0)
                cmdString += ", ";
            cmdString += "\"" + name + "\" using 1:2 notitle smooth unique with lines linestyle 1";
        }
        gp->cmd( cmdString );
    }

    // Plot Thresholds
    {
        gp->cmd("set title \"Thresholds\" font \",18\"");
	    gp->set_xlabel("Channel");
	    gp->set_ylabel("Th [fC]");
	    gp->set_xrange(0, 128);
        gp->set_yautoscale();
        gp->cmd("set size 0.5,0.45");
        gp->cmd("unset colorbox");
        gp->cmd("set style line 1 linetype 1 linecolor rgb \"red\" linewidth 2");

        std::ofstream tmp;
        std::string name;
        string cmdString = "plot ";

        name = gp->create_tmpfile(tmp);
	    for (int ch=0; ch<128; ch++){
                tmp << static_cast<float>(ch) << " " << 
                        static_cast<float>(thresholds[ch]) << std::endl;
            }
        tmp.flush();
        tmp.close();

        cmdString += "\"" + name + "\" using 1:2 notitle smooth unique with lines linestyle 1";
        gp->cmd( cmdString );
    }

    // Plot ENC
    {
        gp->cmd("set title \"Noise\" font \",18\"");
	    gp->set_xlabel("Channel");
	    gp->set_ylabel("ENC [fC]");
	    gp->set_xrange(0, 128);
	    gp->set_yautoscale();
        gp->cmd("set size 0.5,0.45");
        gp->cmd("unset colorbox");
        gp->cmd("set style line 1 linetype 1 linecolor rgb \"red\" linewidth 2");

        std::ofstream tmp;
        std::string name;
        string cmdString = "plot ";

        name = gp->create_tmpfile(tmp);
	    for (int ch=0; ch<128; ch++){
                tmp << static_cast<float>(ch) << " " << 
                        static_cast<float>(ENC[ch]) << std::endl;
            }
        tmp.flush();
        tmp.close();

        cmdString += "\"" + name + "\" using 1:2 notitle smooth unique with lines linestyle 1";
        gp->cmd( cmdString );
    }

    // close the plot
    gp->cmd("unset multiplot");
    sleep(1);
    delete (gp);
}

