#ifndef RMENCODE_H
#define RMENCODE_H

class rmencode
{
public:
    rmencode();
    ~rmencode();
    bool run(int r, int m, int data_in, int *data_out);

private:
	void cleanup();
};

#endif
