#ifndef RMDECODE_H
#define RMDECODE_H

class rmdecode
{
public:
    rmdecode();
    ~rmdecode();
    bool run(int r, int m, int *data_in, int *data_out);

private:
	void cleanup();
};

#endif
