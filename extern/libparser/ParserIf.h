#ifndef ParserIf_First
#define ParserIf_First

#include "ParserExpr.h"

class ParserElseIf;
class ParserElse;

class ParserIf: public ParserExpr
{
	Seq<ParserExpr*> condition;
	Seq<ParserExpr*> action;
public:
	ParserIf(ParserExpr *);

	virtual Parser * Match(void);
	virtual value Evaluate(void);

	virtual Parser * Copy(void);

	friend ParserIf & IF(ParserExpr & condition);

	ParserIf & operator()(ParserExpr & action);
	ParserIf & operator()(const value & action);

	virtual ParserExpr & operator,(ParserExpr &);
	virtual ParserExpr & operator,(ParserElseIf &);
	virtual ParserExpr & operator,(ParserElse &);
};

class ParserElseIf 
{
	friend class ParserIf;

	ParserExpr * condition;
	ParserExpr * action;

	ParserElseIf(ParserExpr *);

public:
	friend ParserElseIf & ELSEIF(ParserExpr & condition);

	ParserElseIf & operator()(ParserExpr & action);
	ParserElseIf & operator()(const value & action);
};

class ParserElse 
{
	friend class ParserIf;

	ParserExpr * action;

	ParserElse(ParserExpr * );
public:

	friend ParserElse & ELSE(ParserExpr & p2);
	friend ParserElse & ELSE(const value & p2);
};

#endif /* ParserIf_First */
