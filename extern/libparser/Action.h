#ifndef Action_First
#define Action_First

#include "Parser.h"

typedef value (*ActionFn)(Context);

class Action: public Parser
{
protected:
	ActionFn fn;

public:
	Action(const Action &);
	Action(ActionFn func);

	virtual Parser * Match(void);
	virtual value Evaluate(void);

	virtual ~Action(void);

	virtual Parser * Copy(void);

// USEFUL STATS

	virtual int Length(void) const;
	virtual int IsLeftRecursive(Parser * root);

};

Parser & operator+(Parser&,ActionFn);
Parser & operator+(ActionFn,Parser&);

Parser & operator|(Parser&,ActionFn);
Parser & operator|(ActionFn,Parser&);

#endif /* Action_First */
