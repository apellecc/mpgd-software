#ifndef ParserOstream_First
#define ParserOstream_First

#include "ParserExpr.h"
#include "ParserVar.h"
#include <iostream>

class ParserOutput;

class ParserOstream
{
protected:
	std::ostream & os;
public:
	ParserOstream(std::ostream &);

	ParserOutput & operator<<(ParserExpr & pe);
	ParserOutput & operator<<(const value & v);
};

class ParserOutput: public ParserExpr
{
protected:
	friend class ParserOstream;

	std::ostream & os;
	Seq<ParserExpr*> arg;
	ParserOutput(std::ostream &os, ParserExpr * pe = 0);
public:
	ParserOutput & operator<<(ParserExpr & pe);
	ParserOutput & operator<<(const value & v);

	virtual Parser * Match(void);
	virtual value Evaluate(void);

	virtual Parser * Copy(void);
};

extern ParserVar ENDL;

#endif /* ParserOstream_First */
