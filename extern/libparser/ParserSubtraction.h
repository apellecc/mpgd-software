#ifndef ParserSubtraction_First
#define ParserSubtraction_First

#include "ParserExpr.h"

class ParserSubtraction : public ParserExpr					
{								
public:								
	ParserSubtraction (ParserExpr * , ParserExpr * );			
								
	virtual Parser * Match(void);				
	virtual value Evaluate(void);				
								
	virtual Parser * Copy(void);				
								
};								
								
ParserSubtraction & operator - (ParserExpr & p1, ParserExpr & p2);		
ParserSubtraction & operator - (const value & p1, ParserExpr & p2);		
ParserSubtraction & operator - (ParserExpr & p1, const value & p2);				

#endif /* ParserSubtraction_First */
