#ifndef pstream_First
#define pstream_First

#include <iostream>
// #include "String.h"
#include "RX.h"
#include "Seq.h"

typedef int Mark;

class pstream
{
private:
	enum State { PS_GOOD = 0, PS_FAIL = 1, PS_BAD = 2, PS_EOF = 4 };

	State state; 

	std::string data;  		// STORES ACCUMULATED CHARS
	int nextchar;		// INDEX OF NEXT CHAR TO BE RETURNED BY >>

	Seq<std::istream*> isp;	// STACK OF OPTIONAL CHAR SOURCES
	Seq<int> firstchar;	// STACK OF FIRST CHAR POSITIONS FOR CHAR SRCS
	Seq<std::string> filename;	// STACK OF FILENAMES FOR CHAR SRCS

	Seq<RX> keyword;      	  // LIST OF KEYWORDS KNOWN TO pstream
	Seq<int> keywordEnabled;  // ARE KWYWORDS ACTIVE?

	int autoprompt;		// WHETHER TO PROMPT ON LOADING CHARS
				// (FALSE BY DEFAULT)

	std::string promptstr;	// STRING TO BE USED AS PROMPT
	std::string promptonstr;	// LIST OF CHARS ON WHICH TO PROMPT

	int (*promptonfcn)(pstream&,char);	// POINTER TO FUNCTION TO DETERMINE IF
						// PROMPTING REQUIRED FOR GIVEN CHAR
	std::string (*promptfcn)(pstream&,char);	// POINTER TO FUNCTION TO GENERATE
						// PROMPT

	std::string lastread;	// STORES THE TEXT OF THE LAST SUCCESSFUL READ
	int lastchar;			// position of last char read from stream

// DEACTIVATE ASSIGNMENT

	pstream& operator= (const pstream & p);

public:

// BY DEFAULT A pstream DOES NOT READ FROM ANY SOURCE (THINKS IT'S AT eof)

	pstream(void);

// MAY ALSO READ FROM A NAMED FILE (asstring==0) OR DIRECTLY FROM A std::string (asstring==1)

	pstream(std::string str, int asstring = 0);

// CAN BIND A pstream TO AN EXISTING istream, ifstream, istrstream, ETC

	pstream(std::istream & m);			 

// CAN CONVERT PROGRAM ARGS INTO A SINGLE std::string AND READ FROM THAT

	pstream(int argc, char ** argv);			 


	pstream(const pstream & p);
	~pstream(void);

// CAN OPEN SOURCES HIERARCHICALLY
// RETURNS 0 ON FAILURE AND RETAINS EXISTING SOURCE IN THAT CASE
// OTHERWISE RETURNS LEVEL OF SOURCE (STARTING AT 1 FOR THE FIRST OPENED)
// autoclose INDICATES IF CURRENT SOURCE SHOULD AUTOMATICALLY CLOSE (AND
// REVERT) ON EOF, NOTE: IGNORED IF LEVEL == 1
// NOTE THAT THESE FUNCTIONS PARALLEL THE THREE NON-DEFAULT CTORS ABOVE

	int Open(std::string str, int asstring = 0);
	int Open(std::istream & m);			 
	int Open(int argc, char ** argv);			 

// CLOSING SOURCES REVERTS pstream TO PREVIOUS SOURCES (IF ANY)
// RETURNS THE LEVEL OF THE SOURCE REVERTED TO (0 INDICTES NO SOURCE)
// NOTE, CAN STILL BACKTRACK INTO CLOSED SOURCES (!)

	int Close(void);

// REPORT CURRENT SOURCE LEVEL

	int SourceLevel(void);

// Marks INDICATE THE CURRENT (get) POSITION OF THE pstream
// LIKE SEEKING ONLY WITH BETTER NAMES :-)

	Mark GetMark(void);
	Mark FirstMark(void);
	Mark SetMark(Mark m);

// INDICATE FAILURE OR SUCCESS, OPTIONALLY ALTERING THE get POSITION

	pstream& SetFail(Mark m);
	pstream& SetFail(void);

	pstream& SetGood(Mark m);
	pstream& SetGood(void);

// CONVERSION TO ALLOW "if (ps>>var)..."

	operator void*(void);

// CURRENT STATE OF pstream

	int eof(void);
	int good(void);
	int bad(void);
	int fail(void);

// CONTROL TOKENIZING BEHAVIOUR 
// DEFINE A PATTERN AS A KEYWORD
// PRECEDENCE OF KEYWORDS IS DETERMINED BY ORDER OF DEFINITION (FIRST->HIGHEST)

	int Keyword(const std::string & pattern);

// TURN OFF pattern AS A KEYWORD (MAY BE TURNED ON AT ITS ORIGINAL PRECEDENCE
// BY ANOTHER CALL TO Keyword()

	void NoKeyword(const std::string & pattern);

// RETURN NON-ZERO IF INPUT STREAM COULD MATCH THE SPECIFIED KEYWORD
// IE: IF STREAM DOES NOT MATCH A KEYWORD DEFINED BEFORE THIS PATTERN

	int CouldMatch(const std::string & pattern);

// READ THE NEXT CHARACTER (DOES _NOT_ SKIP WHITESPACE)

	pstream& get(char& c);

	int get(void);

// SKIP WHITESPACE

	pstream& eatwhite(void);

// STANDARD INPUT OPERATORS

	pstream& operator>>(char& c);
	pstream& operator>>(int& i);
	pstream& operator>>(long& i);
	pstream& operator>>(float& f);
	pstream& operator>>(double& d);
	pstream& operator>>(std::string& s);
	pstream& operator>>(RX& s);


// THIS putback ALLOWS ARBITRARY AMOUNTS OF PUTTING BACK
// (THE PARAMETER IS FOR UNIFORMITY WITH OTHER STREAMS AND IS IGNORED)

	pstream& putback(char c);

// DETERMINE WHETHER THE CURRENT INPUT SOURCE IS INTERACTIVE
// (USEFUL FOR TOGGLING PROMPTING)

	int IsInteractive(void);

// SUPPLY A SET OF CHARS WHICH CAUSE A PROMPT TO BE ISSUED (USUALLY "\n")

	pstream& PromptOn(const std::string & promptchars,int onlyIfInteractive = 0);

// SUPPLY A FCN WHICH TAKES A pstream& AND A char AND DETERMINES WHETHER A PROMPT IS ISSUED
// EG: int PromptOnNL(pstream &ps,char c) { return c=='\n'; }
//     ps.PromptOn(PromptOnNL);
// NOTE THAT THE pstream& IS ALWAYS A REFERENCE TO THE PROMPTING OBJECT

	pstream& PromptOn(int (*fp)(pstream&,char),int onlyIfInteractive = 0);

// SUPPLY A STRING TO BE USED AS THE PROMPT

	pstream& PromptWith(const std::string & prompt);

// SUPPLY A FCN WHICH TAKES A pstream& AND A char AND DETERMINES THE PROMPT TO BE USED 
// (THE FCN IS ONLY CALLED IF A PROMPT IS TO BE ISSUED AND NEED ONLY COVER
//  THOSE CASES SPECIFIED BY THE PromptOn() STRING OR FCN) THE pstream& IS A REFERENCE
// TO THE pstream OBJECT WHICH IS ABOUT TO PROMPT

	pstream& PromptWith(std::string (*fp)(pstream&,char));

// MANUALLY ISSUE A PROMPT (FOR CHAR c, IF A PROMPT FCN IS IN USE)

	pstream& Prompt(char c = '\0');

// SWITCH ON/OFF AUTOMATIC PROMPTING

	pstream& AutoPrompt(int ap = 1);

// EXAMINE THE CONTENTS OF THE pstream
// 1. RETURN A STRING CONTAINING ANY REMAINING DATA (UP TO THE FIRST CHAR
//    APPEARING IN tochars IF IT IS SPECIFIED)

	std::string NextData(void);
	std::string NextData(std::string topattern); 

// 2. LineNumber RETURNS THE NUMBER OF THE CURRENT LINE, STARTING AT 1
//    (IE: RETURN 1 + THE NUMBER OF '\n's BEFORE THE CURRENT get POSITION)
//    FileLineNumber GIVES LINE NUMBER WITHIN CURRENT INPUT SOURCE
//    FileName RETURNS NAME OF CURRENT FILE

	int LineNumber(void);
	int FileLineNumber(void);

	std::string FileName(void);

// 3. RETURN A STRING CONTAINING THE TEXT OF THE CURRENT LINE
//    (IE: THE TEXT BETWEEN THE PREVIOUS '\n' (OR THE START OF FILE)
//         AND THE NEXT '\n' (OR EOF))

	std::string Line(void);

// 4. RETURN A STRING CONTAINING THE TEXT OF THE LINE lineno

	std::string Line(unsigned int lineno);

// 5. RETURN A STRING CONTAINING THE TEXT OF THE LAST SUCCESSFUL >> OPERATION

	std::string LastRead(void);

// 6. RETURN THE NUMBER OF THE LINE READ WITH ERROR
	int ErrorLineNum(void);

// Print a message with errol line number, line content and error position marker	
	void PrintErrorLine();
};

extern pstream pin;

#endif /* pstream_First */
