#ifndef ParserMultiplication_First
#define ParserMultiplication_First

#include "ParserExpr.h"

class ParserMultiplication : public ParserExpr					
{								
public:								
	ParserMultiplication (ParserExpr * , ParserExpr * );			
								
	virtual Parser * Match(void);				
	virtual value Evaluate(void);				
								
	virtual Parser * Copy(void);				
								
};								
								
ParserMultiplication & operator * (ParserExpr & p1, ParserExpr & p2);		
ParserMultiplication & operator * (const value & p1, ParserExpr & p2);		
ParserMultiplication & operator * (ParserExpr & p1, const value & p2);				

#endif /* ParserMultiplication_First */
