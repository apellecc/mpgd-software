#ifndef Rule_First
#define Rule_First

#include "RuleBase.h"

// WRAPPER CLASS FOR PARSERS

class Rule: public RuleBase
{
	friend Parser& Opt(Parser&,int);

protected:
	Parser * therule;


public:
	Rule(const Rule & r);
	Rule(void);
	Rule(std::string rulename);
	~Rule(void);

// INITIATE PARSING THROUGH THIS RULE

	int operator () (pstream& ps,int evaluate = 1);

	virtual Parser * Match(void);
	virtual value Evaluate(void);

	virtual int Length(void) const;
	virtual int IsLeftRecursive(Parser * root);

// DEEP COPY FUNCTION

	virtual Parser * Copy(void) ;

// ACCESS TO COMPONENTS

	virtual Parser * operator[](int i);

// ALGEBRA ON RULES

	virtual Parser & operator | ( Parser &);
	virtual Parser & operator + ( Parser &);

// ASSIGNMENT TO A RULE (CHECKS FOR LEFT RECURSION)

	Rule& operator=(Parser& p);

// MATCHING CRITERION

	virtual void MatchOn(RuleComparator matchrule);

};

#endif /* Rule_First */
