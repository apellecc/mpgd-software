#ifndef RX_First
#define RX_First

// #include "String.h"
#include "string"

class DFA;

class RX
{
private:
	std::string pattern;
	DFA *recognizer;
	int patternerror;
	int isnull;

	void ConvertSpecials(std::string& str);

public:

	enum Outcome {
		       Failed,		// DID NOT MATCH BECAUSE OF ILLEGAL TRANSITION
		       Short,		// DID NOT MATCH BECAUSE DID NOT REACH AN ACCEPTOR STATE
		       Exact,		// MATCHED AND FINISHED IN AN ACCEPTOR STATE
		       Long,		// MATCHED BUT DID NOT FINISH IN AN ACCEPTOR STATE
		       LongFailed,	// REACHED AN ACCEPTOR STATE BUT FOUND A SUBSEQUENT ILLEGAL TRANSITION
		       Invalid		// UNABLE TO MATCH (RX NOT PROPERLY DEFINED)
		       };

// CONSTRUCTORS/DESTRUCTOR

	RX(void);
	RX(std::string pat);
	RX(const RX & rx);

	~RX(void);

// ASSIGNMENT

	RX& operator = (const RX & rx);

// IS THIS RX VALID?

	operator int (void) const;

// ORIGINAL PATTERN STRING

	operator std::string (void) const;

// C-LIKE DEFINITION OF EQUIVALENT FSM

	std::string FSM(void) const;

// ATTEMPT TO MATCH A GIVEN STRING

	Outcome Match(std::string str);

// ATTEMPT TO MATCH SUCCESSIVE CHARACTERS SUPPLIED BY A getchar()-LIKE FUNCTION

	Outcome Match(int (*charsrc)(void));
 
// RETURN THE 0-BASED INDEX OF THE LAST CHARACTER MATCHED IN THE LAST MATCH OPERATION
// WILL BE: < str.length() FOR OUTCOMES Failed AND Long
//          = str.length() FOR OUTCOMES Short AND Exact

	int MatchedTo(void) const;
	int MatchedAt(void) const;
	int FailedAt(void) const;

// OUTPUT

	friend std::ostream& operator<<(std::ostream& os, const RX & rx);

// EQUALITY OF RXS (IFF PATTERNS ARE SYNTACTICALLY IDENTICAL)

	int operator == (const RX & rx);
};


#endif /* RX_First */
