#ifndef ParserExec_First
#define ParserExec_First

#include "ParserExpr.h"
#include "ParserVar.h"
#include "Action.h"
#include <iostream>

class ParserExecOutput;

typedef void (*UserActionFn)(void*);

class ParserExec
{
protected:
	UserActionFn	func;
	void * 			userContext;

public:
	ParserExec(std::string &);
	ParserExec(UserActionFn theFunc, void *userCtx);
	UserActionFn	Func() { return func; }
	void *			UserContext() { return userContext; }
};

class ParserExecOutput: public ParserExpr
{
protected:
	friend class ParserExec;

	UserActionFn 		func;
	void * 			userContext;
	Seq<ParserExpr*> arg;

public:
	ParserExecOutput(UserActionFn theFunc, void *userCtx, ParserExpr * pe = 0);
	ParserExecOutput & operator<<(ParserExpr & pe);
	ParserExecOutput & operator<<(const value & v);

	virtual Parser * Match(void);
	virtual value Evaluate(void);

	virtual Parser * Copy(void);
};

Parser & operator+(ParserExec& pe, Parser& p);
Parser & operator+(Parser& p, ParserExec& pe);


#endif /* ParserExec_First */
