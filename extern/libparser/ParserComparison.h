#ifndef ParserComparison_First
#define ParserComparison_First

#include "ParserExpr.h"

class ParserComparison: public ParserExpr
{
public:
	ParserComparison(ParserExpr * , ParserExpr * );

	virtual Parser * Match(void);
	virtual value Evaluate(void);

	virtual Parser * Copy(void);

};

ParserComparison & operator==(ParserExpr & p1, ParserExpr & p2);
ParserComparison & operator==(const value & p1, ParserExpr & p2);
ParserComparison & operator==(ParserExpr & p1, const value & p2);

#endif /* ParserComparison_First */
