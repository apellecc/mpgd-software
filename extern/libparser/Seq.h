#ifndef Seq_First
#define Seq_First

#include <iostream>
#include <stdlib.h>


// *****************************************************************************
// ARBITRARY ARRAY-BASED SEQUENCE OF Ts
// *****************************************************************************

// USEFUL PSEUDOCOMMAND

#define	foreach(var,seq)   for (var=0;var<seq.Count();var++)

// NEED THIS CLASS TO CORRECTLY HANDLE CTORS FOR Seq<int>

struct SeqSize	// SHOULD BE NESTED BUT CFRONT CAN'T YET
{
	int size;
	SeqSize(int s) : size(s) {}
};

// HERE'S A GENERAL COMPARISON FUNCTION TO USE IF NONE IS PROVIDED

template <class T>
int operator==(const T&, const T&)
{
	return 0;
}

// SEQUENCE CLASS

template <class T>
class Seq
{
private:
	int size;
	int count;
	T *element;

	void resize(void);

public:
	Seq(void);
	Seq(SeqSize initsize);
	Seq(T p);
	Seq(const Seq & s);

	~Seq(void);

	Seq & operator = (const T & p);
	Seq & operator = (const Seq & s);

	int Count(void) const;

	T & operator[] (int index);
	T   operator[] (int index) const;

	T & First(void);
	T   First(void) const;

	T & Last(void);
	T   Last(void) const;

	int Contains(T t);

	Seq & operator += (Seq s); 
	Seq & operator += (T p);

	Seq & operator++ (void);
	Seq & operator++ (int);
	Seq & operator-- (void);
	Seq & operator-- (int);

	Seq & Reset(void);

	Seq & Map(void (*)(T &));
	Seq & Map(void (*)(T));
};

// *****************************************************************************
// ITERATOR FOR SEQ OBJECT (SMART POINTER MODEL)
// *****************************************************************************

template <class T>
class IterSeq
{
private:
	Seq<T> & sequence;
	int nextindex;
public:
	IterSeq(const Seq<T> & s) ;
	IterSeq(Seq<T> & s);

	const IterSeq & operator++ (void) const;

	operator const int(void) const;

	T* operator->(void) ;
	const T* operator->(void) const ;

	T operator*(void) const;
	T & operator*(void);

	void Rewind(void) const;
};


template <class T>
Seq<T> & Seq<T>::Reset(void)
{
	count = 0;
	return *this;
}

template <class T>
void Seq<T>::resize(void)
{ if (count>=size) {
	T *newelement = new T [size+=10];
	int i;
	for (i=0;i<count;i++) {
		newelement[i] = element[i];
		}
	delete [] element;
	element = newelement;
	}
  }

template <class T>
Seq<T>::Seq(void)
{ size= count= 0;
  element = 0;
  }

template <class T>
Seq<T>::Seq(SeqSize initsize)
{ size= initsize.size;
  count = 0;
  element = new T [size];
  }

template <class T>
Seq<T>::Seq(T p)
{ size= count= 1;
  element = new T [size];
  element[0] = p;
  }

template <class T>
Seq<T>::Seq(const Seq<T> & s)
{ size= count= s.count;
  element = new T [size];
  int i;
  for (i=0;i<s.count;i++) {
	element[i] = s.element[i];
	}
  }

template <class T>
Seq<T>::~Seq(void)
{ delete [] element; }

template <class T>
Seq<T> & Seq<T>::operator = (const T & p)
{ size= count= 1;
  delete [] element;
  element = new T [size];
  element[0] = p;
  return *this;
  }

template <class T>
Seq<T> & Seq<T>::operator = (const Seq<T> & s)
{ size= count= s.count;
  delete [] element;
  element = new T [size];
  int i;
  for (i=0;i<s.count;i++) {
	element[i] = s.element[i];
	}
  return *this;
  }

template <class T>
int Seq<T>::Count(void) const
{ return count; }

template <class T>
T & Seq<T>::operator[] (int index)
{ if (index<0||index>=count){
	std::cerr << "Fatal error: Index of Seq [" << index << "] out of range (0.." << count-1 << ")" << std::endl;
	exit(1);
	}
  else {
	return element[index];
	}
  }

template <class T>
T   Seq<T>::operator[] (int index) const
{ if (index<0||index>=count){
	std::cerr << "Fatal error: Index of Seq [" << index << "] out of range (0.." << count-1 << ")" << std::endl;
	exit(1);
	}
  else {
	return element[index];
	}
  }

template <class T>
T & Seq<T>::First(void)
{ if (count<0){
	std::cerr << "Fatal error: Seq has no first element" << std::endl;
	exit(1);
	}
  else {
	return element[0];
	}
  }

template <class T>
T   Seq<T>::First(void) const
{ if (count<0){
	std::cerr << "Fatal error: Seq has no first element" << std::endl;
	exit(1);
	}
  else {
	return element[0];
	}
  }

template <class T>
T & Seq<T>::Last(void)
{ if (count<0){
	std::cerr << "Fatal error: Seq has no last element" << std::endl;
	exit(1);
	}
  else {
	return element[count-1];
	}
  }

template <class T>
T   Seq<T>::Last(void) const
{ if (count<0){
	std::cerr << "Fatal error: Seq has no last element" << std::endl;
	exit(1);
	}
  else {
	return element[count-1];
	}
  }

template <class T>
Seq<T> & Seq<T>::operator += (Seq<T> s) 
{ int i;
  for (i=0;i<s.count;i++) {
	resize();
	element[count++] = s[i];
	}
  return *this;
  }

template <class T>
Seq<T> & Seq<T>::operator += (T p)
{ resize();
  element[count++] = p;
  return *this;
  }

template <class T>
Seq<T> & Seq<T>::operator++ (void)
{ resize();
  count++;
  return *this;
  }

template <class T>
Seq<T> & Seq<T>::operator-- (void)
{ count--;
  return *this;
  }

template <class T>
Seq<T> & Seq<T>::operator++ (int)
{ resize();
  count++;
  return *this;
  }

template <class T>
Seq<T> & Seq<T>::operator-- (int)
{ count--;
  return *this;
  }

template <class T>
int Seq<T>::Contains(T t)
{
	int i;
	for (i=0;i<count;i++) {
		if (t == element[i]) {
			return i+1;
			}
		}
	return 0;
}

template <class T>
Seq<T> & Seq<T>::Map(void (*fcn)(T &))
{
	int i;
	for (i=0;i<count;i++) {
		fcn(element[i]);
		}
	return *this;
}

template <class T>
Seq<T> & Seq<T>::Map(void (*fcn)(T))
{
	int i;
	for (i=0;i<count;i++) {
		fcn(element[i]);
		}
	return *this;
}

template <class T>
IterSeq<T>::IterSeq(const Seq<T> & s) 
:sequence((Seq<T> &)s)
{nextindex = 0;}

template <class T>
IterSeq<T>::IterSeq(Seq<T> & s)
:sequence(s)
{nextindex = 0;}

template <class T>
const IterSeq<T> & IterSeq<T>::operator++ (void) const
{
	((IterSeq<T>*)this)->nextindex++;
	return *this;
}

template <class T>
IterSeq<T>::operator const int (void) const
{
  return (nextindex<sequence.Count());
}

template <class T>
T* IterSeq<T>::operator->(void) 
{ if (nextindex<sequence.Count()) {
	return &(sequence[nextindex]);
	}
  else {
	std::cerr << "Fatal error: No more in iterated Seq" << std::endl;
	exit(1);
	}
  }

template <class T>
const T* IterSeq<T>::operator->(void) const
{ if (nextindex<sequence.Count()) {
	return &(sequence[nextindex]);
	}
  else {
	std::cerr << "Fatal error: No more in iterated Seq" << std::endl;
	exit(1);
	}
  }

template <class T>
T IterSeq<T>::operator*(void) const
{ if (nextindex<sequence.Count()) {
	return sequence[nextindex];
	}
  else {
	std::cerr << "Fatal error: No more in iterated Seq" << std::endl;
	exit(1);
	}
  }

template <class T>
T & IterSeq<T>::operator*(void)
{ if (nextindex<sequence.Count()) {
	return sequence[nextindex];
	}
  else {
	std::cerr << "Fatal error: No more in iterated Seq" << std::endl;
	exit(1);
	}
  }

template <class T>
void IterSeq<T>::Rewind(void) const
{ ((IterSeq<T> *)this)->nextindex = 0; }


#endif /* Seq_First */
