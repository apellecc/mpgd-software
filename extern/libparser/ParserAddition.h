#ifndef ParserAddition_First
#define ParserAddition_First

#include "ParserExpr.h"

class ParserAddition : public ParserExpr					
{								
public:								
	ParserAddition (ParserExpr * , ParserExpr * );			
								
	virtual Parser * Match(void);				
	virtual value Evaluate(void);				
								
	virtual Parser * Copy(void);				
								
};								
								
ParserAddition & operator + (ParserExpr & p1, ParserExpr & p2);		
ParserAddition & operator + (const value & p1, ParserExpr & p2);		
ParserAddition & operator + (ParserExpr & p1, const value & p2);				

#endif /* ParserAddition_First */
