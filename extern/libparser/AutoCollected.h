#ifndef AutoCollected_First
#define AutoCollected_First

#include <stddef.h>
#include <unistd.h>
#include <iostream>

class AutoCollected 
{
private:
	struct Link
	{
		char partition;
		AutoCollected * obj;
		Link * next;
		Link(void);
		~Link(void);
		static void * operator new(size_t size);			
		static void operator delete(void* ptr);				
	};

	static long AC_Count;
	static long AC_Heap;

	static char AC_Dynamic;

	static Link* AC_Head;

	char AC_IsDynamic;
	Link* AC_Link;
	
protected:

	AutoCollected(void);

	static void SetMarker(void);
	static void ReclaimToMarker(void);


public:									
	static void * operator new(size_t size);			
	static void operator delete(void* ptr);				

	virtual ~AutoCollected(void);

	static void Status(void);
};

#endif /* AutoCollected_First */
