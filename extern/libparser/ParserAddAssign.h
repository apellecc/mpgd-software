#ifndef ParserAddAssign_First
#define ParserAddAssign_First

#include "ParserExpr.h"

class ParserAddAssign : public ParserExpr
{
public:
	ParserAddAssign (ParserExpr * , ParserExpr * );

	virtual Parser * Match(void);
	virtual value Evaluate(void);

	virtual Parser * Copy(void);

};

#endif /* ParserAddAssign_First */
