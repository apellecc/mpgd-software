#ifndef ParserPreIncr_First
#define ParserPreIncr_First

#include "ParserExpr.h"

class ParserPreIncr : public ParserExpr					
{								
public:								
	ParserPreIncr (ParserExpr *);			
								
	virtual Parser * Match(void);				
	virtual value Evaluate(void);				
								
	virtual Parser * Copy(void);				
								
};								
								
#endif /* ParserPreIncr_First */
