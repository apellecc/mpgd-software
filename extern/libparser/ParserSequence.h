#ifndef ParserSequence_First
#define ParserSequence_First

#include "ParserExpr.h"

class ParserSequence: public ParserExpr
{
public:
	ParserSequence(ParserExpr * , ParserExpr * );

	virtual Parser * Match(void);
	virtual value Evaluate(void);

	virtual Parser * Copy(void);

	virtual ParserExpr & operator,(ParserExpr &);
	virtual ParserExpr & operator,(class ParserElseIf &);
	virtual ParserExpr & operator,(class ParserElse &);
};

#endif /* ParserSequence_First */
