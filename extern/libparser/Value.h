#ifndef Value_First
#define Value_First

#include <iostream>
#include <string>
// #include "String.h"
#include <ctype.h>
#include <stdlib.h>

// UNCONSTing CONSTS

// #define  UNCONST(type)	((type*)this)->

// PURE VIRTUALS

#define  PURE	= 0

// FRIENDS

class value;

std::ostream & operator<<(std::ostream& os, const value & v);
std::istream & operator>>(std::istream& is, value & v);

value operator&(value v1,value v2);
value operator+(value v1,value v2);
value operator-(value v1,value v2);
value operator*(value v1,value v2);
value operator/(value v1,value v2);
value operator%(value v1,value v2);

class value 
{
private:
	std::string mystring;
	long mylong;
	double mydouble;

	enum valueType {UNDEFINED,
			STRING,
			INTEGER,
			REAL
			};

	valueType type;

	double fmod(double n,double d);

	enum PrintWhat { NONE = 0,
			 QUOTES = 0x1, 
			 UNDEFS = 0x2
			 };
			 
	int printspecial;

	static int PrintAllSpecial(int set, int flag);

public:
	value(void);

	value(std::string str);

	value(char * cp);

	value(char c);

	value(long  lng);
	value(int   lng);
	value(short lng);

	value(void *t);

	value(double dbl);

	void PrintUndefs(int on = 1);
	void PrintQuotes(int on = 1);
		
	static void PrintAllUndefs(int on = 1);
	static void PrintAllQuotes(int on = 1);
		

	int PrintingQuotes(void) const;
	int PrintingUndefs(void) const;
		
	operator std::string () const;
	void* operator!(void) const;
	operator void*(void) const;

	std::string AsString(void) const;
	void* AsBoolean(void) const;
	long AsInt(void) const;
	double AsReal(void) const;

	int IsNumber(void) const;

	value & ConvertToNumber(void);
	value & ConvertToString(void);

	char operator[](int index) const;
	char& operator[](int index); 

	void* operator&&(const value & v2) const;
	void* operator||(const value & v2) const;
	void* operator==(const value& v2) const;
	void* operator==(int i) const;
	void* operator!=(const value& v2) const;
	void* operator<(const value& v2) const;
	void* operator<=(const value& v2) const;
	void* operator>(const value& v2) const;
	void* operator>=(const value& v2) const;

	value operator++(void);
	value operator++(int postfix);
	value operator--(void);
	value operator--(int postfix);

	value & operator=(const value & v2);

// STRING CONCATENATION OPERATOR

	value & operator&=(const value& v2);

// ARITHMETIC OPERATORS

	value operator+(void) const;
	value operator-(void) const;

	value & operator+=(const value & v2);
	value & operator-=(const value & v2);
	value & operator*=(const value & v2);
	value & operator/=(const value & v2);
	value & operator%=(const value & v2);

	friend std::istream & operator>>(std::istream& is, value & v);
	friend std::ostream & operator<<(std::ostream& os,const value & v);

	friend value operator+(value v1,value v2);
	friend value operator-(value v1,value v2);
	friend value operator*(value v1,value v2);
	friend value operator/(value v1,value v2);
	friend value operator%(value v1,value v2);

// STRING CONCATENATION

	friend value operator&(value v1,value v2);

};

#endif /* Value_First */
