#ifndef Token_First
#define Token_First

#include "pstream.h"
// #include "String.h"
#include "Value.h"
#include "RX.h"
#include "Parser.h"

class Token: public Parser
{
private:

// DISABLE EMPTY TOKENS

	Token(void) ;

protected:

	static std::string defPatLeader;

	std::string patLeader;
	RX pattern;
	std::string match;

public:

	Token(std::string str);
	Token(RX pat);
	Token(std::string leader,std::string str);
	Token(std::string leader,RX pat);

// MATCH A TERMINAL, RETURNING THE USED PORTION OF THE INPUT

	virtual Parser * Match(void);

// EVALUATE A TOKEN 

	virtual value Evaluate(void);

// GIVE USEFUL STATS ABOUT IT

	virtual int Length(void) const;

// DEEP COPY FUNCTION

	virtual Parser * Copy(void);

// MANAGE TOKEN LEADERS

	static void SetDefaultLeader(std::string newdef);
	static std::string DefaultLeader(void);

	void SetLeader(std::string newdef);
	virtual std::string Leader(void) const;

	operator std::string(void) const;
};

#endif /* Token_First */
