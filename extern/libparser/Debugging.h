#ifndef Debugging_First
#define Debugging_First

#include <iostream>

// #define EBUG 

#ifdef EBUG
#define DEBUG *messageLog << Parser::ContextHeader()
// #define DEBUG std::cerr << std::endl << Parser::ContextHeader()
#else
#define DEBUG if (0) std::cerr << Parser::ContextHeader()		
#endif  /* EBUG */

class Debugging 
{
protected:

#ifdef EBUG
	static std::ostream * messageLog;
#endif  /* EBUG */

public:

#ifdef EBUG
	// CONTROL DEBUGGING
	static int Debug(int condition = 1);
	static int Debug(const std::string & envvar);
	static int DebugTo(const std::string & filename);
	static int DebugTo(std::ostream &);
#endif  /* EBUG */

};

#endif /* Debugging_First */
