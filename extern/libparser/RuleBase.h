#ifndef RuleBase_First
#define RuleBase_First

#include "Parser.h"

// FUNCTIONS TO COMPARE TWO PARSERS ACCORDING TO SOME CRITERION
// RETURNS NON-ZERO IFF SECOND PARSER "BETTER" THAN FIRST

int LongestMatch(const Parser * incumbent,const Parser * challenger);
int FirstMatch(const Parser * incumbent,const Parser * challenger);

// BASE CLASS FOR Rule, DisjunctiveRule AND SequentialRule

class RuleBase : public Parser
{
protected:
	RuleComparator betterMatch;
	static RuleComparator defBetterMatch;

public:
	RuleBase(const RuleBase &);
	RuleBase(void);

	virtual Parser * Match(void) = 0;
	virtual value Evaluate(void) = 0;

	virtual void MatchOn(RuleComparator matchrule);
	static void DefaultMatchOn(RuleComparator matchrule);

	RuleComparator BetterMatch(void);

	virtual int Length(void) const = 0;

// DEEP COPY FUNCTION

	virtual Parser * Copy(void) = 0 ;

};


#endif /* RuleBase_First */
