#ifndef DisjunctiveRule_First
#define DisjunctiveRule_First

#include "RuleBase.h"

class DisjunctiveRule : public RuleBase
{
private:
	
	Seq<Parser*> alternative;


public:
	DisjunctiveRule(Parser *, Parser*);
	DisjunctiveRule(const DisjunctiveRule &);
	~DisjunctiveRule(void);

	virtual Parser * Match();
	virtual value Evaluate();

	virtual int Length(void) const;
	virtual int IsLeftRecursive(Parser * root);

// DEEP COPY FUNCTION

	virtual Parser * Copy(void) ;

// ALGEBRA ON RULES

	virtual Parser & operator | ( Parser &);

	virtual Parser & operator + ( Parser &);
};



#endif /* DisjunctiveRule_First */
