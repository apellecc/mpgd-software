#ifndef ParserAssign_First
#define ParserAssign_First

#include "ParserExpr.h"

class ParserAssign : public ParserExpr
{
public:
	ParserAssign (ParserExpr * , ParserExpr * );

	virtual Parser * Match(void);
	virtual value Evaluate(void);

	virtual Parser * Copy(void);

};

#endif /* ParserAssign_First */
