/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */

#ifndef StoreUint8_First
#define StoreUint8_First

#include "ParserStore.h"

class StoreUint8: public ParserStore
{
protected:
	uint8_t & uint8Ptr;
	ParserExpr* arg;

public:
	StoreUint8(uint8_t &, ParserExpr * pe = 0);

	value Evaluate(void);
	Parser * Copy(void);
};

StoreUint8 & operator<<(uint8_t &s, const value & v);
StoreUint8 & operator<<(uint8_t &s, ParserExpr & pe);

#endif /* StoreUint8_First */
