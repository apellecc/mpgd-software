#ifndef ParserCatAssign_First
#define ParserCatAssign_First

#include "ParserExpr.h"

class ParserCatAssign : public ParserExpr
{
public:
	ParserCatAssign (ParserExpr * , ParserExpr * );

	virtual Parser * Match(void);
	virtual value Evaluate(void);

	virtual Parser * Copy(void);

};

#endif /* ParserCatAssign_First */
