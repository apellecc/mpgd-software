#ifndef ParserAssignment_First
#define ParserAssignment_First

#include "ParserExpr.h"

class ParserAssignment : public ParserExpr
{
public:
	ParserAssignment (ParserExpr * , ParserExpr * );

	virtual Parser * Match(void);
	virtual value Evaluate(void);

	virtual Parser * Copy(void);

};

#endif /* ParserAssignment_First */
