#ifndef ParserPostIncr_First
#define ParserPostIncr_First

#include "ParserExpr.h"

class ParserPostIncr : public ParserExpr					
{								
public:								
	ParserPostIncr (ParserExpr *);			
								
	virtual Parser * Match(void);				
	virtual value Evaluate(void);				
								
	virtual Parser * Copy(void);				
								
};								
								
#endif /* ParserPostIncr_First */
