#ifndef ErrorDirective_First
#define ErrorDirective_First

#include "Parser.h"

class ErrorDirective: public Parser
{
public:
	ErrorDirective(const ErrorDirective &);
	ErrorDirective(std::string message, char resync);

	virtual Parser * Match(void);
	virtual value Evaluate(void);

	virtual ~ErrorDirective(void);

	virtual Parser * Copy(void);

// USEFUL STATS

	virtual int Length(void) const;
	virtual int IsLeftRecursive(Parser * root);

private:
	std::string myMessage;
	char myResync;
};

extern ErrorDirective & ERROR(std::string message, char resync = '\n');


#endif /* ErrorDirective_First */
