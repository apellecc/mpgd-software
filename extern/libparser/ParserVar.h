#ifndef ParserVar_First
#define ParserVar_First

#include "ParserExpr.h"

class ParserAssign;
class ParserAddAssign;
class ParserPostIncr;
class ParserPreIncr;
class ParserCatAssign;

class ParserVar: public ParserExpr
{
protected:
	int index;

public:
	ParserVar(int idx);
	ParserVar(value v);

	virtual Parser * Match(void);
	virtual value Evaluate(void);
	virtual value & Value(void);

	virtual Parser * Copy(void);

// DEFERRED ARITHMETIC ON ParserVars

	ParserAssign     & operator=(ParserExpr & expr);
	ParserAddAssign  & operator+=(ParserExpr & expr);
	ParserCatAssign  & operator&=(ParserExpr & expr);

	ParserPostIncr   & operator++(int ispostfix);
	ParserPreIncr    & operator++(void);

// USEFUL STATS

	virtual int Length(void) const;
	virtual int IsLeftRecursive(Parser * root);

};

extern ParserVar SS;
extern ParserVar S1;
extern ParserVar S2;
extern ParserVar S3;
extern ParserVar S4;
extern ParserVar S5;
extern ParserVar S6;
extern ParserVar S7;
extern ParserVar S8;
extern ParserVar S9;
extern ParserVar S10;
extern ParserVar S11;
extern ParserVar S12;
extern ParserVar & S(int N);

#endif /* ParserVar_First */
