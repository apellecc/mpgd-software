#ifndef ParserConcat_First
#define ParserConcat_First

#include "ParserExpr.h"

class ParserConcat: public ParserExpr
{
public:
	ParserConcat(ParserExpr * , ParserExpr * );

	virtual Parser * Match(void);
	virtual value Evaluate(void);

	virtual Parser * Copy(void);

};

ParserConcat & operator&(ParserExpr & p1, ParserExpr & p2);
ParserConcat & operator&(const value & p1, ParserExpr & p2);
ParserConcat & operator&(ParserExpr & p1, const value & p2);

#endif /* ParserConcat_First */
