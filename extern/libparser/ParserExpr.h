#ifndef ParserExpr_First
#define ParserExpr_First

#include "Parser.h"

class ParserElseIf;
class ParserElse;

// BASE CLASS FOR ALL EMBEDDED PARSER EXPRESSIONS

class ParserExpr: public Parser
{
protected:
	ParserExpr * lhs;
	ParserExpr * rhs;

	ParserExpr(ParserExpr * left, ParserExpr * right);
	ParserExpr(void);

public:
// THESE MEMBERS ENABLE SEQUENCES OF ParserExprs RO BE CONSTRUCTED
// SPECIAL VERSIONS ARE REQUIRED FOR ParserElseIf AND ParserElse IN
// ORDER TO PERMIT THEM TO CONCATENTATE TO THE PRECEDING ParserIf.

	virtual ParserExpr & operator,(ParserExpr &);
	virtual ParserExpr & operator,(ParserElseIf &);
	virtual ParserExpr & operator,(ParserElse &);
};

#endif /* ParserExpr_First */
