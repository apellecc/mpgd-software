#ifndef RX_Private_First
#define RX_Private_First

#include <string>

#define  FIRST_SYMBOL	0
#define  LAST_SYMBOL	126

#define  TRANSITIONS	(int)(LAST_SYMBOL-FIRST_SYMBOL+1)
#define  NO_TRANSITION	-1

#define  PURE	= 0

class CharClass
{
public:
	friend class CharClassIter;

	enum StdClass { Empty,
			EndOfPattern,
			AnyChar,
			MultiChar,
			OnlyChar,
			Alpha,
			Alnum,
			Blank,
			Cntrl,
			Digit,
			Graph,
			Lower,
			Upper,
			Print,
			Punct,
			Space,
			Xdigit,
			StdClassCount
			};

	static StdClass GetStdClass(std::string stdclassname);

private:
	std::string chars;
	void setcharclass(void);
	void setchars(int (*filter)(int));
	StdClass charclass;
	int complemented;
public:
	CharClass(void);
	CharClass(const CharClass & c);
	CharClass(const std::string & chrs);
	CharClass(StdClass stdclass, char from = FIRST_SYMBOL, char to = LAST_SYMBOL);

	void Complement(void);
	void * operator==(char c) const;
	void * operator==(StdClass s) const;
	void * operator!=(char c) const;
	void * operator!=(StdClass s) const;
	CharClass & operator+=(const CharClass & c);
	CharClass & operator+=(char c);

	std::ostream& PrintTo(std::ostream& os) const;
};

std::ostream & operator << (std::ostream& os,const CharClass & cc);

class CharClassIter
{
private:
	const CharClass & cc;
	int cursor;
public:
	CharClassIter(const CharClass & c);
	operator char (void) const;
	int operator++(void);
};

class State
{
private:
	int index;
	int isacceptor;
	int transition[TRANSITIONS];
public:
	State(unsigned int myindex, int acceptor=0);
	State(const State & s);
	State(int acceptor);
	State(void);

	void SetTransition(char symbol,const State & nextstate);
	void SetTransition(CharClass symbol,const State & nextstate);

	int Index(void) const;

	int NextStateFor(char symbol) const;

	std::string AsFSMState(void) const;

	int IsAcceptor(void) const;

	std::string Name() const;
};

class SynTreePosSet
{
protected:
	int possetsize;
	int * member;
	int nextmember;
	int currmember;
	int label;

public:
	SynTreePosSet(void);

	SynTreePosSet& operator += (int pos);
	SynTreePosSet& operator += (const SynTreePosSet & posset);

	int operator == (const SynTreePosSet & ps) const;

	int Count(void) const;

	int Contains(int pos) const;

	int Label(void) const;
	int & Label(void);

	int First(void) const;
	int Next(void) const;
	int Current(void) const;

	SynTreePosSet operator || (const SynTreePosSet& ps1);
	SynTreePosSet operator && (const SynTreePosSet& ps1);

	friend std::ostream& operator << (std::ostream& os, SynTreePosSet& ps);
};

class SynTreeLeaf;

class SynTreeNode
{
protected:
	SynTreeNode * left;
	SynTreeNode * right;

public:
	SynTreeNode(void);
	SynTreeNode(SynTreeNode *l,SynTreeNode *r);
	virtual ~SynTreeNode() {} ;

	virtual int Nullable(void) const PURE;
	virtual SynTreePosSet FirstPos(void) const PURE;
	virtual SynTreePosSet LastPos(void) const PURE;

	virtual SynTreeLeaf * FindLeaves(int & leafcount);
	virtual void BuildFollowPos(SynTreePosSet * followpos, int nodecount);

	virtual SynTreeNode * Copy(void) const PURE;

	virtual void Print(std::string leader) const PURE;
};

class SynTreeLeaf : public SynTreeNode
{
private:

	static unsigned int nextindex;

	CharClass symbol;
	unsigned int index;

public:
	SynTreeLeaf(const SynTreeLeaf & tl);
	SynTreeLeaf(const CharClass & sym);
	SynTreeLeaf(const CharClass::StdClass & sym);
	SynTreeLeaf(char sym);
	SynTreeLeaf(void);
	virtual ~SynTreeLeaf() {} ;

	static void ResetIndices(void);

	virtual int Nullable(void) const ;
	virtual SynTreePosSet FirstPos(void) const ;
	virtual SynTreePosSet LastPos(void) const ;
	virtual void BuildFollowPos(SynTreePosSet * followpos, int nodecount);

	virtual SynTreeLeaf * FindLeaves(int & leafcount);

	const CharClass & Symbol(void) const;
	
	virtual SynTreeNode * Copy(void) const;
	virtual void Print(std::string leader) const;
};

class SynTreeCat : public SynTreeNode
{
public:
	SynTreeCat(SynTreeNode *l, SynTreeNode *r);
	SynTreeCat(const SynTreeCat & s);

	virtual SynTreeNode * Copy(void) const;

	virtual int Nullable(void) const ;
	virtual SynTreePosSet FirstPos(void) const ;
	virtual SynTreePosSet LastPos(void) const ;

	virtual void BuildFollowPos(SynTreePosSet * followpos, int nodecount);
	virtual void Print(std::string leader) const;
};

class SynTreeOr : public SynTreeNode
{
public:
	SynTreeOr(SynTreeNode *l, SynTreeNode *r);
	SynTreeOr(const SynTreeOr & s);

	virtual SynTreeNode * Copy(void) const;

	virtual int Nullable(void) const ;
	virtual SynTreePosSet FirstPos(void) const ;
	virtual SynTreePosSet LastPos(void) const ;
	virtual void Print(std::string leader) const;
};

class SynTreeStar : public SynTreeNode
{
public:
	SynTreeStar(SynTreeNode *l);
	SynTreeStar(const SynTreeStar & s);

	virtual int Nullable(void) const ;
	virtual SynTreePosSet FirstPos(void) const ;
	virtual SynTreePosSet LastPos(void) const ;

	virtual SynTreeNode * Copy(void) const;
	
	virtual void BuildFollowPos(SynTreePosSet * followpos, int nodecount);
	virtual void Print(std::string leader) const;
};


class DFA
{
protected:
	int matchedat;
	int failedat;
	int laststate;

	int statesize;
	State **state;
	int nextstate;
	
	void BuildDFA(SynTreeNode * root);
public:
	DFA(void);
	DFA(SynTreeNode * root);
	DFA(const DFA & dfa);

	virtual DFA * Copy(void);

	virtual ~DFA(void);

	virtual std::string AsFSM(void) const;

	DFA& operator=(const DFA & dfa);

	virtual int Match(std::string str);
	virtual int Match(int (*charsrc)(void));
	virtual int IsOnAcceptor(void) const;
	int FailedOnNoTransition(void) const;
	int FailedAt(void) const;
	int MatchedAt(void) const;
	virtual int  NewState(int acceptor);
	virtual State* GetState(unsigned int index);
};

class LinearDFA: public DFA
{
private:
	std::string pattern;
public:
	LinearDFA(std::string pat);
	LinearDFA(const LinearDFA & ldfa);

	virtual DFA * Copy(void);

	virtual std::string AsFSM(void) const;

	static int LinearPattern(std::string pat);

	virtual int Match(std::string str);
	virtual int Match(int (*charsrc)(void));
 
	virtual int IsOnAcceptor(void) const;
	int FailedOnNoTransition(void) const;
	int FailedAt(void) const;
	int MatchedAt(void) const;
	virtual int  NewState(int acceptor);
	virtual State* GetState(unsigned int index);
};

#endif /* RX_Private_First */
