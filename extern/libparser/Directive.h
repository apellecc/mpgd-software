#ifndef Directive_First
#define Directive_First

#include "Parser.h"

typedef Parser * (*DirectiveFn)(Context);

class Directive: public Parser
{
protected:
	DirectiveFn fn;

public:
	Directive(const Directive &);
	Directive(DirectiveFn func);

	virtual Parser * Match(void);
	virtual value Evaluate(void);

	virtual ~Directive(void);

	virtual Parser * Copy(void);

// USEFUL STATS

	virtual int Length(void) const;
	virtual int IsLeftRecursive(Parser * root);

};

Parser & operator+(Parser&,DirectiveFn);
Parser & operator+(DirectiveFn,Parser&);

Parser & operator|(Parser&,DirectiveFn);
Parser & operator|(DirectiveFn,Parser&);

extern Parser * FAIL(Context);


#endif /* Directive_First */
