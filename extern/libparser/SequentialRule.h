#ifndef SequentialRule_First
#define SequentialRule_First

#include "RuleBase.h"

class SequentialRule : public RuleBase
{
private:
	
	Seq<Parser*> component;


public:
	SequentialRule(Parser *, Parser *);
	SequentialRule(const SequentialRule &);
	~SequentialRule(void);

	virtual Parser * Match(void);
	virtual value Evaluate(void);

	virtual int Length(void) const;
	virtual int IsLeftRecursive(Parser * root);

// DEEP COPY FUNCTION

	virtual Parser * Copy(void) ;

// ACCESS TO COMPONENTS

	virtual Parser * operator[](int i);

// RULE ALGEBRA

	virtual Parser & operator | ( Parser &);
	virtual Parser & operator + ( Parser &);
};



#endif /* SequentialRule_First */
