#ifndef Parser_First
#define Parser_First

#include "pstream.h"
// #include "String.h"
#include "Value.h"
#include "RX.h"
#include "Debugging.h"
#include "AutoCollected.h"
#include "Seq.h"

class Parser;

typedef int (*RuleComparator)(const Parser*,const Parser*);

struct Context
{
	Parser*     parent;	// PARENT OF THE CURRENT Action
	int         index;	// INDEX OF THE CURRENT Action WITHIN parent
	Parser*     current;	// THE CURRENT Parser OBJECT
	pstream*    stream;	// THE CURRENT pstream BEING PARSED

	Context(void);
	Context(Parser * par, pstream * ps);
};

class ParserError
{
public:
	ParserError(std::string file, long line, std::string context, std::string message);
	ParserError(void);

	std::string File(void);
	long Line(void) const;
	std::string Context(void) const;
	std::string Message(void) const;

	std::string StdFormat(void) const;
private:
	std::string myFile;
	long myLine;
	std::string myContext;
	std::string myMessage;
};

std::ostream & operator<<(std::ostream & os, const ParserError & pe);
std::ostream & operator<<(std::ostream & os, const Seq<ParserError> & spe);

// ====================
// PARSER BASE CLASS
// ====================

class Parser : public Debugging, public AutoCollected
{
protected:
	std::string name;
	value val;

	int isCopy;

	Mark matchmark;

	enum Enabled { active, disabled, invisible };
	Enabled enabled;
	int isoptional;

	static Seq<Context> context;

	static Parser * NullParser(Mark m);
	Parser * Success(Parser * returnval = 0);

	static Seq<ParserError> parseError;

public:
	Parser(const Parser &);
	Parser(std::string n);
	Parser(void);
	Parser(Mark m);

	virtual Parser * Match(void);
	virtual value Evaluate(void);

	virtual ~Parser(void);

	virtual Parser * Copy(void);
	void DeleteIfCopy(void);

	Mark MatchMark(void) const;
	Mark SetMatchMark(Mark);

// USEFUL STATS

	virtual int Length(void) const;
	virtual int IsLeftRecursive(Parser * root);

// SWITCH INDIVIDUAL Parsers ON AND OFF

	void Enable(void);
	void Disable(void);
	void Hide(void);

// CONTROLLING CONTEXT OF PARSING 

	static void PushContext(Parser *parent, pstream * ps = 0);
	static void PopContext(void);
	static const Context & GetContext(void);
	static std::string ContextHeader(void);

	static void SetContext(Parser *curr, int index);
	static bool HasContext(void) { return context.Count()!=0; }

	virtual void MatchOn(RuleComparator matchrule);

// USEFUL INFORMATION

	virtual value& Value(void);
	std::string Name(int bracketted = 0) const;

	virtual std::string Leader(void) const;

	int IsOptional(void) const;
	static int InOptional(void);

// ACCESS iTH COMPONENT OF A PARSER IF IT EXISTS
// RETURNS 0 IF NO SUCH COMPONENT

	virtual Parser * operator[](int i);

// DEFAULT PARSER ALGEBRA
	
	virtual Parser & operator | (Parser &);
	virtual Parser & operator + (Parser &);

// COLLECT PARSER ERRORS

	static const Seq<ParserError> & Errors(void);

};

// CREATE AN OPTIONAL VERSION OF A PARSER

Parser & Opt(Parser & p,int opt = 1);

// ACCESS STANDARD COMPONENTS OF A RULE

#define DollarDollar   context.parent->Value()
#define Dollar1   (*context.parent)[0]->Value()
#define Dollar2   (*context.parent)[1]->Value()
#define Dollar3   (*context.parent)[2]->Value()
#define Dollar4   (*context.parent)[3]->Value()
#define Dollar5   (*context.parent)[4]->Value()
#define Dollar6   (*context.parent)[5]->Value()
#define Dollar7   (*context.parent)[6]->Value()
#define Dollar8   (*context.parent)[7]->Value()
#define Dollar9   (*context.parent)[8]->Value()
#define Dollar10  (*context.parent)[9]->Value()
#define Dollar(N) (*context.parent)[N]->Value()

#endif /* Parser_First */
