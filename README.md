# MPGD software

Software for the the acquisition and analysis of MPGD data.

## Getting started

### How to compile

Run `cmake` after cloning the repository:
```bash
cmake -S . -B build
cmake --build build
```

### Unpacker

To run the unpacker on some example data for the first 10 events use:
```bash
build/unpacker/raw-to-text --input examples/raw/vfat3.bin -n 10
```
