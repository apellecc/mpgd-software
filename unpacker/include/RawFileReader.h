#ifndef RAW_FILE_READER_H
#define RAW_FILE_READER_H

#include <iostream>
#include <string>

#include "RawEvent.h"

class RawFileReader {
  public:
    RawFileReader(const std::string& filename);

    std::size_t read(void* buffer, std::size_t size, std::size_t count);
    std::size_t readEvent(RawEvent *event);

  private:
    std::string m_filename;
    std::FILE* m_file;
};

#endif
