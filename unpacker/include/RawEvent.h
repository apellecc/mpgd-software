#ifndef RAW_EVENT_H
#define RAW_EVENT_H

#include <iostream>

class RawEvent {
  public:
    //virtual ~RawEvent() = 0;
    RawEvent() {}

    virtual std::size_t fromFile(std::FILE *f) { return 0; }
  	virtual uint32_t getEventCounter() { return 0; }
  	virtual uint32_t getBunchCounter() { return 0; }
  	virtual std::string getData() { return 0; }

    void print();
};

class DAQPathEvent : public RawEvent {
  public:
  	typedef struct VFAT3Data_s {
   	  uint32_t EC;
	    uint32_t BC;
	    bool hit[128];
  	} __attribute__((packed)) VFAT3Data_t;

  	VFAT3Data_t data;
  	
    DAQPathEvent(RawEvent r) {}
    std::size_t fromFile(std::FILE *f) { return std::fread(&data, sizeof(data), 1, f); }

  	uint32_t getEventCounter() { return data.EC; }
  	uint32_t getBunchCounter() { return data.BC; }
  	bool getChannelValue(int i) { return data.hit[i]; }

    std::string getData();
}; 

class TriggerPathEvent : public RawEvent {
  public:
    typedef struct VFAT3Data_s {
		  uint32_t EC;
		  uint8_t PHASE;	
		  uint64_t BC;
	  } __attribute__((packed)) TRGData_t;

  	TRGData_t data;
 	
    TriggerPathEvent(RawEvent r) {}
    std::size_t fromFile(std::FILE *f) { return std::fread(&data, sizeof(data), 1, f); }

  	uint32_t getEventCounter() { return data.EC; }
  	uint32_t getBunchCounter() { return data.BC; }
    
    std::string getData();
}; 

#endif
