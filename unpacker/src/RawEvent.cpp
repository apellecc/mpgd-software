#include "RawEvent.h"

#include <sstream>

void RawEvent::print() {
  std::cout << "  EC " << getEventCounter() << std::endl;
  std::cout << "  BC " << getBunchCounter() << std::endl;
  std::cout << "  Data " << getData() << std::endl;
}

std::string DAQPathEvent::getData() {
  std::stringstream dataStream;
  for (int ch=0; ch<128; ch++) {
    dataStream << (int) getChannelValue(ch);
  }
  return dataStream.str();
}

std::string TriggerPathEvent::getData() {
  std::stringstream dataStream;
  dataStream << (int) data.PHASE;
  return dataStream.str();
}
