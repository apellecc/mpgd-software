#include <cstdint>
#include <iostream>
#include <string>
#include <unistd.h>

#include "CLI11.hpp"

#include "RawFileReader.h"
#include "RawEvent.h"

#define ONLINE_SLEEP_TIME 5 // if in online mode, sleep n seconds when file has ended

int main(int argc, char** argv) {
    std::string raw_filename;
    uint64_t max_events{0};
    bool isOnline;

    CLI::App app{"Dump raw file to text"};
    app.add_option("-i,--input", raw_filename, "Raw file path")->required();
    app.add_option("-n,--events", max_events, "Maximum number of events to read");
    app.add_flag("--online", isOnline, "Wait for new events after reaching the end");
    try {
        app.parse(argc, argv);
    } catch (const CLI::ParseError &e) {
        return app.exit(e);
    }

    std::cout << "Parsing input file " << raw_filename << "..." << std::endl;
    RawFileReader rawFile{raw_filename};
    RawEvent event;
    int8_t eventType;
    for (uint64_t n_events{0}; true; n_events++) {
        if (max_events > 0 && n_events > max_events) {
            std::cout << "Reached last event, exiting..." << std::endl;
            break;
        }
        std::size_t hasEvent = rawFile.read(&eventType, sizeof(int8_t), 1);

        if (hasEvent) {
            std::cout << "---- Event # " << n_events << " ----" << std::endl;
        } else if (isOnline) {
            std::cout << "Waiting " << ONLINE_SLEEP_TIME << " s for new events..." << std::endl;
            sleep(ONLINE_SLEEP_TIME);
        } else {
            std::cout << "Reached last event, exiting..." << std::endl;
            break;
        }

        std::cout << "  Event type: " << (int) eventType << std::endl;
        if (eventType==0) {
            DAQPathEvent evt = event;
            rawFile.readEvent(&evt);
            evt.print();
        } else if (eventType==1) {
            TriggerPathEvent evt = event;
            rawFile.readEvent(&evt);
            evt.print();
        } else {
            std::cout << "Event type " << eventType << " not supported." << std::endl;
        }

    }
}
