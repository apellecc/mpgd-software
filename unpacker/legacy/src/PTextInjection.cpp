/*
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>.
 *
 */

#include <iostream>
#include <unistd.h>
#include "iomanip"
#include "PTextInjection.h"

// Test configuration
// Number of injection pulses
const int PTextInjection::numPulses = 1000;
static const int max_dmg_chn = 0;

PTextInjection::PTextInjection(TestBench &t) : GenericTest(t)
{
}

PTextInjection::~PTextInjection()
{
}

void PTextInjection::resetHistogram()
{
	evCounter = 0;
	for (int i=0; i<128; i++)
		histogram[i] = 0;
}

void PTextInjection::histogramCB(void *ctx, VFAT3dataParser::VFAT3data_t *data)
{
	PTextInjection *p = (PTextInjection *) ctx;
	
	p->evCounter++;

	for (int i=0; i<128; i++)
		if (data->hit[i])
			p->histogram[i]++;
}

// Pulse half channels and verify results
void PTextInjection::pulseAndVerify(bool odd)
{
    // Enable external injection for ODD channels
    tb.board->setExtInj(odd, !odd);

	// start pulses injection				
	tb.board->pulser->run(numPulses);

    // get data
	resetHistogram();
	long res;
	do {
		res = tb.board->pollData(50);		// timeout 50 ms
		if (res == 0){
            throw TestError("Timeout reading data");
		}
	} while (evCounter<numPulses);

    // check results
    for (int i=0; i<128; i++)
	{
		//cout <<i<< " numpulse " << histogram[i]<<endl;;
		if (odd)
		{
			if (i%2)
			{
				switch (histogram[i])
				{
					case numPulses:
						tb.ptdata->VFAT3.chn_d[i].bonding = 0; // bonding ok
						break;

					case 0:				
						tb.ptdata->VFAT3.chn_d[i].bonding = 1; // bonding open
						break;

					default:
						tb.ptdata->VFAT3.chn_d[i].bonding = 2; // bonding short 
						break;
				}
			}
			else
			{
				switch (histogram[i])
				{
					case 0:
						tb.ptdata->VFAT3.chn_d[i].bonding = 0; // bonding ok
						break;

					default:
						tb.ptdata->VFAT3.chn_d[i].bonding = 2; // bonding short
						break;
				}
			}									 
		}
		else
		{
			if (!(i%2))
			{
				switch (histogram[i])
				{
					case numPulses:
						if (tb.ptdata->VFAT3.chn_d[i].bonding == 0) // double check with previous injection
						{ 
							tb.ptdata->VFAT3.chn_d[i].bonding = 0; // channel bonding good
						}
						else if (tb.ptdata->VFAT3.chn_d[i].bonding == 2)
						{
							cout << "Channel: " << i << " could be good, screw the socket." << endl;
						}
						break;

					case 0:
						if (tb.ptdata->VFAT3.chn_d[i].bonding == 0)
						{
							tb.ptdata->VFAT3.chn_d[i].bonding = 1; // channel not bonded and isolated from the other channels
							cout << "Channel: " << i << " not bonded" << endl;
						}
						else
						{
							tb.ptdata->VFAT3.chn_d[i].bonding = 3; // channel not connected to the ball but shorted to the neighbour
							cout << "Channel: " << i << " channel not connected to the ball but shorted to the neighbour" << endl;
						}
						break;

					default:
						if (tb.ptdata->VFAT3.chn_d[i].bonding == 0)
						{
							cout << "Channel: " << i << " could be good, screw the socket." << endl;	
						}
						else if (tb.ptdata->VFAT3.chn_d[i].bonding == 2) // channel bondend to the ball and shorted to the neighbour
						{
							tb.ptdata->VFAT3.chn_d[i].bonding = 2;
							cout << "Channel: " << i << " shorted" << endl;
						}
						break;
				}
			}
			else
			{
				switch (histogram[i])
				{
					case 0:
						if (tb.ptdata->VFAT3.chn_d[i].bonding == 0)
						{
							tb.ptdata->VFAT3.chn_d[i].bonding = 0; // channel bonding good
						}
						else if	(tb.ptdata->VFAT3.chn_d[i].bonding == 1)
						{
							tb.ptdata->VFAT3.chn_d[i].bonding = 1;
							cout << "Channel: " << i << " not bonded" << endl; // channel not bonded and isolated from the other channels
						}
						else if	(tb.ptdata->VFAT3.chn_d[i].bonding == 2)
						{
							cout << "Channel: " << i << " could be good, verify threshold." << endl;
						}					
						break;

					default:
						if (tb.ptdata->VFAT3.chn_d[i].bonding == 1)
						{
							tb.ptdata->VFAT3.chn_d[i].bonding = 3; // channel not connected to the ball but shorted to the neighbour
							cout << "Channel: " << i << " channel not connected to the ball but shorted to the neighbour" << endl;
						} 
						else if (tb.ptdata->VFAT3.chn_d[i].bonding == 2)
						{
							tb.ptdata->VFAT3.chn_d[i].bonding = 2; // channel bondend to the ball and shorted to the neighbour
							cout << "Channel: " << i << " shorted" << endl; 
						}
						break;
				}
			}
		} 
    }
}

bool PTextInjection::run(string param)
{
	int dmg_chn_cnt = 0;	// counter for damaged bondings
	int GBL_dac = 40;

	for (int i=0; i<128; i++)
	{
		tb.chipConfig.CH[i].arm_dac = 0;
		tb.chipConfig.CH[i].zcc_dac = 0;
		tb.chipConfig.CH[i].mask = false;
		tb.chipConfig.CH[i].cal = false;
	}

	//CFD settings
	tb.chipConfig.DISC.PT = 100;
	tb.chipConfig.DISC.SEL_POL = 1;
	tb.chipConfig.DISC.EN_HYST = 1;
	tb.chipConfig.DISC.FORCE_EN_ZCC = 0;
	tb.chipConfig.DISC.FORCE_TH = 0;
	tb.chipConfig.DISC.SEL_COMP_MODE = 1;
	tb.chipConfig.DISC.arm_dac = GBL_dac;
	tb.chipConfig.DISC.zcc_dac = 10;
	tb.chipConfig.DISC.hyst_dac = 5;

	//CSA settings - medium gain
	tb.chipConfig.PRE.TP = 100;
	tb.chipConfig.PRE.RES = 1;
	tb.chipConfig.PRE.CAP = 1;

	tb.chipConfig.TRG.latency = 17;
	tb.chipConfig.TRG.PS = 8;


	tb.setChipConfig();
	usleep(10000);

    // Set tester in functional mode (Clock ON)
    tb.board->setTestType(TesterCtrl::TTYPE_Functional);

    // Configure the chip
    try {
	    // Set consumer function
	    tb.board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, histogramCB);

	    // Setup pulses
	    tb.board->pulser->setConfig(20, 400-20);	// 10 us period

	    // Send BC0
	    tb.board->comPortTx->sendBC0();

	    // start run
	    tb.board->mRunControl->startRun();
	    tb.board->comPortTx->sendRunMode();
            
        // test odd channels
        pulseAndVerify(true);

        // test even channels
        pulseAndVerify(false);

	    // Stop run, reset consumer function
	    tb.board->mRunControl->stopRun();
	    tb.board->comPortTx->sendSConly();
	    tb.board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, NULL);
    } 
	catch (MIPBusError) 
	{
	    tb.board->mRunControl->stopRun();
	    tb.board->comPortTx->sendSConly();
	    tb.board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, NULL);
        throw TestError ("Chip comunication error");
    }

	//counting the number of damaged bondings
	for (int i =0; i<128; i++)
	{
		if (tb.ptdata->VFAT3.chn_d[i].bonding) dmg_chn_cnt++;
	}

	//check the number of damaged bondings
	if (dmg_chn_cnt > max_dmg_chn)
	{
		cout << "Bonding test not passed, number of damaged bondings: "<< dmg_chn_cnt << endl;
		throw TestError("Bonding test failed");
	}
	else
	{
		cout << "Bonding test passed" << endl;
		cout << "Number of damaged bondings: " << dmg_chn_cnt << endl;
	}
	
    return true;
}

