#include <iostream>
#include <unistd.h>
#include <cmath>
#include "PTiref.h"
#include "TestBench.h"
#include "VFAT3chip.h"
#include "Tutils.h"

static const int iref_dac_min = 25; 			// Iref DAC scan start
static const int iref_dac_max = 40; 			// Iref DAC scan stop

static const double Gain_min = 120e-9; 			//Iref DAC LSB acceptance range min
static const double Gain_max = 210e-9; 			//Iref DAC LSB acceptance range max
static const double Offset_min = -1e-6;  		//Iref DAC offset acceptance range min
static const double Offset_max = 1e-6;			//Iref DAC offset acceptance range max

static const float iref_opt = 5e-6;				//Iref optimal value

PTiref::PTiref(TestBench &t) : GenericTest(t)
{
	DAC_Iref =  { string("Imon_Gloabl_Iref"), VFAT3chip::RegCfgCtr_5, VFAT3chip::CfgCtr_5_Iref_mask, VFAT3chip::CfgCtr_5_Iref_shift, 0, 32 };
}

PTiref::~PTiref()
{
}

bool PTiref::run(string param){

	DAC_s dac= DAC_Iref;
	VFAT3_data::Iref_s Iref_s;
	bool fit_status;
	float delta_min = 10;
	float delta = 0;
	float vmon = 0;
	int mon_cnt = 0; //counter for monotonicity consecutive errors on the same conf. byte, the usleep func is too slow

	// set mux 
	tb.chipConfig.MON.SEL = 0;

	Iref_s.ScanStart = iref_dac_min;
	Iref_s.ScanStop = iref_dac_max;
	Iref_s.optVal = 0;
			
	for (uint8_t  v=iref_dac_min; v<=iref_dac_max; v++){

		// set DAC
		tb.chipConfig.BIAS.Iref = v;
		tb.setChipConfig();
		usleep(100);

		// Read External ADC and conversion into a current 
		tb.board->readmon(VFAT3board::V_VMON, &vmon);
		Iref_s.ScanData[v-iref_dac_min] = vmon/20000;
		cout << Iref_s.ScanData[v-iref_dac_min] << endl;
		// saving data
		tb.ptdata->VFAT3.iref_d.ScanData[v-iref_dac_min] = Iref_s.ScanData[v-iref_dac_min];

		// Monotonicity test
		if ((v-iref_dac_min) && (Iref_s.ScanData[v-iref_dac_min]<=Iref_s.ScanData[v-iref_dac_min-1]))
		{
			if (mon_cnt == 3)
			{		
				cout<<"Iref DAC test not passed: monotonicity test failed!"<<endl;
				//throw TestError("Iref DAC test not passed: monotonicity test failed!");
			}
			else
			{
				mon_cnt++;
				v--;
			}

		}
		else
		{
			mon_cnt = 0;
		}

		// Optimal value check
		delta = abs(Iref_s.ScanData[v-iref_dac_min] - iref_opt);
		if (delta_min >= delta)
		{
			delta_min = delta;
			Iref_s.optVal = Iref_s.ScanData[v-iref_dac_min];
			Iref_s.optVal_ndac =  v;
		}
		

	}
	
	fit_status = Tutils::bestFitLinDAC(iref_dac_min, iref_dac_max, Iref_s.ScanData, &Iref_s.gain, &Iref_s.offset);
	
	if (fit_status)
	{
		// DAC gain and offset acceptance range check
		if ((Iref_s.gain>=Gain_min) && (Iref_s.gain<=Gain_max) && (Iref_s.offset>=Offset_min) && (Iref_s.offset<=Offset_max))
		{
			//saving data
			tb.ptdata->VFAT3.iref_d.ScanStart = Iref_s.ScanStart;
			tb.ptdata->VFAT3.iref_d.ScanStop = Iref_s.ScanStop;
			tb.ptdata->VFAT3.iref_d.optVal = Iref_s.optVal;
			tb.ptdata->VFAT3.iref_d.optVal_ndac = Iref_s.optVal_ndac;
			tb.ptdata->VFAT3.iref_d.gain = Iref_s.gain;
			tb.ptdata->VFAT3.iref_d.offset = Iref_s.offset;
				
			cout<<"Iref DAC test passed!"<<endl;
			cout<<"Iref optimal value = "<< Iref_s.optVal <<" A"<<endl;
			cout<<"Iref optimal DAC value = "<<Iref_s.optVal_ndac<<endl;

			//Optimal Iref DAC value reg set
			tb.chipConfig.BIAS.Iref = Iref_s.optVal_ndac;
			tb.setChipConfig();

		} 
		else
		{
			cout<<"Iref DAC test not passed: parameters out of acceptance range!"<<endl;

			cout<<"Iref DAC gain = "<<Iref_s.gain<<endl;
			cout<<"Iref gain should be included between: "<< Gain_min << " A/LSB and " << Gain_max << " A/LSB" << endl;

			cout<<"Iref DAC offset = "<<Iref_s.offset<<endl;
			cout<<"Iref offset should be included between: "<< Offset_min << " A/LSB and " << Offset_max << " A/LSB" << endl;

			throw TestError("Iref DAC test not passed: parameters out of acceptance range!");
		}
	}
	else 
	{
		cout<<"Iref DAC linear fitting error"<<endl;
		throw TestError("Iref DAC linear fitting error");
	}
	return true;
}
		


