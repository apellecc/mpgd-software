#include "PTadc.h"
#include "VFAT3chip.h"
#include "Tutils.h"
#include <unistd.h>
#include <iostream>


PTadc::PTadc(TestBench &t) : GenericTest(t)
{
	calib_dac = { string("Vmon_Calib_Vstep"), VFAT3chip::RegCfgCal_0, VFAT3chip::CfgCal_0_DAC_mask, VFAT3chip::CfgCal_0_DAC_shift, 33, 255};
}

bool PTadc::run(string param){
	VFAT3chip::MonitorConfig_t monCfg = tb.chipConfig.MON;
	VFAT3chip::CalibrationConfig_t calCfg = tb.chipConfig.CAL;
	VFAT3_data::ADC_s ADC_s;
	bool fit_status;
	
	uint32_t ADC0[255];
	uint32_t ADC1[255];
	float ADCe[255];
	float vmon;

	calCfg.MODE = 1;
	calCfg.POL = 0;
	tb.chip->addSetCalibrationConfig(calCfg);

	monCfg.SEL = calib_dac.monSel;
	tb.chip->addSetMonitorConfig(monCfg);
	tb.chip->addRMWreg(calib_dac.reg, ~calib_dac.mask, 0);
	tb.chip->execute();
	usleep(10000);

	// ADC0 acceptance range
	const float ADC0_gain_min = 2 * tb.ptdata->VFAT3.adc_d.ADC0_vref / 1024 * 0.8; 	// gain = 2*Vrif/1024, Vrif = 1 V
	const float ADC0_gain_max = 2 * tb.ptdata->VFAT3.adc_d.ADC0_vref / 1024 * 1.2;	
	const float ADC0_offset_max = ( tb.ptdata->VFAT3.vref_d.ADC_VinM - tb.ptdata->VFAT3.adc_d.ADC0_vref ) * 0.7; 	// offset = VinM - Vrif, VinM = 630 mV
	const float ADC0_offset_min = ( tb.ptdata->VFAT3.vref_d.ADC_VinM - tb.ptdata->VFAT3.adc_d.ADC0_vref ) * 1.1;

	// ADC1 acceptance range
	const float ADC1_gain_min = 2.4/1024*0.9;		// gain = 2*Vrif/1024, Vrif = 1.2 V
	const float ADC1_gain_max = 2.4/1024*1.1;
	const float ADC1_offset_max = (tb.ptdata->VFAT3.vref_d.ADC_VinM - 1.2)*0.7; 	// offset = VinM - Vrif, VinM = 630 mV
	const float ADC1_offset_min = (tb.ptdata->VFAT3.vref_d.ADC_VinM - 1.2)*1.1;


	for (uint32_t v=0; v<=calib_dac.maxValue; v++)
	{
		tb.chip->addRMWreg(calib_dac.reg, ~calib_dac.mask, v << calib_dac.shift);
		tb.chip->execute();
		
		// read external ADC
		tb.board->readmon(VFAT3board::V_VMON, &vmon);
		ADCe[v] = vmon;
		// read internal ADC
		tb.chip->addReadADC(0, &ADC0[v]);
		tb.chip->addReadADC(1, &ADC1[v]);

		tb.chip->execute();
	}

	fit_status = Tutils::bestFitLin(255, ADC0, ADCe, &ADC_s.ADC0_gain, &ADC_s.ADC0_offset);
	
	//ADC0 check
	if (fit_status)
	{
		if ((ADC_s.ADC0_gain>=ADC0_gain_min) && (ADC_s.ADC0_gain<=ADC0_gain_max) && (ADC_s.ADC0_offset>=ADC0_offset_min) && (ADC_s.ADC0_offset<=ADC0_offset_max))
		{
			//saving data
			tb.chipConfig.ADC.OFFSET = ADC_s.ADC0_offset;			// offset
			tb.chipConfig.ADC.LSB = ADC_s.ADC0_gain;				// LSB value
			tb.setChipConfig();
			tb.ptdata->VFAT3.adc_d.ADC0_gain = ADC_s.ADC0_gain;
			tb.ptdata->VFAT3.adc_d.ADC0_offset = ADC_s.ADC0_offset;
			cout<<"ADC0 test passed!"<<endl;
		}
		else
		{
			cout<<"ADC0 test not passed: parameters out of acceptance range!"<<endl;
			cout<<"ADC0 gain = "<<ADC_s.ADC0_gain<<endl;
			cout << "ADC0 gain should be included between: " << ADC0_gain_min << " mV/LSB and " << ADC0_gain_max << " mV/LSB" << endl;
			cout<<"ADC0 offset = "<<ADC_s.ADC0_offset<<endl;
			cout << "ADC0 offset should be included between: " << ADC0_offset_min << " mV/LSB and " << ADC0_offset_max << " mV/LSB" << endl;
			throw TestError("ADC0 test not passed: parameters out of acceptance range!");
		}
	}
	else
	{
		cout<<"ADC0 linear fitting error"<<endl;
		throw TestError("ADC0 linear fitting error");
	}

	fit_status = Tutils::bestFitLin(255, ADC1, ADCe, &ADC_s.ADC1_gain, &ADC_s.ADC1_offset);

	//ADC1 check	
	if (fit_status)
	{
		if ((ADC_s.ADC1_gain>=ADC1_gain_min) && (ADC_s.ADC1_gain<=ADC1_gain_max) && (ADC_s.ADC1_offset>=ADC1_offset_min) && (ADC_s.ADC1_offset<=ADC1_offset_max))
		{
			//saving data
			tb.ptdata->VFAT3.adc_d.ADC1_gain = ADC_s.ADC1_gain;
			tb.ptdata->VFAT3.adc_d.ADC1_offset = ADC_s.ADC1_offset;
			cout<<"ADC1 test passed!"<<endl;
		}
		else
		{
			cout<<"ADC1 test not passed: parameters out of acceptance range!"<<endl;
			cout<<"ADC1 gain = "<<ADC_s.ADC1_gain<<endl;
			cout << "ADC1 gain should be included between: " << ADC1_gain_min << " mV/LSB and " << ADC1_gain_max << " mV/LSB" << endl;
			cout<<"ADC1 offset = "<<ADC_s.ADC1_offset<<endl;
			cout << "ADC1 offset should be included between: " << ADC1_offset_min << " mV/LSB and " << ADC1_offset_max << " mV/LSB" << endl;
			throw TestError("ADC1 test not passed: parameters out of acceptance range!");
		}
	}
	else
	{
		cout<<"ADC1 linear fitting error"<<endl;
		throw TestError("ADC1 linear fitting error");
	}

	return true;
}
