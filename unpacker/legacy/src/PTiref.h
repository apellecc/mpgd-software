#ifndef PTIREF_H
#define PTIREF_H

#include "GenericTest.h"

class PTiref : public GenericTest
{
private:
	typedef struct DAC_s {
		string name;
		uint32_t reg;
		uint32_t mask;
		uint32_t shift;
		uint32_t monSel;
		uint32_t maxValue;
	} DAC_t;

	DAC_t DAC_Iref;	

public:
	PTiref(TestBench &tb);
	~PTiref();

	bool run(string param);
};

#endif 
