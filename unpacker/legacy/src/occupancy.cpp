/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include "VFAT3dataParser.h"
#include "TRGdataParser.h"

using std::string;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::ifstream;
using std::ofstream;


int main(int argc, char**argv)
{
	ifstream inputFile;
	ofstream outputFile;
	char type;
	long occ[128];

	if (argc<2)
		goto invokeError;

	inputFile.open (argv[1], std::ifstream::in | std::ifstream::binary);	
	outputFile.open ("occupancy.dat");	

	for (int i=0; i<128; i++)
		occ[i] = 0;

	for (;;){
		if (!inputFile.read (&type, 1)) {
			break;		    
		}
		if (type == 0){
			VFAT3dataParser::VFAT3data_t VFAT3data;
			if (!inputFile.read ((char*) &VFAT3data, sizeof(VFAT3dataParser::VFAT3data_t))) {
				cerr << "Incomplete data read from file! Exit" << endl;
				break;
			}
			for (int i=0; i<128; i++)
				if (VFAT3data.hit[i])
					occ[i]++;
			
		} else if (type == 1){
			TRGdataParser::TRGdata_t TRGdata;
			if (!inputFile.read ((char*) &TRGdata, sizeof(TRGdataParser::TRGdata_t))) {
				cerr << "Incomplete data read from file! Exit" << endl;
				break;
			}
		} else {
			cerr << "Got unknow byte! Exit" << endl;
			exit(0);
		}
	}
	
	// output occupancy data
	for (int i=0; i<128; i++)
		outputFile << i << " " << occ[i] << endl;;

	return 0;	


invokeError:
	cerr << "*******************************************" << endl;
	cerr << "Parameters parsing Error" << endl; 
	cerr << "use:occupancy file.bin" << endl;
	cerr << "===========================================" << endl;
	return 1;
}

