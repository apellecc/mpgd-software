/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#include <fstream>
#include <iostream>
#include <math.h>
#include "TestBench.h"
#include "TBconfig.h"
#include "VFAT3board.h"
#include "VFAT3chip.h"
#include "PTpulse.h"

using std::string;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;

PTpulse::PTpulse(TestBench &t) : GenericTest(t), hitMatrix()
{
	board = tb.board;
}

void PTpulse::resetHistogram()
{
	evCounter = 0;
	for (int i=0; i<128; i++)
		histogram[i] = 0;
}

void PTpulse::histogramCB(void *ctx, VFAT3dataParser::VFAT3data_t *data)
{
	PTpulse *p = (PTpulse *) ctx;
	
	p->evCounter++;

	for (int i=0; i<128; i++)
		if (data->hit[i])
			p->histogram[i]++;
}

void PTpulse::pulseAtChargeDAC(int chargeDAC, unsigned long nPulses)
{
    // set calibration DAC
    if (calCfg.MODE == 1){
	    calCfg.DAC = 255-chargeDAC;		// Vpulse mode
    } else if (calCfg.MODE == 2) {
	    calCfg.DAC = chargeDAC;
    } else {
	    cerr << "Please set Calibration MODE != 0" << endl;
	    exit(0);
    }
    chip->addSetCalibrationConfig(calCfg);
    chip->execute();

	// start pulses injection				
	board->pulser->run(nPulses);

	resetHistogram();
	long res;
	do {
		res = board->pollData(50);		// timeout 50 ms
		if (res == 0){
			cerr << "Timeout reading data" << endl;
			throw TestError("Timeout reading data");
		}
	} while (evCounter<nPulses);
}

bool PTpulse::run(string param)
{
	chip = tb.chip;
	
	//calibration settings
	tb.chipConfig.CAL.MODE = 2;		// voltage injection
	tb.chipConfig.CAL.POL = 0;		// negative injected charge, the CSA output is a positive pulse
	tb.chipConfig.CAL.DUR = 1;
	tb.chipConfig.CAL.PHI = 0;

	//CFD settings
	tb.chipConfig.DISC.PT = 100;
	tb.chipConfig.DISC.SEL_POL = 0;
	tb.chipConfig.DISC.EN_HYST = 1;
	tb.chipConfig.DISC.FORCE_EN_ZCC = 0;
	tb.chipConfig.DISC.FORCE_TH = 0;
	tb.chipConfig.DISC.SEL_COMP_MODE = 1;

	//CSA settings - medium gain
	tb.chipConfig.PRE.TP = 100;
	tb.chipConfig.PRE.RES = 1;
	tb.chipConfig.PRE.CAP = 1;

	//Set Global ARM DAC
	tb.chipConfig.DISC.arm_dac = 20;

	chip->execute();
	
    if (param == "PARALLEL")
        return runParallel();
    else
        return runSerial();
	return false;
}

// Scurves channel by channel
bool PTpulse::runSerial()
{
	uint32_t 						count;
	VFAT3chip::ChannelConfig_t		chCfg;
	std::array<long, 256> 			hist;
	double	                        hitArray[maxChargeDAC];
	VFAT3chip::ChannelConfig_t		chnCfg;

	calCfg = tb.chipConfig.CAL;

	// disable cal bit on all channels
	for (int i=0; i<128; i++)
	{
		chnCfg.zcc_dac = 0;
		chnCfg.arm_dac = 0;
		chnCfg.mask = false;
		chnCfg.cal = false;
		tb.chip->addSetChannelConfig(i,chnCfg);
	}
	chip->execute();

	// Init the charge injection
	tb.initInjCharge(calCfg);

	// Set consumer function
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, histogramCB);

	// Setup pulses
	board->pulser->setConfig(10, 20);	// 0.5 us (plus 250 ns) period

	// Send BC0
	board->comPortTx->sendBC0();

	// start run
	board->mRunControl->startRun();
	board->comPortTx->sendRunMode();

	// Loop on channels
	for (int ch=0; ch<128; ch++){
		chCfg = tb.chipConfig.CH[ch];
        
        // set cal bit
		chCfg.cal = true;
		chip->addSetChannelConfig(ch, chCfg);

        // Search threshold
        int chargeDAC=0;
	    for (int i=0x80; i!=0; i>>=1){
		    chargeDAC = chargeDAC|i;
            pulseAtChargeDAC(chargeDAC, numPulsesSearch);
		    if (histogram[ch] > numPulsesSearch/2)
			    chargeDAC &= ~i;
        }
        int threshold = chargeDAC;

        // Scan around the threshold
	    for (int chargeDAC=threshold; chargeDAC<256; chargeDAC++){
            pulseAtChargeDAC(chargeDAC, numPulses);

			// store data for the pulsed channel	
			hist[chargeDAC] = histogram[ch];

            if (hist[chargeDAC] == numPulses){
                // fill remaining points
                while (chargeDAC<256)
                    hist[chargeDAC++] = numPulses;
                break;
            }
        }    

	    for (int chargeDAC=threshold-1; chargeDAC>=0; chargeDAC--){
            pulseAtChargeDAC(chargeDAC, numPulses);

			// store data for the pulsed channel	
			hist[chargeDAC] = histogram[ch];

            if (hist[chargeDAC] == 0){
                while (chargeDAC>=0)
                    hist[chargeDAC--] = 0;
                break;
            }
        }    

		// reset cal bit
		chCfg.cal = false;
		chip->addSetChannelConfig(ch, chCfg);

		// copy into the result matrix
		for (int dac=0; dac<maxChargeDAC; dac++)
			hitMatrix[dac][ch] = hist[dac];

	}

	board->mTriggerControl->getTriggerCounter(&count);
	cout << "Trigger count:" << count << endl;
	board->mRunControl->stopRun();
	board->comPortTx->sendSConly();
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, NULL);

	// restore chip configuration
	chip->setConfig(tb.chipConfig);

//    cout<<"Press ENTER" << endl;
//    getchar();

    // Fitting data header
    cout << "Fitting data" << endl;

	// Fit matrix data
	for (int ch=0; ch<128; ch++)
	{
		chCfg = tb.chipConfig.CH[ch];
		if ((hitMatrix[0][ch] <= numPulses/2) && (hitMatrix[maxChargeDAC-1][ch] >= numPulses/2))
		{
			for (int i=0; i<maxChargeDAC; i++)
				hitArray[i] = hitMatrix[i][ch];

			double th, ENCval;
			double thErr, ENCerr;

			int ret = Tutils::fitErf(numPulses, maxChargeDAC, chargeArray, hitArray, 
						&th, &thErr, &ENCval, &ENCerr);
			//	if (ret)
			//		cout << "ch:" << ch << " th:" << th << " fC - ENC:" << noise << "fC" << endl;
		    
			if (ret && !chCfg.mask)
			{
		        thresholds[ch] = th;
		        ENC[ch] = fabs(ENCval);

			} 
			else 
			{
		        thresholds[ch] = 0;
		        ENC[ch] = 0;
			}
			vTH[ch] = thresholds[ch];
			vENC[ch] = ENC[ch];
		}
		else
		{
			thresholds[ch] = 0;
		    ENC[ch] = 0;
			cout << "Unable to fit channel " << ch << endl;
		}
	}

	return true;
}



bool PTpulse::runParallel(double *vENC, double *vTH)
{
	uint32_t 						count;
	//VFAT3chip::VFAT3config_t        chipCfg = tb.chipConfig;
	VFAT3chip::CalibrationConfig_t  calCfg = tb.chipConfig.CAL;
	VFAT3chip::ChannelConfig_t		chCfg;
	double	                        hitArray[maxChargeDAC];
	VFAT3chip::ChannelConfig_t		chnCfg;

	// enable cal bit on all channels
	/*for (int i=0; i<128; i++) chipCfg.CH[i].cal = true;
	chip->setConfig(chipCfg);*/
	for (int i=0; i<128; i++)
	{
		chnCfg.zcc_dac = 0;
		chnCfg.arm_dac = 0;
		chnCfg.mask = false;
		chnCfg.cal = true;
		tb.chip->addSetChannelConfig(i,chnCfg);
	}
	chip->execute();
	// Set consumer function
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, histogramCB);

	// Setup pulses
	board->pulser->setConfig(10, 1000);	// 25 us (plus 250 ns) period

	// Send BC0
	board->comPortTx->sendBC0();

	// start run
	board->mRunControl->startRun();
	board->comPortTx->sendRunMode();

	// Fill charge array
	
	for (int chargeDAC=0; chargeDAC<maxChargeDAC; chargeDAC++)
	{
		// set calibration DAC
		if (calCfg.MODE == 1)
		{
			// Vpulse mode
			chargeArray[chargeDAC] = ( ( tb.ptdata->VFAT3.calib_d.vpul_gain * ( 255 - chargeDAC ) + tb.ptdata->VFAT3.calib_d.vpul_offset ) - tb.ptdata->VFAT3.calib_d.cal_bsl ) * tb.ptdata->VFAT3.calib_d.Inj_Cap;	
		} 
		else if (calCfg.MODE == 2) 
		{
			chargeArray[chargeDAC] = ( tb.ptdata->VFAT3.calib_d.ipul_gain * chargeDAC + tb.ptdata->VFAT3.calib_d.ipul_offset ) * calCfg.DUR * 25e-9 * calCfg.FS * 0.1 * 1e6;
		} 
		else {
			cerr << "Please set Calibration MODE != 0" << endl;
			exit(0);
		}	
    }
	// Loop on charge
	for (int chargeDAC=0; chargeDAC<maxChargeDAC; chargeDAC++){
		
		// set calibration DAC
		if (calCfg.MODE == 1)
			calCfg.DAC = 255-chargeDAC;		// Vpulse mode
		else if (calCfg.MODE == 2)
			calCfg.DAC = chargeDAC;
		else {
			cerr << "Please set Calibration MODE != 0" << endl;
			exit(0);
		}
		chip->addSetCalibrationConfig(calCfg);
		chip->execute();
		usleep(1000);

		// start pulses injection				
		board->pulser->run(numPulses);
	
		resetHistogram();
		long res;
		do {
			res = board->pollData(50);		// timeout 50 ms
			if (res == 0){
				cerr << "Timeout reading data" << endl;
				exit(0);
			}
		} while (evCounter<numPulses);

   		// copy into the result matrix
		for (int ch=0; ch<128; ch++) hitMatrix[chargeDAC][ch] = histogram[ch];

	}

	board->mTriggerControl->getTriggerCounter(&count);
	cout << "Trigger count:" << count << endl;
	board->mRunControl->stopRun();
	board->comPortTx->sendSConly();
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, NULL);


    // Fitting data header
    cout << "Fitting data" << endl;

	// Fit matrix data
	for (int ch=0; ch<128; ch++){
		chCfg = tb.chipConfig.CH[ch];
		for (int i=0; i<maxChargeDAC; i++){
			hitArray[i] = hitMatrix[i][ch];
			//cout<<"hitArray[i] "<<hitArray[i]<<endl;
		}

		double th, ENCval;
		double thErr, ENCerr;

		int ret = Tutils::fitErf(numPulses, maxChargeDAC, chargeArray, hitArray, 
					&th, &thErr, &ENCval, &ENCerr);
	//	if (ret)
			//cout << "ch:" << ch << " th:" << th << " fC - ENC:" << ENCval << "fC" << endl;
        
		if (ret && !chCfg.mask){
            vTH[ch] = th;
            vENC[ch] = fabs(ENCval);
		} else {
            thresholds[ch] = 0;
            ENC[ch] = 0;
		}
	}
	return true;
}

bool PTpulse::rungainfast(double *vTH){

	uint32_t 						count;
	float dac1 = 0;
	float dac2 = 0;
	float np1 = 0;
	float np2 = 0;
	double val_m = 0;
	calCfg = tb.chipConfig.CAL;

	// disable cal bit on all channels
	for (int i=0; i<128; i++)
	{
		tb.chipConfig.CH[i].mask = false;
		tb.chipConfig.CH[i].cal = true;
	}
	tb.setChipConfig();

	// Init the charge injection
	tb.initInjCharge(calCfg);

	// Set consumer function
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, histogramCB);

	// Setup pulses
	board->pulser->setConfig(10, 380);	// 10 us (plus 250 ns) period

	// Send BC0
	board->comPortTx->sendBC0();

	// start run
	board->mRunControl->startRun();
	board->comPortTx->sendRunMode();

	for (int ch=0; ch<128; ch++)
	{
        // set cal bit
		tb.chipConfig.CH[ch].cal = true;
		tb.chip->execute();

        // Search threshold
        int chargeDAC=0;
	    for (int i=0x80; i!=0; i>>=1)
		{
		    chargeDAC = chargeDAC|i;
            pulseAtChargeDAC(chargeDAC, numPulsesSearch);
		    if (histogram[ch] > numPulsesSearch/2)
			    chargeDAC &= ~i;
        }
        int threshold = chargeDAC;
		if ( (threshold > 0 ) && (threshold < 255 ) )
		{
			for (int chargeDAC=threshold-2; chargeDAC<256; chargeDAC++) // remove 2 from threshold to be sure that it's lower than numPulses/2
			{
				pulseAtChargeDAC(chargeDAC, numPulses);

				if (histogram[ch] == numPulses/2)
				{
					val_m = chargeDAC;
					break;
				}
				if (histogram[ch] < numPulses/2)
				{
					dac1 = chargeDAC;
					np1 = histogram[ch];
				}
				if (histogram[ch] > numPulses/2) 
				{
					dac2 = chargeDAC;
					np2 = histogram[ch];
					break;
				}
			}
			if (!val_m)
			{
			val_m = (( numPulses - np1 ) / numPulses * dac1 + ( numPulses - np2 ) / numPulses * dac2 ) / ( ( numPulses - np1 ) / numPulses + ( numPulses - np2 ) / numPulses );
			}
			vTH[ch] = ( ( tb.ptdata->VFAT3.calib_d.vpul_gain * ( 255 - val_m ) + tb.ptdata->VFAT3.calib_d.vpul_offset ) - tb.ptdata->VFAT3.calib_d.cal_bsl ) * tb.ptdata->VFAT3.calib_d.Inj_Cap;
			val_m = 0;
		}
		else
		{
			vTH[ch] = 0;
			cout << "Error " << endl;
		}

		// reset cal bit
		tb.chipConfig.CH[ch].cal = false;
		tb.chip->execute();

	}

	board->mTriggerControl->getTriggerCounter(&count);
	cout << "Trigger count:" << count << endl;
	board->mRunControl->stopRun();
	board->comPortTx->sendSConly();
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, NULL);

	return true;

}

