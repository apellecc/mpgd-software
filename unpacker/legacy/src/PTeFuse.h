#ifndef PTEFUSE_H
#define PTEFUSE_H

#include "GenericTest.h"
#include "Reed_Muller.h"

class PTeFuse : public GenericTest
{
public:
    PTeFuse(TestBench &tb);
	~PTeFuse();
	bool run(string param);

private:
	IPbusSC *slowControl;
	Reed_Muller	ReedMuller;

private:
	bool read(int *chip_id);
	bool write();


};


#endif
