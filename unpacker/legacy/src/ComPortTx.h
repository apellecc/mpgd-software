/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */

#ifndef COMPORTTX_H
#define COMPORTTX_H
#include <mwbbslave.h>
#include <stdint.h>

class ComPortTx: public MWbbSlave
{
public:
    ComPortTx();
    ComPortTx(WishboneBus *wbbPtr, uint32_t baseAddress);
	~ComPortTx();
	void setBusAddress(WishboneBus *wbbPtr, uint32_t baseAddress);
	void addSetReg(uint16_t address, uint32_t val);
	void addSetNReg(uint16_t address, uint32_t *val, int size);
	void addRMWReg(uint16_t address, uint32_t mask, uint32_t val);
	void addGetReg(uint16_t address, uint32_t *val);
	void addGetNReg(uint16_t address, uint32_t *data, int size);
	void sendSCpkt(uint8_t hdlcAddress, int size, uint32_t *dataPtr);

private:					// WBB Slave registers map 
	enum regAddress_e {
		regOpCode		= 0,
		regHDLC			= 1,
		regRxStatus		= 2,
		regSyncStatus	= 3,
		regFEreset		= 4,
		regCfg			= 5,
		regHDLCtxBuffer		= 0x100,
		regHDLCtxBufferEnd	= 0x200
		};

private:
	typedef enum opCode_e {
		OC_EC0 = 			0x1,
		OC_BC0 = 			0x2,
		OC_CalPulse = 		0x3,
		OC_ReSync = 		0x4,
		OC_SCOnly = 		0x5,
		OC_RunMode = 		0x6,
		OC_LV1A = 			0x7,
		OC_SC0 = 			0x8,       
		OC_SC1 = 			0x9,       
		OC_ReSC = 			0xa,
		OC_LV1A_EC0 = 		0xb,
		OC_LV1A_BC0 =		0xc,
		OC_LV1A_EC0_BC0 =	0xd,
		OC_EC0_BC0 =		0xe,
		OC_SyncReq = 		0x10,
		OC_SyncVer = 		0x11
	} CP_OPCODE_t;

private:
	enum HDLC_e {
		HDLCaddressMask	= 0xff
	};

	enum regFEreset_e {
		FEreset		= 0x01,
		porDisable 	= 0x02,
		borDisable	= 0x04,
		PORmask		= 0x06
	};

	enum regCfg_e {
		ClkPhase = 0x01
	};
	
public:
	void sendCommand(CP_OPCODE_t opCode);
	void syncReq()		{ sendCommand(OC_SyncReq); }
	void syncVerify() 	{ sendCommand(OC_SyncVer); }
	void sendBC0()		{ sendCommand(OC_BC0); }
	void sendResync()	{ sendCommand(OC_ReSync); }
	void sendRunMode()	{ sendCommand(OC_RunMode); }
	void sendSConly()	{ sendCommand(OC_SCOnly); }
	void getSyncStatus(uint32_t *data) {addGetReg(regSyncStatus, data); execute();}
	void addClearSyncStatus(uint32_t data) {addSetReg(regSyncStatus, data);}
	void addGetRxStatus(uint32_t *st) {addGetReg(regRxStatus, st);}
	void getRxStatus(uint32_t *st) {addGetReg(regRxStatus, st); execute();}
	void addSetClkPhase(bool inv) {addSetReg(regCfg, inv);}

	void addPORsetup(bool porDis, bool borDis) {
				addRMWReg(regFEreset, ~PORmask, (porDis?porDisable:0)|(borDis?borDisable:0));
			}

	void addChipReset() {
				addRMWReg(regFEreset, ~FEreset, FEreset);
				addRMWReg(regFEreset, ~FEreset, 0);
			}
};



#endif // COMPORTTX_H
