/*
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>.
 *
 */

#ifndef ATE_H
#define ATE_H

#include <stdint.h>
#include "mwbbslave.h"

class ATE: public MWbbSlave
{
public:
    typedef struct {
        bool        running;
        bool        unknowStatement;
        bool        endPattern;
        uint32_t    currentPattern;
        uint8_t     logSize;
    } status_t;    

    typedef struct {
        uint32_t    chains;
        uint32_t    pattern;
        uint32_t    cellNum_PO;
    } error_t;    


public:
    ATE(WishboneBus *wbbPtr, uint32_t baseAddress);
    void getStatus(status_t *status);
    void getError(error_t *error);
    void addPushPatterns(int size, uint32_t *data);
    void readPatternFile(string fileName);
    void dumpErrorLog();
    bool run();

private:			
    uint32_t    *pattern;
    uint32_t    patternMemSize;
    long        errorCounter;

    // WBB Slave registers map 
	enum regAddress_e {
		regCtrl				    = 0,    // Control
		regScanLen      	    = 1,    // number of scan clock cycles needed to shift the longest chain (as specified in the spf file)
		regLoadUnloadCondition_L= 2,    // PI state to set during load_unload (as specified in the load_unload procedure definition in spf file)
		regLoadUnloadCondition_H= 3,    // PI state to set during load_unload (as specified in the load_unload procedure definition in spf file)
        regTestSetupCondition_L = 4,    // PI state to set by test_setup (as specified in the test_setup macro definition in spf file)
        regTestSetupCondition_H = 5,    // PI state to set by test_setup (as specified in the test_setup macro definition in spf file)
        regStatus               = 6,    // General status
        regPatternsMem          = 7,    // Pattern memory access (WO)
        regErrorLogMem_L        = 8,    // Error Log Memory access (RO) LSBits
        regErrorLogMem_H        = 9     // Error Log Memory access (RO) MSBits
	};

 	enum Status_fields_e {
        STATUS_RUNNING_MASK     = (1<<0),  // bit  0:   FSM is running (1: decoding patterns, 0:decode EndPattern statement)
        STATUS_RUNNING_SHIFT    = 0,
        STATUS_UNKNOW_MASK      = (1<<1),  // bit  1:   Run abort due to unknow statement in pattern stream
        STATUS_UNKNOW_SHIFT     = 1,
        STATUS_END_MASK         = (1<<2),  // bit  2:   Last pattern processed
        STATUS_END_SHIFT        = 2,
        STATUS_LOG_SIZE_MASK    = (0x1f<<3),  // bits 7:3  Number of errors stored in Error Log Memory
        STATUS_LOG_SIZE_SHIFT   = 3,
        STATUS_PATTERN_MASK     = (0xffffff<<8),  // bits 31:8 Current Pattern
        STATUS_PATTERN_SHIFT    = 8
    };

 	enum Error_fields_H_e {
        ERR_PATTERN_MASK       = (0xffffff<<0),  // bits 23:0  Pattern number
        ERR_PATTERN_SHIFT      = 0,
        ERR_CHAINS_MASK        = (0xff<<24),  // bits 31:24  chains
        ERR_CHAINS_SHIFT       = 24
    };

public:
    void addSetScanLen(uint32_t len) { wbb->addWrite(baseAddress+regScanLen, len); } 
    void addSetLoadUnloadCondition(uint64_t c) { 
            wbb->addWrite(baseAddress+regLoadUnloadCondition_L, c&0xffffffff); 
            wbb->addWrite(baseAddress+regLoadUnloadCondition_H, c>>32); 
        } 
    void addSetTestSetupCondition(uint64_t c) { 
            wbb->addWrite(baseAddress+regTestSetupCondition_L, c&0xffffffff); 
            wbb->addWrite(baseAddress+regTestSetupCondition_H, c>>32); 
        } 
    void addSetCtrl(bool run) { wbb->addWrite(baseAddress+regCtrl, run?1:0 ); }
   



};



#endif // ATE_H
