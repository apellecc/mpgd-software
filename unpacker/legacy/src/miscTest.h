/*
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>.
 *
 */

#ifndef MISCTEST_H
#define MISCTEST_H

#include "iomanip"
#include "GenericTest.h"
#include <unistd.h>

class cfg : public GenericTest
{
public:
    cfg(TestBench &tb) : GenericTest (tb) {};

    bool run(string fileName) {
        if (fileName == ""){
            cerr << "ERROR: Missing configurarion filename" << endl;
            return false;
        }

		tb.readConfigFile(fileName);
		tb.initBoard();
        return true;
    }
};

class chip_init : public GenericTest
{
public:
    chip_init(TestBench &tb) : GenericTest (tb) {};

    bool run(string fileName) {
	    tb.chipReset();
	    tb.chipInit();

        if (!tb.syncVerify()){
            cout << "Sync check FAILED" << endl;
	        return false;
        }

	    tb.setChipConfig();
        return true;
    }
};

class dump_config : public GenericTest
{
public:
    dump_config(TestBench &tb) : GenericTest (tb) {};

    bool run(string fileName) {
	    tb.chipInit();
	    tb.chip->dumpRegisters();
	    tb.chip->dumpConfig();
        return true;
    }
};

class infinite_pulse : public GenericTest
{
public:
    infinite_pulse(TestBench &tb) : GenericTest (tb) {};

    bool run(string param) {
	    uint32_t numPulses;

	    // setup the pulser
	    tb.board->pulser->setConfig(10, 10000, Pulser::OPMODE_ENPLS_BIT );

	    tb.board->mRunControl->startRun();
	    for (;;){
		    tb.board->pulser->run(200);
		    do {
			    tb.board->pulser->getStatus(&numPulses);
		    } while (numPulses);

		    usleep(1000);	// 1 ms between bursts
	    }
	    return true;
    }
};

class simpleReadout : public GenericTest
{
public:
    simpleReadout(TestBench &tb) : GenericTest (tb) {};

    bool run(string param) {
	    VFAT3board		*board = tb.board;
	    uint32_t 		count;

	    // Enable verbose
	    board->vfat3DataParser[tb.getDefaultChip()]->setVerbose(true);

	    // Send BC0
	    board->comPortTx->sendBC0();

	    // start run
	    board->comPortTx->sendRunMode();
	    board->mRunControl->startRun();

	    // Send pulses
	    board->pulser->setConfig(10, 10000-10 /*, Pulser::OPMODE_ENPLS_BIT*/ );
	    board->pulser->run(3);

	    long res;
	    do {
		    res = board->pollData(50);		// timeout 50 ms
	    } while (res);

	    board->mRunControl->stopRun();
	    board->mTriggerControl->getTriggerCounter(&count);
	    cout << "Trigger count:" << count << endl;

	    return true;
    }
};

class test_eFuse : public GenericTest
{
public:
    test_eFuse(TestBench &tb) : GenericTest (tb) {};

    bool run(string param) {
	    IPbusSC *slowControl = tb.board->slowControl;

	    uint32_t chipID;
	    slowControl->addRead(VFAT3chip::RegChipID, &chipID);
	    slowControl->execute();
	    printf("Before: 0x%08x\n", (int) chipID);
		
	    int prgTime = 20e-6/25e-9;
	    tb.chip->eFuseBit(2, prgTime);

	    slowControl->addRead(VFAT3chip::RegChipID, &chipID);
	    slowControl->execute();
	    printf("After: 0x%08x\n", (int) chipID);

	    return true;
    }
};

class test_powerONOFF : public GenericTest
{
public:
    test_powerONOFF(TestBench &tb) : GenericTest (tb) {};

    bool run(string param) {
        if (param == "OFF") {
            tb.board->onOffVDD(VFAT3board::VDD_ALL, false);
        } else {
            // switch on power supply
            tb.board->setVDD(VFAT3board::VDD_V12A, 1.2);
            tb.board->setVDD(VFAT3board::VDD_V12D, 1.2);
            tb.board->setVDD(VFAT3board::VDD_EFUSE, 2.5);
            tb.board->onOffVDD(VFAT3board::VDD_ALL, true);
        }        

        // read ADC
        VFAT3board::powerADC_t res;
		tb.board->setINAgain(VFAT3board::INA_GAIN_50);

        tb.board->getPowerADC(&res);
        cout << "  VddA:" << fixed << setprecision(3) << res.V_V12A << " V  " << fixed << setprecision(3) << res.I_V12A*1000.0 << " mA" << endl;
        cout << "  VddD:" << fixed << setprecision(3) << res.V_V12D << " V  " << fixed << setprecision(3) << res.I_V12D*1000.0 << " mA" << endl;
        cout << "VEfuse:" << fixed << setprecision(3) << res.V_EFUSE << " V" << endl;

	    return true;
    }
};

class test_dumpADC : public GenericTest
{
public:
    test_dumpADC(TestBench &tb) : GenericTest (tb) {};

    bool run(string param) {
        float vmon;

        tb.board->readmon(VFAT3board::V_AVDD, &vmon);
        cout << "V_AVDD: " << vmon << " V" << endl;
        tb.board->readmon(VFAT3board::I_AVDD, &vmon);
        cout << "I_AVDD: " << vmon << " V" << endl;
        tb.board->readmon(VFAT3board::V_DVDD, &vmon);
        cout << "V_DVDD: " << vmon << " V" << endl;
        tb.board->readmon(VFAT3board::I_DVDD, &vmon);
        cout << "I_DVDD: " << vmon << " V" << endl;
        tb.board->readmon(VFAT3board::V_EFUSE, &vmon);
        cout << "V_EFUSE: " << vmon << " V" << endl;
        tb.board->readmon(VFAT3board::V_SLVS, &vmon);
        cout << "V_SLVS: " << vmon << " V" << endl;
        tb.board->readmon(VFAT3board::V_VMON, &vmon);
        cout << "V_VMON: " << vmon << " V" << endl;
        tb.board->readmon(VFAT3board::V_BGR, &vmon);
        cout << "V_BGR: " << vmon << " V" << endl;
	    return true;
    }
};

class test_ATE : public GenericTest
{
public:
    test_ATE(TestBench &tb) : GenericTest (tb) {};

    void init(){
        tb.board->initATE();
    }

    bool run(string param) {
		tb.board->onOffVDD(VFAT3board::VDD_EFUSE, false);
		usleep(2000);

        tb.board->setTestType(TesterCtrl::TTYPE_ATE);
        tb.board->setDiffMux(VFAT3board::MUX_SEL_NONE);
        tb.board->runATE();

		tb.board->onOffVDD(VFAT3board::VDD_EFUSE, true);

	    return true;
    }

	
};

class test_BIST : public GenericTest
{
public:
    test_BIST(TestBench &tb) : GenericTest (tb) {};
    
    bool run(string param) {
        tb.board->setTestType(TesterCtrl::TTYPE_BIST);
        tb.board->setDiffMux(VFAT3board::MUX_SEL_NONE);
        tb.board->runBIST();
	    return true;
    }
};

class test_EYE : public GenericTest
{
public:
    test_EYE(TestBench &tb) : GenericTest (tb) {};
    
    bool run(string param) {
        tb.board->setTestType(TesterCtrl::TTYPE_EYE);
        tb.board->setDiffMux(VFAT3board::MUX_SEL_NONE);
	    return true;
    }
};

class set_default_chip : public GenericTest
{
public:
    set_default_chip(TestBench &tb) : GenericTest (tb) {};
    
    bool run(string param) {
        unsigned int chip = stoi(param);        

        cout << "Setting default chip to " << chip << endl;
        tb.setDefaultChip(chip);
	    return true;
    }
};


#endif // MISCTEST
