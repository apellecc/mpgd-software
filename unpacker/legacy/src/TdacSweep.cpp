/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#include "TestBench.h"
#include "TBconfig.h"
#include "VFAT3board.h"
#include "VFAT3chip.h"
#include "TdacSweep.h"
#include <iostream>
#include "Tutils.h"
#include <fstream>

using std::string;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;



TdacSweep::DAC_t DACs[] = {
	{ string("Imon_Calib_IDC"), VFAT3chip::RegCfgCal_0, VFAT3chip::CfgCal_0_DAC_mask, VFAT3chip::CfgCal_0_DAC_shift, 1, 255 },
	{ string("Imon_Preamp_InpTran"), VFAT3chip::RegCfgBias_1, VFAT3chip::CfgBias_1_PRE_I_BIT_mask, VFAT3chip::CfgBias_1_PRE_I_BIT_shift, 2, 255 },
	{ string("Imon_Preamp_LC"), VFAT3chip::RegCfgBias_2, VFAT3chip::CfgBias_2_PRE_I_BLCC_mask, VFAT3chip::CfgBias_2_PRE_I_BLCC_shift, 3, 63 },
	{ string("Imon_Preamp_SF"), VFAT3chip::RegCfgBias_1, VFAT3chip::CfgBias_1_PRE_I_BSF_mask, VFAT3chip::CfgBias_1_PRE_I_BSF_shift, 4, 63 },
	{ string("Imon_Shap_FC"), VFAT3chip::RegCfgBias_3, VFAT3chip::CfgBias_3_SH_I_BFCAS_mask, VFAT3chip::CfgBias_3_SH_I_BFCAS_shift, 5, 255 },
	{ string("Imon_Shap_Inpair"), VFAT3chip::RegCfgBias_3, VFAT3chip::CfgBias_3_SH_I_BDIFF_mask, VFAT3chip::CfgBias_3_SH_I_BDIFF_shift, 6, 255 },
	{ string("Imon_SD_Inpair"), VFAT3chip::RegCfgBias_4, VFAT3chip::CfgBias_4_SD_I_BDIFF_mask, VFAT3chip::CfgBias_4_SD_I_BDIFF_shift, 7, 255 },
	{ string("Imon_SD_FC"), VFAT3chip::RegCfgBias_5, VFAT3chip::CfgBias_5_SD_I_BFCAS_mask, VFAT3chip::CfgBias_5_SD_I_BFCAS_shift, 8, 255 },
	{ string("Imon_SD_SF"), VFAT3chip::RegCfgBias_5, VFAT3chip::CfgBias_5_SD_I_BSF_mask, VFAT3chip::CfgBias_5_SD_I_BSF_shift, 9, 63 },
	{ string("Imon_CFD_Bias1"), VFAT3chip::RegCfgBias_0, VFAT3chip::CfgBias_0_CFD_DAC_1_mask, VFAT3chip::CfgBias_0_CFD_DAC_1_shift, 10, 63 },
	{ string("Imon_CFD_Bias2"), VFAT3chip::RegCfgBias_0, VFAT3chip::CfgBias_0_CFD_DAC_2_mask, VFAT3chip::CfgBias_0_CFD_DAC_2_shift, 11, 63 },
	{ string("Imon_CFD_Hyst"), VFAT3chip::RegCfgHyst, VFAT3chip::CfgHyst_HYST_DAC_mask, VFAT3chip::CfgHyst_HYST_DAC_shift, 12, 63 },
//	{ string("Imon_CFD_Ireflocal"), VFAT3chip::, VFAT3chip::, VFAT3chip::, 13, 63 },
	{ string("Imon_CFD_ThArm"), VFAT3chip::RegCfgThr, VFAT3chip::CfgThr_ARM_DAC_mask, VFAT3chip::CfgThr_ARM_DAC_shift, 14, 255 },
	{ string("Imon_CFD_ThZcc"), VFAT3chip::RegCfgThr, VFAT3chip::CfgThr_ZCC_DAC_mask, VFAT3chip::CfgThr_ZCC_DAC_shift, 15, 255 },
	{ string("Vmon_Calib_Vstep"), VFAT3chip::RegCfgCal_0, VFAT3chip::CfgCal_0_DAC_mask, VFAT3chip::CfgCal_0_DAC_shift, 33, 255 },
	{ string("Vmon_Preamp_Vref"), VFAT3chip::RegCfgBias_2, VFAT3chip::CfgBias_2_PRE_VREF_mask, VFAT3chip::CfgBias_2_PRE_VREF_shift, 34, 255 },
	{ string("Vmon_Vth_Arm"), VFAT3chip::RegCfgThr, VFAT3chip::CfgThr_ARM_DAC_mask, VFAT3chip::CfgThr_ARM_DAC_shift, 35, 255 },
	{ string("Vmon_Vth_ZCC"), VFAT3chip::RegCfgThr, VFAT3chip::CfgThr_ZCC_DAC_mask, VFAT3chip::CfgThr_ZCC_DAC_shift, 36, 255 }
};

TdacSweep::TdacSweep(TestBench &t) : GenericTest(t)
{
}



bool TdacSweep::run(string param)
{
	VFAT3chip::MonitorConfig_t monCfg = tb.chipConfig.MON;
	uint32_t adc0[256], adc1[256];
	float adcFMC[256];

	// Open output file
	ofstream outputFile;
	outputFile.open (string("Data/")+string("DACsweep_")+Tutils::filenameSuffix()+".txt");

	for (const auto& dac: DACs){
		cout << dac.name << endl;
		outputFile << dac.name << endl;
				
		// set monitor mux
		monCfg.SEL = dac.monSel;
		tb.chip->addSetMonitorConfig(monCfg);
		tb.chip->addRMWreg(dac.reg, ~dac.mask, 0);
		tb.chip->execute();
		usleep(10000);		
		
		for (uint32_t v=0; v<=dac.maxValue; v++){
			// set DAC
			tb.chip->addRMWreg(dac.reg, ~dac.mask, v << dac.shift);

			// read values				
			tb.chip->addReadADC(0, &adc0[v]);
			tb.chip->addReadADC(1, &adc1[v]);
			tb.chip->execute();

			// Read External ADC
			adcFMC[v] = tb.getVmon();
//			cout << to_string(v) << " " << to_string(adc0) << " " << to_string(adc1) << " " << to_string(adcFMC) << endl;
		}
		
		for (uint32_t v=0; v<=dac.maxValue; v++)
			outputFile << adc0[v] << " ";
		outputFile << endl;

		for (uint32_t v=0; v<=dac.maxValue; v++)
			outputFile << adc1[v] << " ";
		outputFile << endl;

		for (uint32_t v=0; v<=dac.maxValue; v++)
			outputFile << adcFMC[v] << " ";
		outputFile << endl;
	}

	outputFile.close();
	return true;
}


