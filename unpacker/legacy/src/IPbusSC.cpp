/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 * 21/12/2015	Added mutex for multithread operation
 */
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <poll.h>
#include <iostream>
#include "VFAT3rcv.h"
#include "ComPortRx.h"
#include "IPbusSC.h"
#include "mexception.h"

IPbusSC::IPbusSC(ComPortTx *tx, int pktSize) 
		: IPbus(pktSize)
{
	comPortTx = tx;
	hdlcAddress = 0;
	numReceivers = 0;
}

IPbusSC::~IPbusSC()
{
}

void IPbusSC::addReceiver(ComPortRx *rcv)
{
	receivers.push_back(rcv);
	numReceivers++;
}

void IPbusSC::execute()
{
	uint32_t rxStatus;
	uint32_t txBuffer32[SC_PACKET_SIZE/4];
	uint32_t rxBuffer32[(SC_PACKET_SIZE/4)+1];

	int size = txSize/4;
	uint8_t *txBufPtr;
	std::lock_guard<std::recursive_mutex> lock(mutex);

	if (txSize==0)
		return;

	txBufPtr = txBuffer;
	for (int i=0; i<size; i++){
		txBuffer32[i]  = ((*txBufPtr++) & 0xff) << 24;
		txBuffer32[i] |= ((*txBufPtr++) & 0xff) << 16;
		txBuffer32[i] |= ((*txBufPtr++) & 0xff) << 8;
		txBuffer32[i] |= ((*txBufPtr++) & 0xff);
	}

	for (int i=0; i<3; i++){
		try {
			// Send the datatgram 
			comPortTx->sendSCpkt(hdlcAddress, size, txBuffer32);

			// Wait for answer
			do {
				try {
					comPortTx->getRxStatus(&rxStatus);		// wait for an answer
				} catch (MIPBusErrorReadTimeout &e) {
					cerr << "IPbusSC::execute timeout in comPortTx->getRxStatus(&rxStatus)" << endl;
					goto retryTX;
					// throw MIPBusErrorReadTimeout(e.what());
				} 

				for (int i=0; i<numReceivers; i++)
					if (rxStatus & (1<<i)){
						int exRxSize = getExpectedRxSize();
						int maxRxSize = exRxSize <= SC_PACKET_SIZE ? exRxSize : SC_PACKET_SIZE;
						receivers[i]->addGetRxStatus(&rxStatus);

						receivers[i]->addGetSCpkt((maxRxSize/4)+1, rxBuffer32);
						receivers[i]->execute();
						if ((rxStatus & VFAT3rcv::HDLCready) != VFAT3rcv::HDLCready)
							throw MIPBusError(string(name()) + " comPortTx status register differs from receiver HDLC ready register");

						// move data from rxBuffer32 to rxBuffer
						rxSize = rxBuffer32[0]*4;
						uint32_t *rx = rxBuffer32+1;
						for (int j=0; j<rxSize;){
							rxBuffer[j++] = (*rx >> 24) & 0xff;
							rxBuffer[j++] = (*rx >> 16) & 0xff;
							rxBuffer[j++] = (*rx >> 8) & 0xff;
							rxBuffer[j++] = (*rx) & 0xff;
							rx++;
						}
						break;
					}	
			} while (duplicatedRxPkt());

			// check the answer packet content
			processAnswer();
			return;

		} catch (MIPBusErrorReadTimeout) {
			throw MIPBusErrorReadTimeout(string(name()) + " Timeout error - Is the chip connected?");
		}
retryTX:
		while(0);	
	}
	throw MIPBusError("Chip comunication error in IPbusSC::execute");	
}


