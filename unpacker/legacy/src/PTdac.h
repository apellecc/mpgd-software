/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */

#include "GenericTest.h"
#include "VFAT3_data.h"

class PTdac : public GenericTest
{
public:
 	PTdac(TestBench &tb);
	bool run(string param);

typedef struct DAC_s {
	string name;
	uint32_t reg;
	uint32_t mask;
	uint32_t shift;
	uint32_t monSel;
	uint32_t maxValue;
	float optVal;
	float gain_min;
	float gain_max;
	float offset_min;
	float offset_max;
	int fit_min;
	int fit_max;
} DAC_t;

private:
	bool dac_check(VFAT3_data::DAC_s *DAC_s, DAC_t dac); 
};




