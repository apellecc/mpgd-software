
#include <string>
#include "TcpClient.h"



class RTOscope : public TcpClient
{
public:
	typedef struct measStatistics_s {
		double	ACTual;
		double	AVG;
		double	EVTCount;
		double	NPEak;
		double	PPEak;
		double	RELiability;
		double	RMS;
		double	WFMCount;
		double	STDDev;
		double	STARt;
		double	STOP;
	} measStatistics_t;

public:
	RTOscope();
	~RTOscope();
	
	void connectToServer( string &hostname, int port=5025);
	void sendCommand(std::string cmd, std::string *res = NULL);
	void waitCompletition();

	void measAmplitudeSetup(int measChannel);
	void measTimeSetup(int measChannel);

	void run();
	double measGetVal(int measChannel);
	void enableStatistics(int measChannel, bool en=true);
	void resetStatistics();
	bool measGetStatistics(int measChannel, measStatistics_t *stat);
	int measGetEVTCount(int measChannel);

};
