/*
 * Copyright (C) 2015
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2015.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include "mexception.h"
#include "VFAT3board.h"
#include "hardware_settings.h"
#include "TestError.h"

// 320 Mbps serial lines
I2CSysPll::pllRegisters_t VFAT3board::sysPLLregContent = {
	.reg = {
			/* Register 0: */ 0x02A9,
			/* Register 1: */ 0x0000,
			/* Register 2: */ 0x000E,
			/* Register 3: */ 0x08F5,
			/* Register 4: */ 0x346F,	// set to 0x346f to set reference clock from secondary input (0x246f for primary)
			/* Register 5: */ 0x0023,
			/* Register 6: */ 0x0004,
			/* Register 7: */ 0x0023,
			/* Register 8: */ 0x0004,
			/* Register 9: */ 0x0003,
			/* Register 10: */ 0x0040,
			/* Register 11: */ 0x0000,
			/* Register 12: */ 0x0003,  // 0x0003, 0x2003 bypass Y5 from primary input
			/* Register 13: */ 0x0040,
			/* Register 14: */ 0x0000,
			/* Register 15: */ 0x0003,
			/* Register 16: */ 0x0040,
			/* Register 17: */ 0x0000,
			/* Register 18: */ 0x0003,
			/* Register 19: */ 0x0040,
			/* Register 20: */ 0x0000,
			/* Register 21: */ 0x0006			// RO register
		}
	};


VFAT3board::VFAT3board() : MBoard()
{
	init();
}

/*
VFAT3board::VFAT3board(const char *IPaddr, int port ) : 
				MBoard(IPaddr, port)
{
	init();
	initSysPll();
	for (int i=0; i<NUM_VFAT3_RECEIVERS; i++)
		vfat3Rcv[i]->addEnable(false);

    initADC();

#ifdef BENCH_VFAT3_PKG
    setTestType(TesterCtrl::TTYPE_Functional);
    setDiffMux(VFAT3board::MUX_SEL_TXD);
    setExtInj(false, false);
#endif
}
*/

void VFAT3board::setIPaddress(const char *IPaddr, int port)
{
	MBoard::setIPaddress(IPaddr, port);
	init();

	initSysPll();
	for (int i=0; i<NUM_VFAT3_RECEIVERS; i++)
		vfat3Rcv[i]->addEnable(false);

    initADC();
#ifdef BENCH_VFAT3_PKG
    setTestType(TesterCtrl::TTYPE_Functional);
    setDiffMux(VFAT3board::MUX_SEL_TXD);
    setExtInj(false, false);
#endif
}

void VFAT3board::init()
{
	//
	// Add components to the base mosaic board
	//
	// Pulser
	pulser = new Pulser(mIPbus, add_pulser);

	// Com port
	for (int i=0; i<NUM_VFAT3_RECEIVERS; i++){
	    comPortTxArray[i] = new ComPortTx(mIPbus, add_vfat3ComPortTx_0+((i*2)<<24));
    }
    comPortTx = comPortTxArray[0];                  // the comport of selected chip

	// IPBus for chip slow control	
	for (int i=0; i<NUM_VFAT3_RECEIVERS; i++){
	    slowControlArray[i] = new IPbusSC(comPortTxArray[i]);
    }
    slowControl = slowControlArray[0];              // the slowcontrol interface of selected chip

	// FMC I2C bus master
#ifdef HAS_FMC_ADC
	i2cBus = new I2Cbus(mIPbus, add_i2cFMC);
#endif

	// VFAT3 data receiver
	for (int i=0; i<NUM_VFAT3_RECEIVERS; i++){
		vfat3Rcv[i] = new VFAT3rcv(mIPbus, add_vfat3Rcv_0+((i*2)<<24));
		slowControlArray[i]->addReceiver(vfat3Rcv[i]);
    	slowControlArray[i]->setHDLCaddress(0);
	}

	// Trigger recorder
	#ifdef HAS_TRG_RECORDER
    	trgRcv = new TRGrcv(mIPbus, add_trgRcv);
    #endif

	// The data consumer for hardware generator 
	dr = new GenConsumer;
	addDataReceiver(0, dr);				// ID 0

	for (int i=0; i<NUM_VFAT3_RECEIVERS; i++){			// ID 1-10
		VFAT3dataParser	*vfat3Parser;
		vfat3Parser = new VFAT3dataParser();
		vfat3Parser->setId(i);
		vfat3DataParser[i] = vfat3Parser;

		addDataReceiver(i+1, vfat3Parser);	
	}

	// Trigger recorder data consumers
	trgDataParser = new TRGdataParser();
	addDataReceiver(NUM_VFAT3_RECEIVERS+1, trgDataParser);

#ifdef HAS_FMC_ADC
    adcMon[0]=NULL;
    adcMon[1]=NULL;
#endif

#ifdef BENCH_VFAT3_PKG
    // Tester control
    testerCtrl  = new TesterCtrl(mIPbus, add_TesterCtrl);

    // ATE
    ate = new ATE(mIPbus, add_ATE);

    rdacVadj    = new AD5254 (i2cBus, I2Caddress_rdacVadj);
    regCFG[0]   = new PCF8574(i2cBus, I2Caddress_CFG_0);
    regCFG[1]   = new PCF8574(i2cBus, I2Caddress_CFG_1);
    adcMon[0]   = new ADS1115(i2cBus, I2Caddress_adcMon_0);
    adcMon[1]   = new ADS1115(i2cBus, I2Caddress_adcMon_1);
    
#elif defined ( HAS_FMC_ADC )
	adcMon[1]   = new ADS1113(i2cBus, 0x49);
#endif
}

void VFAT3board::initADC()
{
#ifdef HAS_FMC_ADC
    if (adcMon[0]!=NULL)
        adcMon[0]->setConfiguration();
    if (adcMon[1]!=NULL)
        adcMon[1]->setConfiguration();
#endif
}

VFAT3board::~VFAT3board() 
{
	// delete objects in creation reverse order
#ifdef BENCH_VFAT3_PKG
    delete adcMon[0];
    delete adcMon[1];
    delete regCFG[0];
    delete regCFG[1];
    delete rdacVadj;
#endif

	delete trgDataParser;
	for (int i=0; i<NUM_VFAT3_RECEIVERS; i++)
		delete vfat3DataParser[i];
	delete dr;
	for (int i=0; i<NUM_VFAT3_RECEIVERS; i++)
		delete  vfat3Rcv[i];
	delete comPortTx;
	delete pulser;
}

void VFAT3board::waitBoardReady()
{
	#ifndef USE_GTP
		return;
	#endif

	// wait for board to be ready
	uint32_t boardStatusReady = ( \
								BOARD_STATUS_GTPLL_LOCK | 
								BOARD_STATUS_EXTPLL_LOCK | 
								BOARD_STATUS_FEPLL_LOCK );

    #ifdef BENCH_VFAT3_X5
        boardStatusReady |= (GTP_USE_MAP<<16);
    #else
	    for (int i=0; i<NUM_VFAT3_RECEIVERS; i++)
		    boardStatusReady |= (BOARD_STATUS_GTP_RESET_DONE<<i);
    #endif 


	// wait 1s for transceivers reset done
	long int init_try;
	for (init_try=1000; init_try>0; init_try--){
		uint32_t st;
		usleep(1000);
		mRunControl->getStatus(&st);
		if ((st & boardStatusReady) == boardStatusReady)
			break;
	}
	if (init_try==0)
		throw MBoardInitError("Timeout");
}

void VFAT3board::resetGTP()
{
    // stop PLL clock to reset GTP 
   	mRunControl->setConfigReg(1);
    usleep(10000);
   	mRunControl->setConfigReg(0);

    // Wait for all PLLs lock and GTP receivers finish the reset 
    waitBoardReady();
}

void VFAT3board::initSysPll()
{
	#ifndef USE_GTP
		return;
	#endif

	// Initialize the System PLL
	mSysPLL->setup(sysPLLregContent);

    // Wait for all PLLs lock and GTP receivers finish the reset 
    waitBoardReady();
}


void VFAT3board::setVerbose(bool v)
{
	for (int i=0; i<NUM_VFAT3_RECEIVERS; i++){
		vfat3DataParser[i]->setVerbose(v);
	}
}

void VFAT3board::addDisableAllReceivers()
{
	for (int i=0; i<NUM_VFAT3_RECEIVERS ; i++){
			vfat3Rcv[i]->addEnable(false);
		}
}

void VFAT3board::addEnableReceiver(int num, bool en)
{
	vfat3Rcv[num]->addEnable(en);
}

void VFAT3board::addReceiverInvertInput(int num, bool en)
{
	vfat3Rcv[num]->addInvertInput(en);
}

void VFAT3board::onOffVDD(VDDch_t ch, bool on)
{
#ifdef BENCH_VFAT3_PKG
    int mask = 0;

    switch (ch) {
        case VDD_V12A:      mask = 1<<0; break;
        case VDD_V12D:      mask = 1<<1; break;
        case VDD_EFUSE:     mask = 1<<2; break;
        case VDD_V25D:      mask = 1<<3; break;
        case VDD_ALL:       mask = 0x0f; break;
        default: throw MException("Illegal channel selection in VFAT3board::onOffVDD");
    }

    // Setting register output will DISABLE regulator
    if (on)
        cfg[0] &= ~mask;
    else
        cfg[0] |= mask;
      
    regCFG[0]->write( cfg[0] );
#endif
}

void VFAT3board::setINAgain(INAgain_t gain)
{
#ifdef BENCH_VFAT3_PKG
    uint8_t gg=0;

    switch (gain) {
        case INA_GAIN_25:   gg = 0; INAgain = 25.0; break;
        case INA_GAIN_50:   gg = 2; INAgain = 50.0; break;
        case INA_GAIN_100:  gg = 1; INAgain = 100.0; break;
        case INA_GAIN_200:  gg = 3; INAgain = 200.0; break;
        default: throw MException("Illegal gain selection in VFAT3board::setINAgain");
    }

    cfg[0] &= ~0x30;
    cfg[0] |= gg<<4;
    regCFG[0]->write( cfg[0] );
#endif
}

void VFAT3board::setDiffMux(MUX_SEL_t mux)
{
#ifdef BENCH_VFAT3_PKG
    uint16_t mm = 1<<mux;

    cfg[0] &= ~0xc0;
    cfg[0] |= (mm&0x03) << 6;
    cfg[1] = (mm >> 2);

    regCFG[0]->write( cfg[0] );
    regCFG[1]->write( cfg[1] );
#endif
}

void VFAT3board::getPowerADC(powerADC_t *res)
{
#ifdef BENCH_VFAT3_PKG
	float tmp;
	const float Rshunt = 0.15; 

    adcMon[0]->convert(0, &res->V_V12A);
    adcMon[0]->convert(1, &tmp);
	res->I_V12A = tmp / (INAgain * Rshunt);
    adcMon[0]->convert(2, &res->V_V12D);
    adcMon[0]->convert(3, &tmp);
	res->I_V12D = tmp / (INAgain * Rshunt);

    adcMon[1]->convert(0, &res->V_EFUSE);
	
#else
    res->V_V12A = 0;
    res->I_V12A = 0;
    res->V_V12D = 0;
    res->I_V12D = 0;
    res->V_EFUSE = 0;
#endif
}

void VFAT3board::readmon(inADC_t param, float *Out)
{
#ifdef BENCH_VFAT3_PKG
	float tmp;
	const float Rshunt = 0.15; 

	switch (param)
	{
		case V_AVDD:
		    adcMon[0]->convert(0, &tmp);
			*Out = tmp;
			break;

		case I_AVDD:
			adcMon[0]->convert(1, &tmp);
			*Out = tmp / (INAgain * Rshunt);
			break;

		case V_DVDD:
			adcMon[0]->convert(2, &tmp);
			*Out = tmp;
			break;

		case I_DVDD:
			adcMon[0]->convert(3, &tmp);
			*Out = tmp / (INAgain * Rshunt);
			break;
		
		case V_EFUSE:
		    adcMon[1]->convert(0, &tmp);
			*Out = tmp;
			break;

		case V_SLVS:
			adcMon[1]->convert(1, &tmp);
			*Out = tmp;
			break;

		case V_VMON:
			adcMon[1]->convert(2, &tmp);
			*Out = tmp;
			break;

		case V_BGR:
			adcMon[1]->convert(3, &tmp);
			*Out = tmp;
			break;

		default:
			cout << "Error ADCs choice" << endl;
			*Out = 0;
			break;
	}
	
#else

    *Out = 0;

#endif
}



// set R-trim for VDD tuning
void VFAT3board::setVDDrdac(VDDch_t ch, uint8_t data)
{
#ifdef BENCH_VFAT3_PKG
    uint8_t c;

    switch (ch) {
        case VDD_V12A:      c=0; break;
        case VDD_V12D:      c=1; break;
        case VDD_EFUSE:     c=2; break;
        default: throw MException("Illegal channel selection in VFAT3board::setVDDrdac");
    }
    
    rdacVadj->setRDAC(c, data);
#endif
}

// set VDD based on nominal resistor values
void VFAT3board::setVDD(VDDch_t ch, float v)
{
#ifdef BENCH_VFAT3_PKG
    uint8_t c;
    uint8_t data;
    float Rb=4.7e3;          // Resistor from FB to GND

    switch (ch) {
        case VDD_V12A:      c=0; Rb=4.7e3; break;
        case VDD_V12D:      c=1; Rb=4.7e3; break;
        case VDD_EFUSE:     c=2; Rb=3.3e3; break;
        default: throw MException("Illegal channel selection in VFAT3board::setVDD");
    }
    
    const float Vr = 0.8;           // reference voltage
    const float Ra = 220;           // Resistor from sensing point
    const float Rw = 200;            // Equivalent wipe resistor 
    const float Rtot = 10e3;        // Total rheostat resistance 

    float Rx = (Rb*(v-Vr) / Vr) - Ra -Rw;
    if (Rx<0)
        Rx = 0;
    if (Rx > Rtot)
        Rx = Rtot;

    data = Rx * (255.0 / Rtot);     // convertion to digital
    rdacVadj->setRDAC(c, data);
#endif
}


void VFAT3board::runBIST()
{
    const long BISTcounterOK = 1080301;
    TesterCtrl::BISTstatus_t status;

    testerCtrl->BISTstart();
    
    do {
        testerCtrl->BISTgetStatus(&status);
    } while (status.running);
    cout << "BIST stop after " << status.counter << endl;
    if (status.counter != BISTcounterOK)
        throw TestError ("BIST failed");
}

//
// Select the default VFAT3 chip.
// The default chip os the chip pointed by 
//  comPortTx and slowControl variables.
// As altenative you can use the array version comPortTxArray[] and  slowControlArray[]
//
void VFAT3board::setDefaultChip(unsigned int chip)
{
    if (chip < NUM_VFAT3_RECEIVERS) {
        comPortTx = comPortTxArray[chip];
        slowControl = slowControlArray[chip];
    } else {
        throw TestError ("Try to set default chip to invalid value");
    }
}


