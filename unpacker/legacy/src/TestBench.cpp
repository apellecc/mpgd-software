/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#include "hardware_settings.h"
#include "mexception.h"
#include "TestBench.h"
#include "TBconfig.h"
#include "VFAT3board.h"
#include "TestError.h"
#include "RTOscope.h"

using std::string;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;

// external monitor resistor value
const float TestBench::Rmon = 20e3;

// Parameter for time measuremants
const long TestBench::pulsePeriod = (1<<9);		// 512 clock cycles
const long TestBench::acceptWindow = (1<<4);		// max 16 clock cycles after Calib pulse 
const long TestBench::acceptMask = (pulsePeriod-1) & ~(acceptWindow-1);	// accept mask 



TestBench::TestBench()
{
	const int CHIP_ID = 0;

	// Configuration manager
	config = new TBconfig();

	// The MOSAIC board with VFAT3 intefaces
	board = new VFAT3board();

    for (int i=0; i<NUM_VFAT3_RECEIVERS; i++){
	    chipArray[i] = new VFAT3chip(CHIP_ID, board->slowControlArray[i], board->vfat3Rcv[i], board->vfat3DataParser[i]);
    }

	// Deafult chip
	chip = chipArray[0];
    defaultChipNum = 0;

	// The RTO scope
	scope = new RTOscope();

	// Production test output data
	ptdata = new VFAT3_data();
}

void TestBench::readConfigFile(string &fileName)
{
	if (!config->readFile(fileName))
		exit(1);
	scopeHostname = config->ScopeAddress();
	MOSAICHostname = config->MosaicAddress();
	chipConfig = config->VFAT3cfg();
}

void TestBench::getDefaultConfig()
{
	scopeHostname = config->ScopeAddress();
	MOSAICHostname = config->MosaicAddress();
	chipConfig = config->VFAT3cfg();
}

void TestBench::initBoard()
{
	board->setIPaddress(MOSAICHostname.c_str());
	std::cout<<"MOSAICHostname: "<<MOSAICHostname.c_str()<<std::endl;
	// setup run control
	std::cout<<"setup run control"<<std::endl;
	board->mRunControl->stopRun();
	std::cout<<" - stopRun"<<std::endl;
	board->mRunControl->clearErrors();
	std::cout<<" - clearErrors"<<std::endl;
	board->mRunControl->setAFThreshold(512000);  
	std::cout<<" - setAFThreshold"<<std::endl;
	board->mRunControl->setLatency(MRunControl::latencyModeEoe, 0);
	std::cout<<" - setLatency"<<std::endl;
	board->mRunControl->setConfigReg(0);		// can be OR of bits: CFG_CLOCK_SEL_BIT
	std::cout<<" - setConfigReg"<<std::endl;
	// setup the POR
	std::cout<<"setup the POR"<<std::endl;
	board->comPortTx->addPORsetup(true, true); // bool porDisable, bool borDisable
	
	// enable Trigger recorder receiver
	#ifdef HAS_TRG_RECORDER
		board->trgRcv->addEnable(true);
		std::cout<<"enable Trigger recorder receiver"<<std::endl;
	#endif

	// Trigger control
	std::cout<<"Trigger control"<<std::endl;
	board->mTriggerControl->addEnableExtTrigger(true, true);	// External trigger, level mode

	// setup the pulser
	std::cout<<"setup the pulser"<<std::endl;
	board->pulser->setConfig(160, 1000);

	// open TCP connection
	std::cout<<"open TCP connection"<<std::endl;
	board->connectTCP();
}


void TestBench::setChipConfig()
{
	// configure chip
	chip->setConfig(chipConfig);

	// configure receiver block
	VFAT3rcv::dataformat_t df;
	df.enablePZS = chipConfig.RO.EnablePZS;
	df.PZSmaxPartitions = chipConfig.RO.MAX_PAR;
	df.eventCountSize = chipConfig.RO.ECb;
	df.bunchCrossSize = chipConfig.RO.BCb;
	board->vfat3Rcv[defaultChipNum]->addSetDataFormat(df);
	board->vfat3Rcv[defaultChipNum]->execute();

	// configure data parser
	board->vfat3DataParser[defaultChipNum]->setDataFormat(df);
}

void TestBench::chipReset()
{
	cout << "Resetting the chip..." << endl;
	board->comPortTx->addChipReset();
	board->comPortTx->execute();
}


void TestBench::chipInit()
{
    // Reset all GTP receivers
    board->resetGTP();

	// Enable the receiver
	board->addEnableReceiver(defaultChipNum, true);
	board->addReceiverInvertInput(defaultChipNum, false);
	board->mIPbus->execute();
	usleep(5000);

	// Sync
	cout << "Try to sync VFAT3 chip" << endl;
	if (!sync()){
		cerr << "TestBench::chipInit Timeout" << endl;
		throw TestError("Timeout in chip init");
	}
	cout << "Sync OK" << endl;
}


bool TestBench::sync()
{
	uint32_t st = 0xffffffff;

	// Sync
	board->comPortTx->addSetClkPhase(0);
	board->comPortTx->addClearSyncStatus(st);
	board->comPortTx->execute();
	board->comPortTx->syncReq();

	int i;
	for (i=100; i>0; i--){
		board->comPortTx->getSyncStatus(&st);
		if (st == 0x01)
			break;
	}
	if (i==0)
		return false;

	board->comPortTx->addClearSyncStatus(st);
	return true;
}



bool TestBench::syncVerify()
{
	uint32_t st = 0xffffffff;

	board->comPortTx->addClearSyncStatus(st);
	board->comPortTx->execute();
	board->comPortTx->syncVerify();

	int i;
	for (i=100; i>0; i--){
		board->comPortTx->getSyncStatus(&st);
		if (st == 0x01)
			break;
	}
	if (i==0)
		return false;

	board->comPortTx->addClearSyncStatus(st);
	return true;
}

float TestBench::getVmon(int sel)
{
	VFAT3chip::MonitorConfig_t monCfg = chipConfig.MON;
	
    if (sel != -1){
	    // set Monitor selection mux
	    monCfg.SEL = sel;
	    //  Set chip mux
	    chip->addSetMonitorConfig(monCfg);
	    chip->execute();
	    usleep(1000);
    }

#ifdef HAS_FMC_ADC
	float Vmon;
	// Read External ADC
	board->readVmon(&Vmon);
	return Vmon;
#else
	uint32_t adc;
	// read internal ADC				
	chip->addReadADC(0, &adc);
	chip->execute();

	return ((float) adc * chipConfig.ADC.LSB - chipConfig.ADC.OFFSET);
#endif
}


void TestBench::initInjCharge(VFAT3chip::CalibrationConfig_t &calCfg)
{
	bool pol = calCfg.POL;	// save	polarity
	int dac = calCfg.DAC;

	/*
		Voltage step injection
	*/
	// read Vstep fixed value
	calCfg.POL = 1;
	chip->addSetCalibrationConfig(calCfg);
	chip->execute();
	InjVstepBase = getVmon(VFAT3chip::Vmon_Calib_Vstep);

	// read Vstep for DAC=0 and DAC=255
	calCfg.POL = 0;
	calCfg.DAC = 0;
	chip->addSetCalibrationConfig(calCfg);
	chip->execute();
	float V0 = getVmon(VFAT3chip::Vmon_Calib_Vstep);

	calCfg.DAC = 255;
	chip->addSetCalibrationConfig(calCfg);
	chip->execute();
	float V1 = getVmon(VFAT3chip::Vmon_Calib_Vstep);

	InjVstepLSB = (V1-V0)/255;
	InjVstepOffset = V0;
//	cout << "InjVstepLSB:" << InjVstepLSB << " InjVstepOffset:" << InjVstepOffset << endl;

	/*
		Current injection
	*/
	calCfg.DAC = 0;
	chip->addSetCalibrationConfig(calCfg);
	chip->execute();
	float VI0 = getVmon(VFAT3chip::Imon_Calib_IDC);
	calCfg.DAC = 255;
	chip->addSetCalibrationConfig(calCfg);
	chip->execute();
	IpulseLSB = (getVmon(VFAT3chip::Imon_Calib_IDC) - VI0) / 255;
//	cout << "IpulseLSB:" << IpulseLSB << endl;


	// Restore Calibration configuration
	calCfg.POL = pol;
	calCfg.DAC = dac;
	chip->addSetCalibrationConfig(calCfg);
	chip->execute();
}

#ifdef HAS_FMC_ADC
//
//  If external ADC is present, Injection charge is evaluated measuring Vstep for Vpulse mode
//  and Iinj for Ipulse mode
//
float TestBench::getInjCharge(VFAT3chip::CalibrationConfig_t &calCfg)
{
	float injCharge;
	bool pol = calCfg.POL;	// save	polarity


	if (calCfg.MODE == 1){	// Vpulse mode
		// read Vstep fixed value
		calCfg.POL = 1;
		chip->addSetCalibrationConfig(calCfg);
		chip->execute();
		float VstepBase = getVmon(VFAT3chip::Vmon_Calib_Vstep);

		// Read variable value
		calCfg.POL = 0;
		chip->addSetCalibrationConfig(calCfg);
		chip->execute();
		float VStepCal = getVmon(VFAT3chip::Vmon_Calib_Vstep);
		injCharge = (VStepCal - VstepBase) * 0.1 * 1e3;
        //cout << "VStepCal " << VStepCal << "       VstepBase " << VstepBase << endl;

		// restore polarity bit
		calCfg.POL = pol;
		chip->addSetCalibrationConfig(calCfg);
		chip->execute();
		usleep(1000);

	} else if (calCfg.MODE == 2) {	// Ipulse mode
		// read reference current
		float VIref = getVmon(VFAT3chip::Imon_Iref);

		// Read injection current
		float VIcal = getVmon(VFAT3chip::Imon_Calib_IDC);
		injCharge = ((VIcal - VIref) / Rmon) * calCfg.DUR * 25e-9 * 1e15 * 0.1; // in fC

	} else {
		throw MException( "Try to measure Injection charge with calCfg.MODE == 0"); 
	}

	if (pol)
		return -injCharge;
	else
		return injCharge;
}

#else // !HAS_FMC_ADC
//
//  If external ADC is NOT present, Injection charge is evaluated by the chip settings and 
//  full scale Voltages measured at injection initialization by the internal ADC
//
float TestBench::getInjCharge(VFAT3chip::CalibrationConfig_t &calCfg)
{
	float injCharge;

	if (calCfg.MODE == 1){	// Vpulse mode
		float VStepCal = (calCfg.DAC*InjVstepLSB)+InjVstepOffset;
		injCharge = (VStepCal - InjVstepBase) * 0.1 * 1e3;
	} else if (calCfg.MODE == 2) {	// Ipulse mode
		// Read injection current
		float VIcal = (calCfg.DAC*IpulseLSB);
		injCharge = (VIcal / Rmon) * calCfg.DUR * 25e-6 * 1e12 * 0.1;
	} else {
		throw MException( "Try to measure Injection charge with calCfg.MODE == 0"); 
	}

	if (calCfg.POL)
		return -injCharge;
	else
		return injCharge;
}
#endif // HAS_FMC_ADC


/*
	Delay measurementes functions
*/
void TestBench::resetChDelayHistogram()
{
	evCounter = 0;
	for (int i=0; i<16; i++)
		ChDelayHistogram[i] = 0;
}

int TestBench::maxChDelayHistogram()
{
	int maxPos = -1;
	int maxVal = -1;
	for (int i=0; i<16; i++){
		if (ChDelayHistogram[i]>maxVal){
			maxVal = ChDelayHistogram[i];
			maxPos = i;
		}
	}
	return maxPos;
}

void TestBench::ChDelayCB(void *ctx, VFAT3dataParser::VFAT3data_t *data)
{
	TestBench *p = (TestBench *) ctx;

	if ((data->BC & acceptMask) != 0)
		return;
	p->evCounter++;

	int slot = data->BC & 0xf;
	p->ChDelayHistogram[slot]++;
}

int TestBench::measBCforPhase(int ph, VFAT3chip::CalibrationConfig_t &calCfg)
{
	calCfg.PHI = ph; 
	chip->addSetCalibrationConfig(calCfg);
	chip->execute();

	// start pulses injection				
	board->comPortTx->sendRunMode();
	board->pulser->run(numDelayMeasurePulses);

	resetChDelayHistogram();
	long res;
	do {
		res = board->pollData(5);		// timeout 5 ms
	} while (evCounter<numDelayMeasurePulses && res!=0);

	// stop reading and flush pending data
	board->comPortTx->sendSConly();
	board->comPortTx->sendResync();
	do {
		res = board->pollData(0);
	} while (res!=0);

	return maxChDelayHistogram();
}

float TestBench::measChDelay(VFAT3chip::CalibrationConfig_t &calCfg)
{
	int bcVector[8];

	// Set consumer function
	board->vfat3DataParser[defaultChipNum]->setDataConsumeFunction(this, ChDelayCB);

	// Setup pulser
	board->pulser->setConfig(1, pulsePeriod-1, Pulser::OPMODE_ENPLS_BIT);	// 25 us (plus 250 ns) period

	// loop on injection phase 
	for (int ph=0; ph<8; ph++){
		bcVector[ph] = measBCforPhase(ph, calCfg);
		if (bcVector[ph] != bcVector[0])
			break;
	}

	float delay = bcVector[0] * 25.0;	// coarse delay
	int baseBC = bcVector[0];
	int ph;
	for (ph=0; ph<8; ph++){
		if (bcVector[ph]!=baseBC){
			break;
		}
	}
	delay += (7-ph) * (25.0/8.0);

	// Set consumer function
	board->vfat3DataParser[defaultChipNum]->setDataConsumeFunction(this, NULL);

	return delay;
}


//
// Select the default VFAT3 chip.
// The default chip os the chip pointed by 
//  comPortTx and slowControl variables.
// As altenative you can use the array version comPortTxArray[] and  slowControlArray[]
//
void TestBench::setDefaultChip(unsigned int c)
{
    if (c < NUM_VFAT3_RECEIVERS) {
        chip = chipArray[c];
        board->setDefaultChip(c);
        defaultChipNum = c;
    } else {
        throw TestError ("Try to set default chip to invalid value");
    }
}




