

#include "Tutils.h"
#include <fstream>
#include <iostream>

using std::string;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::getline;

int main(int argc, char**argv)
{
	const int 	numPulses = 1000;
	const long	maxChargeDAC = 30;
	long 	hitMatrix[maxChargeDAC][128];
	double	chargeArray[maxChargeDAC];
	double	hitArray[maxChargeDAC];
	string  tmp;

	if (argc<2){
		cerr << "missing data file" << endl;
		exit(0);
	}	

	ifstream inputFile;
	inputFile.open (string(argv[1]));

	ofstream plotFile;
	plotFile.open (string(argv[1])+string(".dat"));

	// Read header from file
	getline(inputFile, tmp);
	getline(inputFile, tmp);
	getline(inputFile, tmp);
	
	// Read data
	for (int i=0; i<maxChargeDAC; i++){
		inputFile >> tmp;		// the DAC value
		inputFile >> chargeArray[i];

		for (int ch=0; ch<128; ch++){
			inputFile >> hitMatrix[i][ch];
		}
	}

	for (int ch=0; ch<128; ch++){
		for (int i=0; i<maxChargeDAC; i++)
			hitArray[i] = hitMatrix[i][ch];

		double th, ENC;
		double thErr, ENCerr;

		int ret = Tutils::fitErf(numPulses, maxChargeDAC, chargeArray, hitArray, 
					&th, &thErr, &ENC, &ENCerr);

		if (ret){
			plotFile << ch << " " << ENC*6250.0 << endl;
		} else {
			plotFile << ch << " " << -1 << endl;
		}
	}
}
