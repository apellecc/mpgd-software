/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#include <string>

#ifndef Tutils_h
#define Tutils_h


class Tutils
{
public:
	static std::string filenameSuffix();
	static bool fitErf(long nInject, int nPoints, double *x, double *y, 
					double *th, double *thErr, double *enc, double *encErr);
	static bool bestFitErf(long nInject, int nPoints, double *x, double *y, 
					double *th, double *thErr, double *enc, double *encErr);
	static bool bestFitLinDAC(int Start_fit, int Stop_fit, float *y, double *m, double *q);
	static bool bestFitLin(int Np, float *x, float *y, double *m, double *q);
	static bool bestFitLin(int Np, uint32_t *x, float *y, double *m, double *q);

public:
	struct vars_struct {
		double *x;
		double *y;
		double *ey;
		double nInject;
	};

private:
	static int erf_func(int m, int n, double *p, double *dy, double **dvec, void *vars);


};

#endif  // Tutils_h

