/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#include <iostream>
#include <stdio.h>
#include <sstream>
#include "mboard.h"
#include "mexception.h"
#include "TRGdataParser.h"



TRGdataParser::TRGdataParser()
{
	verbose = false;
	id = -1;
	dataConsumeCtx = NULL;
	dataConsumeFunction = NULL;
}

void TRGdataParser::flush()
{
}


long TRGdataParser::checkEvent(unsigned char *dBuffer, bool *good)
{
	unsigned char *p = dBuffer;
	*good = true;

	// Evaluate data fixed size 
	int expLen = 12; // EC + PHASE + BC  size

	if (expLen > dataBufferUsed)
		throw MDataParserError("Try to parse more bytes than received size");

	for (int i=0; i<4; i++){
		TRGdata.EC >>= 8;
		TRGdata.EC |= ((uint32_t)(*p++) << 24);
	}

	TRGdata.PHASE = *p++;

	for (int i=0; i<7; i++){
		TRGdata.BC >>= 8;
		TRGdata.BC |= ((uint64_t)(*p++) << 48);
	}

	*good = true;
	return(p-dBuffer);
}

// parse the data starting from begin of buffer
long TRGdataParser::parse(int numClosed)
{
	unsigned char *dBuffer = (unsigned char*) &dataBuffer[0];
	unsigned char *p = dBuffer;
	long evSize;
	bool good;

/*for (int i=0; i<10; i++)
	printf("%02x ", p[i]);
printf("\n");
*/
	while (numClosed) {
		evSize = checkEvent(p, &good);
		if (!good)
			return dataBufferUsed;					//can not allign after an error: skip all data

		if (dataConsumeFunction)
			dataConsumeFunction(dataConsumeCtx, &TRGdata);
		if (verbose)
			printf("EC:%d BC:%ld\n", TRGdata.EC, (long) TRGdata.BC);

		p += evSize;
		numClosed--;
	}

	return(p-dBuffer);
}
