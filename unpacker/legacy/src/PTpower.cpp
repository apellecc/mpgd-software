/*
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>.
 *
 */

#include <iostream>
#include <cmath>
#include "iomanip"
#include "PTpower.h"
#include <unistd.h>
#include "miscTest.h"
#include "VFAT3board.h"

// Analog power supply settings
static const float AVDD_1V2_sp = 1.2;			// setpoint
static const float AVDD_1V2_imin = 150e-3;		// minimum current for final check after power on and configuration
static const float AVDD_1V2_imax = 190e-3;		// maximum current for final check after power on and configuration
static const float AVDD_1V2_icpl = 200e-3;		// compliance current

// Digital power supply settings
static const float DVDD_1V2_sp = 1.2;			// setpoint
static const float DVDD_1V2_imin = 90e-3;		// minimum current for final check after power on and configuration
static const float DVDD_1V2_imax = 120e-3;		// maximum current for final check after power on and configuration
static const float DVDD_1V2_icpl = 300e-3;		// compliance current

// EFUSE power supply setting
static const float VDD_EFUSE_sp = 2.5;			// change to 2.5 V after board update

static const float v_short = 0.3;				// voltage reference for short circuit check

uint8_t ndac;

PTpower::PTpower(TestBench &t) : GenericTest(t)
{
}

PTpower::~PTpower()
{
}

bool PTpower::init_config()
{
	dump_config     DumpConfig(tb);

	for (int i = 0; i < 128; i++)			// channel setting
	{
		tb.chipConfig.CH[i].zcc_dac = 0;
		tb.chipConfig.CH[i].arm_dac = 0;
		tb.chipConfig.CH[i].mask = 0;
		tb.chipConfig.CH[i].cal = 0;
	}

	tb.chipConfig.PRE.TP = 100;				// CSA peaking time
	tb.chipConfig.PRE.RES = 0;				// CSA feedback resistor
	tb.chipConfig.PRE.CAP = 0;				// CSA feedback capacitor

	tb.chipConfig.DISC.PT = 100;
	tb.chipConfig.DISC.EN_HYST = 1;		
	tb.chipConfig.DISC.SEL_POL = 0;		
	tb.chipConfig.DISC.FORCE_EN_ZCC = 0;	
	tb.chipConfig.DISC.FORCE_TH = 0;		
	tb.chipConfig.DISC.SEL_COMP_MODE = 1;	
	tb.chipConfig.DISC.zcc_dac = 10;
	tb.chipConfig.DISC.arm_dac = 20;
	tb.chipConfig.DISC.hyst_dac = 5;

	tb.chipConfig.BIAS.Iref = 34;			// Tuning of global reference current, 34
	tb.chipConfig.BIAS.CFD_DAC_1 = 40;		// 	40
	tb.chipConfig.BIAS.CFD_DAC_2 = 40;		//	40
	tb.chipConfig.BIAS.PRE_I_BSF = 13;		//	13
	tb.chipConfig.BIAS.PRE_I_BIT = 150;		//	150	
	tb.chipConfig.BIAS.PRE_I_BLCC = 25;		//	25
	tb.chipConfig.BIAS.PRE_VREF = 86;		//	86
	tb.chipConfig.BIAS.SH_I_BFCAS = 130;	//	130		
	tb.chipConfig.BIAS.SH_I_BDIFF = 80;		//	80	
	tb.chipConfig.BIAS.SD_I_BDIFF = 140;	//	140	
	tb.chipConfig.BIAS.SD_I_BSF = 15;		//	15	
	tb.chipConfig.BIAS.SD_I_BFCAS = 135;	//	135
	tb.chipConfig.BIAS.SLVS_IBIAS = 40;		// SLVS Current (0-63),	40
	tb.chipConfig.BIAS.SLVS_VREF = 40;		// SLVS Vref (0-63),	40

	tb.chipConfig.CAL.POL = 0;				// Pulse polarity
	tb.chipConfig.CAL.PHI = 0;				// Pulse phase in 1/320 MHz delay steps
	tb.chipConfig.CAL.EN_EXT = 0;			// Enable external analog voltage step
	tb.chipConfig.CAL.DAC = 0;				// Amplitude of voltage step or current pulse
	tb.chipConfig.CAL.MODE = 1;				// 00:Normal 01:Voltage Pulse 1x:Current Pulse
	tb.chipConfig.CAL.FS = 0;				// Selection of Full Scale current pulse calibration
	tb.chipConfig.CAL.DUR = 500;			// Pulse duration in 320 MHz clock cycle

	tb.chipConfig.MON.VREF_ADC = 3;			// Tuning of internal ADC reference
	tb.chipConfig.MON.GAIN = 0;				// Gain of ADC monitoring buffer
	tb.chipConfig.MON.SEL = 0;				// ADC input MUX selection

	tb.chipConfig.TRG.latency = 4;
	tb.chipConfig.TRG.PS = 8;				// Pulse Stretcher control. From 1 to 8 clock cycles
	tb.chipConfig.TRG.syncLevelEnable = 0;	// Enable Level Mode 
	tb.chipConfig.TRG.ST = 0;				// Self Trigger
	tb.chipConfig.TRG.DDR = 0;				// DDR mode

	tb.chipConfig.RO.MAX_PAR = 16;			// In SPZS mode set the maximum number of partition to send (0-16)
	tb.chipConfig.RO.EnablePZS = 1;			// Enable Partial Zero Suppression (SPZS)
	tb.chipConfig.RO.SZP = 0;				// Suppress Zero Packet
	tb.chipConfig.RO.SZD = 1;				// Suppress Zero Data
	tb.chipConfig.RO.ECb = 1;				// Event Counter number of bytes to send ( 0-3)
	tb.chipConfig.RO.BCb = 2;				// Bx Counter number of bytes to send (0, 2, 3)

    tb.chipConfig.run = 1	;         		// run mode

	tb.chipConfig.ADC.OFFSET = 0;			// offset
	tb.chipConfig.ADC.LSB = 0;				// LSB value

	tb.chipReset();
	tb.chipInit();

    cout << "Sync check " << endl << endl;
    if (!tb.syncVerify()){
        cout << "Sync check FAILED" << endl;
        throw TestError("Sync check FAILED");
    }

    // Update testbench and chip configuration
    tb.setChipConfig();
	return true;

}

bool PTpower::setVDD(pwr_t param)
{
	VFAT3board::inADC_t vADC, iADC;	// ADC channel selection 
	VFAT3board::VDDch_t VDD_ch;		// Power supply channel selection

	float vmon = 0;					// monitored voltage
	float imon = 0;					// monitored current
	float setpoint = 0;				// final point of the power supply
	float i_comp = 0;				// current compliance
	float vmax = 1.575;				// max power supply for 1.575 V
	int ndac_max = 75;				// DAC resistor max value for 1.2 V, no more than 1.35 V
	float ndac_opt = 0;				// DAC resistor optimal value
	string VDD_name;				// power supply name

	float vbuf = 0;					

	switch (param)
	{
		case AVDD:
			VDD_ch = VFAT3board::VDD_V12A;
			vADC = VFAT3board::V_AVDD;
			iADC = VFAT3board::I_AVDD;
			setpoint = AVDD_1V2_sp;
			i_comp = AVDD_1V2_icpl;
			VDD_name = "Analog";
			break;

		case DVDD:
			VDD_ch = VFAT3board::VDD_V12D;
			vADC = VFAT3board::V_DVDD;
			iADC = VFAT3board::I_DVDD;
			setpoint = DVDD_1V2_sp;
			i_comp = DVDD_1V2_icpl;
			VDD_name = "Digital";
			break;

		case VDD_EFUSE:
			VDD_ch = VFAT3board::VDD_EFUSE;
			vADC = VFAT3board::V_EFUSE;
			iADC = VFAT3board::I_AVDD;		// no monitoring current for EFUSE power supply, analogue current is read
			setpoint = VDD_EFUSE_sp;
			i_comp = 1;						// no monitoring current for EFUSE power supply, compliance set to 1
			vmax = 2.75;					// max power supply for 2.5 V
			ndac_max = 180;					// DAC resistor max value for 2.5 V, no more than 2.6 V
			VDD_name = "EFUSE";
			break;

		default:
			cout << "Error power supply selection" << endl;
			throw TestError("Error power supply setting");
			break;
	}


	tb.board->readmon(vADC, &vmon);
	vbuf = fabs(vmon - setpoint);
	int dec = 0;

	if (vmon > setpoint)						// searching the direction for power supply optimal value search
	{
		dec = -1;
	}
	else
	{
		dec = 1;
	}

	for ( ; ndac <= ndac_max; ndac = ndac + dec) // 75 corresponds to 1.35 V, max supply value 1.32 V
	{
		tb.board->setVDDrdac(VDD_ch, ndac);		// DAC resistor setting
		tb.board->readmon(vADC,&vmon);			// voltage monitoring
		tb.board->readmon(iADC,&imon);			// current monitoring

		if (imon >= i_comp)						// current compliance check
		{
			tb.board->onOffVDD(VFAT3board::VDD_ALL, false);
			cout << VDD_name <<" current absoption = " << imon << endl;
			cout << VDD_name << "current compliance = " << i_comp << endl;
			throw TestError("Power supply current compliance reached!");
		}
		
		if (vmon <= v_short)					// short circuit check
		{
			tb.board->onOffVDD(VFAT3board::VDD_ALL, false);
			cout << VDD_name << " power supply = " << vmon  << endl;
			cout << VDD_name << " power supply short limit = " << v_short << endl;
			throw TestError("Power supply in short!");
		}

		if (vmon > vmax)						// power supply voltage compliance check
		{
			tb.board->onOffVDD(VFAT3board::VDD_ALL, false);
			cout << VDD_name << " power supply = " << vmon  << endl;
			cout << VDD_name << "power supply max value = " << vmax << endl;
			throw TestError("Power supply voltage abnormal value!");
		}
		
		if (vbuf >= fabs(vmon - setpoint))		// searching for optimal value of the dac resistor setting
		{
			vbuf = fabs(vmon - setpoint);
			ndac_opt = ndac;
		}

		if ((dec > 0) && (vmon > setpoint))		// searching stop criteria
		{
			break;
		}

		if ((dec < 0) && (vmon < setpoint))
		{
			break;
		}	
		

	}

	tb.board->setVDDrdac(VDD_ch, ndac_opt);		// writing the optimal value of dac resistor setting

return true;

}


bool PTpower::run(string param)
{
	const float Vr = 0.8;           	// reference voltage
    const float Ra = 220;           	// Resistor from sensing point
    const float Rw = 200;            	// Equivalent wipe resistor, 75 Ohm if VDD = 5 V 
    const float Rtot = 10e3;        	// Total rheostat resistance
	const float Vout_init_1V2 = 1.2;	// AVDD and DVDD initial value
	const float Vout_init_2V5 = 2.5;  	// EFUSE power supply initial value

	float RG_1V2 = 4.7e3;				// Gain resistance for 1.2 V power supply
	float RG_2V5 = 3.3e3;				// Gain resistance for 2.5 V power supply

	uint8_t ndac_1V2A;
	uint8_t ndac_1V2D;
	uint8_t ndac_2V5;

    // Set tester in functional mode (Clock ON)
//    tb.board->setTestType(TesterCtrl::TTYPE_Functional);

    // ADCs power supply measurement results
    VFAT3board::powerADC_t res;

	// initial value calculation for digital trimmer
	ndac_1V2A = ((Vout_init_1V2/Vr-1) * RG_1V2 - Rw - Ra) * 256 / Rtot; // Set
	if (ndac_1V2A < 0)
	{
		ndac_1V2A = 0;
	}

	ndac_1V2D = ndac_1V2A;

	ndac_2V5 = ((Vout_init_2V5/Vr-1) * RG_2V5 - Rw - Ra) * 256 / Rtot; // Set
	if (ndac_2V5 < 0)
	{
		ndac_2V5 = 0;
	}

    // switch on power supply with initial values
    tb.board->setVDDrdac(VFAT3board::VDD_V12A, ndac_1V2A);
    tb.board->setVDDrdac(VFAT3board::VDD_V12D, ndac_1V2D);
    tb.board->setVDDrdac(VFAT3board::VDD_EFUSE, ndac_2V5);
    tb.board->onOffVDD(VFAT3board::VDD_ALL, true);

	//check for short circuits
    tb.board->getPowerADC(&res);

	if ((res.I_V12A >= AVDD_1V2_icpl) || (res.V_V12A <= v_short))
	{
		tb.board->onOffVDD(VFAT3board::VDD_ALL, false);
		cout << "Analog current absoption = " << res.I_V12A << endl;
		cout << "Analog current compliance = " << AVDD_1V2_icpl << endl;
		throw TestError("Analog power supply current compliance reached!");
	}

	if ((res.I_V12D >= DVDD_1V2_icpl) || (res.V_V12D <= v_short))
	{
		tb.board->onOffVDD(VFAT3board::VDD_ALL, false);
		cout << "Digital current absoption = " << res.I_V12D << endl;
		cout << "Digital current compliance = " << DVDD_1V2_icpl << endl;
		throw TestError("Digital power supply current compliance reached!");
	}

	if (res.V_EFUSE <= v_short)
	{
		tb.board->onOffVDD(VFAT3board::VDD_ALL, false);
		cout << "EFUSE power supply = " << res.V_EFUSE << endl;
		cout << "Power supply short limit = " << v_short << endl;
		throw TestError("EFUSE power supply short!");		
	}
	
	// set analog power supply
	ndac = ndac_1V2A;
	setVDD(AVDD);
	
	// set digital power supply
	ndac = ndac_1V2D;
	setVDD(DVDD);

	//set EFUSE power supply
	ndac = ndac_2V5;
	setVDD(VDD_EFUSE);

	cout << "Configuring VFAT3: analogue section off, digital section in run mode" << endl;
	init_config();
	tb.board->getPowerADC(&res);
	cout << "Analogue power supply: " << res.V_V12A << " V" << endl;
	cout << "Analogue power supply current absorption: " << res.I_V12A*1000 << " mA" << endl << endl;
	cout << "Digital power supply: " << res.V_V12D << " V" << endl;
	cout << "Digital power supply current absorption: " << res.I_V12D*1000 << " mA" << endl	<< endl;
	cout << "EFUSE power supply: " << res.V_EFUSE << " V" << endl << endl;

	if (res.I_V12A < AVDD_1V2_imin || res.I_V12A > AVDD_1V2_imax)
	{
		cout << "Analog current absorption out of limits: " << res.I_V12A << " A"<< endl;
		cout << "Analog current lower limit: " << AVDD_1V2_imin << " A" << endl;
		cout << "Analog current upper limit: " << AVDD_1V2_imax << " A" << endl;
		throw TestError("Analog current absorption out of limits");
	}

	if (res.I_V12D < DVDD_1V2_imin || res.I_V12D > DVDD_1V2_imax)
	{
		cout << "Digital current absorption out of limits: " << res.I_V12D << " A"<< endl;
		cout << "Digital current lower limit: " << DVDD_1V2_imin << " A" << endl;
		cout << "Digital current upper limit: " << DVDD_1V2_imax << " A" << endl;
		throw TestError("Digital current absorption out of limits");
	}

/*
	tb.ptdata->VFAT3.pwr_d.AVDD = res.V_V12A;
	tb.ptdata->VFAT3.pwr_d.IAVDD = res.I_V12A;
	tb.ptdata->VFAT3.pwr_d.DVDD = res.V_V12D;
	tb.ptdata->VFAT3.pwr_d.IDVDD = res.I_V12D;
	tb.ptdata->VFAT3.pwr_d.VDD_EFUSE = res.V_EFUSE;	
*/

	cout << "Power supply accomplished" << endl;


    return true;
}

