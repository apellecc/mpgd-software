/*
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>.
 *
 */

#ifndef PTEXTINJECTION_H
#define PTEXTINJECTION_H

#include "GenericTest.h"

class PTextInjection : public GenericTest
{
public:
    PTextInjection(TestBench &tb);
    ~PTextInjection();
    bool run(string param);

private:
    void resetHistogram();
	static void histogramCB(void *ctx, VFAT3dataParser::VFAT3data_t *data);
    void pulseAndVerify(bool odd);

private:
	static const int 	numPulses;
	unsigned long		evCounter;
	std::array<long, 128> histogram;
};




#endif // PTEXTINJECTION_H
