/*
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>.
 *
 */

#ifndef PTPEAKTIME_H
#define PTPEAKTIME_H

#include "GenericTest.h"

class PTpeaktime : public GenericTest
{
public:
    PTpeaktime(TestBench &tb);
    ~PTpeaktime();
    bool run(string param);

private:
	void resetHistogram();
	static void histogramCB(void *ctx, VFAT3dataParser::VFAT3data_t *data);
	static void ChDelayCB(void *ctx, VFAT3dataParser::VFAT3data_t *data);
    void pulse(unsigned long nPulses, int triggerForPulse=1);
    void scanThresold( int minTh, int maxTh);
    void equalizeThresholds( int minTh, int maxTh );
    void resetChDelayHistogram();
    void measureDelay();

private:
    static const unsigned long   coarseNpulses; 
    static const unsigned long   fineNpulses; 
    static const unsigned long   refineNpulses; 
    static const uint8_t         PULSE_CAL_DAC; 
    static const long            pulsePeriod;
    static const long            acceptCycles;
    static const unsigned long   delayNpulses;

	std::array<long, 128> histogram;
    long ChDelayHistogram [128][16][8];     // channel, time, injPhase
    int  threshold[128];
	VFAT3board		*board;
	unsigned long	evCounter;
    int             targetThreshold;
    int             injPhase;    

private:
    int int2ldac( int x ) { return x<0? -63 : x>126 ? 63 : x-63; }

};




#endif
