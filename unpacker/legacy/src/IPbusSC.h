/*
 * Copyright (C) 2014
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2014.
 *
 */

#ifndef IPBUSSC_H
#define IPBUSSC_H

#include <stdint.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <mutex>
#include <vector>
#include <string>
#include <ipbus.h>
#include "ComPortTx.h"

#define SC_PACKET_SIZE 				1016

class ComPortRx;

class IPbusSC : public IPbus
{
public:
	IPbusSC(ComPortTx *tx, int pktSize = SC_PACKET_SIZE);
    ~IPbusSC();
	void addReceiver(ComPortRx *rcv);
	void execute();
	const std::string name() {return "IPbusSC ";}

private:
	ComPortTx		*comPortTx;
	uint8_t			hdlcAddress;
	std::vector<ComPortRx*> receivers;
	int				numReceivers;

public:
	void setHDLCaddress(uint8_t add) {hdlcAddress = add;}
};

#endif // IPBUSSC_H
