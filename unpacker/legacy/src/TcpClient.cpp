#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <poll.h>

#include <string>
#include <iostream>
#include <fstream>

#include "TcpClient.h"
#include "mexception.h"


class TcpClientError : public MException 
{
public:
	explicit TcpClientError(const string& __arg);
};

TcpClientError::TcpClientError(const string& arg)
{
	 msg = "TCP Client Error: " + arg;
}

// Data receive over TCP
TcpClient::TcpClient()
: currentHostName( "" )
, currentPort( 0 )
, currentSocketDescr( 0 )
, serverAddress ( )
, currentHostInfo( NULL )
, clientIsConnected( false )
, receiveBufferSize( 1024 )
{
}


TcpClient::~TcpClient()
{
	currentHostInfo = NULL;
}


void TcpClient::connectToServer( string &hostname, int port )
{
	currentHostInfo = gethostbyname( hostname.c_str( ) );

	if( currentHostInfo == NULL )
	{
		// int error = WSAGetLastError();

		currentHostName   = "";
		currentPort       = 0;
		currentHostInfo   = NULL;
		clientIsConnected = false;
		throw TcpClientError("error connecting host");
	}

	currentHostName = hostname;
	currentPort     = port;

	currentSocketDescr = socket(AF_INET, SOCK_STREAM, 0);

	if( currentSocketDescr == 0 )
	{
		currentHostName   = "";
		currentPort       = 0;
		currentHostInfo   = NULL;
		clientIsConnected = false;
		throw TcpClientError("can't create socket");
	}

	serverAddress.sin_family = currentHostInfo->h_addrtype;
	serverAddress.sin_port   = htons( currentPort );

	memcpy( (char *) &serverAddress.sin_addr.s_addr, currentHostInfo->h_addr_list[0], currentHostInfo->h_length );

	if( connect( currentSocketDescr, ( struct sockaddr *) &serverAddress, sizeof( serverAddress ) ) < 0 )
	{
		throw TcpClientError("can't connect server");
	}

	clientIsConnected = true;
}


void TcpClient::disconnect( )
{
	if( clientIsConnected )
		close( currentSocketDescr );

	currentSocketDescr = 0;
	currentHostName    = "";
	currentPort        = 0;
	currentHostInfo    = NULL;
	clientIsConnected  = false;
}


void TcpClient::transmit( string &txString )
{
	if( !clientIsConnected )
	{
		throw TcpClientError("connection must be established before any data can be sent\n");
	}

	char * transmitBuffer = new char[txString.length() +1];


	memcpy( transmitBuffer, txString.c_str(), txString.length() );
	transmitBuffer[txString.length()] = '\n'; //newline is needed!

	if( send( currentSocketDescr, transmitBuffer, txString.length() + 1, 0 ) < 0 )
	{
		throw string("can't transmit data\n");
	}

	delete [] transmitBuffer;

}

#if 0
void TcpClient::receive( string &rxString )
{
	if( !clientIsConnected )
	{
		throw TcpClientError("connection must be established before any data can be received\n");
	}

	char * receiveBuffer = new char[receiveBufferSize];

	memset( receiveBuffer, 0, receiveBufferSize );

	bool receiving = true;
	while( receiving )
	{
		int receivedByteCount = recv( currentSocketDescr, receiveBuffer, receiveBufferSize, 0 );
		if( receivedByteCount < 0 )
		{
			throw TcpClientError("error while receiving data\n");
		}

		rxString += string( receiveBuffer );

		receiving = ( receivedByteCount == receiveBufferSize );
	}
	delete [] receiveBuffer;
}
#endif

void TcpClient::receive(string &rxString, int timeout)
{
	struct pollfd ufds;
	int rv;
	ssize_t rxSize;
	char receiveBuffer[receiveBufferSize];

	if( !clientIsConnected )
		throw TcpClientError("connection must be established before any data can be received\n");

	
	memset( receiveBuffer, 0, receiveBufferSize );

	bool receiving = true;
	while( receiving ){
		ufds.fd = currentSocketDescr;
		ufds.events = POLLIN|POLLNVAL; // check for normal read or error
		rv = poll(&ufds, 1, timeout);	

		if (rv == -1)
			throw TcpClientError("Poll system call");

		if (rv == 0){
			return;			// timeout
			
		}
	
		// check for events on sockfd:
		rxSize = 0;
		if (ufds.revents & POLLIN) {
			rxSize = recv(currentSocketDescr, receiveBuffer, receiveBufferSize, 0);
			if (rxSize==0 || rxSize == -1)
				throw TcpClientError("Board connection closed. Fatal error!");
		} else if (ufds.revents & POLLNVAL){
			throw TcpClientError("Invalid file descriptor in poll system call");
		}

		rxString += string( receiveBuffer );
		receiving = ( rxSize == receiveBufferSize );
	}
}




string TcpClient::getCurrentHostName( ) const
{
	return currentHostName;
}


int TcpClient::getCurrentPort( ) const
{
	return currentPort;
}


