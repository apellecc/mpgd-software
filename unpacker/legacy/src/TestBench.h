/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#ifndef TESTBENCH_H
#define TESTBENCH_H

#include "VFAT3board.h"
#include "VFAT3chip.h"
#include "VFAT3_data.h"
#include "ADS1113.h"
#include "ISL23318.h"

class TBconfig;
class RTOscope;

class TestBench {
public:
	TestBench();
	void initBoard();
	void setChipConfig();
	void readConfigFile(std::string &fileName);
	void getDefaultConfig();
	void chipReset();
	void chipInit();
	bool sync();
	bool syncVerify();
	void sendAnalogPulse(uint16_t low, uint16_t high, double width, double delay, int nPulses=-1);
//	void setAVDD(float Vout);
//	void setDVDD(float Vout);
	float getVmon(int sel=-1);
	float getInjCharge(VFAT3chip::CalibrationConfig_t &calCfg);
	void initInjCharge(VFAT3chip::CalibrationConfig_t &calCfg);
	float measChDelay(VFAT3chip::CalibrationConfig_t &calCfg);
    void setDefaultChip(unsigned int chip);
    unsigned int getDefaultChip() {return defaultChipNum;};

private:
	void resetChDelayHistogram();
	int maxChDelayHistogram();
	static void ChDelayCB(void *ctx, VFAT3dataParser::VFAT3data_t *data);
	int measBCforPhase(int ph, VFAT3chip::CalibrationConfig_t &calCfg);


public:
	VFAT3board 						*board;
	VFAT3chip 						*chipArray[NUM_VFAT3_RECEIVERS];
	VFAT3chip 						*chip;
	TBconfig 						*config;
	RTOscope 						*scope;
	VFAT3chip::VFAT3config_t 		chipConfig;
	VFAT3_data						*ptdata;

private:
    unsigned int defaultChipNum;
	static const float			Rmon;
	std::string scopeHostname;
	std::string MOSAICHostname;

	// Delay measurement related vars and constants
	static const int 			numDelayMeasurePulses = 50;
	unsigned long				evCounter;
	std::array<long, 16> 		ChDelayHistogram;
	static const long pulsePeriod;		
	static const long acceptWindow;		 
	static const long acceptMask;

	// Charge injection calibration
	float InjVstepBase;	
	float InjVstepLSB;
	float InjVstepOffset;
	float IpulseLSB;

};


#endif 		// TESTBENCH_h
