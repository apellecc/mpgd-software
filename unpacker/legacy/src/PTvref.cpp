#include "PTvref.h"
#include "VFAT3chip.h"
#include "Tutils.h"
#include <unistd.h>
#include <iostream>
#include <cmath>
#include "miscTest.h"

//bandgap acceptance range
static const float Vbg_min = 0.3; 				// min
static const float Vbg_max = 0.37; 				// max

//ADC negative input acceptance range
static const float ADC0_VinM_min= 0.63*0.9;			// min
static const float ADC0_VinM_max= 0.63*1.1;			// max

//ADC0 gain acceptance range
static const float ADC0_ref_Gain_min = 0.05*0.9;	// min
static const float ADC0_ref_Gain_max = 0.05*1.1;	// max

//ADC0 gain acceptance range
static const float ADC0_ref_Offset_min = 0.9*0.9;	// min
static const float ADC0_ref_Offset_max = 0.9*1.1;	// max

//ADC0 reference optimal value
static const float ADC0_ref_opt = 1;

PTvref::PTvref(TestBench &t) : GenericTest(t)
{
}

PTvref::~PTvref()
{
}

bool PTvref::run(string param){

	VFAT3_data::Vref_s Vref_s;
	/*VFAT3_data::ADC_s ADC_s;

	/bool fit_status;
	float delta_min = 10;
	float delta = 0;
	ADC_s.ADC0_vref_optVal_ndac = 3;*/
	float vmon = 0;

	// Bandgap voltage monitoring
	tb.chipConfig.MON.SEL = tb.chip->Vmon_BGR; 
	tb.setChipConfig();
	usleep(10000);

	tb.board->readmon(VFAT3board::V_VMON, &vmon);
	Vref_s.Vbgr = vmon;

	if ((Vref_s.Vbgr <= Vbg_min) || (Vref_s.Vbgr >= Vbg_max))
	{
		cout<<"Bandgap reference out of acceptance range!"<<endl;
		cout<<"Bandgap reference = "<< Vref_s.Vbgr <<" V"<<endl;
		cout << "Bandgap reference should be included between: " << Vbg_min << " V and " << Vbg_max << " V" << endl;
		throw TestError("Voltage references test error");
	}

	//cout << "Bandgap reference voltage: " << Vref_s.Vbgr*1000 << " mV" << endl;

	// ADCs negative input reference voltage monitoring
	tb.chipConfig.MON.SEL = tb.chip->ADC_VinM; 
	tb.setChipConfig();
	usleep(10000);

	tb.board->readmon(VFAT3board::V_VMON, &vmon);
	Vref_s.ADC_VinM = vmon;

	if ((Vref_s.ADC_VinM<=ADC0_VinM_min) || (Vref_s.ADC_VinM>=ADC0_VinM_max))
	{
		cout << "ADCs negative input reference out of acceptance range!" << endl;
		cout << "ADC_VinM = " << Vref_s.ADC_VinM << " V" << endl;
		cout << "ADC_VinM should be included between " << ADC0_VinM_min << " V and " << ADC0_VinM_max << " V" << endl;
		throw TestError("Voltage references test error");
	}

	//cout << "ADCs negative input reference: " << Vref_s.ADC_VinM*1000 << " mV" << endl;

	// ADC0 reference voltage monitoring
	tb.chipConfig.MON.SEL = tb.chip->ADC_ref; 
	usleep(10000);

	for (int v=0; v<4; v++)
	{
		// set DAC
		tb.chipConfig.MON.VREF_ADC = v;
		tb.setChipConfig();

		// Read External ADC
		tb.board->readmon(VFAT3board::V_VMON, &vmon);
		Vref_s.ADC0_vrf_Scan[v] = vmon;

		tb.ptdata->VFAT3.vref_d.ADC0_vrf_Scan[v] = Vref_s.ADC0_vrf_Scan[v]; //saving data
/*		cout<<"ADC0 vref "<<Vref_s.ADC0_vrf_Scan[v]<<endl;

		// ADC0 vref optimal value check
		delta = abs( Vref_s.ADC0_vrf_Scan[v] - ADC0_ref_opt);
		if (delta_min >= delta)
		{
			delta_min = delta;
			ADC_s.ADC0_vref_optVal_ndac = v;
		}

		//Monotonicity test
		if ((v) && (Vref_s.ADC0_vrf_Scan[v]<=Vref_s.ADC0_vrf_Scan[v-1]))
		{		
			cout<<"ADC0 voltage reference test not passed: monotonicity test failed!"<<endl;
			throw TestError("ADC0 voltage reference test not passed: monotonicity test failed!");
		}
*/
	}

	/*fit_status = Tutils::bestFitLinDAC(0, 3, Vref_s.ADC0_vrf_Scan, &Vref_s.ADC0_vrf_gain, &Vref_s.ADC0_vrf_offset);
	
	if (fit_status)
	{
		if ((Vref_s.ADC0_vrf_gain<=ADC0_ref_Gain_min) || (Vref_s.ADC0_vrf_gain>=ADC0_ref_Gain_max))
		{
			cout<<"ADC0 Vref test not passed: parameters out of acceptance range!"<<endl;
			cout<<"ADC0 Vref gain = "<<Vref_s.ADC0_vrf_gain<<endl;
			cout<<"ADC0 Vref gain should be included between: " << ADC0_ref_Gain_min << " V/LSB and " << ADC0_ref_Gain_max << " V/LSB" <<endl;
		}

		if ((Vref_s.ADC0_vrf_offset<=ADC0_ref_Offset_min) || (Vref_s.ADC0_vrf_offset>=ADC0_ref_Offset_max))
		{
			cout<<"ADC0 Vref offset = "<<Vref_s.ADC0_vrf_offset<<endl;
			cout<<"ADC0 Vref offset should be included between: " << ADC0_ref_Offset_min << " V/LSB and " << ADC0_ref_Offset_max << " V/LSB" <<endl;
		}
			throw TestError("Voltage references test error");
	}
	else 
	{
		cout<<"ADC0 Vref linear fitting error"<<endl;
		throw TestError("Voltage references test error");
	}*/

	//saving data
	tb.ptdata->VFAT3.vref_d.Vbgr = Vref_s.Vbgr;
	tb.ptdata->VFAT3.vref_d.ADC_VinM = Vref_s.ADC_VinM;
	//tb.ptdata->VFAT3.vref_d.ADC0_vrf_gain = Vref_s.ADC0_vrf_gain;
	//tb.ptdata->VFAT3.vref_d.ADC0_vrf_offset = Vref_s.ADC0_vrf_offset;
	tb.ptdata->VFAT3.adc_d.ADC0_vref_optVal_ndac = 3;
	tb.ptdata->VFAT3.adc_d.ADC0_vref = tb.ptdata->VFAT3.vref_d.ADC0_vrf_Scan[3];

	cout << "Voltage references test passed!" << endl;
	cout << "Bandgap reference = " << Vref_s.Vbgr << " V" << endl;
	cout << "ADC_VinM = " << Vref_s.ADC_VinM << " V" << endl;
	cout << "ADC0 Vref gain = " << Vref_s.ADC0_vrf_Scan[3] << endl;
	//cout << "ADC0 Vref gain = " << Vref_s.ADC0_vrf_gain << " V/LSB" << endl;
	//cout << "ADC0 Vref offset = " << Vref_s.ADC0_vrf_offset << " V" << endl;

	//ADC0 vref optimal value reg set
	tb.chipConfig.MON.VREF_ADC = 3;
	tb.setChipConfig();

	return true;
}
		


