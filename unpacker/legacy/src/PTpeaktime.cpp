/*
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>.
 *
 */

#include <iostream>
#include <unistd.h>
#include <strings.h>
#include "PTpeaktime.h"
#include "PTscurves.h"
#include "Tutils.h"

const double lcldac_gain_min = -1000;
const double lcldac_gain_max = 6000;

const double lcldac_offset_min = -600;
const double lcldac_offset_max = 3000;

PTpeaktime::PTpeaktime(TestBench &t) : GenericTest(t)
{
	board = tb.board;
}

PTpeaktime::~PTpeaktime()
{
}

void PTpeaktime::resetHistogram()
{
	evCounter = 0;
	for (int i=0; i<128; i++)
	histogram[i] = 0;
}

void PTpeaktime::histogramCB(void *ctx, VFAT3dataParser::VFAT3data_t *data)
{
	PTpeaktime *p = (PTpeaktime *) ctx;
	
	p->evCounter++;

	for (int i=0; i<128; i++)
	if (data->hit[i])
	p->histogram[i]++;
}

void PTpeaktime::pulseAtChargeDAC(unsigned long nPulses)
{
	// start pulses injection				
	board->pulser->run(nPulses);

	resetHistogram();
	long res;
	do {
		res = board->pollData(50);		// timeout 50 ms
		if (res == 0){
			cerr << "Timeout reading data" << endl;break;
			throw TestError("Timeout reading data");
		}
	} while (evCounter<nPulses);
}

bool PTpeaktime::run(string param)
{
	PTscurves fit_scurve(tb);
	bool fit_status;

	int arm_th = 0;
	int chargeDAC = 0;
	int numPulses = 100;
	
	int GBL_dac = 150;

	double vth_l[4][128];
	
	int i_pt = 0;

	// disable cal bit on all channels
	for (int i=0; i<128; i++)
	{
		tb.chipConfig.CH[i].arm_dac = 0;
		tb.chipConfig.CH[i].zcc_dac = 0;
		tb.chipConfig.CH[i].mask = false;
		tb.chipConfig.CH[i].cal = false;
	}

	//calibration settings
	tb.chipConfig.CAL.MODE = 2;		// current injection
	tb.chipConfig.CAL.POL = 0;		// negative injected charge, the CSA output is a positive pulse
	tb.chipConfig.CAL.DUR = 1;
	tb.chipConfig.CAL.PHI = 0;
	tb.chipConfig.CAL.FS = 3;

	//CFD settings
	tb.chipConfig.DISC.PT = 100;
	tb.chipConfig.DISC.SEL_POL = 0;
	tb.chipConfig.DISC.EN_HYST = 1;
	tb.chipConfig.DISC.FORCE_EN_ZCC = 0;
	tb.chipConfig.DISC.FORCE_TH = 0;
	tb.chipConfig.DISC.SEL_COMP_MODE = 1;

	//CSA settings - medium gain
	tb.chipConfig.PRE.TP = 100;
	tb.chipConfig.PRE.RES = 1;
	tb.chipConfig.PRE.CAP = 1;
	tb.chipConfig.TRG.syncLevelEnable = 0;

	for (int ip = 25; ip <125; ip = ip+25) // gain measurement for each PT
	{
		i_pt= ip/25-1;
		//cout << "Peak time: " << ip << endl;
		tb.chipConfig.DISC.PT = ip;
		tb.chipConfig.PRE.TP = ip;

		//Set Global ARM DAC
		tb.chipConfig.DISC.arm_dac = GBL_dac;
		tb.setChipConfig();
		usleep(10000);

		fit_status = fit_scurve.rungainfast(vth_l[i_pt]);
	}

	// Init the charge injection
	calCfg = tb.chipConfig.CAL;
	tb.initInjCharge(calCfg);

	// Set consumer function
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, histogramCB);

	// Setup pulses
	board->pulser->setConfig(10, 380);	// 10 us (plus 250 ns) period

	// Send BC0
	board->comPortTx->sendBC0();

	// start run
	board->mRunControl->startRun();
	board->comPortTx->sendRunMode();
	tb.chip->execute();

	for (int i=0; i<128; i++)
	{
		tb.chipConfig.CH[i].arm_dac = 0;
		tb.chipConfig.CH[i].zcc_dac = 0;
		tb.chipConfig.CH[i].mask = false;
		tb.chipConfig.CH[i].cal = false;
	}

	tb.chipConfig.TRG.syncLevelEnable = 0;	// Disable level mode
	tb.chipConfig.TRG.PS = 1;
	
	tb.setChipConfig();

    for (int ip = 25; ip <= 100; ip = ip +25)
	{
		i_pt= ip/25-1;
		tb.chipConfig.DISC.PT = ip;
		tb.chipConfig.PRE.TP = ip;

		cout << "Peaking time: " << ip << " ns" << endl;

		int vLAT[2];
		int vPHI[2];

		double pkt;

		for (int ch = 0; ch<128; ch++)
		{
			for (int i_th=0; i_th < 2; i_th++)
			{
				switch (i_th)
				{
					//case 0: arm_th = ( vth_l[i_pt][ch] * gain[i_pt][ch]*1e-3 - tb.ptdata->VFAT3.dac_d[16].offset ) / tb.ptdata->VFAT3.dac_d[16].gain; break;
					//case 1: arm_th = ( 0.1* vth_l[i_pt][ch] * gain[i_pt][ch]*1e-3 - tb.ptdata->VFAT3.dac_d[16].offset ) / tb.ptdata->VFAT3.dac_d[16].gain; break;
					case 0: arm_th = GBL_dac * 0.9; break;
					case 1: arm_th = GBL_dac * 0.1; break;
					default: break;
				}

				switch (tb.chipConfig.CAL.MODE)
				{
					case 1: chargeDAC = 255 - ( ( vth_l[i_pt][ch] / tb.ptdata->VFAT3.calib_d.Inj_Cap) + tb.ptdata->VFAT3.calib_d.cal_bsl - tb.ptdata->VFAT3.calib_d.vpul_offset) / tb.ptdata->VFAT3.calib_d.vpul_gain; break;
					case 2: chargeDAC = ( vth_l[i_pt][ch] / ( tb.chipConfig.CAL.DUR * 25 * (tb.chipConfig.CAL.FS + 1) * 0.1 * 1e6) - tb.ptdata->VFAT3.calib_d.ipul_offset ) / tb.ptdata->VFAT3.calib_d.ipul_gain; break;
					default: break;
				}				

				//cout << "arm_th "<< arm_th << endl;
				//cout << "dac " << chargeDAC << endl;

				// set global arm dac
				tb.chipConfig.DISC.arm_dac = arm_th;
				// set cal dac
				tb.chipConfig.CAL.DAC = chargeDAC;
				// set cal bit		
				tb.chipConfig.CH[ch].cal = true;

				tb.chipConfig.CAL.PHI = 0;

				tb.setChipConfig();
				usleep(10000);
				int max = 0;
				int buf_h = 0;
				for (int i_lat = 0; i_lat < 15; i_lat++)
				{   
					tb.chipConfig.TRG.latency = i_lat;
					tb.setChipConfig();
					
					pulseAtChargeDAC(numPulses);
					
					vLAT[i_th] = i_lat;

					//cout << "lat: " << i_lat << " nump: " << histogram[ch] << endl;
					if ( histogram[ch] > buf_h)
					{
						max = i_lat;
						buf_h = histogram[ch];
					}
						
					if ( histogram[ch] == 100) {
						max = i_lat;
						break;
					}
				}
				//cout << "max lat " << max << endl;
				int i_phi = 1;
				if (max == 0)
				{
					vLAT[i_th] = 1;
				}
				else
				{
					vLAT[i_th] = max;
				}
				tb.chipConfig.TRG.latency = vLAT[i_th] - 1;

				buf_h = 0;
				for (i_phi = 1; i_phi < 8; i_phi++)
				{

					tb.chipConfig.CAL.PHI = i_phi;
					
					tb.setChipConfig();
					pulseAtChargeDAC(numPulses);
					//cout << "phi " << i_phi << " nump: " << histogram[ch] <<endl;
					if ( histogram[ch] > buf_h)
					{
						max = i_phi;
						buf_h = histogram[ch];
					}
				
					if ( histogram[ch] > 50) break;
				}
				//cout << "max phi " << max << endl;
				vPHI[i_th] = max;
			}

			tb.chipConfig.CH[ch].cal = false;
			//cout << "LAT A " << vLAT[1]<<  endl;
			//cout << "LAT B " << vLAT[0]<<  endl;
			//cout << "PHI A " << vPHI[1]<<  endl;
			//cout << "PHI B " << vPHI[0]<<  endl;
			pkt = ( vLAT[1] - vLAT[0] ) * 25 + ( vPHI[1] - vPHI[0] ) * 3.125;
			cout << "Channel " << ch << " Nominal peaking time: " << ip << " ns Measured peaking time " << pkt << " ns"<< endl;
			tb.ptdata->VFAT3.chn_d[ch].peakTime[i_pt] = pkt;
		}
	}

	tb.chipConfig.TRG.latency = 4;
	tb.chipConfig.TRG.PS = 8;				// Pulse Stretcher control. From 1 to 8 clock cycles
	tb.chipConfig.TRG.syncLevelEnable = 0;	// Enable Level Mode 
	tb.chipConfig.TRG.ST = 0;				// Self Trigger
	tb.chipConfig.TRG.DDR = 0;	

	tb.setChipConfig();

    return true;
}

