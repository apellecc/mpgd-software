/*
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>.
 *
 */

#include <iostream>
#include <unistd.h>
#include <strings.h>
#include "PTpeaktime.h"
#include "PTscurves.h"
#include "TestError.h"
#include "Tutils.h"

// if defined print additional messages on selected channel
#define DEBUG_CH 1

// Parameter for threshold measure/equalization
const unsigned long PTpeaktime::coarseNpulses   = 100; 
const unsigned long PTpeaktime::fineNpulses     = 1000; 
const unsigned long PTpeaktime::refineNpulses   = 2000; 
const uint8_t       PTpeaktime::PULSE_CAL_DAC   = 100; 

// Parameter for time measuremants
const long PTpeaktime::pulsePeriod = (1<<9);		// 512 clock cycles
const long PTpeaktime::acceptCycles = 7;	    	// number of clock cycle to accept data after pulse 
const unsigned long PTpeaktime::delayNpulses    = 500;
//const long PTpeaktime::acceptMask = (pulsePeriod-1) & ~(acceptWindow-1);	// accept mask 


PTpeaktime::PTpeaktime(TestBench &t) : GenericTest(t)
{
	board = tb.board;
}

PTpeaktime::~PTpeaktime()
{
}

void PTpeaktime::resetHistogram()
{
	evCounter = 0;
	for (int i=0; i<128; i++)
    	histogram[i] = 0;
}

void PTpeaktime::histogramCB(void *ctx, VFAT3dataParser::VFAT3data_t *data)
{
	PTpeaktime *p = (PTpeaktime *) ctx;
	
	p->evCounter++;

	for (int ch=0; ch<128; ch++)
	    if (data->hit[ch])
	        p->histogram[ch]++;
}

void PTpeaktime::resetChDelayHistogram()
{
	evCounter = 0;
    for (int del=0; del<16; del++){
	    for (int ch=0; ch<128; ch++)
            for (int ph=0; ph<8; ph++)
            	ChDelayHistogram[ch][del][ph] = 0;
    }
}

void PTpeaktime::ChDelayCB(void *ctx, VFAT3dataParser::VFAT3data_t *data)
{
	PTpeaktime *p = (PTpeaktime *) ctx;
	
	p->evCounter++;

    // with pulser setting PtoT_delay set to 1 => Data BC# starts from 2 
    int del = (data->BC & 0x0f) - 2;
	for (int ch=0; ch<128; ch++)
	    if (data->hit[ch])
	        p->ChDelayHistogram[ch][del][p->injPhase]++;

//    printf("BC:%x Hit:%d\n", data->BC, data->hit[0]);
}

void PTpeaktime::pulse(unsigned long nPulses, int triggerForPulse)
{
	// start pulses injection				
	board->pulser->run(nPulses);

	resetHistogram();
    nPulses*=triggerForPulse;
	long res;
	do {
		res = board->pollData(50);		// timeout 50 ms
		if (res == 0){
			cerr << "Timeout reading data" << endl;break;
			throw TestError("Timeout reading data");
		}
	} while (evCounter<nPulses);
}

void PTpeaktime::scanThresold( int minTh, int maxTh )
{
    long mat[256][128]; 
    int dac;

    // Pulse all channels in the assigned range
    for (int dac=minTh; dac<=maxTh ; dac++){
        tb.chipConfig.DISC.arm_dac = dac;
        tb.setChipConfig();
        pulse(coarseNpulses);
        for (int ch=0; ch<128; ch++)    
            mat[dac][ch] = histogram[ch];
    }

    // find channel threshold for the injected charge
    for (int ch=0; ch<128; ch++){
        for (dac=minTh; dac<maxTh; dac++){
            if (mat[dac][ch] > (int)(coarseNpulses/2) && mat[dac+1][ch] <= (int)(coarseNpulses/2)){
                cout << "ch:" << ch << " Found th:" << dac << endl;
                threshold[ch] = dac;
                break;
            }
        }
        if (dac==maxTh){
            cout << "PTpeaktime::scanThresold failed to find threshold for channel " << ch << endl;
            throw TestError("Failed to find threshold");
        }
    }
}


void PTpeaktime::equalizeThresholds( int minTh, int maxTh )
{
	// Set consumer function
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, histogramCB);

	// Setup pulses
	board->pulser->setConfig(10, 390);	

	// Send BC0
	board->comPortTx->sendBC0();

	// start run
	board->mRunControl->startRun();
	board->comPortTx->sendRunMode();
	tb.chip->execute();

	for (int ch=0; ch<128; ch++){
		tb.chipConfig.CH[ch].arm_dac = 0;
    }

    // inital scan
    scanThresold( minTh, maxTh );

    // evaluate target threshold
    int minDAC = 256;
    int maxDAC = -1;
	for (int ch=0; ch<128; ch++){
        if (minDAC > threshold[ch])
            minDAC = threshold[ch];
        if (maxDAC < threshold[ch])
            maxDAC = threshold[ch];
    }
    targetThreshold = (minDAC + maxDAC)/2;
    cout << "targetThreshold:" << targetThreshold << endl;

    // tune local dacs
    tb.chipConfig.DISC.arm_dac = targetThreshold;
    int local_dac[128] = {};

	for (int i=0x40; i!=0; i>>=1){
        for (int ch=0; ch<128; ch++){
            local_dac[ch] |= i;
            tb.chipConfig.CH[ch].arm_dac = int2ldac(local_dac[ch]);
        }

        tb.setChipConfig();
        usleep(1000);
        pulse(fineNpulses);

        for (int ch=0; ch<128; ch++){
            if (histogram[ch] < (long) (fineNpulses/2)){
                local_dac[ch] &= ~i;
                tb.chipConfig.CH[ch].arm_dac = int2ldac(local_dac[ch]);      
            }
        }
        #ifdef DEBUG_CH
            cout << "local_dac:" << to_string(tb.chipConfig.CH[DEBUG_CH].arm_dac) << " Hystogram:" << to_string(histogram[DEBUG_CH]) << endl;
        #endif
    }

    // refine tuning
    int best_ldac[128];
    int best_error[128];
    
    for (int ch=0; ch<128; ch++)
        best_error[ch] = refineNpulses;
        
// TODO: rivedere
    for (int t=0; t<3; t++){
        bool changed = false;
        pulse(refineNpulses);
        #ifdef DEBUG_CH
            cout << "Refine: local_dac:" << to_string(tb.chipConfig.CH[DEBUG_CH].arm_dac) << " Hystogram:" << to_string(histogram[DEBUG_CH]) << endl;
        #endif
        for (int ch=0; ch<128; ch++){
            if (abs(histogram[ch] - (long)(refineNpulses/2)) < best_error[ch]) {
                best_error[ch] = abs(histogram[ch] - (long)(refineNpulses/2));
                best_ldac[ch] = local_dac[ch];
            }
            if ( histogram[ch] < (long)(refineNpulses/2) )
                local_dac[ch]--;
            else
                local_dac[ch]++;
            tb.chipConfig.CH[ch].arm_dac = int2ldac(local_dac[ch]);
            changed = true;
        }
        if (changed){
            tb.setChipConfig(); 
            cout << "Update chip config" << endl;
        }
        else
            break;
    }
    
    // Update config with the best values    
    for (int ch=0; ch<128; ch++)
        tb.chipConfig.CH[ch].arm_dac = int2ldac(best_ldac[ch]);
    tb.setChipConfig(); 

    scanThresold( minTh, maxTh );

// TODO: Controllare soglie entro +/-1 conteggio di DAC

	board->mRunControl->stopRun();
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, NULL);
}


void PTpeaktime::measureDelay()
{
	// Setup pulser
	board->pulser->setConfig(1, pulsePeriod-1, 
        Pulser::OPMODE_ENPLS_BIT|Pulser::OPMODE_ENTRG_BIT, 
        acceptCycles-1);

	// Set consumer function
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, ChDelayCB);

	// Send BC0
	board->comPortTx->sendBC0();

	// start run
	board->mRunControl->startRun();
	board->comPortTx->sendRunMode();
	tb.chip->execute();

    // reset and fill the ChDelayHistogram
    resetChDelayHistogram();
    for (injPhase=0; injPhase<8; injPhase++){
        tb.chipConfig.CAL.PHI = injPhase;
        tb.setChipConfig();
        pulse(delayNpulses, acceptCycles);

    #ifdef DEBUG_CH
        for (int del=0; del<8; del++)
            cout << "del:" << del << " Hit:" << ChDelayHistogram[DEBUG_CH][del][injPhase] << endl;
    #endif
    }

	board->mRunControl->stopRun();
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, NULL);

    // evaluate Pulse to data delay
    int del;
    int ph;

    for (int ch=0; ch<128; ch++){
        float totalDelay=0;
        for (del=0; del<16; del++){
            if (ChDelayHistogram[ch][del][0] > (long) (delayNpulses/2)){
                break;
            } else {
                totalDelay+=25.0;
            }
        }
        if (del==16){
            cout << "PTpeaktime::measureDelay cannot find delay for channel " << ch << endl;
            throw TestError("PTpeaktime::measureDelay cannot find delay");
        }

        for (ph=0; ph<8; ph++){
            if (ChDelayHistogram[ch][del][ph] < (long) (delayNpulses/2)) {
                break;
            } else {
                totalDelay -= (25.0/8.0);
            }
        }
        cout << "Ch:" << ch << " Delay:" << totalDelay << endl;
    }
}



bool PTpeaktime::run(string param)
{
    VFAT3chip::VFAT3config_t  chipCfg = tb.chipConfig;

    // Setup the parallel voltage pulse on all channels
	for (int ch=0; ch<128; ch++){
		tb.chipConfig.CH[ch].arm_dac = 0;
		tb.chipConfig.CH[ch].zcc_dac = 0;
		tb.chipConfig.CH[ch].mask = false;
		tb.chipConfig.CH[ch].cal = true;
	}

	// CSA settings - medium gain
	tb.chipConfig.PRE.TP = 100;
	tb.chipConfig.PRE.RES = 1;
	tb.chipConfig.PRE.CAP = 1;

	// CFD settings
	tb.chipConfig.DISC.PT = 100;
	tb.chipConfig.DISC.SEL_POL = 0;
	tb.chipConfig.DISC.EN_HYST = 1;
	tb.chipConfig.DISC.FORCE_EN_ZCC = 0;
	tb.chipConfig.DISC.FORCE_TH = 0;
	tb.chipConfig.DISC.SEL_COMP_MODE = 1;
	tb.chipConfig.DISC.arm_dac = 50;
	tb.chipConfig.DISC.zcc_dac = 10;
	tb.chipConfig.DISC.hyst_dac = 5;

	// calibration settings
	tb.chipConfig.CAL.MODE = 1;		// voltage injection
	tb.chipConfig.CAL.POL = 0;		// negative injected charge, the CSA output is a positive pulse
	tb.chipConfig.CAL.DUR = 50;
	tb.chipConfig.CAL.PHI = 0;
	tb.chipConfig.CAL.FS = 3;
	tb.chipConfig.CAL.EN_EXT = 0;
	tb.chipConfig.CAL.DAC = PULSE_CAL_DAC;

	tb.chipConfig.TRG.syncLevelEnable = 0;
	tb.chipConfig.TRG.latency = 4;			// Internal LV1A latency
	tb.chipConfig.TRG.PS = 8;				// Pulse Stretcher control (1-8)
	tb.chipConfig.TRG.ST = 0;				// Self Trigger
	tb.chipConfig.TRG.DDR = 0;				// Set the trigger output mode in DDR

	for (int ip = 25; ip <125; ip = ip+25) // gain measurement for each PT
//	for (int ip = 100; ip <=100; ip = ip+25) // gain measurement for each PT
	{
//		i_pt= ip/25-1;
		//cout << "Peak time: " << ip << endl;
		tb.chipConfig.DISC.PT = ip;
		tb.chipConfig.PRE.TP = ip;
	    tb.chipConfig.CAL.DAC = PULSE_CAL_DAC;
		tb.setChipConfig();
		usleep(10000);

        // Equalize chanells threshold
        equalizeThresholds( 20, 220 );

        // move global threshold to 95% of peak
        tb.chipConfig.DISC.arm_dac = targetThreshold * 0.95;
	    tb.chipConfig.TRG.latency = 1;			// Internal LV1A latency
		tb.setChipConfig();

        // measure time
        measureDelay();
	}

    // restore chip configuration
    tb.chipConfig = chipCfg;
	tb.setChipConfig();

    return true;
}



