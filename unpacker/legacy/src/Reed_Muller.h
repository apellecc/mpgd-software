#ifndef RMULLER_H
#define RMULLER_H

#include <string>

class Reed_Muller
{
public:
    Reed_Muller();
    ~Reed_Muller();
    bool run(std::string param, int *chip_id, int *chip_id_efuse);

};

#endif
