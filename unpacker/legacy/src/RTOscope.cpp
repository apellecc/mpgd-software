#include <string>
#include <stdexcept>
#include <iostream>
#include <unistd.h>
#include "RTOscope.h"

// #define TRACE_CMD

using namespace std;

RTOscope::RTOscope()
{
}


RTOscope::~RTOscope()
{
}

void RTOscope::connectToServer( string &hostname, int port)
{
	TcpClient::connectToServer(hostname, port);
}

void RTOscope::sendCommand(std::string cmd, std::string *res)
{
	string rcStr = "";

#ifdef TRACE_CMD	
	cerr<< cmd << endl;
#endif
	receive (rcStr, 0);	// remove pending data from receive buffer

	transmit( cmd ); //send string

	int qPos = cmd.find( "?", 0 );

	//receive string only when needed
	if( qPos > 0 ){
		receive( rcStr );
#ifdef TRACE_CMD	
		cerr<< rcStr << endl;
#endif
		if (res!=NULL)
			*res = rcStr;
	}
}

void RTOscope::measAmplitudeSetup(int measChannel)
{
	sendCommand("MEASurement"+to_string(measChannel)+":SOUR C1W1");
	sendCommand("MEASurement"+to_string(measChannel)+":MAIN AMPL");	// Configure amplitude measurement
	sendCommand("MEAS"+to_string(measChannel)+" ON; *WAI");
}

void RTOscope::measTimeSetup(int measChannel)
{
	// Clear current status
	sendCommand("IFC");
	sendCommand("DCL");
	sendCommand("*CLS");

	sendCommand("MEASurement"+to_string(measChannel)+":SOUR C1W1");
	sendCommand("MEASurement"+to_string(measChannel)+":MAIN DTOTrigger");
	sendCommand("MEAS"+to_string(measChannel)+" ON; *WAI");
}

/*	// NOT WELL WORKING in SINGLE MODE
void RTOscope::run(int count)
{
	if (count>0){
		sendCommand("ACQuire:COUNt " + to_string(count));
		sendCommand("*WAI");
		sendCommand("SINGle");
	} else {
		sendCommand("ACQuire:COUNt 1; *WAI");
		sendCommand("RUN");	
	}
}
*/

void RTOscope::run()
{
	sendCommand("RUN; *WAI");	
}

double RTOscope::measGetVal(int measChannel)
{
	string res;

	sendCommand("MEASurement"+to_string(measChannel)+":RES:ACTual?", &res);
	return stod(res);
}

void RTOscope::enableStatistics(int measChannel, bool en)
{
	sendCommand("MEASurement"+to_string(measChannel)+":STATistics:ENABle " + (en ? "1" : "0"));
}


void RTOscope::resetStatistics()
{
	sendCommand("MEASurement:Statistics:Reset; *OPC?"); // reset and wait for execution
}

bool RTOscope::measGetStatistics(int measChannel, measStatistics_t *stat)
{
	string res;

	try{
//		sendCommand("MEASurement"+to_string(measChannel)+":RES:ACTual?", &res);
//		stat->ACTual = stod(res);
		sendCommand("MEASurement"+to_string(measChannel)+":RES:AVG?", &res);
		stat->AVG = stod(res);
		sendCommand("MEASurement"+to_string(measChannel)+":RES:STDDev?", &res);
		stat->STDDev = stod(res);
		sendCommand("MEASurement"+to_string(measChannel)+":RES:EVTCount?", &res);
		stat->EVTCount = stod(res);
		sendCommand("MEASurement"+to_string(measChannel)+":RES:NPEak?", &res);
		stat->NPEak = stod(res);
		sendCommand("MEASurement"+to_string(measChannel)+":RES:PPEak?", &res);
		stat->PPEak = stod(res);
		sendCommand("MEASurement"+to_string(measChannel)+":RES:RELiability?", &res);
		stat->RELiability = stod(res);
		sendCommand("MEASurement"+to_string(measChannel)+":RES:RMS?", &res);
		stat->RMS = stod(res);
		sendCommand("MEASurement"+to_string(measChannel)+":RES:WFMCount?", &res);
		stat->WFMCount = stod(res);
	//	sendCommand("MEASurement"+to_string(measChannel)+":RES:STARt?", &res);
	//	stat->STARt = stod(res);
	//	sendCommand("MEASurement"+to_string(measChannel)+":RES:STOP?", &res);
	//	stat->STOP = stod(res);

	} catch (invalid_argument& e){
		return false;
	}

	return true;
}

int RTOscope::measGetEVTCount(int measChannel)
{
	string res;
	int evCount;

	try{
		sendCommand("MEASurement"+to_string(measChannel)+":RES:EVTCount?", &res);
		evCount = stoi(res);
	} catch (std::invalid_argument){
		return -1;
	}

	return evCount;
}



