/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */

#ifndef VFAT3BOARD_H
#define VFAT3BOARD_H

#include <stdint.h>
#include "mdatagenerator.h"
#include "pulser.h"
#include "i2cbus.h"
#include "mwbb.h"
#include "mboard.h"
#include "mdatasave.h"
#include "genconsumer.h"
#include "i2cbus.h"
#include "VFAT3rcv.h"
#include "ComPortTx.h"
#include "IPbusSC.h"
#include "VFAT3dataParser.h"
#include "TRGdataParser.h"
#include "TRGrcv.h"
#include "hardware_settings.h" // for NUM_VFAT3_RECEIVERS
#include "TesterCtrl.h"
#include "ADS1113.h"
#include "ADS1115.h"
#include "PCF8574.h"
#include "AD5254.h"
#include "ATE.h"


class VFAT3board : public MBoard
{
public:
    typedef enum {
        VDD_V12A,
        VDD_V12D,
        VDD_EFUSE,
        VDD_V25D,
        VDD_ALL = -1
    } VDDch_t;

    typedef enum {
        INA_GAIN_25,
        INA_GAIN_50,
        INA_GAIN_100,
        INA_GAIN_200
    } INAgain_t;

	typedef enum {
		V_AVDD,
		I_AVDD,
		V_DVDD,
		I_DVDD,
		V_EFUSE,
		V_SLVS,
		V_VMON,
		V_BGR
	} inADC_t;

    typedef enum {
        MUX_SEL_TU_SOT      = 0,
        MUX_SEL_TU_TXD_0    = 1,
        MUX_SEL_TU_TXD_1    = 2,
        MUX_SEL_TU_TXD_2    = 3,
        MUX_SEL_TU_TXD_3    = 4,
        MUX_SEL_TU_TXD_4    = 5,
        MUX_SEL_TU_TXD_5    = 6,
        MUX_SEL_TU_TXD_6    = 7,
        MUX_SEL_TU_TXD_7    = 8,
        MUX_SEL_TXD         = 9,
        MUX_SEL_NONE        = 10
    } MUX_SEL_t;

    typedef struct {
        float V_V12A;
        float I_V12A;
        float V_V12D;
        float I_V12D;
        float V_EFUSE;
    } powerADC_t;


public:
    VFAT3board();
    VFAT3board(const char *IPaddr, int UDPport=DEFAULT_UDP_PORT);
    ~VFAT3board();
	void setIPaddress(const char *IPaddr, int UDPport=DEFAULT_UDP_PORT);
	void setVerbose(bool v);
	void setCheckData(bool v);
	void addDisableAllReceivers();
	void addEnableReceiver(int num, bool en);
	void addReceiverInvertInput(int num, bool en);
    void resetGTP();
    void setDefaultChip(unsigned int chip);

    void onOffVDD(VDDch_t ch, bool on);
    void setINAgain(INAgain_t gain);
    void setDiffMux(MUX_SEL_t mux);
    void getPowerADC(powerADC_t *res);
    void setVDD(VDDch_t ch, float v);
    void setVDDrdac(VDDch_t ch, uint8_t data);

	void readmon (inADC_t param, float *Out);

#ifdef HAS_FMC_ADC
    void readVslvs_CM(float *Vout)  { adcMon[1]->convert(1, Vout); }
    void readVmon(float *Vout)      { adcMon[1]->convert(2, Vout); }
    void readVbgr(float *Vout)      { adcMon[1]->convert(3, Vout); }
#endif

#ifdef BENCH_VFAT3_PKG
    void setLED(TesterCtrl::LEDid_t led, bool on, bool flash) { testerCtrl->setLED(led, on, flash); }
    void setLEDoff(TesterCtrl::LEDid_t led) { testerCtrl->setLED(led, false, false); }
    void setLEDon(TesterCtrl::LEDid_t led) { testerCtrl->setLED(led, true, false); }
    void setLEDflash(TesterCtrl::LEDid_t led) { testerCtrl->setLED(led, true, true); }
    bool getPushButton() { return testerCtrl->getPushButton(); }
    void setTestType(TesterCtrl::testType_t type) { testerCtrl->setTestType(type); }
    void setExtInj(bool od, bool ev) { testerCtrl->setExtInj(od, ev); }
#else
    void setLED(TesterCtrl::LEDid_t led, bool on, bool flash) {}
    void setLEDoff(TesterCtrl::LEDid_t led) {}
    void setLEDon(TesterCtrl::LEDid_t led) {}
    void setLEDflash(TesterCtrl::LEDid_t led) {}
    bool getPushButton() { return 0; }
    void setTestType(TesterCtrl::testType_t type) {}
    void setExtInj(bool od, bool ev) {}
#endif

public:
    void initATE() {ate->readPatternFile("patterns.tbv");}
    bool runATE() {return ate->run();}
    void runBIST(); 

private:
	void init();
	void initADC();
	void initSysPll();
    void waitBoardReady();

private:
	// extend WBB address definitions in mwbb.h
#if defined( BENCH_VFAT3_RAD ) || defined( BENCH_VFAT3_GEM ) 
	enum baseAddress_e {		
		add_pulser					= (4 << 24),
		add_vfat3ComPortTx_0		= (5 << 24),
		add_vfat3inputPulser		= (6 << 24),
		add_i2cFMC					= (7 << 24),
		add_vfat3Rcv_0				= (8 << 24),
		// total of 1 vfat3Rcv 
		add_trgRcv					= (9 << 24)
		};

#elif defined( BENCH_VFAT3_X5 )
	enum baseAddress_e {		
		add_pulser					= (4 << 24),
		add_trgRcv					= (5 << 24),
		add_vfat3ComPortTx_0    	= (6 << 24),
		add_vfat3Rcv_0				= (7 << 24),
		add_vfat3ComPortTx_1    	= (8 << 24),
		add_vfat3Rcv_1				= (9 << 24),
		add_vfat3ComPortTx_2    	= (10 << 24),
		add_vfat3Rcv_2	    		= (11 << 24),
		add_vfat3ComPortTx_3    	= (12 << 24),
		add_vfat3Rcv_3				= (13 << 24),
		add_vfat3ComPortTx_4    	= (14 << 24),
		add_vfat3Rcv_4				= (15 << 24)
        // other comPortTx/Receiver from 8 to 15
		};

#elif defined( BENCH_VFAT3_PKG )
	enum baseAddress_e {		
		add_pulser					= (4 << 24),
		add_vfat3ComPortTx_0		= (5 << 24),
		add_i2cFMC  				= (6 << 24),
		add_vfat3Rcv_0				= (7 << 24),
		add_ATE				        = (8 << 24),
		add_TesterCtrl				= (9 << 24)
		};

#endif

	// status register bits
	enum BOARD_STATUS_BITS {
		BOARD_STATUS_FEPLL_LOCK			= 0x0001,
		BOARD_STATUS_EXTPLL_LOCK		= 0x0002,
		BOARD_STATUS_GTPLL_LOCK			= 0x0004,
		BOARD_STATUS_GTP_RESET_DONE		= 0x10000		// gtp_resetDone from 1 receivers
	};

    // I2C shadow registers
    uint8_t         cfg[2];

	// I2C device addresses
	enum {
        // Chip on board is PCF8574A, instead of PCF8574 as from schematics
//        I2Caddress_CFG_0                = 0x20,     // PCF8574 U1
//        I2Caddress_CFG_1                = 0x21,     // PCF8574 U26
        I2Caddress_CFG_0                = 0x38,     // PCF8574A U1
        I2Caddress_CFG_1                = 0x39,     // PCF8574A U26
        I2Caddress_rdacVadj             = 0x2c,     // AD5254  U7
        I2Caddress_adcMon_0 			= 0x48,     // ADS1115 U13
        I2Caddress_adcMon_1 			= 0x49      // ADS1115 U14
	};	

public:
	Pulser			 	*pulser;
	ComPortTx			*comPortTxArray[NUM_VFAT3_RECEIVERS];
	ComPortTx			*comPortTx;
	I2Cbus 	 			*i2cBus;
	IPbusSC				*slowControlArray[NUM_VFAT3_RECEIVERS];
	IPbusSC				*slowControl;
	VFAT3rcv			*vfat3Rcv[NUM_VFAT3_RECEIVERS];
	VFAT3dataParser		*vfat3DataParser[NUM_VFAT3_RECEIVERS];
	TRGrcv				*trgRcv;
	TRGdataParser		*trgDataParser;
	GenConsumer 		*dr;

private:
    TesterCtrl          *testerCtrl;
    ATE                 *ate;
	AD5254				*rdacVadj;
	PCF8574				*regCFG[2];

#ifdef BENCH_VFAT3_PKG
    ADS1115             *adcMon[2];
	float 				INAgain;
#elif defined ( HAS_FMC_ADC )
    ADS1113             *adcMon[2];
#endif

	static I2CSysPll::pllRegisters_t sysPLLregContent;

};

#endif // VFAT3BOARD_H
