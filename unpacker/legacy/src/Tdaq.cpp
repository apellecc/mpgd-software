/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#include "TestBench.h"
#include "TBconfig.h"
#include "VFAT3board.h"
#include "Tdaq.h"
#include "Tutils.h"
#include <iostream>
#include <fstream>
#include <cmath>
#include <time.h>

using std::string;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;


Tdaq::Tdaq(TestBench &t) : GenericTest(t)
{
	board = tb.board;
	chipCfg = tb.chipConfig;
}

void Tdaq::VFAT3dataSave(void *ctx, VFAT3dataParser::VFAT3data_t *data)
{
	Tdaq *p = (Tdaq *) ctx;
	const char type = 0; 	

	p->evCounter++;
	p->outputFile.write(&type, 1);
	p->outputFile.write((char*) data, sizeof(VFAT3dataParser::VFAT3data_t));
}

void Tdaq::TRGdataSave(void *ctx, TRGdataParser::TRGdata_t *data)
{
	Tdaq *p = (Tdaq *) ctx;
	const char type = 1; 	
	
	p->trgCounter++;
	p->outputFile.write(&type, 1);
	p->outputFile.write((char*) data, sizeof(TRGdataParser::TRGdata_t));
}


bool Tdaq::run(string param)
{
	VFAT3board		*board = tb.board;
	uint32_t 		count;
	time_t			oldTime=0, newTime, startTime;

	chip = tb.chip;

	// Enable verbose
//	board->vfat3DataParser[tb.getDefaultChip()]->setVerbose(true);

	// Send BC0
	board->comPortTx->sendBC0();

	// Open output file
	outputFile.open (string("Data/")+string("DAQ_")+Tutils::filenameSuffix()+".bin", ios::out | ios::binary);

	// Set consumer functions
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, VFAT3dataSave);
	board->trgDataParser->setDataConsumeFunction(this, TRGdataSave);

	// start run
	evCounter = 0;
	trgCounter = 0;
	startTime = time(NULL);
	oldTime = startTime;
	board->comPortTx->sendRunMode();
	board->mRunControl->startRun();

	// Send pulses
//	board->pulser->setConfig(10, 10000-10 /*, Pulser::OPMODE_ENPLS_BIT*/ );
//	board->pulser->run(3);

	long res;
	do {
		res = board->pollData(1000);		// timeout 1 s
		newTime = time(NULL);
		if (newTime!=oldTime){
			oldTime=newTime;
			long ss = newTime - startTime;
			board->mTriggerControl->getTriggerCounter(&count);
			long mm = ss/60;
			ss %= 60;
			long hh = mm/60;
			mm %=60;

			// print current state
			printf("\rTime:%ld:%02ld:%02ld Trg:%ld Ev:%ld", hh,mm,ss, trgCounter, evCounter);
			fflush(stdout);

			// Flush the data file to disk
			outputFile.flush();

			// check chip sync state
			if (!tb.syncVerify()){
				cerr << "Sync check fail. Resync" << endl;
				int i;
				for (i=5; i>0; i--) 		
					 if (tb.sync())
						break;
				if (i==0){
					cerr << "Failed to resync the chip. Abort" << endl;
					exit(1);
				}
			}
		}
	} while (res>=0);

	// stop the run
	board->comPortTx->sendSConly();
	board->mRunControl->stopRun();

	outputFile.close();
	return true;
}



