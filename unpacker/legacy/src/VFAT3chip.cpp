/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <strings.h>
#include "VFAT3rcv.h"
#include "VFAT3dataParser.h"
#include "VFAT3chip.h"
#include "IPbusSC.h"
#include "mexception.h"
#include "codeDebugger.h"

using namespace std;


VFAT3chip::VFAT3chip(int chip_id, IPbusSC *sc, VFAT3rcv *rcv, VFAT3dataParser *prs)
{
	chipID = chip_id;
	slowControl = sc;
	receiver = rcv;
	parser = prs;	
}

void VFAT3chip::addWriteReg(uint32_t add, uint32_t data)
{
	slowControl->setHDLCaddress(chipID);
	slowControl->addWrite(add, data);
}

void VFAT3chip::writeReg(uint32_t add, uint32_t data)
{
	addWriteReg(add, data);
	slowControl->execute();
}

void VFAT3chip::addRMWreg(uint32_t add, uint32_t mask, uint32_t data)
{
	slowControl->setHDLCaddress(chipID);
	slowControl->addRMWbits(add, mask, data);
}

void VFAT3chip::addReadReg(uint32_t add, uint32_t *data)
{
	slowControl->setHDLCaddress(chipID);
	slowControl->addRead(add, data);
}

void VFAT3chip::addReadADC(int adc, uint32_t *data)
{
	addReadReg(RegADCread_0+adc, data);	
}

#define FROM_DAC7(dac) ((dac & 0x40) ? -(dac&0x3f) : (dac&0x3f))
#define TO_DAC7(val)  ((val<0) ? ((-val&0x3f) | 0x40) : (val&0x3f))

void VFAT3chip::chkRange(string valName, int val, int min, int max)
{
	
	if (val<min || val>max)
		throw MException( "Try to set an invalid value for \"" + valName + "\". (val:" + 
				to_string(val) + " range: "+ to_string(min) + " to " + to_string(max) + ")"); 

}

void VFAT3chip::addSetChannelConfig(int ch, ChannelConfig_t &c)
{
	uint32_t tmp;

	chkRange("Channel arm_dac", c.arm_dac, -63, 63);
	chkRange("Channel zcc_dac", c.arm_dac, -63, 63);

	tmp  = (TO_DAC7(c.arm_dac) << CfgCh_arm_dac_shift) & CfgCh_arm_dac_mask;
	tmp |= (TO_DAC7(c.zcc_dac)  << CfgCh_zcc_dac_shift) & CfgCh_zcc_dac_mask;
	tmp |= c.mask ? CfgCh_mask : 0;
	tmp |= c.cal ? CfgCh_cal : 0;
	addWriteReg(RegCfgChannel_0+ch, tmp);
	if(DEBUG) std::cout<<"<DEBUG> Write "<<tmp<<" to channel : "<< RegCfgChannel_0+ch<<std::endl;
}

void VFAT3chip::addSetPreConfig(PreConfig_t &c)
{
	uint32_t tmp;
	uint8_t tp;
	uint8_t cap=0;
	
	if (c.TP == 25)
		tp = 0x00;
	else if (c.TP == 50)
		tp = 0x01;
	else if (c.TP == 75)
		tp = 0x03;
	else if (c.TP == 100)
		tp = 0x07;
	else
		throw MException( "Front end peaking time must be 25, 50, 75 or 100" ); 

	chkRange("Preamplifier Resistence set", c.RES, 0, 2);
	chkRange("Preamplifier Capacity set", c.CAP, 0, 2);

	if (c.CAP == 0)
		cap = 0x00;
	else if (c.CAP==1)
		cap = 0x01;
	else if (c.CAP==2)
		cap = 0x03;

	tmp  = (tp  << CfgCtr_2_TP_FE_shift) & CfgCtr_2_TP_FE_mask;
	tmp |= ((1<<c.RES) << CfgCtr_2_RES_PRE_shift) & CfgCtr_2_RES_PRE_mask;
	tmp |= (cap << CfgCtr_2_CAP_PRE_shift) & CfgCtr_2_CAP_PRE_mask;
	addWriteReg(RegCfgCtr_2, tmp);
}

void VFAT3chip::addSetDiscriminatorConfig(DiscriminatorConfig_t &c)
{
	uint32_t tmp;
	uint8_t pt;

	if (c.PT == 25)
		pt = 0x01;
	else if (c.PT == 50)
		pt = 0x03;
	else if (c.PT == 75)
		pt = 0x07;
	else if (c.PT == 100)
		pt = 0x0f;
	else
		throw MException( "CFD Time Constant time must be 25, 50, 75 or 100" ); 

	chkRange("CFD output mode", c.SEL_COMP_MODE, 0, 3);

	tmp  = c.EN_HYST ? CfgCtr_3_EN_HYST : 0;
	tmp |= c.SEL_POL ? CfgCtr_3_SEL_POL : 0;
	tmp |= c.FORCE_EN_ZCC ? CfgCtr_3_FORCE_EN_ZCC : 0;
	tmp |= c.FORCE_TH ? CfgCtr_3_FORCE_TH : 0;
	tmp |= (c.SEL_COMP_MODE  << CfgCtr_3_SEL_COMP_MODE_shift) & CfgCtr_3_SEL_COMP_MODE_mask;
	tmp |= (pt  << CfgCtr_3_PT_shift) & CfgCtr_3_PT_mask;
	addWriteReg(RegCfgCtr_3, tmp);

	tmp  = (c.zcc_dac  << CfgThr_ZCC_DAC_shift) & CfgThr_ZCC_DAC_mask;
	tmp |= (c.arm_dac  << CfgThr_ARM_DAC_shift) & CfgThr_ARM_DAC_mask;
	addWriteReg(RegCfgThr, tmp);

	tmp  = (c.hyst_dac  << CfgHyst_HYST_DAC_shift) & CfgHyst_HYST_DAC_mask;
	addWriteReg(RegCfgHyst, tmp);
}

void VFAT3chip::addSetBiasConfig(BiasConfig_t &c)
{
	uint32_t tmp;

	chkRange("Global reference current tuning", c.Iref, 0, 63);
	chkRange("CFD Bias1", c.CFD_DAC_1, 0, 63);
	chkRange("CFD Bias2", c.CFD_DAC_2, 0, 63);
	chkRange("PRE_I_BSF", c.PRE_I_BSF, 0, 63);
	chkRange("PRE_I_BIT", c.PRE_I_BIT, 0, 255);
	chkRange("PRE_I_BLCC", c.PRE_I_BLCC, 0, 63);
	chkRange("PRE_VREF", c.PRE_VREF, 0, 255);
	chkRange("SH_I_BFCAS", c.SH_I_BFCAS, 0, 255);
	chkRange("SH_I_BDIFF", c.SH_I_BDIFF, 0, 255);
	chkRange("SD_I_BDIFF", c.SD_I_BDIFF, 0, 255);
	chkRange("SD_I_BSF", c.SD_I_BSF, 0, 63);
	chkRange("SD_I_BFCAS", c.SD_I_BFCAS, 0, 255);
	chkRange("SLVS_IBIAS", c.SLVS_IBIAS, 0, 63);
	chkRange("SLVS_VREF", c.SLVS_VREF, 0, 63);

	tmp  = (c.Iref  << CfgCtr_5_Iref_shift) & CfgCtr_5_Iref_mask;
	addWriteReg(RegCfgCtr_5, tmp);
	
	tmp   = (c.CFD_DAC_1  << CfgBias_0_CFD_DAC_1_shift) & CfgBias_0_CFD_DAC_1_mask;
	tmp  |= (c.CFD_DAC_2  << CfgBias_0_CFD_DAC_2_shift) & CfgBias_0_CFD_DAC_2_mask;
	addWriteReg(RegCfgBias_0, tmp);

	tmp  = (c.PRE_I_BSF  << CfgBias_1_PRE_I_BSF_shift) & CfgBias_1_PRE_I_BSF_mask;
	tmp |= (c.PRE_I_BIT  << CfgBias_1_PRE_I_BIT_shift) & CfgBias_1_PRE_I_BIT_mask;
	addWriteReg(RegCfgBias_1, tmp);

	tmp  = (c.PRE_I_BLCC  << CfgBias_2_PRE_I_BLCC_shift) & CfgBias_2_PRE_I_BLCC_mask;
	tmp |= (c.PRE_VREF  << CfgBias_2_PRE_VREF_shift) & CfgBias_2_PRE_VREF_mask;
	addWriteReg(RegCfgBias_2, tmp);

	tmp  = (c.SH_I_BFCAS  << CfgBias_3_SH_I_BFCAS_shift) & CfgBias_3_SH_I_BFCAS_mask;
	tmp |= (c.SH_I_BDIFF  << CfgBias_3_SH_I_BDIFF_shift) & CfgBias_3_SH_I_BDIFF_mask;
	addWriteReg(RegCfgBias_3, tmp);

	tmp  = (c.SD_I_BDIFF  << CfgBias_4_SD_I_BDIFF_shift) & CfgBias_4_SD_I_BDIFF_mask;
	addWriteReg(RegCfgBias_4, tmp);

	tmp  = (c.SD_I_BSF  << CfgBias_5_SD_I_BSF_shift) & CfgBias_5_SD_I_BSF_mask;
	tmp |= (c.SD_I_BFCAS  << CfgBias_5_SD_I_BFCAS_shift) & CfgBias_5_SD_I_BFCAS_mask;
	addWriteReg(RegCfgBias_5, tmp);

	tmp  = (c.SLVS_IBIAS  << CfgBias_6_SLVS_IBIAS_shift) & CfgBias_6_SLVS_IBIAS_mask;
	tmp |= (c.SLVS_VREF  << CfgBias_6_SLVS_VREF_shift) & CfgBias_6_SLVS_VREF_mask;
	addWriteReg(RegCfgBias_6, tmp);
}

void VFAT3chip::addSetCalibrationConfig(CalibrationConfig_t &c)
{
	uint32_t tmp;

	chkRange("Calibration pulse phase", c.PHI, 0, 7);
	chkRange("Calibration DAC", c.DAC, 0, 255);
	chkRange("Calibration pulse mode", c.MODE, 0, 2);
	chkRange("Calibration pulse mode", c.DUR, 1, 512);
	chkRange("Calibration scale factor", c.FS, 0, 3);

	tmp  = c.POL ? CfgCal_0_SEL_POL : 0;
	tmp |= c.EN_EXT ? CfgCal_0_EXT : 0;
	tmp |= (c.PHI  << CfgCal_0_PHI_shift) & CfgCal_0_PHI_mask;
	tmp |= (c.DAC  << CfgCal_0_DAC_shift) & CfgCal_0_DAC_mask;
	tmp |= (c.MODE  << CfgCal_0_MODE_shift) & CfgCal_0_MODE_mask;
	addWriteReg(RegCfgCal_0, tmp);

	tmp  = (c.FS  << CfgCal_1_CAL_FS_shift) & CfgCal_1_CAL_FS_mask;
	tmp |= ((c.DUR-1)  << CfgCal_1_CAL_DUR_shift) & CfgCal_1_CAL_DUR_mask;
	addWriteReg(RegCfgCal_1, tmp);
}

void VFAT3chip::addSetMonitorConfig(MonitorConfig_t &c)
{
	uint32_t tmp;

	chkRange("ADC input MUX selection", c.SEL, 0, 41);
	chkRange("ADC internal reference tuning", c.VREF_ADC, 0, 3);

	tmp = c.GAIN ? CfgCtr_4_MON_GAIN : 0;
	tmp |= (c.VREF_ADC  << CfgCtr_4_VREF_ADC_shift) & CfgCtr_4_VREF_ADC_mask;
	tmp |= (c.SEL  << CfgCtr_4_MON_SEL_shift) & CfgCtr_4_MON_SEL_mask;
	addWriteReg(RegCfgCtr_4, tmp);
}

void VFAT3chip::addSetTriggerConfig(TriggerConfig_t &c)
{
	uint32_t tmp;

	chkRange("Trigger Latency", c.latency, 0, 1023);
	chkRange("Pulse Stretcher", c.PS, 1, 8);

	addWriteReg(RegCfgLat, c.latency);
	tmp = ((c.PS-1)  << CfgCtr_0_PS_shift) & CfgCtr_0_PS_mask;
	tmp |= c.syncLevelEnable ? CfgCtr_0_syncLevelEnable : 0;
	tmp |= c.ST ? CfgCtr_0_ST : 0;
	tmp |= c.DDR ? CfgCtr_0_DDR : 0;
	addWriteReg(RegCfgCtr_0, tmp);
}

void VFAT3chip::addSetReadoutConfig(ReadoutConfig_t &c)
{
	uint32_t tmp;
	uint32_t tt=0;

	chkRange("SPZS Maximum partition number", c.MAX_PAR, 0, 16);
	chkRange("Event Counter number of bytes", c.ECb, 0, 3);
	chkRange("Bx Counter number of bytes", c.BCb, 0, 3);

	if (c.BCb==1)
		throw MException( "Bx Counter number of bytes can not be one" ); 
	
	if (c.ECb!=0 && c.BCb!=0)
		tt = 0x00;
	else if (c.ECb!=0 && c.BCb==0)
		tt = 0x01;
	else if (c.ECb==0 && c.BCb!=0)
		tt = 0x02;
	else
		throw MException( "ECb and BCb can not be both zero" ); 

	tmp  = c.SZP ? CfgCtr_1_SZP : 0;
	tmp |= c.SZD ? CfgCtr_1_SZD : 0;
	tmp |= c.EnablePZS ? CfgCtr_1_DT : 0;
	tmp |= (c.MAX_PAR == 0) ? CfgCtr_1_P16 : 0;
	tmp |= ((c.MAX_PAR-1) << CfgCtr_1_PAR_shift) & CfgCtr_1_PAR_mask;
	if (c.ECb>0)
		tmp |= ((c.ECb-1) << CfgCtr_1_ECb_shift) & CfgCtr_1_ECb_mask;
	tmp |= (c.BCb==3) ? CfgCtr_1_BCb : 0;
	tmp |= (tt << CfgCtr_1_TT_shift) & CfgCtr_1_TT_mask;
	addWriteReg(RegCfgCtr_1, tmp);
}

void VFAT3chip::addSetRun(bool r)
{
	addWriteReg(RegCfgRun, r);
}

void VFAT3chip::setConfig(VFAT3config_t &cfg)
{
//	cout << "setConfig" << endl; 
	for (int i=0; i<CHIP_CFG_CHANNELS; i++){
		if(DEBUG) std::cout << "<DEBUG> setConfig CHIP CHANNELS:"<< i<< " of "<< CHIP_CFG_CHANNELS-1  << std::endl; 
		addSetChannelConfig(i, cfg.CH[i]);
	}
	addSetPreConfig(cfg.PRE);
	addSetDiscriminatorConfig(cfg.DISC);
	addSetBiasConfig(cfg.BIAS);
	addSetCalibrationConfig(cfg.CAL);
	addSetMonitorConfig(cfg.MON);
	addSetTriggerConfig(cfg.TRG);
	addSetReadoutConfig(cfg.RO);
	addSetRun(cfg.run);
	slowControl->execute();

	// enable data receiver
	receiver->addEnable(true);
	receiver->execute();
}

void VFAT3chip::getConfig(VFAT3config_t &cfg)
{
	const int nregs = RegCfgBias_6+1;
	uint32_t regs[nregs];
	uint32_t tmp;
	uint32_t runReg;

	// read registers
	slowControl->setHDLCaddress(chipID);
	slowControl->addRead(nregs, RegCfgChannel_0, regs);
	slowControl->addRead(RegCfgRun, &runReg);
	slowControl->execute();

	// convert to config_t
	for (int i=0; i<CHIP_CFG_CHANNELS; i++){
		tmp = regs[i];
		cfg.CH[i].zcc_dac = FROM_DAC7((tmp & CfgCh_zcc_dac_mask) >> CfgCh_zcc_dac_shift);
		cfg.CH[i].arm_dac  = FROM_DAC7((tmp & CfgCh_arm_dac_mask) >> CfgCh_arm_dac_shift);
		cfg.CH[i].mask  = (tmp & CfgCh_mask) ? true : false;
		cfg.CH[i].cal   = (tmp & CfgCh_cal) ? true : false;
	}

	// Front end preamp
	uint8_t tp;
	uint8_t cap;
	uint8_t res;

	tmp = regs[RegCfgCtr_2];
	tp = (tmp & CfgCtr_2_TP_FE_mask) >> CfgCtr_2_TP_FE_shift;
	if (tp==0x00)
		cfg.PRE.TP = 25;
	else if (tp==0x01)
		cfg.PRE.TP = 50;
	else if (tp==0x03)
		cfg.PRE.TP = 75;
	else if (tp==0x07)
		cfg.PRE.TP = 100;
	
	cap = (tmp & CfgCtr_2_CAP_PRE_mask) >> CfgCtr_2_CAP_PRE_shift;
	if (cap==0x00)
		cfg.PRE.CAP = 0;
	else if (cap==0x01)
		cfg.PRE.CAP = 1;
	else if (cap==0x03)
		cfg.PRE.CAP = 2;

	res = (tmp & CfgCtr_2_RES_PRE_mask) >> CfgCtr_2_RES_PRE_shift;
	if (res==0x01)
		cfg.PRE.RES = 0;
	else if (res==0x02)
		cfg.PRE.RES = 1;
	else if (res==0x04)
		cfg.PRE.RES = 2;

	// Discriminator 
	uint8_t pt;
	tmp = regs[RegCfgCtr_3];
	pt = (tmp & CfgCtr_3_PT_mask) >> CfgCtr_3_PT_shift;
	if (pt==0x01)
		cfg.DISC.PT = 25;
	else if (pt==0x03)
		cfg.DISC.PT = 50;
	else if (pt==0x07)
		cfg.DISC.PT = 75;
	else if (pt==0x0f)
		cfg.DISC.PT = 100;

	cfg.DISC.SEL_COMP_MODE = (tmp & CfgCtr_3_SEL_COMP_MODE_mask) >> CfgCtr_3_SEL_COMP_MODE_shift;
	cfg.DISC.EN_HYST = tmp & CfgCtr_3_EN_HYST ? true : false;
	cfg.DISC.SEL_POL = tmp & CfgCtr_3_SEL_POL ? true : false;
	cfg.DISC.FORCE_EN_ZCC = tmp & CfgCtr_3_FORCE_EN_ZCC ? true : false;
	cfg.DISC.FORCE_TH = tmp & CfgCtr_3_FORCE_TH ? true : false;

	tmp = regs[RegCfgThr];
	cfg.DISC.zcc_dac = (tmp & CfgThr_ZCC_DAC_mask) >> CfgThr_ZCC_DAC_shift;
	cfg.DISC.arm_dac = (tmp & CfgThr_ARM_DAC_mask) >> CfgThr_ARM_DAC_shift;

	tmp = regs[RegCfgHyst];
	cfg.DISC.hyst_dac = (tmp & CfgHyst_HYST_DAC_mask) >> CfgHyst_HYST_DAC_shift;

	// BIAS
	tmp = regs[RegCfgCtr_5];
	cfg.BIAS.Iref = (tmp & CfgCtr_5_Iref_mask) >> CfgCtr_5_Iref_shift;

	tmp = regs[RegCfgBias_0];
	cfg.BIAS.CFD_DAC_1  = (tmp & CfgBias_0_CFD_DAC_1_mask) >> CfgBias_0_CFD_DAC_1_shift;
	cfg.BIAS.CFD_DAC_2  = (tmp & CfgBias_0_CFD_DAC_2_mask) >> CfgBias_0_CFD_DAC_2_shift;
	
	tmp = regs[RegCfgBias_1];
	cfg.BIAS.PRE_I_BSF  = (tmp & CfgBias_1_PRE_I_BSF_mask) >> CfgBias_1_PRE_I_BSF_shift;
	cfg.BIAS.PRE_I_BIT  = (tmp & CfgBias_1_PRE_I_BIT_mask) >> CfgBias_1_PRE_I_BIT_shift;

	tmp = regs[RegCfgBias_2];
	cfg.BIAS.PRE_I_BLCC = (tmp & CfgBias_2_PRE_I_BLCC_mask) >> CfgBias_2_PRE_I_BLCC_shift;
	cfg.BIAS.PRE_VREF   = (tmp & CfgBias_2_PRE_VREF_mask) >> CfgBias_2_PRE_VREF_shift;

	tmp = regs[RegCfgBias_3];
	cfg.BIAS.SH_I_BFCAS  = (tmp & CfgBias_3_SH_I_BFCAS_mask) >> CfgBias_3_SH_I_BFCAS_shift;
	cfg.BIAS.SH_I_BDIFF  = (tmp & CfgBias_3_SH_I_BDIFF_mask) >> CfgBias_3_SH_I_BDIFF_shift;

	tmp = regs[RegCfgBias_4];
	cfg.BIAS.SD_I_BDIFF  = (tmp & CfgBias_4_SD_I_BDIFF_mask) >> CfgBias_4_SD_I_BDIFF_shift;

	tmp = regs[RegCfgBias_5];
	cfg.BIAS.SD_I_BSF  = (tmp & CfgBias_5_SD_I_BSF_mask) >> CfgBias_5_SD_I_BSF_shift;
	cfg.BIAS.SD_I_BFCAS  = (tmp & CfgBias_5_SD_I_BFCAS_mask) >> CfgBias_5_SD_I_BFCAS_shift;

	tmp = regs[RegCfgBias_6];
	cfg.BIAS.SLVS_IBIAS  = (tmp & CfgBias_6_SLVS_IBIAS_mask) >> CfgBias_6_SLVS_IBIAS_shift;
	cfg.BIAS.SLVS_VREF  = (tmp & CfgBias_6_SLVS_VREF_mask) >> CfgBias_6_SLVS_VREF_shift;
	
	// Calibration
	tmp = regs[RegCfgCal_0];
	cfg.CAL.POL = (tmp & CfgCal_0_SEL_POL) ? true : false;
	cfg.CAL.EN_EXT = (tmp & CfgCal_0_EXT) ? true : false;
	cfg.CAL.PHI = (tmp & CfgCal_0_PHI_mask) >> CfgCal_0_PHI_shift;
	cfg.CAL.DAC = (tmp & CfgCal_0_DAC_mask) >> CfgCal_0_DAC_shift;
	cfg.CAL.MODE = (tmp & CfgCal_0_MODE_mask) >> CfgCal_0_MODE_shift;

	tmp = regs[RegCfgCal_1];
	cfg.CAL.FS = (tmp & CfgCal_1_CAL_FS_mask) >> CfgCal_1_CAL_FS_shift;
	cfg.CAL.DUR = ((tmp & CfgCal_1_CAL_DUR_mask) >> CfgCal_1_CAL_DUR_shift)+1;

	// Monitor
	tmp = regs[RegCfgCtr_4];
	cfg.MON.GAIN = (tmp & CfgCtr_4_MON_GAIN) ? true : false;
	cfg.MON.VREF_ADC = (tmp & CfgCtr_4_VREF_ADC_mask) >> CfgCtr_4_VREF_ADC_shift;
	cfg.MON.SEL = (tmp & CfgCtr_4_MON_SEL_mask) >> CfgCtr_4_MON_SEL_shift;

	// Trigger
	uint8_t ps;
	cfg.TRG.latency = regs[RegCfgLat];

	tmp = regs[RegCfgCtr_0];
	ps = (tmp & CfgCtr_0_PS_mask) >> CfgCtr_0_PS_shift;
	cfg.TRG.PS = ps+1;
	cfg.TRG.syncLevelEnable = (tmp & CfgCtr_0_syncLevelEnable) ? true : false;
	cfg.TRG.ST = (tmp & CfgCtr_0_ST) ? true : false;
	cfg.TRG.DDR = (tmp & CfgCtr_0_DDR) ? true : false;

	// Readout
	uint8_t tt;
	tmp = regs[RegCfgCtr_1];
	cfg.RO.SZP	= (tmp & CfgCtr_1_SZP) ? true : false;
	cfg.RO.SZD	= (tmp & CfgCtr_1_SZD) ? true : false;
	cfg.RO.EnablePZS	= (tmp & CfgCtr_1_DT) ? true : false;
	if (tmp & CfgCtr_1_P16)
		cfg.RO.MAX_PAR = 0;
	else
		cfg.RO.MAX_PAR = ((tmp & CfgCtr_1_PAR_mask) >> CfgCtr_1_PAR_shift)+1;

	tt = (tmp & CfgCtr_1_TT_mask) >> CfgCtr_1_TT_shift;
	if (tt==0x00)
		tt = 3;
	cfg.RO.ECb = 0;
	cfg.RO.BCb = 0;
	if (tt & 0x02)
		cfg.RO.BCb = (tmp & CfgCtr_1_BCb) ? 3 : 2;
		
	if (tt & 0x01)
		cfg.RO.ECb = ((tmp & CfgCtr_1_ECb_mask) >> CfgCtr_1_ECb_shift) + 1;

	// Sleep/RUN
	cfg.run = runReg & 0x01;
}


VFAT3chip::~VFAT3chip()
{
}

static char *regName[] = { 
	(char *) "Control register 0",
	(char *) "Control register 1",
	(char *) "Control register 2",
	(char *) "Control register 3",
	(char *) "Control register 4",
	(char *) "Control register 5",
	(char *) "Global Threshold",
	(char *) "Global Hysteresis",
	(char *) "Latency",
	(char *) "Calibration 0",
	(char *) "Calibration 1",
	(char *) "Bias 0",
	(char *) "Bias 1",
	(char *) "Bias 2",
	(char *) "Bias 3",
	(char *) "Bias 4",
	(char *) "Bias 5",
	(char *) "Bias 6"
};

static char *HW_IDregName[] = {
	(char *) "RegID",
	(char *) "RegVER",
	(char *) "RegRWreg",
	(char *) "RegChipID",
	(char *) "RegProg"
};

void VFAT3chip::dumpRegisters()
{
	uint32_t cfgRegs[0x93];

	slowControl->setHDLCaddress(chipID);
	slowControl->addRead(0x93, 0, cfgRegs);
	slowControl->execute();

	printf("VFAT3 0x%02x registers dump:\n", chipID);
	for (int i=0; i<CHIP_CFG_CHANNELS; i++)
		printf("0x%04x CfgChannel_%d\n", (int) cfgRegs[i], i);

	int dIdx = 0;
	for (int i=RegCfgCtr_0; i<=RegCfgBias_6; i++){
		printf("0x%04x %s\n", (int) cfgRegs[i], regName[dIdx++]);
	}	

	uint32_t HW_ID[5];
	slowControl->addRead(4, RegID, HW_ID);
	slowControl->execute();

	for (int i=0; i<4; i++){
		printf("0x%08x %s\n", (int) HW_ID[i], HW_IDregName[i]);
	}	
		
	printf("\n");
}

void VFAT3chip::dumpConfig()
{
	VFAT3config_t cfg;

	getConfig(cfg);

	cout << endl << "VFAT3 " << chipID << "configuration dump" << endl;
	for (int i=0; i<CHIP_CFG_CHANNELS; i++){
		cout << "Channel " << i << endl;
		cout << " zcc_dac      " << to_string(cfg.CH[i].zcc_dac) << endl;
		cout << " arm_dac      " << to_string(cfg.CH[i].arm_dac) << endl;
		cout << " mask         " << (cfg.CH[i].mask ? "ON" : "off") << endl;
		cout << " cal          " << (cfg.CH[i].cal ? "ON" : "off") << endl;
	}
	cout << "Pre" << endl;
		cout << " TP         " << to_string(cfg.PRE.TP) << endl;
		cout << " RES        " << to_string(cfg.PRE.RES) << endl;
		cout << " CAP        " << to_string(cfg.PRE.CAP) << endl;

	cout << "Discriminator" << endl;
		cout << " PT            " << to_string(cfg.DISC.PT) << endl;
		cout << " EN_HYST       " << (cfg.DISC.EN_HYST ? "ON" : "off") << endl;
		cout << " SEL_POL       " << (cfg.DISC.SEL_POL ? "ON" : "off") << endl;
		cout << " FORCE_EN_ZCC  " << (cfg.DISC.FORCE_EN_ZCC ? "ON" : "off") << endl;
		cout << " FORCE_TH      " << (cfg.DISC.FORCE_TH ? "ON" : "off") << endl;
		cout << " SEL_COMP_MODE " << to_string(cfg.DISC.SEL_COMP_MODE) << endl;
		cout << " zcc_dac       " << to_string(cfg.DISC.zcc_dac) << endl;
		cout << " arm_dac       " << to_string(cfg.DISC.arm_dac) << endl;
		cout << " hyst_dac      " << to_string(cfg.DISC.hyst_dac) << endl;

	cout << "Monitor" << endl;
		cout << " VREF_ADC      " << to_string(cfg.MON.VREF_ADC) << endl;
		cout << " GAIN          " << (cfg.MON.GAIN ? "ON" : "off") << endl;
		cout << " SEL           " << to_string(cfg.MON.SEL) << endl;

	cout << "Readout" << endl;
		cout << " MAX_PAR      " << to_string(cfg.RO.MAX_PAR) << endl;
		cout << " EnablePZS    " << (cfg.RO.EnablePZS ? "ON" : "off") << endl;
		cout << " SZP          " << (cfg.RO.SZP ? "ON" : "off") << endl;
		cout << " SZD          " << (cfg.RO.SZD ? "ON" : "off") << endl;
		cout << " ECb          " << to_string(cfg.RO.ECb) << endl;
		cout << " BCb          " << to_string(cfg.RO.BCb) << endl;

	cout << "Calibration" << endl;
		cout << " POL          " << (cfg.CAL.POL ? "ON" : "off") << endl;
		cout << " PHI          " << to_string(cfg.CAL.PHI) << endl;
		cout << " EN_EXT       " << (cfg.CAL.EN_EXT ? "ON" : "off") << endl;
		cout << " DAC          " << to_string(cfg.CAL.DAC) << endl;
		cout << " MODE         " << to_string(cfg.CAL.MODE) << endl;
		cout << " FS           " << to_string(cfg.CAL.FS) << endl;
		cout << " DUR          " << to_string(cfg.CAL.DUR) << endl;

	cout << "Bias" << endl;
		cout << " Iref          " << to_string(cfg.BIAS.Iref) << endl;
		cout << " CFD_DAC_1     " << to_string(cfg.BIAS.CFD_DAC_1) << endl;
		cout << " CFD_DAC_2     " << to_string(cfg.BIAS.CFD_DAC_2) << endl;
		cout << " PRE_I_BSF     " << to_string(cfg.BIAS.PRE_I_BSF) << endl;
		cout << " PRE_I_BIT     " << to_string(cfg.BIAS.PRE_I_BIT) << endl;
		cout << " PRE_I_BLCC    " << to_string(cfg.BIAS.PRE_I_BLCC) << endl;
		cout << " PRE_VREF      " << to_string(cfg.BIAS.PRE_VREF) << endl;
		cout << " SH_I_BFCAS    " << to_string(cfg.BIAS.SH_I_BFCAS) << endl;
		cout << " SH_I_BDIFF    " << to_string(cfg.BIAS.SH_I_BDIFF) << endl;
		cout << " SD_I_BDIFF    " << to_string(cfg.BIAS.SD_I_BDIFF) << endl;
		cout << " SD_I_BSF      " << to_string(cfg.BIAS.SD_I_BSF) << endl;
		cout << " SD_I_BFCAS    " << to_string(cfg.BIAS.SD_I_BFCAS) << endl;
		cout << " SLVS_IBIAS    " << to_string(cfg.BIAS.SLVS_IBIAS) << endl;
		cout << " SLVS_VREF     " << to_string(cfg.BIAS.SLVS_VREF) << endl;

	cout << "Trigger" << endl;
		cout << " latency         " << to_string(cfg.TRG.latency) << endl;
		cout << " PS              " << to_string(cfg.TRG.PS) << endl;
		cout << " syncLevelEnable " << (cfg.TRG.syncLevelEnable ? "ON" : "off") << endl;
		cout << " ST              " << (cfg.TRG.ST ? "ON" : "off") << endl;
		cout << " DDR             " << (cfg.TRG.DDR ? "ON" : "off") << endl;

	cout << "RUN/Sleep" << endl;
		cout << " run             " << (cfg.run ? "ON" : "off") << endl;
		
	cout << "End of configuration dump" << endl;
}

// program one eFuse bit
// bit: from 0 to 32 - bit to be fuse
// prgTime in 25 ns period. Programming time (from 10 to 30 us)
void VFAT3chip::eFuseBit(int bit, int prgTime)
{
	uint32_t tmp;
	uint32_t bit32;

	bit32 = bit;

	tmp  = (bit32      << Prg_BIT_shift) & Prg_BIT_mask;
	tmp |= (prgTime  << Prg_TIME_shift) & Prg_TIME_mask;

	slowControl->addWrite(RegProg, tmp);
	slowControl->execute();
}



