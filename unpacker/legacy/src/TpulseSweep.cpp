/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include "pulser.h"
#include "RTOscope.h"
#include "Tutils.h"
#include <fstream>
#include "TBconfig.h"
#include "Tutils.h"
#include "TpulseSweep.h"

using std::string;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;

TpulseSweep::TpulseSweep(TestBench &t) : GenericTest(t)
{
	board = tb.board;
	chip= tb.chip;
}


bool TpulseSweep::run (string revert)
{
	VFAT3chip::CalibrationConfig_t str_cal;
	VFAT3chip::VFAT3config_t conf_chip;
	uint32_t		numPulses;
	RTOscope		scope;
	RTOscope::measStatistics_t meas_scope;

    bool revertScan = (revert == "REV") ? true: false;

	//address oscilloscope
	string scopeAddress = tb.config->ScopeAddress();

	// connection to oscilloscope
	scope.connectToServer( scopeAddress );
	scope.enableStatistics(1);

	// Open output file
	ofstream outputFile;
	outputFile.open (string("Data/")+string("PulseSweep_")+Tutils::filenameSuffix()+".txt");

	// start run
	board->mRunControl->startRun();

	// Send pulses
	board->pulser->setConfig(10, 40000-10, Pulser::OPMODE_ENPLS_BIT );

	chip->getConfig(conf_chip);
	str_cal=conf_chip.CAL;

	tb.initInjCharge(str_cal);

	for (int i=30; i<256; i+=10){
		int EVTCount;
		scope.resetStatistics();
		if (revertScan)
			str_cal.DAC=255-i;
		else
			str_cal.DAC=i;

		chip->addSetCalibrationConfig(str_cal);
		chip->execute();

		do {
			board->pulser->run(5000);
			do {
				tb.board->pulser->getStatus(&numPulses);
			} while (numPulses);
			EVTCount = scope.measGetEVTCount(1);
			if (EVTCount == -1)
				return false;
		} while (EVTCount < 100);

		scope.measGetStatistics(1,&meas_scope);
		float charge = tb.getInjCharge(str_cal);
		cout<< "DAC:" << i << " Charge:" << charge << " Avg: "<< meas_scope.AVG << "  Std: " << meas_scope.STDDev << endl;
		outputFile << i << " " << charge << " " << meas_scope.AVG << " " << meas_scope.STDDev << endl;
	}
		
	board->mRunControl->stopRun();

	outputFile.close();
	return true;
}

