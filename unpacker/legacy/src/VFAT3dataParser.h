/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */

#ifndef VFAT3DATAPARSER_H
#define VFAT3DATAPARSER_H

#include <stdint.h>
#include "mdatareceiver.h"
#include "VFAT3rcv.h"
#include "VFAT3chip.h"


typedef void (*dataSaveFunction_t)(void *, size_t);

class VFAT3dataParser : public MDataReceiver
{
public:
	VFAT3dataParser();
	void flush();
	void setDataFormat(VFAT3rcv::dataformat_t &f) {dataFormat = f;}
	void setStatisticsPtr(VFAT3chip::statistics_t *sPtr);
	void resetStatistics();
	
protected:
	long parse(int numClosed);

public:
	typedef struct VFAT3data_s {
		uint32_t		EC;
		uint32_t		BC;
		bool		hit[128];
	} __attribute__((packed)) VFAT3data_t;
	VFAT3data_t VFAT3data;

typedef void (*dataConsumeFunction_t)(void *, VFAT3data_t *);

private:
	enum CPHDR_e {
		HDR_1 	= 	0x1e,		// Data packet 
		HDR_1W 	= 	0x5e,		// Data packet with FIFO full
		HDR_2 	= 	0x1a,		// Empty data packet
		HDR_2W 	= 	0x56		// Empty data packet with FIFO full
	};

private:
	bool verbose;
	int id;
	uint16_t crc;
	dataSaveFunction_t dataSaveFunction;
	void *dataConsumeCtx;
	dataConsumeFunction_t dataConsumeFunction;
	VFAT3rcv::dataformat_t dataFormat;
	VFAT3chip::statistics_t *statisticsPtr;

public:
	void setVerbose(bool v) { verbose = v; }
	void setId(int i) { id = i; }
	void setDataSaveFunction(dataSaveFunction_t f) {dataSaveFunction = f;}
	void setDataConsumeFunction(void *ctx, dataConsumeFunction_t f) 
		{
			dataConsumeCtx = ctx; 
			dataConsumeFunction = f;
		}

private:
	uint8_t addCrc(uint8_t b);
	long checkEvent(unsigned char *dBuffer, bool *good);
	void updateStatistics();
};


#endif // VFAT3DATAPARSER_H
