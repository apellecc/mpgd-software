/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */


#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include "mexception.h"
#include "TestBench.h"
#include "IPbusTester.h"
#include "TmonState.h"
#include "TdacSweep.h"
#include "TpulseSweep.h"
#include "TScurves.h"
#include "Tcalibrate.h"
#include "TGain.h"
#include "Tcfd.h"
#include "Tdaq.h"
#include "pulser.h"
#include "miscTest.h"
#include "ProductionTest.h"
#include "PTpeaktime.h"

typedef struct paramList_s {
	string name;
	GenericTest *classPtr;
    string paramenter;
	string description;
} paramList_t;

TestBench tb;

// Configuration class
cfg             Config_cfg(tb);
test_powerONOFF Power(tb);
test_dumpADC    DumpADC(tb);
chip_init       ChipInit(tb);
dump_config     DumpConfig(tb);
infinite_pulse  InfinitePulse(tb);
test_eFuse      TestEfuse(tb);
IPbusTester     TestIPBus(tb);
TmonState       monState(tb);
TdacSweep       dacSweep(tb);
TpulseSweep     VpulseSweep(tb);
simpleReadout   SimpleReadout(tb);
TScurves        Scurves(tb);
TGain           TGain(tb);
Tcalibrate      Calibrate(tb);
Tcfd            TCFD(tb);
Tdaq            DAQ(tb);
ProductionTest  PTest(tb);
test_ATE        TestATE(tb);
test_BIST       TestBIST(tb);
test_EYE        TestEYE(tb);
PTpeaktime      test_peakTime(tb);
set_default_chip setDefaultChip(tb);

// Production test calsses 
// PTgain      PTest_Gain(tb);

paramList_t TestList [] = {
	{ "-cfg", 	        &Config_cfg, 	"<Config_file>",    "Read configuration file" },
	{ "-power", 	    &Power, 	    "[OFF]",            "Power ON/OFF the chip" },
	{ "-dump_ADC", 	    &DumpADC, 	    "",                 "Dump ADC inputs" },
	{ "-chip_init", 	&ChipInit,  	"",                 "Chip initializzation and configuration" },
	{ "-dump_config", 	&DumpConfig,  	"",                 "Dump chip configuration" },
	{ "-eFuse", 		&TestEfuse, 	"",                 "eFuse test" },
	{ "-ipbus", 	    &TestIPBus,  	"",                 "Full IPbus transaction tests" },
	{ "-monitor",		&monState,      "",                 "Read all monitor values" },
    { "-dacSweep",		&dacSweep,      "",                 "Sweep of all DACs" },
	{ "-VpulseSweep",	&VpulseSweep,   "[REV]",            "Calibration pulse sweep" },
	{ "-infinite_pulse", &InfinitePulse,"",                 "Infinite burst of analogue pulses" },
	{ "-simpleReadout",  &SimpleReadout,"",             	"Simple pulse-readout test" },
	{ "-scurves",		&Scurves,	    "[PARALLEL]",       "Scurves of all channels one by one or in parallel" },
	{ "-gain", 			&TGain,		    "",                 "Gain scan" }, 
	{ "-calibrate",		&Calibrate,	    "",                 "Calibrate ARM & CFD locale DACs" },
	{ "-CFD",			&TCFD,		    "",                 "Test CFD" },
	{ "-DAQ",			&DAQ,		    "",                 "Standard DAQ run" },
	{ "-peak",			&test_peakTime,	"",                 "Measure Peaking time" },
	{ "-set_default_chip", &setDefaultChip, "<Chip_number>","Set default chip number" },

	{ "-ATE",	        &TestATE,		"",                 "Digital test ATE" },
	{ "-BIST",	        &TestBIST,		"",                 "Digital test BIST" },
	{ "-EYE",	        &TestEYE,		"",                 "Eye diagram" },
	{ "-production",	&PTest,		    "",                 "Production test loop" }
};


int main(int argc, char**argv)
{

	if (argc<2)
		goto invokeError;

	for (int i=1; i<argc; i++){
        bool found = false;
        string parameter = "";
		for (const auto& test: TestList){
			if (test.name == argv[i]){
				found = true;
                if (i+1 < argc && argv[i+1][0] != '-'){ // get command argument 
                    parameter = argv[i+1];
                    i++;
                }

				cout << "Running \"" << test.description << "\"" << endl;
				if (!test.classPtr->run(parameter))
					return 0;
				break;
			}
		}
		if (!found)
			goto invokeError;
    }
	return 0;	

invokeError:
	cerr << "*******************************************" << endl;
	cerr << "Parameters parsing Error" << endl; 
	cerr << "use:VFAT3 <Argument list>" << endl;
	cerr << "===========================================" << endl;
	cerr << "List of arguments:" << endl;
	for (const auto& test: TestList)
        cerr << test.name << " " << test.paramenter << " " << test.description << endl;
	cerr << "===========================================" << endl;
	return 1;
}


