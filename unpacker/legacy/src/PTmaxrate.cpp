/*
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>.
 *
 */

#include <iostream>
#include <unistd.h>
#include <strings.h>
#include "PTmaxrate.h"
#include "PTscurves.h"
#include "Tutils.h"

const double lcldac_gain_min = -1000;
const double lcldac_gain_max = 6000;

const double lcldac_offset_min = -600;
const double lcldac_offset_max = 3000;

PTmaxrate::PTmaxrate(TestBench &t) : GenericTest(t)
{
	board = tb.board;
}

PTmaxrate::~PTmaxrate()
{
}

void PTmaxrate::resetHistogram()
{
	evCounter = 0;
	for (int i=0; i<128; i++)
	histogram[i] = 0;
}

void PTmaxrate::histogramCB(void *ctx, VFAT3dataParser::VFAT3data_t *data)
{
	PTmaxrate *p = (PTmaxrate *) ctx;
	
	p->evCounter++;

	for (int i=0; i<128; i++)
	if (data->hit[i])
	p->histogram[i]++;
}

void PTmaxrate::pulseAtChargeDAC(unsigned long nPulses)
{
	// start pulses injection				
	board->pulser->run(nPulses);

	resetHistogram();
	long res;
	do {
		res = board->pollData(50);		// timeout 50 ms
		if (res == 0){
			cerr << "Timeout reading data" << endl;
			throw TestError("Timeout reading data");
		}
	} while (evCounter<nPulses);
}

bool PTmaxrate::run(string param)
{
	PTscurves fit_scurve(tb);
	bool fit_status;

	double vbsl[128];
	int arm_th = 0;
	int chargeDAC = 0;
	int numPulses = 2000;
	
	int GBL_dac = 100;

	// disable cal bit on all channels
	for (int i=0; i<128; i++)
	{
		tb.chipConfig.CH[i].arm_dac = 0;
		tb.chipConfig.CH[i].zcc_dac = 0;
		tb.chipConfig.CH[i].mask = false;
		tb.chipConfig.CH[i].cal = false;
	}

	//calibration settings
	tb.chipConfig.CAL.MODE = 2;		// current injection
	tb.chipConfig.CAL.POL = 0;		// negative injected charge, the CSA output is a positive pulse
	tb.chipConfig.CAL.DUR = 1;
	tb.chipConfig.CAL.PHI = 0;
	tb.chipConfig.CAL.FS = 1;

	//CFD settings
	tb.chipConfig.DISC.PT = 100;
	tb.chipConfig.DISC.SEL_POL = 0;
	tb.chipConfig.DISC.EN_HYST = 1;
	tb.chipConfig.DISC.FORCE_EN_ZCC = 0;
	tb.chipConfig.DISC.FORCE_TH = 0;
	tb.chipConfig.DISC.SEL_COMP_MODE = 1;

	//CSA settings - medium gain
	tb.chipConfig.PRE.TP = 100;
	tb.chipConfig.PRE.RES = 1;
	tb.chipConfig.PRE.CAP = 1;

	//Set Global ARM DAC
	tb.chipConfig.DISC.arm_dac = GBL_dac;
	tb.setChipConfig();
	usleep(10000);
	calCfg = tb.chipConfig.CAL;

	//fit_status = fit_scurve.rungainfast(vbsl);
	double vENC[128];
	fit_scurve.run("SERIAL", vENC, vbsl);

	// Init the charge injection
	calCfg = tb.chipConfig.CAL;
	tb.initInjCharge(calCfg);

	// Set consumer function
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, histogramCB);

	// Setup pulses
	board->pulser->setConfig(10, 10);	// 10 us (plus 250 ns) period

	// Send BC0
	board->comPortTx->sendBC0();

	// start run
	board->mRunControl->startRun();
	board->comPortTx->sendRunMode();
	tb.chip->execute();

	for (int i=0; i<128; i++)
	{
		tb.chipConfig.CH[i].arm_dac = 0;
		tb.chipConfig.CH[i].zcc_dac = 0;
		tb.chipConfig.CH[i].mask = false;
		tb.chipConfig.CH[i].cal = false;
	}
	tb.setChipConfig();

	arm_th = 0.9 * GBL_dac;
	tb.chipConfig.DISC.arm_dac = arm_th;

	for (int ch = 0; ch<128; ch++)
	{
		//cout << "vbsl " << vbsl[ch] << endl;
		
		//arm_th = ( 0.9* vbsl[ch] * tb.ptdata->VFAT3.chn_d[ch].gain[1]*1e-3 - tb.ptdata->VFAT3.dac_d[16].offset ) / tb.ptdata->VFAT3.dac_d[16].gain;
		chargeDAC = ( vbsl[ch] / ( (tb.chipConfig.CAL.DUR) * 25 * (tb.chipConfig.CAL.FS + 1) * 0.1 * 1e6) - tb.ptdata->VFAT3.calib_d.ipul_offset ) / tb.ptdata->VFAT3.calib_d.ipul_gain;
		//cout << "dac arm th " <<arm_th << endl;
		//cout << "dac inj " <<chargeDAC << endl;

		// set global arm dac
		// tb.chipConfig.DISC.arm_dac = arm_th;
		// set cal dac
		tb.chipConfig.CAL.DAC = chargeDAC;

		// set cal bit		
		tb.chipConfig.CH[ch].cal = true;
		tb.setChipConfig();
		usleep(10000);
        
        pulseAtChargeDAC(numPulses);
		if (histogram[ch] == numPulses)
		{	
			cout << "Channel " << ch << " max rate test passed" << endl;
		}
		else
		{
			cout << "Channel " << ch << " max rate test not passed, num pulses: " << histogram[ch] << " of " << numPulses << endl;
		}
		
		tb.chipConfig.CH[ch].cal = false;

	}

	tb.setChipConfig();

    return true;
}

