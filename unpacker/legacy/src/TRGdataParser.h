/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */

#ifndef TRGDATAPARSER_H
#define TRGDATAPARSER_H

#include <stdint.h>
#include "mdatareceiver.h"


// typedef void (*dataSaveFunction_t)(void *, size_t);

class TRGdataParser : public MDataReceiver
{
public:
	TRGdataParser();
	void flush();
	
protected:
	long parse(int numClosed);

public:
	typedef struct VFAT3data_s {
		uint32_t		EC;
		uint8_t			PHASE;	
		uint64_t		BC;
	} __attribute__((packed)) TRGdata_t;
	TRGdata_t TRGdata;

typedef void (*dataConsumeFunction_t)(void *, TRGdata_t *);

private:
	bool verbose;
	int id;
	void *dataConsumeCtx;
	dataConsumeFunction_t dataConsumeFunction;

public:
	void setVerbose(bool v) { verbose = v; }
	void setId(int i) { id = i; }
	void setDataConsumeFunction(void *ctx, dataConsumeFunction_t f) 
		{
			dataConsumeCtx = ctx; 
			dataConsumeFunction = f;
		}

private:
	long checkEvent(unsigned char *dBuffer, bool *good);
};


#endif // TRGDATAPARSER_H
