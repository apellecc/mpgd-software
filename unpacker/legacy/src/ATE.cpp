/*
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include "mexception.h"
#include "TestError.h"
#include "ATE.h"


#define CONTINUE_ON_FAULT

ATE::ATE(WishboneBus *wbbPtr, uint32_t baseAdd) : 
			MWbbSlave(wbbPtr, baseAdd)
{
    pattern        = NULL;
    patternMemSize  = 0;
}

// Push program patterns in the ATE
void ATE::addPushPatterns(int size, uint32_t *data)
{
    wbb->addNIWrite(size, baseAddress+regPatternsMem, data);
}

// Get ATE status
void ATE::getStatus(status_t *status)
{
    uint32_t d;

    wbb->addRead(baseAddress+regStatus, &d);
    wbb->execute();


       
    status->running         = (d & STATUS_RUNNING_MASK) >> STATUS_RUNNING_SHIFT;
    status->unknowStatement = (d & STATUS_UNKNOW_MASK)  >> STATUS_UNKNOW_SHIFT;
    status->endPattern      = (d & STATUS_END_MASK)     >> STATUS_END_SHIFT;
    status->logSize         = (d & STATUS_LOG_SIZE_MASK)>> STATUS_LOG_SIZE_SHIFT;
    status->currentPattern  = (d & STATUS_PATTERN_MASK) >> STATUS_PATTERN_SHIFT;

/*
cout << "ATE::getStatus status:" << (int) d;
cout << "  running:" << status->running;
cout << "  unknowStatement:" << status->unknowStatement;
cout << "  logSize:" << (int) status->logSize;
cout << "  currentPattern:" << status->currentPattern << endl;
*/
}

// read one record from error log memory
void ATE::getError(error_t *error)
{
    uint32_t d_L, d_H;

    wbb->addRead(baseAddress+regErrorLogMem_L, &d_L);
    wbb->addRead(baseAddress+regErrorLogMem_H, &d_H);
    wbb->execute();
    
    error->cellNum_PO = d_L;
    error->pattern    = (d_H & ERR_PATTERN_MASK) >> ERR_PATTERN_SHIFT;
    error->chains     = (d_H & ERR_CHAINS_MASK)  >> ERR_CHAINS_SHIFT;
}

void ATE::readPatternFile(string fileName)
{
    std::ifstream file(fileName, std::ios::binary | std::ios::ate);       // Set cursor at End of file

    if (!file.is_open()){
        cerr << "ATE::readPatternFile - Cannot open input file " << fileName << endl;   
        exit(0);
    }

    std::streamsize size = file.tellg();
    file.seekg(0, std::ios::beg);                       // Se cursor at begin of file

    uint8_t *buffer = new uint8_t[size];

    if (!file.read((char*)buffer, size)) {
        std::cerr << "Error reading patterns file. Abort" << std::endl;
        exit(0);
    }

    // Allocate pattern memory
    patternMemSize = size/4;
    pattern = new uint32_t[patternMemSize];

    // convert bytes in words
    uint8_t  *bPtr = buffer;
    uint32_t *wPtr = pattern;
    for (uint32_t i=0; i<patternMemSize; i++){
        uint32_t tmp = *(uint32_t*) bPtr;
        *wPtr++ = __builtin_bswap32(tmp);
        bPtr += 4;
    }

    delete buffer;
}

bool ATE::run()
{
    // Before to call this function you can change default settings using functions:
    //   void addSetScanLen(uint32_t len); 
    //   void addSetLoadUnloadCondition(uint32_t c); 
    //   void addSetTestSetupCondition(uint32_t c);

    errorCounter = 0;
    status_t status;
    readPatternFile("patterns.tbv");        // FIXME scoprire come evitare che vengano create due instance di questa classe
    
    // start ATE FSM
    addSetCtrl(true);
    getStatus(&status);
    addSetCtrl(false);
    getStatus(&status);
    addSetCtrl(true);
    getStatus(&status);
    getStatus(&status);
    getStatus(&status);

    cout << "ATE::run Starting ATE test" << endl;

    // Push patterns using data paskets of 1 Kbyte
    const int pattSeg = 256;

    for (uint32_t i=0; i<patternMemSize; i+=pattSeg) {
        if (i > 0 && (i%100000 == 0)){
            int percent = (i*100)/patternMemSize;
            std::cout << to_string(percent) << "% " << std::flush;
            fflush(stdout);
        }

	    try {
            int s = (patternMemSize-i < pattSeg) ? (patternMemSize-i) : pattSeg;
            addPushPatterns(s, pattern+i);
            getStatus(&status);

            if (status.unknowStatement){
                std::cerr << "ATE::run - Unknow statement detecter. Abort" << std::endl;
                exit(0);
            }

            if (!status.running){
                std::cerr << "ATE::run - ATE FSM is not running!. Abort" << std::endl;
                exit(0);
            }

            if (status.endPattern){
                std::cerr << "ATE::run - Unexpected endPattern detected. Abort" << std::endl;
                exit(0);
            }

            if (status.logSize != 0 ){
                dumpErrorLog();
#ifndef CONTINUE_ON_FAULT
                goto abortRun;
#endif
            }

	    } catch (MIPBusErrorWrite) {
            // If error log memory is half full the write transaction is closed with error
            // If we need to continue the test, we should recover from this exception pushing unprecessed pattern data
            // IPBUS class now does not support recover...

            dumpErrorLog();
#ifndef CONTINUE_ON_FAULT
            goto abortRun;
#endif
/*
	    } catch (MIPBusError) {
cout << "Sent " << i << " pattern words" << endl;
// Si blocca dopo "Sent 2056 pattern words" con un "IPBus Error: Remote bus timeout in write" => probabilmente prg_full

            dumpErrorLog();
            goto abortRun;
*/
        }
    }

    std::cout << "100%" << std::endl;

    // Wait last pattern to be processed
    do {
        getStatus(&status);
        if (status.unknowStatement){
            std::cerr << "ATE::run - Unknow statement detecter. Abort" << std::endl;
            exit(0);
        }

        if (status.logSize != 0 ){
            dumpErrorLog();
#ifndef CONTINUE_ON_FAULT
            goto abortRun;
#endif
        }
    } while (!status.endPattern); 


    // Close the run
    addSetCtrl(false);
    if (errorCounter>0) {
        std::cout << "*********************************************************" << endl;
        std::cout << "*********************************************************" << endl;
        std::cout << "ATE::run FAILED with " << to_string(errorCounter) << " total faults detected" << endl;
        std::cout << "*********************************************************" << endl;
        std::cout << "*********************************************************" << endl;
        throw TestError("ATE test failed");
    } else {
        std::cout << "*********************************************************" << endl;
        std::cout << "ATE::run finished successufully after " << to_string(status.currentPattern) << " test patterns" << endl;
        std::cout << "*********************************************************" << endl;
    }
    return true;


#ifndef CONTINUE_ON_FAULT
abortRun:
    addSetCtrl(false);
    throw TestError("ATE test failed");
    return false;
#endif
}

void ATE::dumpErrorLog()
{

    status_t status;
    getStatus(&status);
    
    int logSize = status.logSize;

    for (int i=0; i<logSize; i++){
        error_t error;
        getError(&error);
        
        errorCounter++;
        if (errorCounter < 100) {
            std::cout << std::endl << "Error on Pattern:" << to_string(error.pattern);
     
            if (error.chains==0){ 
                // error on primary output
                std::cout << " PO_err:0x" << std::hex << error.cellNum_PO;
            } else {
                std::cout << " Cell:" << to_string(error.cellNum_PO);
                std::cout << " Chains:0x" << std::hex << error.chains;
            }
            std::cout << std::endl;
        }
    }
}



