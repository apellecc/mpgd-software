#include "VFAT3_data.h"
#include "TestBench.h"
#include "GenericTest.h"

class PTvref : public GenericTest
{
public:
	PTvref(TestBench &tb);
	~PTvref();
	bool run(string param);

};
