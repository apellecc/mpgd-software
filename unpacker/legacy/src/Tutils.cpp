/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */

#include "mpfit.h"
#include "Tutils.h"
#include <string.h>
#include <strings.h>
#include <math.h>
#include <iostream>

using namespace std;


string Tutils::filenameSuffix()
{
	char suffix[20];
	time_t       t = time(0);   // get time now
	struct tm 	*now = localtime( & t );
	sprintf(suffix, "%04d%02d%02d_%02d%02d%02d", now->tm_year + 1900, now->tm_mon + 1, now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);

	return string(suffix);
}


/* 
 * error function fit function
 *
 * m - number of data points
 * n - number of parameters (4)
 * p - array of fit parameters 
 *     p[0] = x position
 *     p[1] = gaussian sigma width
 * dy - array of residuals to be returned
 * vars - private data (struct vars_struct *)
 *
 * RETURNS: error code (0 = success)
 */
int Tutils::erf_func(int m, int n, double *p, double *dy, double **dvec, void *vars)
{
  int i;
  struct vars_struct *v = (struct vars_struct *) vars;
  double *x, *y;
  double nInject;

  x = v->x;
  y = v->y;
  nInject = (double) (v->nInject) * 0.5;
// printf("p[0]:%f p[1]:%f nInject:%f\n", p[0], p[1], nInject);

  for (i=0; i<m; i++) {
	double f =  nInject * erf((x[i]-p[0]) / (sqrt(2)*p[1])) + nInject;
	dy[i] = (y[i] - f);

//	printf("x:%f y:%f f:%f dy[%d]=%f\n", x[i], y[i],  f, i, dy[i]);
  }

  return 0;
}


bool Tutils::fitErf(long nInject, int nPoints, double *x, double *y, 
				double *th, double *thErr, double *enc, double *encErr)
{
	double p[2];       	/* Initial conditions */
	p[0] = x[nPoints/2];
	p[1] = x[nPoints-1] - x[0];

	double perror[2];			   	/* Returned parameter errors */
	mp_par pars[2];			   		/* Parameter constraints */         
	mp_result result;
	int status;

	memset(&result,0,sizeof(result));      /* Zero results structure */
	result.xerror = perror;

	memset(pars,0,sizeof(pars));        /* Initialize constraint structure */

	struct vars_struct v;
	v.x = x;
	v.y = y;
	v.nInject = nInject;

	/* Call fitting function for data points and 2 parameters (no
		parameters fixed) */
	status = mpfit(erf_func, nPoints, 2, p, pars, NULL, (void *) &v, &result);

	if (status <= 0 || status == MP_MAXITER)
		return false;

	/*if (p[1] < 0.1)
	{
		for (int i = 0; i < nPoints; i++)
		{
			cout << "Carica: " << x[i] << "[fC] - Hit: "<< y[i] << endl;
		}
	}*/


// cout << result.orignorm << " " << result.bestnorm << endl;

	*th = p[0];
	*thErr = perror[0];
	*enc = p[1];
	*encErr = perror[1];
	return true;
} 

bool Tutils::bestFitErf(long nInject, int nPoints, double *x, double *y, 
				double *th, double *thErr, double *enc, double *encErr)
{
	mp_config config;
	memset(&config, 0, sizeof(config));
	config.maxiter = 1000;

	double p[2];       	/* Initial conditions */
	p[0] = nPoints/2;
	p[1] = nPoints-1;

	double perror[2];			   	/* Returned parameter errors */
	mp_par pars[2];			   		/* Parameter constraints */         
	mp_result result;
	int status;

	memset(&result,0,sizeof(result));      /* Zero results structure */
	result.xerror = perror;

	memset(pars,0,sizeof(pars));        /* Initialize constraint structure */

	struct vars_struct v;
	v.x = x;
	v.y = y;
	v.nInject = nInject;

	/* Call fitting function for data points and 2 parameters (no
		parameters fixed) */
	status = mpfit(erf_func, nPoints, 2, p, pars, &config, (void *) &v, &result);

	if (status<0 || status == MP_MAXITER)
		return false;

	// got coarse fitting, loop around it
	double thr = p[0];
	double sigma = p[1];

cout << "thr:" << thr << " sigma:" << sigma << " chi2:" << result.bestnorm << endl;

	for (float t=thr-sigma*2; t<thr+sigma*2; t+=sigma*0.2){
		p[0] = t;
		p[1] = sigma/8;

		memset(&result,0,sizeof(result));      /* Zero results structure */
		result.xerror = perror;

		memset(pars,0,sizeof(pars));        /* Initialize constraint structure */
		status = mpfit(erf_func, nPoints, 2, p, pars, &config, (void *) &v, &result);

		if (status<0 || status == MP_MAXITER)
			return false;

cout << result.orignorm << " " << result.bestnorm << " " << perror[1] << endl;

		
	} 

	*th = p[0];
	*thErr = perror[0];
	*enc = p[1];
	*encErr = perror[1];
	return true;
}

bool Tutils::bestFitLinDAC(int Start_fit, int Stop_fit, float *y, double *m, double *q)
{
	int Np;
	double sum_x = 0;
	double sum_y = 0;
	double sum_xy = 0;
	double sum_xs = 0;
	double var = 0;
	double slp = 0;
	double intr = 0;

	Np= Stop_fit - Start_fit + 1;

	for (int i = Start_fit; i <= Stop_fit; i++){
		sum_x = sum_x + i;
		sum_y = sum_y + y[i-Start_fit];
		sum_xy = sum_xy + i*y[i-Start_fit];
		sum_xs = sum_xs + pow(i,2);
	}

	var = sum_xs*Np - pow(sum_x,2);

	if (var) {
		slp = (Np*sum_xy - sum_x*sum_y)/var;
		intr = sum_y/Np - slp * sum_x/Np;
	} 
	else {
		return false;
	}

	*m = slp;
	*q = intr;

	return true;
}

bool Tutils::bestFitLin(int Np, float *x, float *y, double *m, double *q)
{
	double sum_x = 0;
	double sum_y = 0;
	double sum_xy = 0;
	double sum_xs = 0;
	double var = 0;
	double slp = 0;
	double intr = 0;

	for (int i = 0; i < Np; i++){
		sum_x = sum_x + x[i];
		sum_y = sum_y + y[i];
		sum_xy = sum_xy + x[i]*y[i];
		sum_xs = sum_xs + pow(x[i],2);
	}

	var = sum_xs*Np - pow(sum_x,2);

	if (var) {
		slp = (Np*sum_xy - sum_x*sum_y)/var;
		intr = sum_y/Np - slp * sum_x/Np;
	} 
	else {
		return false;
	}

	*m = slp;
	*q = intr;

	return true;
}

bool Tutils::bestFitLin(int Np, uint32_t *x, float *y, double *m, double *q)
{
	double sum_x = 0;
	double sum_y = 0;
	double sum_xy = 0;
	double sum_xs = 0;
	double var = 0;
	double slp = 0;
	double intr = 0;

	for (int i = 0; i < Np; i++){
		sum_x = sum_x + x[i];
		sum_y = sum_y + y[i];
		sum_xy = sum_xy + x[i]*y[i];
		sum_xs = sum_xs + pow(x[i],2);
	}

	var = sum_xs*Np - pow(sum_x,2);

	if (var) {
		slp = (Np*sum_xy - sum_x*sum_y)/var;
		intr = sum_y/Np - slp * sum_x/Np;
	} 
	else {
		return false;
	}

	*m = slp;
	*q = intr;

	return true;
}
