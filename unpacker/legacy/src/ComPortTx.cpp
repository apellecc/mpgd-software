/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include "iostream"
#include <mexception.h>
#include "ComPortTx.h"


ComPortTx::ComPortTx() 
{
}


ComPortTx::ComPortTx(WishboneBus *wbbPtr, uint32_t baseAdd) : 
			MWbbSlave(wbbPtr, baseAdd)
{
}

ComPortTx::~ComPortTx()
{
}

//
// set register
//
void ComPortTx::addSetReg(uint16_t address, uint32_t val)
{
	if (!wbb)
		throw MIPBusError("No IPBus configured");

	wbb->addWrite(baseAddress+address, val);
}

void ComPortTx::addSetNReg(uint16_t address, uint32_t *val, int size)
{
	if (!wbb)
		throw MIPBusError("No IPBus configured");

	wbb->addWrite(size, baseAddress+address, val);
}

void ComPortTx::addRMWReg(uint16_t address, uint32_t mask, uint32_t val)
{
	if (!wbb)
		throw MIPBusError("No IPBus configured");

	wbb->addRMWbits(baseAddress+address, mask, val);
}


//
// Read register
//
void ComPortTx::addGetReg(uint16_t address, uint32_t *val)
{
	if (!wbb)
		throw MIPBusError("No IPBus configured");

	wbb->addRead(baseAddress+address, val);
}

void ComPortTx::addGetNReg(uint16_t address, uint32_t *data, int size)
{
	if (!wbb)
		throw MIPBusError("No IPBus configured");

	wbb->addRead(size, baseAddress+address, data);
}

//
//	
//
void ComPortTx::sendCommand(CP_OPCODE_t opCode)
{
	addSetReg(regOpCode, (uint32_t) opCode);
	wbb->execute();
}

void ComPortTx::sendSCpkt(uint8_t hdlcAddress, int size, uint32_t *dataPtr)
{
	const int bufferSize = (regHDLCtxBufferEnd-regHDLCtxBuffer);

	if (size > bufferSize-1)
		throw MIPBusError("IPBus packet larger then TX buffer");

	addSetReg (baseAddress+regHDLCtxBuffer, size);				// Packet size
	addSetNReg(baseAddress+regHDLCtxBuffer+1, dataPtr, size);	// Payload
	addSetReg (baseAddress+regHDLC, hdlcAddress);				// set target address and trigger packet transmission
	wbb->execute();
}


