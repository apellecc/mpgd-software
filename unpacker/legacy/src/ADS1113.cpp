/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#include "hardware_settings.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <mexception.h>
#include "ADS1113.h"

ADS1113::ADS1113(I2Cbus *busPtr, uint8_t address) : I2Cslave(busPtr, address)
{
}

/*
	2 byte write operation
*/
void ADS1113::write(uint8_t address, uint16_t data)
{
	i2cBus->addAddress(i2c_deviceAddress, I2Cbus::I2C_Write);
	i2cBus->addWriteData(address);		
	i2cBus->addWriteData(data>>8);		
	i2cBus->addWriteData(data&0xff, I2Cbus::RWF_stop);		

	// send commands packet
	i2cBus->execute();
}


void ADS1113::addWriteAddressPointer(uint8_t address)
{
	i2cBus->addAddress(i2c_deviceAddress, I2Cbus::I2C_Write);
	i2cBus->addWriteData(address, I2Cbus::RWF_stop);		
}

/*
	2 byte read from arbitrary register
*/
void ADS1113::read(uint8_t address, uint16_t *data)
{
	// Write the address pointer
	addWriteAddressPointer(address);

	// Read data
    read(data);
}

// Raed from the register currently pointed
void ADS1113::read(uint16_t *data)
{
	uint32_t rh, rl;

	// Read data
	i2cBus->addAddress(i2c_deviceAddress, I2Cbus::I2C_Read);
	i2cBus->addRead(&rh);
	i2cBus->addRead(&rl, I2Cbus::RWF_dontAck | I2Cbus::RWF_stop);

	// send commands packet
	i2cBus->execute();

	*data = ((rh<<8) & 0xff00) | (rl&0xff);
}

/*
	Configure the channels to read
*/
void ADS1113::setConfiguration()
{
    CFG_shadow = (uint16_t) (CFG_DR_860|CFG_Mode);
	write(REG_Config, CFG_shadow);
}

void ADS1113::convert(uint8_t channel, uint16_t *result)
{
#ifdef HAS_FMC_ADC
	uint16_t state;
	int i;

	// start convertion
	write(REG_Config, CFG_shadow|CFG_OS);

	// wait end of convertion
	for (i=1000; i>0; i--){ // ~160 ms
		read(&state);
		if ((state&CFG_OS) == CFG_OS)
			break;
	}

	if (i==0)
		throw MException("ADS1113 convertion timeout");

	// Read data
	read(REG_Convertion, result);
#else
	*result = 0;
#endif
}


void ADS1113::convert(uint8_t channel, float *result)
{
	uint16_t data;

	convert(0, &data);

	*result = (float) data * 62.5e-6;
}
