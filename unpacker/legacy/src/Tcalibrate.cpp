/*
 * Copyright (C) 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * ====================================================
 *     __  __   __  _____  __   __
 *    / / /  | / / / ___/ /  | / / SEZIONE di BARI
 *   / / / | |/ / / /_   / | |/ /
 *  / / / /| / / / __/  / /| / /
 * /_/ /_/ |__/ /_/    /_/ |__/  	 
 *
 * ====================================================
 * Written by Giuseppe De Robertis <Giuseppe.DeRobertis@ba.infn.it>, 2017.
 *
 */
#include "TestBench.h"
#include "TBconfig.h"
#include "VFAT3board.h"
#include "Tcalibrate.h"
#include "Tutils.h"
#include <iostream>
#include <fstream>
#include <cmath>

using std::string;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;

Tcalibrate::Tcalibrate(TestBench &t) : GenericTest(t)
{
	board = tb.board;
	chipCfg = tb.chipConfig;
}


void Tcalibrate::resetHistogram()
{
	evCounter = 0;
	for (int i=0; i<128; i++)
		histogram[i] = 0;
}

void Tcalibrate::histogramCB(void *ctx, VFAT3dataParser::VFAT3data_t *data)
{
	Tcalibrate *p = (Tcalibrate *) ctx;
	
	p->evCounter++;

	for (int i=0; i<128; i++)
		if (data->hit[i])
			p->histogram[i]++;
}

// return number of hits over threshold
int Tcalibrate::injectChannel(int ch, unsigned chargeDAC)
{
	VFAT3chip::CalibrationConfig_t 	calCfg = chipCfg.CAL;		

	if (calCfg.MODE == 1){
		calCfg.DAC = 255-chargeDAC;		// Vpulse mode
	} else if (calCfg.MODE == 2) {
		calCfg.DAC = chargeDAC;			// Ipulse mode
	} else {
		cerr << "Please set Calibration MODE != 0" << endl;
		exit(0);
	}
	chip->addSetCalibrationConfig(calCfg);
	chip->execute();
	usleep(1000);

	// start pulses injection				
	board->pulser->run(numPulses);

	resetHistogram();
	long res;
	do {
		res = board->pollData(50);		// timeout 50 ms
		if (res == 0){
			cerr << "Timeout reading data" << endl;
			exit(0);
		}
	} while (evCounter<numPulses);
	
	return histogram[ch];
}


// return the charge (in DAC counts) of the point nearest 50% of efficency
int Tcalibrate::findThreshold(int channel)
{
//	VFAT3chip::CalibrationConfig_t 	calCfg = chipCfg.CAL;
	unsigned  chargeDAC=0;

	for (int i=0x80; i!=0; i>>=1){
		chargeDAC = chargeDAC|i;
		
// cout << "try " << chargeDAC << endl;

		int ret = injectChannel(channel, chargeDAC);

		if (ret > numPulses/2)
			chargeDAC &= ~i;	
	}

//	cout << "ch:" << channel << " chargeDAC:" << chargeDAC << endl;

	if (chargeDAC==0 || chargeDAC==255){
		cerr << "Tcalibrate::findThreshold can not find the threshold" << endl;
		return -1; 
	}

	int ret_m = injectChannel(channel, chargeDAC-1);
	int best = chargeDAC-1;
	int err = abs(ret_m - (numPulses/2));
	int bestErr = err;
	int ret = injectChannel(channel, chargeDAC);
	err = abs(ret - (numPulses/2));
	if (err < bestErr)
		best = chargeDAC;
	int ret_p = injectChannel(channel, chargeDAC+1);
	err = abs(ret_p - (numPulses/2));
	if (err < bestErr)
		best = chargeDAC+1;

	cout << "ch:" << channel << " Best chargeDAC:" << best << endl;
	

	return best;
}

// return the local DAC value nearest to 50% of efficency
int Tcalibrate::tuneLocalArm(VFAT3chip::ChannelConfig_t &chCfg, int channel, int target)
{
	unsigned  th=0;
	int t;
	int ret;

	for (int i=0x40; i!=0; i>>=1){
		th = th|i;
		
		t = -63+th;
		if (t>=63)
			t = 63;

		chCfg.arm_dac = t;
		chip->addSetChannelConfig(channel, chCfg);
		chip->execute();
		
		ret = injectChannel(channel, target);
		if (ret < numPulses/2)
			th &= ~i;	
	}		
	t = -63+th;
	if (t < -63)
		t = -63;
	if (t > 63)
		t = 63;

	int best = t;
	int bestErr = abs(ret-numPulses/2);
	if (ret < numPulses/2)
		t--;
	else
		t++;

	if (t < -63)
		t = -63;
	if (t > 63)
		t = 63;

	chCfg.arm_dac = t;
	chip->addSetChannelConfig(channel, chCfg);
	chip->execute();
	ret = injectChannel(channel, target);
	if (abs(ret-numPulses/2) < bestErr)
		best = t;

	cout << "ch:" << channel << " Best arm_dac:" << best << endl;
	return best;
}

bool Tcalibrate::calibrateLocalArm()
{
	VFAT3chip::ChannelConfig_t		chCfg;

    chipCfg = tb.chipConfig;

	// disable cal bit on all channels
	for (int i=0; i<129; i++)
		chipCfg.CH[i].cal = false;

	chipCfg.DISC.EN_HYST = true;		// Arming comp. hysteresis
	chipCfg.DISC.SEL_POL = 0;			// CFD input charge polarity (1=negative)
	chipCfg.DISC.FORCE_EN_ZCC = 0;		// Force ZCC output (independent from arming comp.)
	chipCfg.DISC.FORCE_TH = 0;			// Force active ZCC threshold
	chipCfg.DISC.SEL_COMP_MODE = 1;		// CFD output mode (0-3)
	chipCfg.DISC.zcc_dac = 10;			// ZCC global threshold
//	chipCfg.DISC.arm_dac = 30;			// Arming global threshold
	chipCfg.DISC.hyst_dac = 5;			// Global hysteresis

	chipCfg.CAL.POL = 0;				// Positive injection
	chipCfg.CAL.EN_EXT = 0;
	chipCfg.CAL.MODE = 1;				// Cal Pulse mode 2=Ipulse
	chipCfg.CAL.FS = 0;					// Cal current pulse scale factor (0-3)
	chipCfg.CAL.DUR = 200;				// 1=25ns

	chipCfg.TRG.PS = 8;					// Pulse Stretcher control (1-8)
	chipCfg.TRG.syncLevelEnable = 0;	// Disable level mode
	chipCfg.TRG.ST = 0;					// Self Trigger

	chip->setConfig(chipCfg);

	// Set consumer function
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, histogramCB);

	// Setup pulses
	board->pulser->setConfig(10, 1000);	// 25 us (plus 250 ns) period

	// Send BC0
	board->comPortTx->sendBC0();

	// start run
	board->mRunControl->startRun();
	board->comPortTx->sendRunMode();


	// scan uncalibrated channel and evaluate avarage charge for selected threshold
	int nChannels = 0;
	int avg = 0;

	for (int ch=0; ch<128; ch++){
		// set calibration bit on selected channel
		chCfg = chipCfg.CH[ch];
		chCfg.cal = true;
		chCfg.arm_dac = 0;
		chip->addSetChannelConfig(ch, chCfg);
		chip->execute();
	
		// find charge@ threshold
		int th = findThreshold(ch);
		if (th!=-1){
			avg += th;
			nChannels++;
		}

		// reset cal bit
		chCfg.cal = false;
		chip->addSetChannelConfig(ch, chCfg);
	}	
	chip->execute();
	
	if (nChannels<128){
		cout << "Found thresold for only " << nChannels << " exit." << endl;
	//	goto exitError;
	}
	avg /= nChannels;
	cout << "Avarage threshold: " << avg << endl;	

	// tune channels to match the threshold
	for (int ch=0; ch<128; ch++){
		// set calibration bit on selected channel
		chCfg = chipCfg.CH[ch];
		chCfg.cal = true;
		chip->addSetChannelConfig(ch, chCfg);
		chip->execute();
	
		tb.chipConfig.CH[ch].arm_dac = tuneLocalArm(chCfg, ch, avg);	

		// reset cal bit
		chCfg.cal = false;
		chip->addSetChannelConfig(ch, chCfg);
	}	
	chip->execute();

	// Reset consumer function
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, NULL);

	return true;

// exitError:
	// Reset consumer function
	board->vfat3DataParser[tb.getDefaultChip()]->setDataConsumeFunction(this, NULL);

	return false;
}

// Parameters used in CFD calibration
const int Tcalibrate::CalDacHigh = 100;	// ~ 11 fC
const int Tcalibrate::CalDacLow = 8;	// ~ 0.9 fC
const int Tcalibrate::CalDacStartScan = 4;	// ~ 0.5 fC
const int Tcalibrate::CalDacEndScan = 25;	// ~2.5 fC

float Tcalibrate::measSumChDelay(VFAT3chip::CalibrationConfig_t &calCfg)
{
	float sum=0;

	calCfg.DAC = CalDacHigh;
	chip->addSetCalibrationConfig(calCfg);
	chip->execute();

	float targetDelay = tb.measChDelay(calCfg);
	cout << "Target delay for fine search " << targetDelay << endl;

	for (int dac=CalDacStartScan; dac<=CalDacEndScan; dac++){
		calCfg.DAC = dac;
		chip->addSetCalibrationConfig(calCfg);
		chip->execute();
		float d = tb.measChDelay(calCfg);
// cout << "d=" << d << endl;
		sum += d - targetDelay;
	}

	cout << "Cumulative delay: " << sum << endl;
	return sum;
}


// calibrate one channel
int Tcalibrate::tuneLocalZcc(VFAT3chip::VFAT3config_t &chipCfg, int channel)
{
	unsigned  th=0;
	int t=0;
	float prevSum;
	int prevT;

	// enable calibration
	chipCfg.CH[channel].mask = false;
	chipCfg.CH[channel].cal = true;
	chipCfg.CH[channel].zcc_dac = 0;
	chip->addSetChannelConfig(channel, chipCfg.CH[channel]);

	// read delay for large charge
	chipCfg.CAL.DAC = CalDacHigh;
	chip->addSetCalibrationConfig(chipCfg.CAL);
	chip->execute();


// cout << " Charge " << tb.getInjCharge(chipCfg.CAL) << endl;	

	float targetDelay = tb.measChDelay(chipCfg.CAL);
// cout << "Target delay " << targetDelay << endl;

	// sel injection charge
	chipCfg.CAL.DAC = CalDacLow;
	chip->addSetCalibrationConfig(chipCfg.CAL);
	chip->execute();
// cout << " Charge " << tb.getInjCharge(chipCfg.CAL) << endl;	

	// dicotomic search of coarse value
	for (int i=0x40; i!=0; i>>=1){
		th = th|i;
	
		t = -63+th;
		if (t>=63)
			t = 63;

		chipCfg.CH[channel].zcc_dac = t;
		chip->addSetChannelConfig(channel, chipCfg.CH[channel]);
		chip->execute();

		float del = tb.measChDelay(chipCfg.CAL);		
//	cout << "lzcc:" << t << " delay:" << del << endl;	

		if (del < targetDelay)
			th &= ~i;
		else if (del == targetDelay)
			break;
	}
	t = -63+th;

cout << "coarse zcc_dac:" << t << endl;

	// Fine serach
	chipCfg.CH[channel].zcc_dac = t;
	chip->addSetChannelConfig(channel, chipCfg.CH[channel]);
	chip->execute();
	float sum = measSumChDelay(chipCfg.CAL);
//	float minErr = abs(sum);
	int minErrDac = t;

	int step;
	int start;
	if (sum==0){
		minErrDac = t;
		goto exitChannel;
	} else if (sum < 0){
		start = t-1;
		step = -1;
	} else {
		start = t+1;
		step = 1;
	}

#if 0
	for (t=start; t>=-63 && t<=63; t+=step){
cout << "fine zcc_dac:" << t << endl;
		chipCfg.CH[channel].zcc_dac = t;
		chip->addSetChannelConfig(channel, chipCfg.CH[channel]);
		chip->execute();
		sum = measSumChDelay(chipCfg.CAL);
		if (sum==0){
			minErrDac = t;
			break;
		}
		if (abs(sum) < minErr){
			minErr = abs(sum);
			minErrDac = t;
		} else if ((abs(sum) > minErr) && (minErr < 9.0)) {
			break;
		}
	}
#endif

	prevSum=sum;
	prevT=t;
	for (t=start; t>=-63 && t<=63; t+=step){

cout << "fine zcc_dac:" << t << endl;
		chipCfg.CH[channel].zcc_dac = t;
		chip->addSetChannelConfig(channel, chipCfg.CH[channel]);
		chip->execute();
		sum = measSumChDelay(chipCfg.CAL);

		if (sum==0){
			minErrDac = t;
			break;
		}

		if (sum*prevSum < 0) {
			if (abs(sum) < abs(prevSum))
				minErrDac = t;
			else
				minErrDac = prevT;
			break;
		}

		prevSum = sum;
		prevT = t;
	}

exitChannel:
	// disable calibration
	chipCfg.CH[channel].mask = true;
	chipCfg.CH[channel].cal = false;
	chip->addSetChannelConfig(channel, chipCfg.CH[channel]);

cout << "Final zcc_dac:" << minErrDac << endl;
	return minErrDac;		
}


bool Tcalibrate::calibrateLocalZcc()
{
	VFAT3chip::VFAT3config_t       		chipCfg = tb.chipConfig;

	/*
		Set configuration for tuning
	*/
	// disable cal bit and anable mask on all channels
	for (int i=0; i<129; i++){
		chipCfg.CH[i].cal = false;
		chipCfg.CH[i].mask = true;
	}

	chipCfg.DISC.EN_HYST = true;		// Arming comp. hysteresis
	chipCfg.DISC.SEL_POL = 0;			// CFD input charge polarity (1=negative)
	chipCfg.DISC.FORCE_EN_ZCC = 0;		// Force ZCC output (independent from arming comp.)
	chipCfg.DISC.FORCE_TH = 0;			// Force active ZCC threshold
	chipCfg.DISC.SEL_COMP_MODE = 0;		// CFD output mode (0-3)
	chipCfg.DISC.zcc_dac = 10;			// ZCC global threshold
	chipCfg.DISC.arm_dac = 10;			// Arming global threshold
	chipCfg.DISC.hyst_dac = 5;			// Global hysteresis

	chipCfg.CAL.POL = 0;				// Positive injection
	chipCfg.CAL.EN_EXT = 0;
	chipCfg.CAL.MODE = 2;				// Cal Pulse mode 2=Ipulse
	chipCfg.CAL.FS = 1;					// Cal current pulse scale factor (0-3)
	chipCfg.CAL.DUR = 1;				// 1=25ns

	chipCfg.TRG.PS = 1;					// Pulse Stretcher control (1-8)
	chipCfg.TRG.syncLevelEnable = 0;	// Disable level mode
	chipCfg.TRG.ST = 1;					// Self Trigger

	chip->setConfig(chipCfg);


	// start run
	board->mRunControl->startRun();
//	board->comPortTx->sendRunMode();

	for (int ch=0; ch<128; ch++){
//	for (int ch=105; ch<=105; ch++){
		cout << "Calibrating CFD channel " << ch << endl;
	
		tb.chipConfig.CH[ch].zcc_dac = tuneLocalZcc(chipCfg, ch);
	}

	// stop the run
	board->comPortTx->sendSConly();
	board->mRunControl->stopRun();

	// restore chip configuration
	chip->setConfig(tb.chipConfig);

	return true;
}

bool Tcalibrate::run(string param)
{
   	chip = tb.chip;
	return runTH();
}

bool Tcalibrate::runCFD()
{
   	chip = tb.chip;
	if (!calibrateLocalZcc())
		return false;

	saveChannelsFile();
	return true;
}


bool Tcalibrate::runTH()
{
   	chip = tb.chip;
	if (!calibrateLocalArm())
		return false;

	saveChannelsFile();
	return true;
}




void Tcalibrate::saveChannelsFile()
{
	// Open output file
	ofstream outputFile;
	outputFile.open (string("Data/")+string("Calibrate_")+Tutils::filenameSuffix()+".cfg");
	outputFile << "# Channels Calibration " << endl;

	for (int ch=0; ch<128; ch++){
		outputFile << "\tCHANNEL " << ch << " {" << endl;
		outputFile << "\t\tzcc_dac = " << (int) tb.chipConfig.CH[ch].zcc_dac << ";" << endl;
		outputFile << "\t\tarm_dac = " << (int) tb.chipConfig.CH[ch].arm_dac << ";" << endl;
		outputFile << "\t\tmask = " << tb.chipConfig.CH[ch].mask << ";" << endl;
		outputFile << "\t\tcal = " << tb.chipConfig.CH[ch].cal << ";" << endl;
		outputFile << "\t}" << endl;
	}
	outputFile.close();
}

