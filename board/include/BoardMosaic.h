#ifndef BOARD_MOSAIC_H
#define BOARD_MOSAIC_H

#include <iostream>

#include <mwbbslave.h>
#include <mboard.h>

class BoardMosaic: public MWbbSlave, MBoard {
    public:
        BoardMosaic();
        BoardMosaic(WishboneBus *wbbPtr, uint32_t baseAddress);
	
        void writeRegister(uint16_t address, uint32_t value);
        void sendBC0();
};

#endif
