#include "BoardMosaic.h"

BoardMosaic::BoardMosaic() {
}

BoardMosaic::BoardMosaic(WishboneBus *wbbPtr, uint32_t baseAddress): MWbbSlave(wbbPtr, baseAddress) {
}

void BoardMosaic::writeRegister(uint16_t address, uint32_t value) {
    wbb->addWrite(baseAddress + address, value);
    wbb->execute();
}

void BoardMosaic::sendBC0() {
    writeRegister(0, 0x2);
}
