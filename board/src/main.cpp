#include <cstdint>
#include <iostream>
#include <string>
#include <unistd.h>
#include <chrono>
#include <thread>

#include "CLI11.hpp"

#include <mexception.h>

#include "TestBench.h"
#include "BoardMosaic.h"

int main(int argc, char** argv) {
    std::string cfg_filename;
    uint64_t max_events{0};
    bool isOnline;

    CLI::App app{"Dump raw file to text"};
    app.add_option("-c,--config", cfg_filename, "Configuration file path")->required();
    //app.add_flag("--online", isOnline, "Wait for new events after reaching the end");
    try {
        app.parse(argc, argv);
    } catch (const CLI::ParseError &e) {
        return app.exit(e);
    }

    BoardMosaic b;
    TestBench tb;

    tb.readConfigFile(cfg_filename);
    tb.initBoard();
    tb.chipReset();
    tb.chipInit();
    if (!tb.syncVerify()) {
        std::cout << "Sync check failed" << std::endl;
        return 1;
    }
    tb.setChipConfig();

    tb.board->comPortTx->sendBC0();
    tb.board->comPortTx->sendRunMode();
	tb.board->mRunControl->startRun();
    std::cout << "Run started." << std::endl;

    long res;
    while (true) {
        res = tb.board->pollData(1000);
        //std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
}
