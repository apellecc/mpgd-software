#include "RawFileReader.h"
#include "RawEvent.h"

RawFileReader::RawFileReader(const std::string& filename) {
  m_filename = filename;
  m_file = std::fopen(filename.data(), "rb");
}

std::size_t RawFileReader::read(void* buffer, std::size_t size, std::size_t count) {
  return std::fread(buffer, size, count, m_file);
}

std::size_t RawFileReader::readEvent(RawEvent *event) {
  return event->fromFile(m_file);
}
